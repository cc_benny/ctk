/*
 * tkInt.h (Ctk) --
 *
 *	Declarations for things used internally by the Tk
 *	procedures but not exported outside the module.
 *
 * Copyright (c) 1990-1994 The Regents of the University of California.
 * Copyright (c) 1994 Sun Microsystems, Inc.
 * Copyright (c) 1994-1995 Cleveland Clinic Foundation
 *
 * See the file "license.terms" for information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 */

#ifndef _TKINT
#define _TKINT

#ifndef _TK
#include "tk.h"
#endif
#include <tcl.h>
#include <ctype.h>

/*
 * One of the following structures is maintained for each display
 * containing a window managed by Tk:
 */

struct TkDisplay {
    /*
     * Maintained by ctkDisplay.c
     */
    const char *name;		/* Name of display device. Malloc-ed. */
    const char *type;		/* Device type. Malloc-ed. */
    ClientData display;		/* Curses's info about display. */
    Tcl_Channel chan;		/* Input channel for the device */
    int fd;			/* Input file descriptor for device. */
    FILE *inPtr;		/* Input file pointer for device. */
    TkWindow *cursorPtr;	/* Window to display cursor in. */
    int cursorX, cursorY;	/* Position in `cursWinPtr' to display
				 * cursor. */

    /*
     * Maintained by tkWindow.c
     */
    int numWindows;		/* Windows currently existing in display
    				 * (including root). */
    TkWindow *rootPtr;		/* Root window of display. */
    TkWindow *focusPtr;		/* Window that has the keyboard focus. */
    struct TkDisplay *nextPtr;	/* Next in list of all displays. */
};

/*
 * One of the following structures exists for each event handler
 * created by calling Tk_CreateEventHandler.  This information
 * is used by tkEvent.c only.
 */

struct TkEventHandler {
    unsigned long mask;		/* Events for which to invoke
				 * proc. */
    Tk_EventProc *proc;		/* Procedure to invoke when an event
				 * in mask occurs. */
    ClientData clientData;	/* Argument to pass to proc. */
    struct TkEventHandler *nextPtr;
				/* Next in list of handlers
				 * associated with window (NULL means
				 * end of list). */
};

/*
 * Tk keeps one of the following data structures for each main
 * window (created by a call to Tk_CreateMainWindow).  It stores
 * information that is shared by all of the windows associated
 * with a particular main window.
 */

struct TkMainInfo {
    int refCount;		/* Number of windows whose "mainPtr" fields
				 * point here.  When this becomes zero, can
				 * free up the structure (the reference
				 * count is zero because windows can get
				 * deleted in almost any order;  the main
				 * window isn't necessarily the last one
				 * deleted). */
    struct TkWindow *winPtr;	/* Pointer to main window. */
    Tcl_Interp *interp;		/* Interpreter associated with application. */
    Tcl_HashTable nameTable;	/* Hash table mapping path names to TkWindow
				 * structs for all windows related to this
				 * main window.  Managed by tkWindow.c. */
    Tk_BindingTable bindingTable;
				/* Used in conjunction with "bind" command
				 * to bind events to Tcl commands. */
    TkDisplay *curDispPtr;	/* Display for last binding command invoked
				 * in this application;  used only  by
				 * tkBind.c. */
    int bindingDepth;		/* Number of active instances of Tk_BindEvent
				 * in this application.  Used only by
				 * tkBind.c. */
    struct ElArray *optionRootPtr;
				/* Top level of option hierarchy for this
				 * main window.  NULL means uninitialized.
				 * Managed by tkOption.c. */
    struct TkMainInfo *nextPtr;	/* Next in list of all main windows managed by
				 * this process. */
};

/*
 * Pointer to first entry in list of all displays currently known.
 */

extern TkDisplay *tkDisplayList;

/*
 * Flags passed to TkMeasureChars:
 */

#define TK_WHOLE_WORDS		 1
#define TK_AT_LEAST_ONE		 2
#define TK_PARTIAL_OK		 4
#define TK_NEWLINES_NOT_SPECIAL	 8
#define TK_IGNORE_TABS		16

/*
 * Location of library directory containing Tk scripts.  This value
 * is put in the $tkLibrary variable for each application.
 */

#ifndef CTK_LIBRARY
#define CTK_LIBRARY "/usr/local/lib/ctk"
#endif

/*
 * Special flag to pass to Tk_CreateFileHandler to indicate that
 * the file descriptor is actually for a display, not a file, and
 * should be treated specially.  Make sure that this value doesn't
 * conflict with TK_READABLE, TK_WRITABLE, or TK_EXCEPTION from tk.h.
 */

#define TK_IS_DISPLAY	32

/*
 * This macro is used to modify a "char" value (e.g. by casting it to
 * an unsigned character) so that it can be used safely with macros
 * such as tolower and isupper.  NB: This is a legacy bit, usually we
 * want to use the inline functions below for the attributes from
 * ctype.h.  But at this time we do not have correct Unicode case
 * distinction, so we leave the casing code alone for now.
 */

#define UCHAR(c) ((unsigned char) (c))

/*
 * The parameter of the function passed to Tcl_EventuallyFree,
 * i.e. Tcl_FreeProc.
*/

#if TCL_MAJOR_VERSION == 8
typedef char * Tcl_MemPtr;
#elif TCL_MAJOR_VERSION == 9
typedef void * Tcl_MemPtr;
#endif

/*
 * Miscellaneous variables shared among Tk modules but not exported
 * to the outside world:
 */

extern Tk_Uid			tkActiveUid;
extern void			(*tkDelayedEventProc) (void);
extern Tk_Uid			tkDisabledUid;
extern Tk_Uid			tkNormalUid;

/*
 * Internal procedures shared among Tk modules but not exported
 * to the outside world:
 */

extern void		TkBindEventProc (TkWindow *winPtr, XEvent *eventPtr);
extern void		TkComputeTextGeometry (const char *string,
			    int numBytes, int wrapLength, int *widthPtr,
			    int *heightPtr);
extern int		TkCopyAndGlobalEval (Tcl_Interp *interp,
			    const char *script);
extern Time		TkCurrentTime (void);
extern int		TkDeadAppCmd (ClientData clientData,
			    Tcl_Interp *interp, int argc, const char *argv[]);
extern void		TkDisplayChars (TkWindow *winPtr,
			    Ctk_Style style, const char *string,
			    int numBytes, int x, int y, int tabOrigin,
			    int flags);
extern void		TkDisplayText (TkWindow *winPtr, Ctk_Style style,
			    const char *string, int numBytes, int x, int y,
			    int length, Tk_Justify justify, int underline);
extern void		TkEventCleanupProc (
			    ClientData clientData, Tcl_Interp *interp);
extern void		TkEventDeadWindow (TkWindow *winPtr);
extern void		TkFocusDeadWindow (TkWindow *winPtr);
extern int		TkFocusFilterEvent (TkWindow *winPtr, XEvent *eventPtr);
extern void		TkFreeBindingTags (TkWindow *winPtr);
extern TkWindow *	TkGetFocus (TkWindow *winPtr);
extern int		TkGetInterpNames (Tcl_Interp *interp, Tk_Window tkwin);
extern const char *	TkInitFrame (Tcl_Interp *interp,
			    Tk_Window tkwin, int toplevel, int argc,
			    const char *argv[]);
extern int		TkMeasureChars (
			    const char *source, int maxBytes, int startX, int maxX,
			    int tabOrigin, int flags, int *nextXPtr);
extern int		TkTextWidth(const char *string, int numBytes);
extern void		TkOptionClassChanged (TkWindow *winPtr);
extern void		TkOptionDeadWindow (TkWindow *winPtr);
extern void		TkQueueEvent (TkDisplay *dispPtr, XEvent *eventPtr);

extern void		TkDeleteMain (TkMainInfo *mainPtr);

#define CtkIsDisplayed(tkwin)	(((tkwin)->flags)& CTK_DISPLAYED)

typedef void (CtkSpanProc) (int left, int right, int y, ClientData data);

#define CtkSpanIsEmpty(left, right) ((left) >= (right))
#define CtkCopyRect(dr,sr) (memcpy((dr), (sr), sizeof(Ctk_Rect)))
#define CtkMoveRect(rect,x,y) \
	((rect)->left += (x), (rect)->top += (y), \
	(rect)->right += (x), (rect)->bottom += (y))
#define CtkSetRect(rect,l,t,r,b) \
	((rect)->left = (l), (rect)->top = (t), \
	(rect)->right = (r), (rect)->bottom = (b))

/* A little inline helper to avoid casting the string for
 * Tcl_SetResult all the time.  */
static inline void Ctk_SetResult(Tcl_Interp *interp, const char *result,
                                 Tcl_FreeProc *freeProc)
{
    Tcl_SetResult(interp, (char *) result, freeProc);
}

extern char *	    CtkStrdup (const char *);

extern int	    CtkDisplayInit (Tcl_Interp *interp,
		        TkDisplay *dispPtr, const char *displayName);
extern void	    CtkDisplayEnd (TkDisplay *dispPtr);
extern void	    CtkKeysInit (Tcl_Interp *interp,
			TkDisplay *dispPtr);
extern void	    CtkKeysEnd (TkDisplay *dispPtr);
extern void	    CtkSetDisplay (TkDisplay *dispPtr);
extern void	    CtkDisplayBell (TkDisplay *dispPtr);

extern void         CtkSetFocus (TkWindow *winPtr);
extern void	    Ctk_Forget (Tk_Window tkwin);

extern void	    CtkFillRegion (TkDisplay *dispPtr,
			CtkRegion *rgn_ptr, Ctk_Style style, int ch);

extern void	    CtkIntersectSpans (int *left_ptr,
			int *right_ptr, int left2, int right2);
extern void	    CtkIntersectRects (Ctk_Rect *r1_ptr,
			const Ctk_Rect *r2_ptr);
extern CtkRegion *  CtkCreateRegion (Ctk_Rect *rect);
extern void	    CtkDestroyRegion (CtkRegion *rgn);
extern void	    CtkForEachIntersectingSpan (CtkSpanProc *func,
			ClientData func_data, int left, int right, int y,
			CtkRegion *rgn);
extern void	    CtkForEachSpan (CtkSpanProc *func,
			ClientData func_data, CtkRegion *rgn);
extern CtkRegion *  CtkRegionMinusRect (CtkRegion *rgn_id,
			Ctk_Rect *rect, int want_inter);
extern void	    CtkUnionRegions (CtkRegion *rgn1, CtkRegion *rgn2);
extern void	    CtkRegionGetRect (CtkRegion *rgn, Ctk_Rect *rect_ptr);
extern int	    CtkPointInRegion (int x, int y, CtkRegion *rgn);

extern int	    Ctk_CursesCmd(ClientData clientData, Tcl_Interp *interp,
			int argc, const char *argv[]);

#endif  /* _TKINT */
