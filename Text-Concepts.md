[//]: # (Text-Concepts.md)

Text Concepts, Problems and Tools
====

There are a number of different levels or concepts of characters
relevant to Ctk.

### Bytes

What the C language calls `char`.  This is the original concept of
Ctk, but by itself it does not support anything but ASCII.  In the C
API of Tcl, `char*` strings contain UTF-8.

### Unicode codepoints

What Tcl 8.6 calls `Tcl_UniChar`.  Tcl can convert this from and to
UTF-8.  This level is what the Tcl language itself means when it talks
about text length or the 10th character in a string.  So this is the
interface that Tk or Ctk needs to use for positions in a text, like
where a menu entry has its underline, where the current test selection
starts and ends or where the cursor is or should be.

### Character clusters

Any combination of a base character and zero or more non-spacing
diacritics.  Diacritics are separate codepoints but do not occupy
their own cells in the terminal matrix.  From the user POV a character
cluster is the basic unit.

This maps to the following requirements and features: The difference
between pre-composed and normalized characters should not be visible
to the user.  The cursor should pass over a cluster in one step.
Selection should select whole clusters.  Delete and backspace should
delete a whole cluster.  Only when inputting a new character, the
diacritics should be separate entities so that the user can build a
character cluster in the first place.  For modifying a character
cluster without rebuilding it from scratch, an IME could be used.

Some sources prefer backspace to just remove the last diacritic, which
sounds usefull, especially in combination with a basic postfix input
scheme, where diacritics are just input in the same order as layed out
in memory, so removing them in reverse order seems logical.  But this
means that users have to understand where a character is precomposed
and where it is not, and this is also not logical with a prefix input
scheme, like the common deadkey keyboard layouts.

### Terminal cells

Where normal base characters occupy one cell and diacritics occupy
none, Chinese, Japanese and Korean characters and most modern Emojis
occupy two cells, they are also called "fullwidth" characters.  This
should be handled when string widths are calculated and also for
cursor movement, selection etc, see notes on "character clusters".

### Extended Plane Characters

Classical Unicode codepoints fit into 2 bytes (16 bits), making up a
range that is called the "Basic Plane".  But in recent years more and
more characters are allocated beyond that range, most importantly
Emojis, these are called "Extended Plane Characters" here.  Tcl 8.6
does not support those characters.  Besides UTF-8, the only Unicode
representation inside Tcl 8.6 is UCS-2 (arrays of 16-bit numbers)
using the `Tcl_UniChar` type.  `Tcl_UniChar` (UCS-2) is used in Tcl
for handling of Unicode strings as simple arrays and also for
interfacing with OSes and text rendering engines.

Tcl 8.7, currently in alpha, will switch to UTF-16 or UCS-4 (with a
compile-time switch to decide).  UCS-4 means using 32-bit numbers for
characters, so that Extended Plane Characters are supported as regular
characters with no further trickery, but using the double amount of
space.  UTF-16 is also using arrays of 16-bit numbers like UCS-2, but
includes a representation of Extended Plane Characters known as
"Surrogate Pairs".  This is what OSes and some rendering engines use
already.  In Tcl 8.7 with UTF-16, for the purposes of `Tcl_UniChars`
and so in the Tcl language itself, Surrogate Pairs have to count as
two characters.

With UTF-16, conversion from UTF-8 to `Tcl_UniChars` becomes tricky,
because that involves two API calls for one UTF-8 sequence, but Tcl
does not have any good place to put the intermediate state.  The
current implementation assumes that the output parameter still
contains the data from the first call as input to the second call to
remember what is up.  However context is stored, callers need to make
sure to make that second call as soon as possible, so as not to loose
the context.

On the Tcl language level the difference between UTF-16 and UCS-4 is
that with UTF-16, Extended Plane Characters count as two characters.
On the C-API level, calling some of the Tcl APIs becomes tricky,
because the product of two compilation modes in the extension (8.6 and
8.7/UCS-4) and three modes of the Tcl library have to be considered
(8.6, 8.7/UTF-16 and 8.7/UCS-4).  The extension compilation mode
8.7/UTF-16 does not have to be considered, because an 8.7/UCS-4
extension can be written to be runtime-compatible with Tcl 8.7/UTF-16.

Tips:

* [389, Full support for Unicode 10.0 and later (part
   1)](https://core.tcl-lang.org/tips/doc/trunk/tip/389.md), Tcl
   branch `tip-389`.
* [542, Support for switchable Full
   Unicode](https://core.tcl-lang.org/tips/doc/trunk/tip/542.md), Tcl
   branch `utf-max`.

Note that not all combinations are valid as long as the TIP branches
are not merged.  Ctk 8.7/UCS-4 crashes with Tcl 8.7/UTF-16, because
the new API is missing.  Ctk 8.6 and Ctk 8.7/UTF-16 do not use native
mode in Tcl 8.7/UCS-4, because a new API is unknown.  Ctk 8.6 with Tcl
8.7/UCS-4 does not handle Extended Plan Characters, because Tcl
8.7/UCS-4 does not provide the features of Tcl 8.7/UTF-16 as long as
not both TIP branches are merged.

### External encodings

External encodings beside UTF-8 are supported in Ctk and NCurses, but
only lightly tested.  Latin-1 works in XTerm, Japanese with EUC-JP
encoding works under Luit.

### Curses

The code is tested with
[NCurses](https://invisible-island.net/ncurses/) in its "wide"
version.  No Unicode-specific APIs are used, so the code might work
with other UTF-8-enabled versions of Curses, but that is untested.
For discovery of additional keys on the keyboard, NCurses-specific
functions are used when possible, so those would be missing with a
non-compatible Curses, too.

----
[//]: # (eof)
