#!/usr/bin/tclsh
# ------------------------------------------------------------------------
#   get-special-keys.tcl
#
#   For the current TERM produce the keys that Ncurses registers
#   dynamically.  See `get-special-keys.sh' for context.
# ------------------------------------------------------------------------

proc produce {fname} {

    set ofs [open $fname w]

    # This is the area where Ncurses allocates the special keys.
    for {set i 0o1000} {$i < 0o2000} {incr i} {
	catch {
	    set name [curses keyname $i]
	    puts $ofs [format "0%03o %s" $i $name]
	}
    }

    close $ofs
}

produce {*}$argv

exit 0

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
