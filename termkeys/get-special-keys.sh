#!/bin/bash
# ------------------------------------------------------------------------
#   get-special-keys.sh
#
#   Routines to enumerate the special keys that Ncurses registers
#   automatically for the known terminal definitions.
# ------------------------------------------------------------------------

set -e	# Exit on error.

# list-keys term destination-file - Have get-special-keys.tcl put all
# special keys for TERM into DESTINATION-FILE.  No file is produced,
# if there are no such keys.
function list-keys () {
    export TERM="$1"
    local dest="${2:-target/$TERM}"

    # stdin = /dev/tty - Make sure that Ctk does not complain.
    # stdout = /dev/null - Do not actually bother the user with output
    # at this time, especially because we output code for terminals
    # other than the one where we actually run.
    ../cwish.tcl get-special-keys.tcl $dest < /dev/tty > /dev/null
    if [ ! -s "$dest" ]; then
	rm -f "$dest"
    fi
}

# list-terms - Enumerate all the known terminal definitions by looking
# into the terminfo database directories.
function list-terms () {
    local term
    for term in /{etc,lib,usr/share}/terminfo/*/*; do
	if [ -r "$term" ]; then
	    basename "$term"
	fi
    done | sort -u
}

# drop-dups files... - Remove duplicate files from the list given.  We
# do not expect many files so this uses a brute-force algorithm.
function drop-dups () {
    local fb
    local fo
    for fb in "$@"; do
	for fo in "$@"; do
	    if [ -r "$fb" -a -r "$fo" -a "$fb" != "$fo" ] \
		   && diff -q "$fb" "$fo" >/dev/null 2>&1; then
	       rm "$fo"
	    fi
	done
    done
}

# show-keys keydir - Show which keys are listed in the files in
# KEYDIR.
function show-keys () {
    local dir="${1:-target}"
    
    cat "$dir"/* | cut -d ' ' -f 2 | sort -u | (
	local lang="$LANG"
	export LANG=C
	
	local k
	local name
	local mod
	local -A keys
	while read k; do
	    if [[ $k = *[A-Z]* ]]; then
		name=${k%[1-9]}
		mod=${k:${#name}}
		if [ -z "${keys[$name]}" ]; then
		    keys[$name]="$name, mods:"
		fi
		if [ -z "$mod" ]; then
		    keys[$name]+=" ''"
		else
		    keys[$name]+=" $mod"
		fi
	    else
		keys[$k]=$k
	    fi
	done

	LANG=$lang
	for k in "${keys[@]}"; do
	    echo $k
	done | sort
    )
}

# list-all-keys destdir - For all terminals known to `list-terms',
# output a file into DESTDIR with the special keys.
function list-all-keys () {
    local destdir="${1:-target}"

    rm -fr "$destdir"
    mkdir -p "$destdir"

    list-terms | \
	while read term; do
	    echo -n "Checking $term ... "
	    list-keys "$term" "$destdir/$term.keys"
	    if [ -e "$destdir/$term.keys" ]; then
		wc -l < "$destdir/$term.keys"
	    else
		echo 0
	    fi
	done

    drop-dups "$destdir"/*
}

# main destdir - The default sub command for this script.  Collect all
# keys into DESTDIR and than format the result.
function main () {
    local destdir="${1:-target}"
    list-all-keys "$destdir"
    echo ""
    echo "Keys defined somewhere:"
    show-keys "$destdir"
}

cd $(dirname "$0")

"${@:-main}"

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
