/*
 * ctkUnicode.h --
 *
 *	Character classification routines used in Ctk.
 *
 * See the file "license.terms" for information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 */

#ifndef _CTKUNICODE
#define _CTKUNICODE

#ifndef _TCL
#include <tcl.h>
#endif
#include <ctype.h>

#if TCL_MAJOR_VERSION < 9

typedef int Ctk_UtfToCodepoint_t(const char *source, Tcl_UniChar *uniChar);
extern Ctk_UtfToCodepoint_t *Ctk_UtfToCodepoint;

typedef int Ctk_CodepointToUtf_t(Tcl_UniChar codepoint, char * utf);
extern Ctk_CodepointToUtf_t *Ctk_CodepointToUtf;

extern int	CtkUtfCharComplete(const char *string, int length);

#else

#define Ctk_UtfToCodepoint Tcl_UtfToUniChar
#define Ctk_CodepointToUtf Tcl_UniCharToUtf
#define CtkUtfCharComplete Tcl_UtfCharComplete

#endif /* TCL_MAJOR_VERSION < 9 */

extern void	CtkInitUnicode(void);
extern int	CtkUnicodeCapabilities(Tcl_Interp *interp);
extern int	CtkCharWidth(Tcl_UniChar ch);
extern int	CtkCharNextN(const char *string, int maxBytes, int count);
extern int	CtkClusterNextN(const char *string, int maxBytes, int count);
extern int	CtkColumnNextN(const char *string, int maxBytes, int count);
extern int	CtkClusterPrevN(const char *string, int startOffset,
			int count);
extern int	CtkNumClusters(const char *string, int maxBytes,
			int *charCountPtr);

/*
 * Safe replacements for the ctype.h macros.
 */

static inline int CtkIsAscii (Tcl_UniChar codepoint)
{ return 0 == (codepoint & ~0x7F); }

/*
 * isprint: We do not actually do anything with Unicode control chars,
 * so for now we just handle all non-ASCII as printable.  That
 * simplifies the detection code.
 */

static inline int CtkIsPrint (int byte)
{ return CtkIsAscii(byte) && isprint(byte); }

/*
 * isspace, isdigit, isalnum: Some of the time these are just used for
 * parsing options, where ASCII-only is fine.  FIXME: But some of the
 * code does need work to replace with Tcl_UniCharIs* functions.
 */

static inline int CtkIsSpace (int byte)
{ return CtkIsAscii(byte) && isspace(byte); }

static inline int CtkIsDigit (int byte)
{ return CtkIsAscii(byte) && isdigit(byte); }

static inline int CtkIsAlnum (int byte)
{ return CtkIsAscii(byte) && isalnum(byte); }

/*
 * isalpha: Only used to parse options, where Unicode is irrelevant.
 */

static inline int CtkIsAlpha (int byte)
{ return CtkIsAscii(byte) && isalpha(byte); }

#endif  /* _CTKUNICODE */

