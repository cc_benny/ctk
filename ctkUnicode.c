/*
 * ctkUnicode.c (Ctk) --
 *
 *	CTK Unicode functions
 *
 * See the file "license.terms" for information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 */

#define _XOPEN_SOURCE /* Required for wcwidth. */
#include "tkInt.h"
#include "ctkUnicode.h"
#include <string.h>
#include <wchar.h>
#include <stdlib.h>
#include <limits.h>

/*
 * A compile time assert: We want to be always compiled with TCL_UTF_MAX
 * such that Tcl_UniChar can represent UCS-4.
 */
typedef char assert_SizeOfTclUniChar[sizeof(Tcl_UniChar) >= 4 ? 1 : -1];

typedef unsigned long long BitBucket;
#define BITS_PER_BUCKET (8 * sizeof (BitBucket))

struct PageTable {
    const BitBucket *pages;
    const unsigned short *indices;
    int wordsPerPage;
    int bitsPerPage;
    int maxOffset;
};

#include "uniZeroWidth.h"
const struct PageTable zeroWidthTable = {
    zeroWidth_pages, zeroWidth_offsets, zeroWidth_wordsPerPage,
    zeroWidth_wordsPerPage * BITS_PER_BUCKET, zeroWidth_maxOffset
};

#include "uniDoubleWidth.h"
const struct PageTable doubleWidthTable = {
    doubleWidth_pages, doubleWidth_offsets, doubleWidth_wordsPerPage,
    doubleWidth_wordsPerPage * BITS_PER_BUCKET, doubleWidth_maxOffset
};

#if TCL_MAJOR_VERSION < 9

#ifndef Tcl_UtfToChar16
#define Tcl_UtfToChar16 Tcl_UtfToUniChar
typedef Tcl_UniChar ShortChar_t;
#else
typedef unsigned short ShortChar_t;
#endif

#endif /* TCL_MAJOR_VERSION < 9 */

static const char *supplCharMode = "uninit";
static int supplCharSize = -1;

#if TCL_MAJOR_VERSION < 9

/*
 * A function to increment a char* just the same amount as Tcl does for
 * one Tcl character.  Among other things, supplemental plane characters
 * are two Tcl characters in 8.6 (surrogate pairs), but just one in 8.7
 * with TCL_UTF_MAX==4.  The resulting codepoint itself is ignored by
 * the caller.
 */
typedef int SkipOneTclUniChar_t(const char *source, Tcl_UniChar *uniChar);
static SkipOneTclUniChar_t *SkipOneTclUniChar = NULL;

/*
 * A function to get a Unicode codepoint (including supplemental plane
 * characters) from a UTF-8 string.  Implemented natively on 8.7 with
 * TCL_UTF_MAX==4, with a compatibility function otherwise.
 */
Ctk_UtfToCodepoint_t *Ctk_UtfToCodepoint = NULL;

/*
 * A function to get a UTF-8 string from a Unicode codepoint (including
 * supplemental plane characters).  Implemented natively on 8.7 with
 * TCL_UTF_MAX==4, with a compatibility function otherwise.
 */
Ctk_CodepointToUtf_t *Ctk_CodepointToUtf = NULL;

static int	UtfToCodepointEmulate (const char *source,
			Tcl_UniChar *codepointPtr);
static int	CodepointToUtfEmulate (Tcl_UniChar codepoint,
			char * utf);

#else

#define SkipOneTclUniChar Ctk_UtfToCodepoint

#endif /* TCL_MAJOR_VERSION < 9 */


#define INT_ERROR INT_MIN
static int	ParseInt(const char * string);
static int	TestCharProp(Tcl_Interp *interp, int (*subcommand)(Tcl_UniChar),
			int codepoint);
static int	TestNext(Tcl_Interp *interp,
			int (*subcommand)(const char *, int, int),
			const char *string, int length, int offset);
static int	TestClusterPrevN(const char *string, int startOffset,
			int count);

static inline int
LookupBit (const struct PageTable *pageTable, int ch)
{
    if (ch >= pageTable->maxOffset) {
	return 0;
    }

    int pageoffset = ch % pageTable->bitsPerPage;
    int pageindex  = ch / pageTable->bitsPerPage;
    int wordoffset = pageoffset % BITS_PER_BUCKET;
    int wordindex  = pageoffset / BITS_PER_BUCKET;
    const BitBucket *bucket = (pageTable->pages
			       + pageTable->indices[pageindex]
			       + wordindex);
    return 0 != (*bucket & (((BitBucket) 1) << wordoffset));
}


/*
 *--------------------------------------------------------------
 *
 * CtkCharWidth --
 *
 *	Calculate the width of a character, i.e. if it is double
 *	width, normal or a combining character.  Try not to call this
 *	function too often, it is recommended that ASCII characters
 *	are screened out before even calling this.
 *
 * Results:
 *	A count of matrix cells.
 *
 * Side effects:
 *	None.
 *
 *--------------------------------------------------------------
 */

int
CtkCharWidth(Tcl_UniChar ch)
{
    /*
     * Emoji skintone modifiers are in both tables, so look in the
     * modifiers table (zero width) first.
     */
    if (LookupBit(&zeroWidthTable, ch)) {
	return 0;
    } else if (LookupBit(&doubleWidthTable, ch)) {
	return 2;
    } else {
	return 1;
    }
}


/*
 *--------------------------------------------------------------
 *
 * CtkInitUnicode --
 *
 *	Initialize the handling of supplemental plane characters.
 *
 * Side effects:
 *	None.
 *
 *--------------------------------------------------------------
 */

void
CtkInitUnicode (void)
{
#if TCL_MAJOR_VERSION < 9

    const Tcl_UniChar catFaceCode = 0x1F639;
    const char catFaceUtf[] = "\xF0\x9F\x98\xB9";

    /*
     * Check if Tcl_UtfToUniChar works as intended.
     */
    Tcl_UniChar catFaceActual = 0;
    if (Tcl_UtfToUniChar(catFaceUtf, &catFaceActual) > 0
	    && catFaceCode == catFaceActual) {
	/*
	 * Tcl_UtfToUniChar works for supplemental plane characters.
	 * This means we are in 8.7.  The casts are just so that this
	 * compiles in 8.6, but we never get here in 8.6.
	 */
	Ctk_UtfToCodepoint = (Ctk_UtfToCodepoint_t*)Tcl_UtfToUniChar;
	Ctk_CodepointToUtf = (Ctk_CodepointToUtf_t*)Tcl_UniCharToUtf;

	supplCharMode = "native";
    } else {
	/*
	 * Tcl_UtfToUniChar does not work.  Try our emulation.  Even if
	 * it does not work either, the emulation function has the
	 * correct signature, while Tcl_UtfToUniChar does not, at least
	 * in 8.6.
	 */
	Ctk_UtfToCodepoint = UtfToCodepointEmulate;
	Ctk_CodepointToUtf = CodepointToUtfEmulate;

	Tcl_UniChar catFaceActual2 = 0;
	UtfToCodepointEmulate(catFaceUtf, &catFaceActual2);
	if (catFaceCode == catFaceActual2) {
	    supplCharMode = "emulated";
	} else {
	    supplCharMode = "none";
	}
    }

    supplCharSize = Tcl_NumUtfChars(catFaceUtf, -1);
    if (1 == supplCharSize) {
	SkipOneTclUniChar = (SkipOneTclUniChar_t*)Ctk_UtfToCodepoint;
    } else {
	SkipOneTclUniChar = (SkipOneTclUniChar_t*)Tcl_UtfToChar16;
	/*
	 * Even in "native" mode we may be in a TCL_UTF_MAX==3
	 * interpreter where we still have surrogates in the internal
	 * UTF-8 and than we still need our emulation code.
	 */
	Ctk_UtfToCodepoint = UtfToCodepointEmulate;
    }

#else

    supplCharMode = "native";
    supplCharSize = 1;

#endif /* TCL_MAJOR_VERSION < 9 */
}


/*
 *---------------------------------------------------------------------------
 *
 * CtkUnicodeCapabilities --
 *
 * Results:
 *	What can we actually do here?  Returns a dict as the result in
 *	`interp' with these keys:
 *
 *	* tclVersionCompiled - The compiled-in Tcl version.  Just for
 *	  debugging.
 *
 *	* supplCharMode - One of "uninit", "native", "emulated" or "none".
 *
 *	* supplCharSize - The number of Tcl_UniChars that an
 *	  supplemental plane character takes on the Tcl level.  One of
 *	  -1 (uninit), 1 (the interpreter was compiled with TCL_UTF_MAX
 *	  > 3), 2 (the interpreter supports supplemental plane
 *	  characters, but uses surrogate pairs) or 4 (the interpreter
 *	  interpretes supplemental plane chars as a series of
 *	  windows-1252 characters).
 *
 * Side effects:
 *	None.
 *
 *---------------------------------------------------------------------------
 */

int
CtkUnicodeCapabilities(Tcl_Interp *interp)
{
    Tcl_AppendElement(interp, "tclVersionCompiled");
    Tcl_AppendElement(interp, TCL_PATCH_LEVEL);

    Tcl_AppendElement(interp, "supplCharMode");
    Tcl_AppendElement(interp, supplCharMode);

    Tcl_AppendElement(interp, "supplCharSize");
    char numbuf[5];
    sprintf(numbuf, "%d", supplCharSize);
    Tcl_AppendElement(interp, numbuf);

    /*
     * This should aways be 4 at this point.
     */
    Tcl_AppendElement(interp, "uniCharSize");
    sprintf(numbuf, "%d", (int) sizeof(Tcl_UniChar));
    Tcl_AppendElement(interp, numbuf);

    return TCL_OK;
}

#if TCL_MAJOR_VERSION < 9


/*
 *--------------------------------------------------------------
 *
 * UtfToCodepointEmulate --
 *
 *	A compatibilty function to get an supplemental plane character
 *	from a UTF-8 string on 8.7 with TCL_UTF_MAX==3.  While this same
 *	function is also used on 8.6, it does only work there after
 *	8.6.10, because Tcl_UtfToUniChar (Tcl_UtfToChar16) does not
 *	return surrogate pairs before.
 *
 * Results:
 *	The number of bytes skipped.
 *
 * Side effects:
 *	None.
 *
 *--------------------------------------------------------------
 */

static inline int IsLeadWord (Tcl_UniChar codepoint)
{ return (codepoint & ~0x03FF) == 0xD800; }

static inline Tcl_UniChar CombinePair (
    unsigned short lead, unsigned short trail)
{ return (((lead & 0x03FF) << 10) | (trail & 0x03FF)) + 0x10000; }

static int
UtfToCodepointEmulate (const char *source, Tcl_UniChar *codepointPtr)
{
    ShortChar_t uniChar = 0, uniChar1;
    int off;
    off = Tcl_UtfToChar16(source, &uniChar);
    if (IsLeadWord(uniChar)) {
	uniChar1 = uniChar;
	off += Tcl_UtfToChar16(source+off, &uniChar);
	*codepointPtr = CombinePair(uniChar1, uniChar);
    } else {
	*codepointPtr = uniChar;
    }
    return off;
}


/*
 *--------------------------------------------------------------
 *
 * CodepointToUtfEmulate --
 *
 *	A compatibilty function to get an UTF-8 string from an
 *	supplemental plane character with TCL_UTF_MAX==3.  While this
 *	same function is also used on 8.6, it does only work there after
 *	8.6.10, because Tcl does not handle surrogate pairs before.
 *	After 8.6.10 this produces surrogate pairs in the UTF-8 with
 *	TCL_UTF_MAX==3, which is the internal representation.  Surrogate
 *	pairs will be converted from and to the correct UTF-8 in the
 *	encodings.
 *
 * Results:
 *	The number of bytes skipped.
 *
 * Side effects:
 *	None.
 *
 *--------------------------------------------------------------
 */

static inline void SplitPair (
    Tcl_UniChar codepoint, unsigned short *leadPtr, unsigned short *trailPtr)
{
    codepoint -= 0x10000;
    *leadPtr  = 0xD800 | ((codepoint >> 10) & 0x03FF);
    *trailPtr = 0xDC00 | ( codepoint        & 0x03FF);
}

static int
CodepointToUtfEmulate (Tcl_UniChar codepoint, char * utf)
{
    if (codepoint > 0xFFFF) {
	unsigned short lead, trail;
	SplitPair(codepoint, &lead, &trail);
	int len = 0;
	len += Tcl_UniCharToUtf(lead, utf);
	utf += len;
	len += Tcl_UniCharToUtf(trail, utf);
	return len;
    } else {
	return Tcl_UniCharToUtf(codepoint, utf);
    }
}


/*
 *---------------------------------------------------------------------------
 *
 * CtkUtfCharComplete --
 *
 *	See if a UTF-8 sequence is complete.  We open-code this, because
 *	in UTF-16 mode, the Tcl version does not help at all for
 *	supplemental plane characters.
 *
 * Results:
 *	1 (true), 0 (false), -1 (error).
 *
 * Side effects:
 *	None.
 *
 *---------------------------------------------------------------------------
 */

static const signed char ByteTypes[] = {
    /* ASCII, 0x00 -> 0x7F */
    /* 00 */ 1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,1,
    /* 10 */ 1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,1,
    /* 20 */ 1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,1,
    /* 30 */ 1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,1,
    /* 40 */ 1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,1,
    /* 50 */ 1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,1,
    /* 60 */ 1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,1,
    /* 70 */ 1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,1,
    /* Trail bytes, 0x80 -> 0xBF */
    /* 80 */ 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0,
    /* 90 */ 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0,
    /* A0 */ 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0,
    /* B0 */ 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0,
    /* Lead bytes, 2, 0xC0 -> 0xDF */
    /* C0 */ 2,2,2,2, 2,2,2,2, 2,2,2,2, 2,2,2,2,
    /* D0 */ 2,2,2,2, 2,2,2,2, 2,2,2,2, 2,2,2,2,
    /* Lead bytes, 3, 0xE0 -> 0xEF */
    /* E0 */ 3,3,3,3, 3,3,3,3, 3,3,3,3, 3,3,3,3,
    /* Lead bytes, 4, 0xF0 -> 0xF7 */
    /* F0 */ 4,4,4,4, 4,4,4,4, -1,-1,-1,-1, -1,-1,-1,-1,
};

typedef char assert_ByteTypes_size[256 == sizeof(ByteTypes) ? 1 : -1];

static inline int ByteType (int ch)
{ return ByteTypes[0xFF & ch]; }

static inline int IsUtfTrailByte (int ch)
{ return 0 == ByteType(ch); }

int
CtkUtfCharComplete(const char *string, int length)
{
    if (length <= 0) {
	return 0;
    }
    int countBytes = ByteType(string[0]);
    if (countBytes <= 0) {
	return -1;
    }
    if (length != countBytes) {
	return 0;
    }
    for (int i=1; i<countBytes; ++i) {
	if (!IsUtfTrailByte(string[i])) {
	    return -1;
	}
    }
    return 1;

    /*
     * FIXME: This does not detect surrogate pairs, as they occur in
     * UTF-8 in some Tcl versions.  Currently, this function is only
     * used for incoming keyboard keys (ctkKeys.c), so we do not need
     * that.
     */
}

#endif /* TCL_MAJOR_VERSION < 9 */


/*
 *---------------------------------------------------------------------------
 *
 * CtkCharNextN --
 *
 *	Find the byte offset after the next COUNT Tcl-level codepoints.
 *	IOW, skip COUNT codepoints.
 *
 *	Parameter maxBytes: A certain amount of overshoot (with the
 *	trailbytes of a UTF-8 sequence) is possible.  This should not
 *	usually cause problems.
 *
 *	This is for communication with Tcl.  Its functions will send
 *	and accept coordinates in this system.
 *
 * Results:
 *	The return value is the number of bytes from STRING that cover
 *	the next COUNT codepoints.
 *
 * Side effects:
 *	None.
 *
 *---------------------------------------------------------------------------
 */

int
CtkCharNextN(const char *string, int maxBytes, int count)
{
    const char *start = string;
    const char *end = string + (maxBytes >= 0 ? maxBytes : strlen(string));
    const char *next;
    /*
     * Make sure to reuse the same variable each time, because in 8.7
     * with TCL_UTF_MAX == 3, the variable space is used for status when
     * it contains the first of a surrogate pair.  Let's just hope we do
     * not get called with a split surrogate pair in that situation.
     */
    Tcl_UniChar uniChar = 0;
    while (string < end && *string != '\0') {

	if (CtkIsAscii(*string)) {
	    next = string + 1;
	} else {
	    /*
	     * We'd have liked to use Tcl_UtfNext here, but that treats
	     * surrogate pairs as units in one of the environments,
	     * which is what we do not want, instead we want exactly
	     * what Tcl uses.  The resulting Tcl_UniChar is ignored.
	     */
	    next = string + SkipOneTclUniChar(string, &uniChar);
	}

	if (--count < 0) {
	    break;
	}

	string = next;
    }

    return string - start;
}


/*
 *---------------------------------------------------------------------------
 *
 * CtkClusterNextN --
 *
 *	Find the byte offset after the next COUNT character clusters.
 *	IOW, skip COUNT base characters and the following combining
 *	characters that belong to them.
 *
 *	Parameter maxBytes: A certain amount of overshoot (with the
 *	trailbytes of a UTF-8 sequence) is possible.  This should not
 *	usually cause problems.
 *
 *	This is for moving in a string, like the cursor does, or when we want
 *	to add or remove a character in our own functions.  That is why control
 *	characters, especially '\n' and '\t' count as clusters.
 *
 * Results:
 *	The return value is the number of bytes that cover the next
 *	COUNT character clusters.
 *
 * Side effects:
 *	None.
 *
 *---------------------------------------------------------------------------
 */

int
CtkClusterNextN(const char *string, int maxBytes, int count)
{
    const char *start = string;
    const char *end = string + (maxBytes >= 0 ? maxBytes : strlen(string));

#if TCL_MAJOR_VERSION < 9
    /*
     * Tcl 8.7, TCL_UTF_MAX=3 may leave us in the middle of a UTF-8
     * sequence.  Get out of there.
     */
    while (string < end && IsUtfTrailByte(*string)) {
	++string;
    }
#endif /* TCL_MAJOR_VERSION < 9 */

    while (string < end && *string != '\0') {

	if (CtkIsAscii(*string)) {

	    if (--count < 0) {
		break;
	    }
	    ++string;

	} else {

	    Tcl_UniChar codepoint;
	    const char *next = string + Ctk_UtfToCodepoint(string, &codepoint);
	    int charWidth = CtkCharWidth(codepoint);
	    if (0 != charWidth && --count < 0) {
		break;
	    }
	    string = next;
	}
    }

    return string - start;
}

/*
 *---------------------------------------------------------------------------
 *
 * CtkColumnNextN --
 *
 *	Find the byte offset after the next COUNT terminal columns.
 *
 *	Parameter maxBytes: A certain amount of overshoot (with the
 *	trailbytes of a UTF-8 sequence) is possible.  This should not
 *	usually cause problems.
 *
 *	This is a more basic version of this functionality than
 *	TkMeasureChars, it does not handle control characters like
 *	newline and tabs.
 *
 *	This is for fitting strings in the terminal cell matrix.
 *
 * Results:
 *
 *	The return value is the number of bytes that cover the next
 *	COUNT matrix cells.
 *
 * Side effects:
 *	None.
 *
 *---------------------------------------------------------------------------
 */

int
CtkColumnNextN(const char *string, int maxBytes, int count)
{
    const char *start = string;
    const char *end = string + (maxBytes >= 0 ? maxBytes : strlen(string));
    while (string < end && *string != '\0') {

	if (CtkIsAscii(*string)) {

	    if (--count < 0) {
		break;
	    }
	    ++string;

	} else {

	    Tcl_UniChar codepoint;
	    const char *next = string + Ctk_UtfToCodepoint(string, &codepoint);
	    count -= CtkCharWidth(codepoint);
	    if (count < 0) {
		break;
	    }
	    string = next;
	}
    }

    return string - start;
}


/*
 *---------------------------------------------------------------------------
 *
 * CtkClusterPrevN --
 *
 *	Find the byte offset before the next COUNT character clusters.  IOW,
 *	skip COUNT base characters backwards and the combining characters that
 *	belong to them.  If COUNT is 0, assume that we are inside the cluster
 *	and go back to the base character.
 *
 *	This is for moving in a string, like the cursor does, or when we want
 *	to add or remove a character in our own functions.  That is why control
 *	characters, especially '\n' and '\t' count as clusters.
 *
 *	Note that this does not work well if we start inside UTF-8 trail bytes,
 *	because Tcl 8.7 mis-interpretes those as CP1252.
 *
 * Results:
 *	The return value is the number of bytes that cover the previous COUNT
 *	character clusters.
 *
 * Side effects:
 *	None.
 *
 *---------------------------------------------------------------------------
 */

int
CtkClusterPrevN(const char *string, int startOffset, int count)
{
    const char *curr = string + startOffset;
    Tcl_UniChar codepoint;

    if (startOffset < 0) {
	return startOffset;
    }

#if TCL_MAJOR_VERSION < 9
    /*
     * Tcl 8.7, TCL_UTF_MAX=3 may leave us in the middle of a UTF-8
     * sequence.  Get out of there.
     */
    while (curr > string && IsUtfTrailByte(*curr)) {
	--curr;
    }
#endif /* TCL_MAJOR_VERSION < 9 */

    /*
     * When count == 0, we may already be inside a cluster, so we need
     * to check the current character extra.
     */

    if (0 == count) {
	Ctk_UtfToCodepoint(curr, &codepoint);
	if (CtkIsAscii(codepoint) || 0 != CtkCharWidth(codepoint)) {
	    return string + startOffset - curr;
	}
    }

    while (curr > string) {
	curr = Tcl_UtfPrev(curr, string);
	Ctk_UtfToCodepoint(curr, &codepoint);
	if (CtkIsAscii(codepoint) || 0 != CtkCharWidth(codepoint)) {
	    if (--count <= 0) {
		return string + startOffset - curr;
	    }
	}
    }

    return startOffset;
}


/*
 *---------------------------------------------------------------------------
 *
 * CtkNumClusters --
 *
 *	How many character clusters does a given string contain?  Additionally,
 *	also count the number of Tcl characters, if parameter charCountPtr is
 *	not NULL.
 *
 * Side effects:
 *	None.
 *
 *---------------------------------------------------------------------------
 */

int
CtkNumClusters(const char *string, int maxBytes, int *charCountPtr)
{
    const char *end = string + (maxBytes >= 0 ? maxBytes : strlen(string));
    int clusterCount = 0;
    int charCount = 0;

    while (string < end) {

	if (CtkIsAscii(*string)) {

	    /*
	     * This includes ASCII controls like \n and \t, which is what we
	     * want.
	     */

	    ++clusterCount;
	    ++charCount;
	    ++string;

	} else {

	    Tcl_UniChar codepoint;
	    string += Ctk_UtfToCodepoint(string, &codepoint);
	    int charWidth = CtkCharWidth(codepoint);
	    if (0 != charWidth) {
		++clusterCount;
	    }
	    ++charCount;
	    if (codepoint > 0xFFFF && supplCharSize > 1) {
		++charCount;
	    }
	}
    }

    if (charCountPtr != NULL) {
	*charCountPtr = charCount;
    }

    return clusterCount;
}

/*
 *---------------------------------------------------------------------------
 *
 * Ctk_ClusterCountCmd --
 *
 * Results:
 *	How many character clusters does a given string contain?
 *
 * Side effects:
 *	None.
 *
 *---------------------------------------------------------------------------
 */

int
Ctk_ClusterCountCmd(
    ClientData clientData,	/* Ignored. */
    Tcl_Interp *interp,		/* Current interpreter. */
    int argc,			/* Number of arguments. */
    const char *argv[])		/* Argument strings. */
{
    if (2 != argc) {
	Tcl_AppendResult(interp, "wrong # args: should be \"",
		argv[0], " string\"", (char *) NULL);
	return TCL_ERROR;
    }

    int count = CtkNumClusters(argv[1], -1, NULL);

    char numbuf[10];
    sprintf(numbuf, "%d", count);
    Tcl_SetResult(interp, numbuf, TCL_VOLATILE);

    return TCL_OK;
}

/*
 *---------------------------------------------------------------------------
 *
 * Ctk_ColumnCountCmd --
 *
 * Results:
 *	How many columns does a given string cover?
 *
 * Side effects:
 *	None.
 *
 *---------------------------------------------------------------------------
 */

int
Ctk_ColumnCountCmd(
    ClientData clientData,	/* Ignored. */
    Tcl_Interp *interp,		/* Current interpreter. */
    int argc,			/* Number of arguments. */
    const char *argv[])		/* Argument strings. */
{
    if (2 != argc) {
	Tcl_AppendResult(interp, "wrong # args: should be \"",
		argv[0], " string\"", (char *) NULL);
	return TCL_ERROR;
    }

    const char *string = argv[1];
    const char *end = string + strlen(string);
    int count = 0;
    while (string < end) {

	if (CtkIsAscii(*string)) {

	    ++count;
	    ++string;

	} else {

	    Tcl_UniChar codepoint;
	    string += Ctk_UtfToCodepoint(string, &codepoint);
	    count += CtkCharWidth(codepoint);
	}
    }

    char numbuf[10];
    sprintf(numbuf, "%d", count);
    Tcl_SetResult(interp, numbuf, TCL_VOLATILE);

    return TCL_OK;
}


/* Only strip the test command when explicitly compiled with TEST=0  */
#if ! defined(TEST) || TEST
/*
 *---------------------------------------------------------------------------
 *
 * Ctk_TestUnicodeCmd --
 *
 *	Expose internal functions for testing.  All string parameters
 *	and string results are represented as byte arrays, because we
 *	want to test the UTF-8 stuff in byte-level detail.
 *
 * Side effects:
 *	None.
 *
 *---------------------------------------------------------------------------
 */

/*
 * Versions of functions that fit the prototype required for use with
 * TestCharProp.
 */
#if TCL_MAJOR_VERSION < 9
static int TestByteType (Tcl_UniChar ch) { return ByteType(ch); }
#endif /* TCL_MAJOR_VERSION < 9 */

static int TestWcwidth(Tcl_UniChar ch) { return wcwidth(ch); }

int
Ctk_TestUnicodeCmd(
    ClientData clientData,	/* Ignored. */
    Tcl_Interp *interp,		/* Current interpreter. */
    int argc,			/* Number of arguments. */
    const char *argv[])		/* Argument strings. */
{
    if (argc < 2) {
	Tcl_AppendResult(interp, "wrong # args: should be \"",
		argv[0], " subcommand ...\"", (char *) NULL);
	return TCL_ERROR;
    }

    const char *subcmd = argv[1];
    if (0 == strcmp(subcmd, "isascii")) {
	if (argc != 3) {
	    Tcl_AppendResult(interp, "wrong # args: should be \"",
		argv[0], " ", subcmd, " codepoint\"",
		(char *) NULL);
	    return TCL_ERROR;
	}
	return TestCharProp(interp, CtkIsAscii, ParseInt(argv[2]));
#if TCL_MAJOR_VERSION < 9
    } else if (0 == strcmp(subcmd, "isleadword")) {
	if (argc != 3) {
	    Tcl_AppendResult(interp, "wrong # args: should be \"",
		argv[0], " ", subcmd, " codepoint\"",
		(char *) NULL);
	    return TCL_ERROR;
	}
	return TestCharProp(interp, IsLeadWord, ParseInt(argv[2]));
#endif /* TCL_MAJOR_VERSION < 9 */
    } else if (0 == strcmp(subcmd, "charwidth")) {
	if (argc != 3) {
	    Tcl_AppendResult(interp, "wrong # args: should be \"",
		argv[0], " ", subcmd, " codepoint\"",
		(char *) NULL);
	    return TCL_ERROR;
	}
	return TestCharProp(interp, CtkCharWidth, ParseInt(argv[2]));
    } else if (0 == strcmp(subcmd, "wcwidth")) {
	/*
	 * NCurses uses wcwidth internally, so have a command that
	 * exercises that.
	 */
	if (argc != 3) {
	    Tcl_AppendResult(interp, "wrong # args: should be \"",
		argv[0], " ", subcmd, " codepoint\"",
		(char *) NULL);
	    return TCL_ERROR;
	}
	return TestCharProp(interp, TestWcwidth, ParseInt(argv[2]));
#if TCL_MAJOR_VERSION < 9
    } else if (0 == strcmp(subcmd, "utfbytetype")) {
	if (argc != 3) {
	    Tcl_AppendResult(interp, "wrong # args: should be \"",
		argv[0], " ", subcmd, " byte\"",
		(char *) NULL);
	    return TCL_ERROR;
	}
	return TestCharProp(interp, TestByteType, ParseInt(argv[2]));
#endif /* TCL_MAJOR_VERSION < 9 */
    } else if (0 == strcmp(subcmd, "charnext")) {
	if (argc != 5) {
	    Tcl_AppendResult(interp, "wrong # args: should be \"",
		argv[0], " ", subcmd, " utf8bytes size offset\"",
		(char *) NULL);
	    return TCL_ERROR;
	}
	return TestNext(interp, CtkCharNextN, argv[2],
		ParseInt(argv[3]), ParseInt(argv[4]));
    } else if (0 == strcmp(subcmd, "clusternext")) {
	if (argc != 5) {
	    Tcl_AppendResult(interp, "wrong # args: should be \"",
		argv[0], " ", subcmd, " utf8bytes size offset\"",
		(char *) NULL);
	    return TCL_ERROR;
	}
	return TestNext(interp, CtkClusterNextN, argv[2],
		ParseInt(argv[3]), ParseInt(argv[4]));
    } else if (0 == strcmp(subcmd, "clusterprev")) {
	if (argc != 5) {
	    Tcl_AppendResult(interp, "wrong # args: should be \"",
		argv[0], " ", subcmd, " utf8bytes size offset\"",
		(char *) NULL);
	    return TCL_ERROR;
	}
	return TestNext(interp, TestClusterPrevN, argv[2],
		ParseInt(argv[3]), ParseInt(argv[4]));
    } else if (0 == strcmp(subcmd, "columnnext")) {
	if (argc != 5) {
	    Tcl_AppendResult(interp, "wrong # args: should be \"",
		argv[0], " ", subcmd, " utf8bytes size offset\"",
		(char *) NULL);
	    return TCL_ERROR;
	}
	return TestNext(interp, CtkColumnNextN, argv[2],
		ParseInt(argv[3]), ParseInt(argv[4]));
    } else if (0 == strcmp(subcmd, "utftocodepoint")) {
	if (argc != 3) {
	    Tcl_AppendResult(interp, "wrong # args: should be \"",
		argv[0], " ", subcmd, " utf8bytes\"",
		(char *) NULL);
	    return TCL_ERROR;
	}
	Tcl_Obj *obj = Tcl_NewStringObj(argv[2], -1);
	Tcl_IncrRefCount(obj);
	int bytesLen = 0;
	const unsigned char *bytes = Tcl_GetByteArrayFromObj(obj, &bytesLen);
	Tcl_UniChar codepoint = -1;
	int byteOff = Ctk_UtfToCodepoint((const char*)bytes, &codepoint);
	if (byteOff > bytesLen) {
	    char len[20], off[20];
	    sprintf(len, "%d", bytesLen);
	    sprintf(off, "%d", byteOff);
	    Tcl_AppendResult(interp, "offset ", off, " larger than size ",
		    len, (char *) NULL);
	    return TCL_ERROR;
	}
	/*
	 * sprintf: Use a string object to control the default
	 * formatting of the number.
	 */
	char buff[20];
	sprintf(buff, "0x%04X", codepoint);
	Tcl_Obj *elements[] = {
	    Tcl_NewStringObj(buff, -1),
	    Tcl_NewByteArrayObj(bytes + byteOff, bytesLen - byteOff)
	};
	Tcl_Obj *list = Tcl_NewListObj(2, elements);
	Tcl_SetObjResult(interp, list);
	Tcl_DecrRefCount(obj);
	return TCL_OK;
    } else if (0 == strcmp(subcmd, "codepointtoutf")) {
	if (argc != 3) {
	    Tcl_AppendResult(interp, "wrong # args: should be \"",
		argv[0], " ", subcmd, " codepoint\"",
		(char *) NULL);
	    return TCL_ERROR;
	}
	int codepoint = ParseInt(argv[2]);
	if (codepoint < 0) {
	    Tcl_AppendResult(interp, "codepoint must be >= 0", (char *) NULL);
	    return TCL_ERROR;
	}
	char utf[10];
	int len = Ctk_CodepointToUtf(codepoint, utf);
	Tcl_SetObjResult(interp,
		Tcl_NewByteArrayObj((const unsigned char*)utf, len));
	return TCL_OK;
    } else {
	Tcl_AppendResult(interp, "unknown subcommand \"", argv[1], "\"",
		(char *) NULL);
	return TCL_ERROR;
    }
}


/*
 *---------------------------------------------------------------------------
 *
 * ParseInt --
 *
 *	A simple number parser with validation for the test command.
 *
 * Side effects:
 *	None.
 *
 *---------------------------------------------------------------------------
 */
static int
ParseInt(const char * string)
{
    int result = INT_ERROR;
    if (TCL_OK != Tcl_GetInt(NULL, string, &result)) {
	return INT_ERROR;
    }
    return result;
}


/*
 *---------------------------------------------------------------------------
 *
 * TestCharProp --
 *
 *	A generic test routine for character attributes.
 *
 * Side effects:
 *	None.
 *
 *---------------------------------------------------------------------------
 */
static int
TestCharProp(
    Tcl_Interp *interp,
    int (*subcommand)(Tcl_UniChar),
    int codepoint)
{
    if (codepoint < 0) {
	Tcl_AppendResult(interp, "codepoint/byte must be >= 0", (char *) NULL);
	return TCL_ERROR;
    }
    int result = subcommand(codepoint);
    char buff[20];
    sprintf(buff, "%d", result);
    Tcl_AppendResult(interp, buff, (char *) NULL);
    return TCL_OK;
}


/*
 *---------------------------------------------------------------------------
 *
 * TestNext --
 *
 *	A generic test routine for going forward in a string.
 *
 * Side effects:
 *	None.
 *
 *---------------------------------------------------------------------------
 */
static int
TestNext(
    Tcl_Interp *interp,
    int (*subcommand)(const char *, int, int),
    const char *string,
    int length,
    int offset)
{
    if (length < -1) {
	Tcl_AppendResult(interp, "length must be a positive integer or -1",
		(char *) NULL);
	return TCL_ERROR;
    }
    if (offset < 0) {
	Tcl_AppendResult(interp, "offset must be a positive integer",
		(char *) NULL);
	return TCL_ERROR;
    }
    int result = TCL_ERROR;
    Tcl_Obj *obj = Tcl_NewStringObj(string, -1);
    Tcl_IncrRefCount(obj);
    int bytesLen = 0;
    const unsigned char *bytes = Tcl_GetByteArrayFromObj(obj, &bytesLen);
    int coverLen = bytesLen;
    if (length != -1) {
	if (length > bytesLen) {
	    Tcl_AppendResult(interp, "length must be less or equal string length",
		    (char *) NULL);
	    goto done;
	}
	coverLen = length;
    }
    int byteOff = subcommand((const char*)bytes, coverLen, offset);
    if (byteOff > bytesLen) {
	char len[20], off[20];
	sprintf(len, "%d", bytesLen);
	sprintf(off, "%d", byteOff);
	Tcl_AppendResult(interp, "offset ", off, " larger than size ",
		len, (char *) NULL);
	goto done;
    }
    if (byteOff < 0) {
	Tcl_AppendResult(interp, "some error occurred", (char *) NULL);
	goto done;
    }
    Tcl_SetObjResult(interp,
	    Tcl_NewByteArrayObj(bytes + byteOff, bytesLen - byteOff));
    result = TCL_OK;
done:
    Tcl_DecrRefCount(obj);
    return result;
}

/*
 *---------------------------------------------------------------------------
 *
 * TestClusterPrevN --
 *
 *	To use CtkClusterPrevN with TestNext, the return value must be
 *	inverted.
 *
 * Side effects:
 *	None.
 *
 *---------------------------------------------------------------------------
 */
static int
TestClusterPrevN(const char *string, int startOffset, int count)
{
    int result = CtkClusterPrevN(string, startOffset, count);
    if (result < 0) {
	return result;
    }
    return startOffset - result;
}

#endif /* TEST */
