/* 
 * tkTextIndex.c (Ctk) --
 *
 *	This module provides procedures that manipulate indices for
 *	text widgets.
 *
 * Copyright (c) 1992-1994 The Regents of the University of California.
 * Copyright (c) 1994-1995 Sun Microsystems, Inc.
 *
 * See the file "license.terms" for information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 */


#include "default.h"
#include "tkPort.h"
#include "tkInt.h"
#include "tkText.h"
#include "ctkUnicode.h"

/*
 * Index to use to select last character in line (very large integer):
 */

#define LAST_CHAR 1000000

/*
 * Forward declarations for procedures defined later in this file:
 */

static int		GetLastCluster(TkTextSegment *segPtr,
				int currentOffset);
static TkTextSegment *	NextSegment(TkTextSegment *segPtr,
				TkTextIndex *indexPtr);
static TkTextSegment *	PrevSegment(TkTextSegment *segPtr,
				TkTextIndex *indexPtr);
static char *		ForwBack (char *string, TkTextIndex *indexPtr);
static char *		StartEnd (char *string, TkTextIndex *indexPtr);

/*
 *--------------------------------------------------------------
 *
 * TkTextMakeIndex --
 *
 *	Given a line index and a byte index, look things up in
 *	the B-tree and fill in a TkTextIndex structure.  If
 *	byteOffset is -1, calculate the effective byteOffset
 *	from charIndex.
 *
 *	Handling of character clusters: If byteOffset != -1 and
 *	before the end of the line, the caller is responsible
 *	that it points to the start of a character cluster.  If
 *	byteOffset is larger than the line or if we have to
 *	calculate it from charIndex, this function adjusts
 *	backwards to the start of the relevant cluster.
 *
 * Results:
 *	The structure at *indexPtr is filled in with information
 *	about lineIndex and byteOffset (or the closest existing
 *	character, if the specified one doesn't exist), and
 *	indexPtr is returned as result.
 *
 * Side effects:
 *	None.
 *
 *--------------------------------------------------------------
 */

TkTextIndex *
TkTextMakeIndex(
    TkTextBTree tree,		/* Tree that lineIndex and byteOffset refer
				 * to. */
    int lineIndex,		/* Index of desired line (0 means first
				 * line of text). */
    int byteOffset,		/* Offset of desired character or -1. */
    int charIndex,		/* Index of desired character. */
    TkTextIndex *indexPtr)	/* Structure to fill in. */
{
    int index;

    indexPtr->tree = tree;
    if (lineIndex < 0) {
	lineIndex = 0;
	byteOffset = 0;
    }
    indexPtr->linePtr = TkBTreeFindLine(tree, lineIndex);
    if (indexPtr->linePtr == NULL) {
	indexPtr->linePtr = TkBTreeFindLine(tree, TkBTreeNumLines(tree));
	byteOffset = 0;
    }

    if (byteOffset < 0) {

	/*
	 * If byteCount < 0, we need to calculate it from
	 * charIndex.
	 */

	TkTextSegment *segPtr, *lastSegPtr = NULL;

	byteOffset = 0;
	for (segPtr = indexPtr->linePtr->segPtr; ;
	     segPtr = segPtr->nextPtr) {

	    if (segPtr == NULL) {
		byteOffset = GetLastCluster(lastSegPtr, byteOffset);
		break;
	    }

	    if (charIndex == 0) {
		/*
		 * We are not at the end of the line yet.  But
		 * because the last segment is always a newline,
		 * we are before that last character so we do
		 * not have to check any further and are fine
		 * here.
		 */
		break;
	    }

	    if (charIndex < segPtr->sizeChars) {
		int segOffset;
		segOffset = CtkCharNextN(segPtr->body.chars,
			segPtr->sizeBytes, charIndex);
		/*
		 * Adjust for the start of the cluster.
		 */
		segOffset -= CtkClusterPrevN(segPtr->body.chars, segOffset,
			0);
		byteOffset += segOffset;
		break;
	    }

	    charIndex -= segPtr->sizeChars;
	    byteOffset += segPtr->sizeBytes;

	    if (segPtr->sizeBytes > 0) {
		lastSegPtr = segPtr;
	    }
	}

	indexPtr->byteOffset = byteOffset;

    } else {

	/*
	 * Verify that the index is within the range of the line.
	 * If not, use the offset of the last character cluster
	 * in the line.
	 */

	TkTextSegment *segPtr, *lastSegPtr = NULL;

	for (index = 0, segPtr = indexPtr->linePtr->segPtr; ;
	     segPtr = segPtr->nextPtr) {

	    if (segPtr == NULL) {
		indexPtr->byteOffset = GetLastCluster(lastSegPtr, index);
		break;
	    }

	    index += segPtr->sizeBytes;
	    if (index > byteOffset) {
		indexPtr->byteOffset = byteOffset;
		break;
	    }

	    if (segPtr->sizeBytes > 0) {
		lastSegPtr = segPtr;
	    }
	}
    }

    return indexPtr;
}

static int
GetLastCluster(TkTextSegment *segPtr, int currentOffset)
{
    if (segPtr == NULL) {
	return 0;
    } else {
	if (segPtr->sizeBytes <= 0) {
	    Tcl_Panic("GetLastCluster: segPtr was not text type");
	}
	char last = segPtr->body.chars[segPtr->sizeBytes-1];
	if (last != '\n') {
	    Tcl_Panic("GetLastCluster: Last character should be NL, "
		    "but was %02X", 0xFF & last);
	}
	return currentOffset - CtkClusterPrevN(segPtr->body.chars,
		segPtr->sizeBytes, 1);
    }
}

/*
 *--------------------------------------------------------------
 *
 * TkTextIndexToSeg --
 *
 *	Given an index, this procedure returns the segment and
 *	offset within segment for the index.
 *
 * Results:
 *	The return value is a pointer to the segment referred to
 *	by indexPtr; this will always be a segment with non-zero
 *	size.  The variable at *offsetPtr is set to hold the
 *	integer offset (in bytes) within the segment of the
 *	character given by indexPtr.  If indexPtr->byteOffset
 *	points beyont the last segment, NULL is returned and
 *	offsetPtr is not filled-in.
 *
 * Side effects:
 *	None.
 *
 *--------------------------------------------------------------
 */

TkTextSegment *
TkTextIndexToSeg(
    TkTextIndex *indexPtr,		/* Text index. */
    int *offsetPtr)			/* Where to store offset within
					 * segment, or NULL if offset isn't
					 * wanted. */
{
    register TkTextSegment *segPtr;
    int offset;

    for (offset = indexPtr->byteOffset, segPtr = indexPtr->linePtr->segPtr;
	    segPtr != NULL && offset >= segPtr->sizeBytes;
	    offset -= segPtr->sizeBytes, segPtr = segPtr->nextPtr) {
	/* Empty loop body. */
    }
    if (segPtr != NULL && offsetPtr != NULL) {
	*offsetPtr = offset;
    }
    return segPtr;
}

/*
 *--------------------------------------------------------------
 *
 * TkTextSegToOffset --
 *
 *	Given a segment pointer and the line containing it, this
 *	procedure returns the offset of the segment within its
 *	line.
 *
 * Results:
 *	The return value is the offset (within its line, in
 *	bytes) of the first character in segPtr.
 *
 * Side effects:
 *	None.
 *
 *--------------------------------------------------------------
 */

int
TkTextSegToOffset(
    TkTextSegment *segPtr,		/* Segment whose offset is desired. */
    TkTextLine *linePtr)		/* Line containing segPtr. */
{
    TkTextSegment *segPtr2;
    int offset;

    offset = 0;
    for (segPtr2 = linePtr->segPtr; segPtr2 != segPtr;
	    segPtr2 = segPtr2->nextPtr) {
	offset += segPtr2->sizeBytes;
    }
    return offset;
}

/*
 *----------------------------------------------------------------------
 *
 * TkTextGetIndex --
 *
 *	Given a string, return the line and byte indices that it
 *	describes.
 *
 * Results:
 *	The return value is a standard Tcl return result.  If
 *	TCL_OK is returned, then everything went well and the index
 *	at *indexPtr is filled in;  otherwise TCL_ERROR is returned
 *	and an error message is left in interp->result.
 *
 * Side effects:
 *	None.
 *
 *----------------------------------------------------------------------
 */

int
TkTextGetIndex(
    Tcl_Interp *interp,		/* Use this for error reporting. */
    TkText *textPtr,		/* Information about text widget. */
    const char *string,		/* Textual description of position. */
    TkTextIndex *indexPtr)	/* Index structure to fill in. */
{
    register char *p;
    char *end, *endOfBase;
    Tcl_DString stringCopy;
    char *stringRw;
    Tcl_HashEntry *hPtr;
    TkTextTag *tagPtr;
    TkTextSearch search;
    TkTextIndex first, last;
    int wantLast, result;
    char c;

    /*
     *---------------------------------------------------------------------
     * Stage 1: check to see if the index consists of nothing but a mark
     * name.  We do this check now even though it's also done later, in
     * order to allow mark names that include funny characters such as
     * spaces or "+1c".
     *---------------------------------------------------------------------
     */

    if (TkTextMarkNameToIndex(textPtr, string, indexPtr) == TCL_OK) {
	return TCL_OK;
    }

    /*
     *------------------------------------------------
     * Stage 2: start again by parsing the base index.
     *------------------------------------------------
     */

    indexPtr->tree = textPtr->tree;

    /*
     * First look for the form "tag.first" or "tag.last" where "tag"
     * is the name of a valid tag.  Try to use up as much as possible
     * of the string in this check (strrchr instead of strchr below).
     * Doing the check now, and in this way, allows tag names to include
     * funny characters like "@" or "+1c".
     */

    Tcl_DStringInit(&stringCopy);
    stringRw = Tcl_DStringAppend(&stringCopy, string, strlen(string));

    p = strrchr(stringRw, '.');
    if (p != NULL) {
	if ((p[1] == 'f') && (strncmp(p+1, "first", 5) == 0)) {
	    wantLast = 0;
	    endOfBase = p+6;
	} else if ((p[1] == 'l') && (strncmp(p+1, "last", 4) == 0)) {
	    wantLast = 1;
	    endOfBase = p+5;
	} else {
	    goto tryxy;
	}
	*p = 0;
	hPtr = Tcl_FindHashEntry(&textPtr->tagTable, stringRw);
	*p = '.';
	if (hPtr == NULL) {
	    goto tryxy;
	}
	tagPtr = (TkTextTag *) Tcl_GetHashValue(hPtr);
	TkTextMakeIndex(textPtr->tree, 0, 0, 0, &first);
	TkTextMakeIndex(textPtr->tree, TkBTreeNumLines(textPtr->tree), 0, 0,
		&last);
	TkBTreeStartSearch(&first, &last, tagPtr, &search);
	if (!TkBTreeCharTagged(&first, tagPtr) && !TkBTreeNextTag(&search)) {
	    Tcl_AppendResult(interp,
		    "text doesn't contain any characters tagged with \"",
		    Tcl_GetHashKey(&textPtr->tagTable, hPtr), "\"",
		    (char *) NULL);
	    Tcl_DStringFree(&stringCopy);
	    return TCL_ERROR;
	}
	*indexPtr = search.curIndex;
	if (wantLast) {
	    while (TkBTreeNextTag(&search)) {
		*indexPtr = search.curIndex;
	    }
	}
	goto gotBase;
    }

    tryxy:
    if (stringRw[0] == '@') {
	/*
	 * Find character at a given x,y location in the window.
	 */

	int x, y;

	p = stringRw+1;
	x = strtol(p, &end, 0);
	if ((end == p) || (*end != ',')) {
	    goto error;
	}
	p = end+1;
	y = strtol(p, &end, 0);
	if (end == p) {
	    goto error;
	}
	TkTextPixelIndex(textPtr, x, y, indexPtr);
	endOfBase = end;
	goto gotBase; 
    }

    if (CtkIsDigit(stringRw[0]) || (stringRw[0] == '-')) {
	int lineIndex, byteOffset = -1, charIndex = -1;

	/*
	 * Base is identified with line and character indices.
	 */

	lineIndex = strtol(stringRw, &end, 0) - 1;
	if ((end == stringRw) || (*end != '.')) {
	    goto error;
	}
	p = end+1;
	if ((*p == 'e') && (strncmp(p, "end", 3) == 0)) {
	    byteOffset = LAST_CHAR;
	    endOfBase = p+3;
	} else {
	    charIndex = strtol(p, &end, 0);
	    if (end == p) {
		goto error;
	    }
	    endOfBase = end;
	}
	TkTextMakeIndex(textPtr->tree, lineIndex, byteOffset, charIndex,
		indexPtr);
	goto gotBase;
    }

    for (p = stringRw; *p != 0; p++) {
	if (CtkIsSpace(*p) || (*p == '+') || (*p == '-')) {
	    break;
	}
    }
    endOfBase = p;
#if 0
    if (stringRw[0] == '.') {
	/*
	 * See if the base position is the name of an embedded window.
	 */

	c = *endOfBase;
	*endOfBase = 0;
	result = TkTextWindowIndex(textPtr, stringRw, indexPtr);
	*endOfBase = c;
	if (result != 0) {
	    goto gotBase;
	}
    }
#endif
    if ((stringRw[0] == 'e')
	    && (strncmp(stringRw, "end", endOfBase-stringRw) == 0)) {
	/*
	 * Base position is end of text.
	 */

	TkTextMakeIndex(textPtr->tree, TkBTreeNumLines(textPtr->tree),
		0, 0, indexPtr);
	goto gotBase;
    } else {
	/*
	 * See if the base position is the name of a mark.
	 */

	c = *endOfBase;
	*endOfBase = 0;
	result = TkTextMarkNameToIndex(textPtr, stringRw, indexPtr);
	*endOfBase = c;
	if (result == TCL_OK) {
	    goto gotBase;
	}
    }
    goto error;

    /*
     *-------------------------------------------------------------------
     * Stage 3: process zero or more modifiers.  Each modifier is either
     * a keyword like "wordend" or "linestart", or it has the form
     * "op count units" where op is + or -, count is a number, and units
     * is "chars" or "lines".
     *-------------------------------------------------------------------
     */

    gotBase:
    p = endOfBase;
    while (1) {
	while (CtkIsSpace(*p)) {
	    p++;
	}
	if (*p == 0) {
	    break;
	}
    
	if ((*p == '+') || (*p == '-')) {
	    p = ForwBack(p, indexPtr);
	} else {
	    p = StartEnd(p, indexPtr);
	}
	if (p == NULL) {
	    goto error;
	}
    }
    Tcl_DStringFree(&stringCopy);
    return TCL_OK;

    error:
    Tcl_AppendResult(interp, "bad text index \"", string, "\"",
	    (char *) NULL);
    Tcl_DStringFree(&stringCopy);
    return TCL_ERROR;
}

/*
 *----------------------------------------------------------------------
 *
 * TkTextPrintIndex --
 *
 *	This procedure generates a string description of an index,
 *	suitable for reading in again later.
 *
 * Results:
 *	The characters pointed to by string are modified.
 *
 * Side effects:
 *	None.
 *
 *----------------------------------------------------------------------
 */

void
TkTextPrintIndex(
    TkTextIndex *indexPtr,	/* Pointer to index. */
    char *string)		/* Place to store the position.  Must have
				 * at least TK_POS_CHARS characters. */
{
    TkTextSegment *segPtr;
    int charOffset, byteOffset;

    charOffset = 0;
    byteOffset = indexPtr->byteOffset;

    for (segPtr = indexPtr->linePtr->segPtr;
	 segPtr != NULL && byteOffset > 0;
	 segPtr = segPtr->nextPtr) {

	if (byteOffset < segPtr->sizeBytes) {
	    charOffset += Tcl_NumUtfChars(segPtr->body.chars, byteOffset);
	    break;
	}

	byteOffset -= segPtr->sizeBytes;
	charOffset += segPtr->sizeChars;
    }

    sprintf(string, "%d.%d", TkBTreeLineIndex(indexPtr->linePtr) + 1,
	    charOffset);
}

/*
 *--------------------------------------------------------------
 *
 * TkTextIndexCmp --
 *
 *	Compare two indices to see which one is earlier in
 *	the text.
 *
 * Results:
 *	The return value is 0 if index1Ptr and index2Ptr refer
 *	to the same position in the text, -1 if index1Ptr refers
 *	to an earlier position than index2Ptr, and 1 otherwise.
 *
 * Side effects:
 *	None.
 *
 *--------------------------------------------------------------
 */

int
TkTextIndexCmp(
    TkTextIndex *index1Ptr,		/* First index. */
    TkTextIndex *index2Ptr)		/* Second index. */
{
    int line1, line2;

    if (index1Ptr->linePtr == index2Ptr->linePtr) {
	if (index1Ptr->byteOffset < index2Ptr->byteOffset) {
	    return -1;
	} else if (index1Ptr->byteOffset > index2Ptr->byteOffset) {
	    return 1;
	} else {
	    return 0;
	}
    }
    line1 = TkBTreeLineIndex(index1Ptr->linePtr);
    line2 = TkBTreeLineIndex(index2Ptr->linePtr);
    if (line1 < line2) {
	return -1;
    }
    if (line1 > line2) {
	return 1;
    }
    return 0;
}

/*
 *----------------------------------------------------------------------
 *
 * ForwBack --
 *
 *	This procedure handles +/- modifiers for indices to adjust
 *	the index forwards or backwards.
 *
 * Results:
 *	If the modifier in string is successfully parsed then the
 *	return value is the address of the first character after the
 *	modifier, and *indexPtr is updated to reflect the modifier.
 *	If there is a syntax error in the modifier then NULL is returned.
 *
 * Side effects:
 *	None.
 *
 *----------------------------------------------------------------------
 */

static char *
ForwBack(
    char *string,		/* String to parse for additional info
				 * about modifier (count and units). 
				 * Points to "+" or "-" that starts
				 * modifier. */
    TkTextIndex *indexPtr)	/* Index to update as specified in string. */
{
    register char *p;
    const char *units;
    char *end;
    int count, lineIndex;
    size_t length;

    /*
     * Get the count (how many units forward or backward).
     */

    p = string+1;
    while (CtkIsSpace(*p)) {
	p++;
    }
    count = strtol(p, &end, 0);
    if (end == p) {
	return NULL;
    }
    p = end;
    while (CtkIsSpace(*p)) {
	p++;
    }

    /*
     * Find the end of this modifier (next space or + or - character),
     * then parse the unit specifier and update the position
     * accordingly.
     */

    units = p; 
    while ((*p != 0) && !CtkIsSpace(*p) && (*p != '+') && (*p != '-')) {
	p++;
    }
    length = p - units;
    if ((*units == 'c') && (strncmp(units, "chars", length) == 0)) {
	if (*string == '+') {
	    TkTextIndexForwChars(indexPtr, count, indexPtr);
	} else {
	    TkTextIndexBackChars(indexPtr, count, indexPtr);
	}
    } else if ((*units == 'l') && (strncmp(units, "lines", length) == 0)) {
	lineIndex = TkBTreeLineIndex(indexPtr->linePtr);
	if (*string == '+') {
	    lineIndex += count;
	} else {
	    lineIndex -= count;
	    if (lineIndex < 0) {
		lineIndex = 0;
	    }
	}
	TkTextMakeIndex(indexPtr->tree, lineIndex, indexPtr->byteOffset,
		0, indexPtr);
    } else {
	return NULL;
    }
    return p;
}

/*
 *----------------------------------------------------------------------
 *
 * TkTextIndexForwChars --
 *
 *	Given an index for a text widget, this procedure creates a new index
 *	that points "count" character clusters ahead of the source index.
 *
 * Results:
 *	*dstPtr is modified to refer to the character "count" character
 *	clusters after srcPtr, or to the last cluster in the text if there
 *	aren't "count" clusters left in the text.
 *
 * Side effects:
 *	None.
 *
 *----------------------------------------------------------------------
 */

void
TkTextIndexForwChars(
    const TkTextIndex *srcPtr,		/* Source index. */
    int count,				/* How many character clusters forward
					 * to move.  May be negative. */
    TkTextIndex *dstPtr)		/* Destination index: gets modified. */
{
    TkTextLine *lastLinePtr;
    TkTextSegment *segPtr, *lastSegPtr = NULL;
    int segByteOffset, segClusterOffset;

    if (count < 0) {
	TkTextIndexBackChars(srcPtr, -count, dstPtr);
	return;
    }

    *dstPtr = *srcPtr;

    if (0 == count) {
	return;
    }

    /*
     * See if we can just find it on the current segment.  This
     * is the only time we need to consider an offset into the
     * segment.
     */

    segPtr = TkTextIndexToSeg(dstPtr, &segByteOffset);
    segClusterOffset = CtkNumClusters(segPtr->body.chars, segByteOffset,
	    NULL);
    if ((segClusterOffset+count) < segPtr->sizeClusters) {
	dstPtr->byteOffset += CtkClusterNextN(
		segPtr->body.chars + segByteOffset,
		segPtr->sizeBytes - segByteOffset, count);
	return;
    }

    /*
     * Substract the rest of the segment.
     */

    count -= segPtr->sizeClusters - segClusterOffset;
    dstPtr->byteOffset += segPtr->sizeBytes - segByteOffset;

    while (1) {
	lastSegPtr = segPtr;
	lastLinePtr = dstPtr->linePtr;

	segPtr = NextSegment(segPtr, dstPtr);
	if (segPtr == NULL) {
	    dstPtr->byteOffset += GetLastCluster(lastSegPtr, 0);
	    dstPtr->linePtr = lastLinePtr;
	    return;
	}

	if (count < segPtr->sizeClusters) {
	    if (0 != count) {
		dstPtr->byteOffset += CtkClusterNextN(segPtr->body.chars,
			segPtr->sizeBytes, count);
	    }
	    return;
	}

	count -= segPtr->sizeClusters;
	dstPtr->byteOffset += segPtr->sizeBytes;
    }
}

/*
 *----------------------------------------------------------------------
 *
 * TkTextIndexForwBytes --
 *
 *	Given an index for a text widget, this procedure creates a new index
 *	that points "count" bytes ahead of the source index.
 *
 * Results:
 *	*dstPtr is modified to refer to the byte "count" bytes after srcPtr, or
 *	to the last cluster in the text if there aren't "count" bytes left in
 *	the text.
 *
 * Side effects:
 *	None.
 *
 *----------------------------------------------------------------------
 */

void
TkTextIndexForwBytes(
    const TkTextIndex *srcPtr,		/* Source index. */
    int count,				/* How many bytes forward to move.  May
					 * be negative. */
    TkTextIndex *dstPtr)		/* Destination index: gets modified. */
{
    TkTextLine *lastLinePtr;
    TkTextSegment *segPtr, *lastSegPtr = NULL;
    int segByteOffset;

    if (count < 0) {
	TkTextIndexBackBytes(srcPtr, -count, dstPtr);
	return;
    }

    *dstPtr = *srcPtr;

    if (0 == count) {
	return;
    }

    /*
     * See if we can just find it on the current segment.  This
     * is the only time we need to consider an offset into the
     * segment.
     */

    segPtr = TkTextIndexToSeg(dstPtr, &segByteOffset);
    if ((segByteOffset+count) < segPtr->sizeBytes) {
	dstPtr->byteOffset += count;
	return;
    }

    /*
     * Substract the rest of the segment.
     */

    count -= segPtr->sizeBytes - segByteOffset;
    dstPtr->byteOffset += segPtr->sizeBytes - segByteOffset;

    while (1) {
	lastSegPtr = segPtr;
	lastLinePtr = dstPtr->linePtr;

	segPtr = NextSegment(segPtr, dstPtr);
	if (segPtr == NULL) {
	    dstPtr->byteOffset += GetLastCluster(lastSegPtr, 0);
	    dstPtr->linePtr = lastLinePtr;
	    return;
	}

	if (count < segPtr->sizeBytes) {
	    dstPtr->byteOffset += count;
	    return;
	}

	count -= segPtr->sizeBytes;
	dstPtr->byteOffset += segPtr->sizeBytes;
    }
}

static TkTextSegment *
NextSegment(TkTextSegment *segPtr, TkTextIndex *indexPtr)
{
    do {
	segPtr = segPtr->nextPtr;
	if (NULL == segPtr) {
	    /*
	     * Continue on the next line, if necessary.
	     */
	    TkTextLine *lPtr = TkBTreeNextLine(indexPtr->linePtr);
	    if (lPtr == NULL) {
		return NULL;
	    }
	    indexPtr->linePtr = lPtr;
	    indexPtr->byteOffset = 0;
	    segPtr = lPtr->segPtr;
	}
    } while (segPtr->sizeBytes == 0);

    return segPtr;
}

/*
 *----------------------------------------------------------------------
 *
 * TkTextIndexBackChars --
 *
 *	Given an index for a text widget, this procedure creates a
 *	new index that points "count" characters earlier than the
 *	source index.
 *
 * Results:
 *	*dstPtr is modified to refer to the character "count" characters
 *	before srcPtr, or to the first character in the text if there aren't
 *	"count" characters earlier than srcPtr.
 *
 * Side effects:
 *	None.
 *
 *----------------------------------------------------------------------
 */

void
TkTextIndexBackChars(
    const TkTextIndex *srcPtr,		/* Source index. */
    int count,				/* How many characters backward to
					 * move.  May be negative. */
    TkTextIndex *dstPtr)		/* Destination index: gets modified. */
{
    TkTextSegment *segPtr;
    int segByteOffset, segClusterOffset;

    if (count < 0) {
	TkTextIndexForwChars(srcPtr, -count, dstPtr);
	return;
    }

    *dstPtr = *srcPtr;

    if (0 == count) {
	return;
    }

    /*
     * See if we can just find it on the current segment.  This
     * is the only time we need to consider an offset into the
     * segment.
     */

    segPtr = TkTextIndexToSeg(dstPtr, &segByteOffset);
    segClusterOffset = CtkNumClusters(segPtr->body.chars, segByteOffset,
	    NULL);
    if (segClusterOffset == count) {
	dstPtr->byteOffset -= segByteOffset;
	return;
    }
    if (segClusterOffset > count) {
	dstPtr->byteOffset -= CtkClusterPrevN(segPtr->body.chars,
		segByteOffset, count);
	return;
    }

    /*
     * Substract the rest of the segment.
     */

    count -= segClusterOffset;
    dstPtr->byteOffset -= segByteOffset;

    while (count > 0) {
	segPtr = PrevSegment(segPtr, dstPtr);
	if (NULL == segPtr) {
	    return;
	}

	if (count == segPtr->sizeClusters) {
	    dstPtr->byteOffset -= segPtr->sizeBytes;
	    return;
	}

	if (count < segPtr->sizeClusters) {
	    dstPtr->byteOffset -= CtkClusterPrevN(segPtr->body.chars,
		    segPtr->sizeBytes, count);
	    return;
	}

	count -= segPtr->sizeClusters;
	dstPtr->byteOffset -= segPtr->sizeBytes;
    }
}

/*
 *----------------------------------------------------------------------
 *
 * TkTextIndexBackBytes --
 *
 *	Given an index for a text widget, this procedure creates a new index
 *	that points "count" bytes earlier than the source index.
 *
 * Results:
 *	*dstPtr is modified to refer to the byte "count" bytes before srcPtr,
 *	or to the first byte in the text if there aren't "count" bytes earlier
 *	than srcPtr.
 *
 * Side effects:
 *	None.
 *
 *----------------------------------------------------------------------
 */

void
TkTextIndexBackBytes(
    const TkTextIndex *srcPtr,		/* Source index. */
    int count,				/* How bytes backward to move.  May be
					 * negative. */
    TkTextIndex *dstPtr)		/* Destination index: gets modified. */
{
    TkTextSegment *segPtr;
    int segByteOffset;

    if (count < 0) {
	TkTextIndexForwBytes(srcPtr, -count, dstPtr);
	return;
    }

    *dstPtr = *srcPtr;

    if (0 == count) {
	return;
    }

    /*
     * See if we can just find it on the current segment.  This
     * is the only time we need to consider an offset into the
     * segment.
     */

    segPtr = TkTextIndexToSeg(dstPtr, &segByteOffset);
    if (segByteOffset == count) {
	dstPtr->byteOffset -= segByteOffset;
	return;
    }
    if (segByteOffset > count) {
	dstPtr->byteOffset -= count;
	return;
    }

    /*
     * Substract the rest of the segment.
     */

    count -= segByteOffset;
    dstPtr->byteOffset -= segByteOffset;

    while (count > 0) {
	segPtr = PrevSegment(segPtr, dstPtr);
	if (NULL == segPtr) {
	    return;
	}

	if (count <= segPtr->sizeBytes) {
	    dstPtr->byteOffset -= count;
	    return;
	}

	count -= segPtr->sizeBytes;
	dstPtr->byteOffset -= segPtr->sizeBytes;
    }
}

static TkTextSegment *
PrevSegment(TkTextSegment *segPtr, TkTextIndex *indexPtr)
{
    /*
     * Are we at the beginning of the line?  Than try to go up.
     */
    if (indexPtr->byteOffset == 0) {
	int lineIndex = TkBTreeLineIndex(indexPtr->linePtr);
	if (lineIndex == 0) {
	    return NULL;
	}
	indexPtr->linePtr = TkBTreeFindLine(indexPtr->tree, lineIndex-1);
	/*
	 * This finds the last segPtr in the line in the loop below.
	 */
	segPtr = NULL;
    }

    TkTextSegment *lastSegPtr = NULL;
    TkTextSegment *iterSegPtr;
    for (iterSegPtr = indexPtr->linePtr->segPtr; iterSegPtr != NULL;
	 iterSegPtr = iterSegPtr->nextPtr) {
	if (iterSegPtr->sizeBytes > 0) {
	    lastSegPtr = iterSegPtr;
	    /*
	     * If we went back one line above, calculate the byteOffset for
	     * that line along the way.
	     */
	    if (segPtr == NULL) {
		indexPtr->byteOffset += iterSegPtr->sizeBytes;
	    }
	}
	if (segPtr == iterSegPtr->nextPtr) {
	    return lastSegPtr;
	}
    }

    /*
     * This can't happen, segPtr must be NULL or in linePtr's list.
     */
    return NULL;
}

/*
 *----------------------------------------------------------------------
 *
 * StartEnd --
 *
 *	This procedure handles modifiers like "wordstart" and "lineend"
 *	to adjust indices forwards or backwards.
 *
 * Results:
 *	If the modifier is successfully parsed then the return value
 *	is the address of the first character after the modifier, and
 *	*indexPtr is updated to reflect the modifier. If there is a
 *	syntax error in the modifier then NULL is returned.
 *
 * Side effects:
 *	None.
 *
 *----------------------------------------------------------------------
 */

static char *
StartEnd(
    char *string,		/* String to parse for additional info
				 * about modifier (count and units). 
				 * Points to first character of modifer
				 * word. */
    TkTextIndex *indexPtr)	/* Index to modify based on string. */
{
    char *p;
    size_t length;
    register TkTextSegment *segPtr;

    /*
     * Find the end of the modifier word.
     */

    for (p = string; CtkIsAlnum(*p); p++) {
	/* Empty loop body. */
    }
    length = p-string;
    if ((*string == 'l') && (strncmp(string, "lineend", length) == 0)
	    && (length >= 5)) {
	indexPtr->byteOffset = 0;
	for (segPtr = indexPtr->linePtr->segPtr; segPtr != NULL;
		segPtr = segPtr->nextPtr) {
	    indexPtr->byteOffset += segPtr->sizeBytes;
	}

	/*
	 * We know '\n' is encoded with a single byte index.
	 */

	indexPtr->byteOffset -= 1;
    } else if ((*string == 'l') && (strncmp(string, "linestart", length) == 0)
	    && (length >= 5)) {
	indexPtr->byteOffset = 0;
    } else if ((*string == 'w') && (strncmp(string, "wordend", length) == 0)
	    && (length >= 5)) {
	int firstChar = 1;
	int offset;

	/*
	 * If the current character isn't part of a word then just move
	 * forward one character.  Otherwise move forward until finding
	 * a character that isn't part of a word and stop there.
	 */

	segPtr = TkTextIndexToSeg(indexPtr, &offset);
	while (1) {
	    int chSize = 1;

	    if (segPtr->typePtr == &tkTextCharType) {

		Tcl_UniChar ch;
		chSize = Ctk_UtfToCodepoint(segPtr->body.chars + offset, &ch);
		// FIXME: Check if this handles modifieres and ZWJ.
		if (!Tcl_UniCharIsWordChar(ch)) {
		    break;
		}
		firstChar = 0;
	    }
	    offset += chSize;
	    indexPtr->byteOffset += chSize;
	    if (offset >= segPtr->sizeBytes) {
		segPtr = TkTextIndexToSeg(indexPtr, &offset);
	    }
	}
	if (firstChar) {
	    TkTextIndexForwChars(indexPtr, 1, indexPtr);
	}
    } else if ((*string == 'w') && (strncmp(string, "wordstart", length) == 0)
	    && (length >= 5)) {
	int firstChar = 1;
	int offset;

	/*
	 * Starting with the current character, look for one that's not
	 * part of a word and keep moving backward until you find
	 * one. Then if the character found wasn't the first one, move
	 * forward again one position.
	 */

	segPtr = TkTextIndexToSeg(indexPtr, &offset);
	while (1) {
	    int chSize = 1;

	    if (segPtr->typePtr == &tkTextCharType) {

		Tcl_UniChar ch;
		Ctk_UtfToCodepoint(segPtr->body.chars + offset, &ch);
		// FIXME: Check if this handles modifieres and ZWJ.
		if (!Tcl_UniCharIsWordChar(ch)) {
		    break;
		}
		if (offset + 1 > 1) {
		    chSize = CtkClusterPrevN(segPtr->body.chars, offset, 1);
		}
		firstChar = 0;
	    }
            if (offset == 0) {
		TkTextIndexBackChars(indexPtr, 1, indexPtr);
            } else {
                indexPtr->byteOffset -= chSize;
            }
            offset -= chSize;
	    if (offset < 0) {
		if (indexPtr->byteOffset == 0) {
		    goto done;
		}
		segPtr = TkTextIndexToSeg(indexPtr, &offset);
	    }
	}
	if (!firstChar) {
	    TkTextIndexForwChars(indexPtr, 1, indexPtr);
	}
    } else {
	return NULL;
    }
    done:
    return p;
}
