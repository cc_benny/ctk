# ------------------------------------------------------------------------
#   command-loop.tcl
#
#   The standard command loop as documented in the man page for tclsh,
#   re-implemented as a file event handler in pure Tcl.
# ------------------------------------------------------------------------

namespace eval ctkCommandLoop {
    # Defaults for the constants that each instance uses.  See the
    # `start' proc on the meanings of these parameters and how to
    # override them.
    variable defaultContext {
	stopHandler ""
	exitCmd "\\q"
	prefix ""
	prompt1 "% "
	prompt2 "> "
    }
    # Running context for each instance, indexed by the input file
    # channel `ifs'.
    variable commands; array set commands {}
}

# ctkCommandLoop::start ifs ofs ... - Start a file event handler on
# IFS to execute commands and output the results on OFS.  IFS defaults
# to stdin and OFS defaults to stdout.  Optional parameters in
# key-value form:
#
# * stopHandler - What to do when the command loop is stopped somehow.
#   This happens when IFS is EOF or when the `exitCmd' was issued.
#   E.g. the handler can destroy a window or set a vwait variable.
#   The default does nothing.
#
# * exitCmd - A string that will be intercepted to stop the command
#   loop.  The default is "\q".
#
# * prefix - A string to prepend to prompts and to the output.  This
#   is used during testing to easily discriminate this code from the
#   regular command loop.  The default is "".
#
# * prompt1 - The primary prompt.  This is only used when the global
#   variable `tcl_prompt1' is not set.  The default is "% ".
#
# * prompt2 - The secondary prompt.  This is only used when the global
#   variable `tcl_prompt2' is not set.  The default is "> ".
proc ctkCommandLoop::start {{ifs stdin} {ofs stdout} args} {
    variable defaultContext
    set context [dict replace $defaultContext {*}$args]
    lappend context ifs $ifs ofs $ofs

    lappend context prevHandler [fileevent $ifs readable]
    fileevent $ifs readable [namespace code [list handler $context]]
    prompt1 $context
    return $context
}

# ctkCommandLoop::handler context - This is called whenever there is
# input on IFS.  When the input is a complete command, it is evaluated
# and the result is output on OFS.
proc ctkCommandLoop::handler {context} {
    dict with context {
	if {[gets $ifs line] < 0} {
	    stop $context
	    close $ifs
	    return
	}

	variable commands
	append commands($ifs) $line "\n"

	if {"$exitCmd\n" eq $commands($ifs)} {
	    stop $context
	    return
	}

	if {[info complete $commands($ifs)]} {
	    if {"\n" ne $commands($ifs)} {

		global tcl_interactive
		set interactive $tcl_interactive
		set tcl_interactive 1
		set script [info script]
		info script ""

		catch {uplevel #0 $commands($ifs)} result

		set tcl_interactive $interactive
		info script $script

		if {"" ne $result} {
		    puts $ofs "$prefix$result"
		}
	    }

	    catch {unset commands($ifs)}

	    prompt1 $context
	} else {
	    prompt2 $context
	}
    }
}

# ctkCommandLoop::stop context - Disable the command loop, cleanup and
# restore the previous behaviour.
proc ctkCommandLoop::stop {context} {
    dict with context {
	variable commands
	catch {unset commands($ifs)}
	fileevent $ifs readable $prevHandler
	uplevel #0 $stopHandler
    }
}

# ctkCommandLoop::prompt1 context - Output the primary prompt on OFS.
proc ctkCommandLoop::prompt1 {context} {
    dict with context {
	prompt $context tcl_prompt1 $prompt1
    }
}
# ctkCommandLoop::prompt1 context - Output the secondary prompt on
# OFS.
proc ctkCommandLoop::prompt2 {context} {
    dict with context {
	prompt $context tcl_prompt2 $prompt2
    }
}
# ctkCommandLoop::prompt context promptVar deflt - Output a prompt on
# OFS.  If the global by the name PROMPTVAR is set, evaluate it as a
# scrípt and output the result on OFS.  Otherwise output DEFLT on OFS.
proc ctkCommandLoop::prompt {context promptVar deflt} {
    dict with context {
	upvar #0 $promptVar prompt
	if {[info exists prompt]} {
	    uplevel #0 $prompt
	    # tcl_prompt1 does not get a channel parameter, therefore
	    # most versions will run on stdout, so flush that
	    # separately just in case.
	    flush stdout
	} else {
	    puts -nonewline $ofs $prefix$deflt
	}
	flush $ofs
    }
}

namespace eval ctkCommandLoop {
    namespace export start
}

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
