# ------------------------------------------------------------------------
#   clipboardX11.tcl
#
#   Implement the clipboard command (backend only).  On X11 with xclip
#   available, implement a clipboard based on that.
#
#   This supports copy-and-paste between controls in the same
#   application, and to and from other X11 applications.
#
#   This backend exports the functions isSupported, getClipboard and
#   setClipboard.
# ------------------------------------------------------------------------

namespace eval ctkX11Clipboard {
}

# If this backend will work in the current application setup.  This
# backend is supported, if X11 is available and the tool XCLIP exists.
proc ctkX11Clipboard::isSupported {} {
    expr {[info exists ::env(DISPLAY)]
	  && ! [catch {exec xclip -help 2>/dev/null}]}
}

# Get the data of a given TYPE (e.g. "STRING", "UTF8_STRING" or
# "FILE_NAME") if it exists on the clipboard.  If data of the TYPE is
# not available, return an empty string.
proc ctkX11Clipboard::getClipboard {type} {
    if {[catch {exec xclip -selection clipboard -target $type -out} result]} {
	return ""
    } else {
	return $result
    }
}

# Set the clipboard with the DATA of TYPE (e.g. "STRING",
# "UTF8_STRING" or "FILE_NAME") in FORMAT.  If DATA is the empty
# string, clear the clipboard.  The only supported FORMAT is "STRING".
proc ctkX11Clipboard::setClipboard {type format data} {
    if {"STRING" != "$format"} {
	error "Unsupported format '$format'"
    }
    set cb [open "| [list xclip -selection clipboard -target $type -in \
    	   	          >/dev/null]" w]
    puts -nonewline $cb $data
    # xclip will just continue in the background until another app
    # takes responsibility for the clipboard.
    close $cb
}

namespace eval ctkX11Clipboard {
    namespace export getClipboard setClipboard
}

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
