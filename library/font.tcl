# ------------------------------------------------------------------------
#   font.tcl
#
#   Implement the font command.
# ------------------------------------------------------------------------
#   Directions:
#
#   * Some of the `options' could be settable (-weight bold -underline
#     true).  Than just put the option lists as dict values into
#     `fonts', the C code can read those dicts and use them.
#
#     Note that there are two ways to configure a font, by "font
#     create"/"font configure", and also implicitly, by using a list
#     [list family size styles...] instead of a font name.  In the
#     implicit declaration, we'd ignore "family", check "size" for
#     number and ignore it and translate "styles" into options.
#
#   * "-reverse" could be an additional option used to implement the
#     "selected" font, which is currently hard-coded in the C code.
# ------------------------------------------------------------------------

namespace eval ctkFont {
    # The list of configurable font attributes.  In Ctk these are
    # currently all constants.  The family name "fixed" is the one
    # also used in the C code.
    variable options {
	-family fixed
	-size 1
	-weight normal
	-slant roman
	-underline 0
	-overstrike 0
    }
    # The list of unchangeable font attributes.  In Ctk these are
    # always the same.
    variable metrics {
	-ascent 0
	-descent 0
	-linespace 1
	-fixed 1
    }
    # Helper variable to create automatic font names.
    variable lastfontid 0
    # An array of the fonts created so far.
    variable fonts

    # Error messages: "wrong # args" error messages are generated from
    # this dict.
    variable usages {
	actual "font actual font ?-displayof window? ?-option? ?--? ?char?"
	configure "font configure fontname ?-option value ...?"
	create "font create ?fontname? ?-option value ...?"
	delete "font delete fontname ?fontname ...?"
	families "font families ?-displayof window?"
	measure "font measure font ?-displayof window? text"
	metrics "font metrics font ?-displayof window? ?-option?"
	names "font names"
    }
    # Also register the same error messages for namespaced versions of
    # the subcommands.
    foreach {key value} $usages {
	dict set usages ctkFont::$key $value
	dict set usages ::ctkFont::$key $value
    }
    unset key value
}

# font -- Implement Tk's font command.
proc font {option args} {
    if {{} == [info commands .]} {
	error "can't invoke \"font\" command: application has been destroyed"
    }

    # Implement all subcommands as procs in the `ctkFont' namespace.
    set cmds [info commands ctkFont::$option*]
    if {1 == [llength $cmds]} {
	set cmd {*}$cmds
    } else {
	error "bad option \"$option\": must be\
	    actual, configure, create, delete, families, measure,\
	    metrics, or names"
    }

    if {[catch {$cmd {*}$args} result]} {
	# Fix up the "wrong # args" error.
	if {[string match "wrong # args: should be*" $result]} {
	    ctkFont::WrongNoArgs $cmd
	} else {
	    error $result
	}
    } else {
	return $result
    }
}

# Implement "font actual".
proc ctkFont::actual {font args} {
    variable options
    while {{} ne $args} {
	set args [lassign $args arg]
	switch -glob -- $arg {
	    -displayof {
		# Eat the window parameter and (mostly) ignore it.
		if {{} == $args} {
		    error "value for \"-displayof\" missing"
		}
		set args [lassign $args window]
		if {![winfo exists $window]} {
		    error "bad window path name \"$window\""
		}
	    }
	    -- {
		# This is the end-of-options sign.  The only possible
		# parameter left is the `char', so get and check that.

		if {{} ne $args} {
		    # Eat the char parameter.
		    set args [lassign $args arg]
		    if {[info exists char]} {
			error "Only one char can be specified"
		    } else {
			set char $arg
		    }
		}
	    }
	    -* {
		if {[info exists option]} {
		    error "Only one option can be specified"
		}
		set option $arg
	    }
	    default {
		# The only possible parameter except an option is the
		# `char', so get and check that.
		if {[info exists char]} {
		    WrongNoArgs actual
		} else {
		    set char $arg
		}
	    }
	}
    }
    if {[info exists char] && 1 != [string length $char]} {
	error "expected a single character but got \"$char\""
    }
    if {[info exists option]} {
	if {! [dict exists $options $option]} {
	    error "bad option \"$option\": must be\
		-family, -size, -weight, -slant, -underline, or -overstrike"
	}
	return [dict get $options $option]
    } else {
	return [list {*}$options]
    }
}

# Implement "font configure".
proc ctkFont::configure {font args} {
    variable fonts
    if {! [info exists fonts($font)]} {
	error "named font \"$font\" doesn't exist"
    }

    variable options
    switch [llength $args] {
	0 {
	    # No option given, return them all.
	    return $options
	}
	1 {
	    # One option given, return that.
	    set option {*}$args
	    if {! [dict exists $options $option]} {
		error "bad option \"$option\": must be\
		    -family, -size, -weight, -slant, -underline,\
		    or -overstrike"
	    }
	    return [dict get $options $option]
	}
	default {
	    # Options with values given, check them, but ignore the
	    # values.
	    while {{} ne $args} {
		set args [lassign $args option]
		if {{} == $args} {
		    error "value for \"$option\" option missing"
		}
		set args [lassign $args value]
		if {! [dict exists $options $option]} {
		    error "bad option \"$option\": must be\
			-family, -size, -weight, -slant, -underline,\
			or -overstrike"
		}
	    }
	    return {}
	}
    }
}

# Implement "font create".
proc ctkFont::create {args} {
    variable lastfontid
    variable fonts
    if {{} == $args} {
	set name ""
    } else {
	set args [lassign $args first]
	if {[string match "-*" $first]} {
	    set name ""
	    # No font name given, just options and values.
	    set args [concat [list $first] $args]
	} else {
	    # The first parameter is a font name, the rest are options and
	    # values.
	    set name $first
	    if {[info exists fonts($name)]} {
		error "named font \"$name\" already exists"
	    }
	}
    }

    if {"" == $name} {
	while true {
	    set name [format "font%d" [incr lastfontid]]
	    if {! [info exists fonts($name)]} {
		break
	    }
	}
    }

    set fonts($name) $name

    # Check the options.
    set value [configure $name {*}$args]
    if {1 == [llength $value]} {
	set option [lindex $args end]
	error "value for \"$option\" option missing"
    }

    return $name
}

# Implement "font delete".
proc ctkFont::delete {args} {
    if {{} == $args} {
	WrongNoArgs delete
    }
    variable fonts
    foreach name $args {
	if {! [info exists fonts($name)]} {
	    error "named font \"$name\" doesn't exist"
	}
	# The tests expect font ids to be reused.
	if {1 == [scan $name "font%d" n]} {
	    variable lastfontid
	    if {$n <= $lastfontid} {
		set lastfontid $n
		incr lastfontid -1
	    }
	}
	unset fonts($name)
    }
}

# Implement "font families".
proc ctkFont::families {args} {
    lassign $args option window
    switch [llength $args] {
	0 {}
	1 {
	    if {"-displayof" ne $option} {
		WrongNoArgs families
	    }
	    error "value for \"-displayof\" missing"
	}
	2 {
	    if {"-displayof" ne $option} {
		WrongNoArgs families
	    }
	    if {! [winfo exists $window]} {
		error "bad window path name \"$window\""
	    }
	}
	default {
	    WrongNoArgs families
	}
    }
    return fixed
}

# Implement "font measure".
proc ctkFont::measure {font args} {
    switch [llength $args] {
	3 {
	    # "font measure font -displayof window text"
	    lassign $args option window text
	    if {"-displayof" ne $option} {
		error "Invalid option: $option"
	    }
	}
	1 {
	    # "font measure font text"
	    lassign $args text
	}
	default {
	    WrongNoArgs measure
	}
    }

    # Prepare for the Unicode branch, where we will have
    # `ctk_columncount' to do this right.
    if {! [catch {ctk_columncount $text} result]} {
	return $result
    } else {
	return [string length $text]
    }
}

# Implement "font metrics".
proc ctkFont::metrics {font args} {
    variable metrics
    switch [llength $args] {
	2 - 3 {
	    # "font metrics font -displayof window metric"
	    lassign $args option window metric
	    if {"-displayof" ne $option} {
		WrongNoArgs metrics
	    }
	}
	0 - 1 {
	    # "font metrics font metric" or "font metrics font".  In
	    # the latter case just assign "" to `metric'.
	    lassign $args metric
	}
	default {
	    WrongNoArgs metrics
	}
    }

    if {"-displayof" == $metric} {
	error "value for \"-displayof\" missing"
    } elseif {"" == $metric} {
	# The metric was left out, return them all.
	return [list {*}$metrics]
    } elseif {! [dict exists $metrics $metric]} {
	error "bad metric \"$metric\": must be -ascent, -descent,\
	    -linespace, or -fixed"
    } else {
	return [dict get $metrics $metric]
    }
}

# Implement "font names".
proc ctkFont::names {} {
    variable fonts
    return [array names fonts]
}

# Format a "wrong # args" error message.
proc ctkFont::WrongNoArgs {cmd} {
    variable usages
    set cmd [dict get $usages $cmd]
    error "wrong # args: should be \"$cmd\""
}

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
