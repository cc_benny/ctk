# command.tcl --
#
# This file defines the Ctk command dialog procedure.
#
# Copyright (c) 1995 Cleveland Clinic Foundation
#
# See the file "license.terms" for information on usage and redistribution
# of this file, and for a DISCLAIMER OF ALL WARRANTIES.
#

proc ctk_menu {} {
    if ![winfo exists .ctkMenu] {
	menu .ctkMenu
	.ctkMenu add command -label "Command..." -underline 0 -command ctkDialog
	if {[info exists ctk_display] || [info exists ::env(CTK_DISPLAY)]} {
	    .ctkMenu add command -label "Command (stdio)..." -underline 9 \
		    -command ctkCommandLoop::start
	}
	.ctkMenu add command -label "Next" -underline 0 \
		-command {ctkNextTop [focus]}
	.ctkMenu add command -label "Redraw" -underline 0 \
		-command {ctk redraw .ctkMenu}
	.ctkMenu add command -label "Exit" -underline 1 -command exit
    }
    tk_popup .ctkMenu 0 0 0
}

proc ctkDialog {} {ctkCommand .ctkDlg}

# ctkCommand --
#
proc ctkCommand w {
    if [winfo exists $w] {
    	wm deiconify $w
	raise $w
    } else {
	toplevel $w -class Dialog -width 30 -height 10
	wm title $w Command
	entry $w.entry
	text $w.output -state disabled -takefocus 1
	button $w.close -command "destroy $w" -text Close

	pack $w.entry -side top -fill x
	pack $w.output -side top -fill both -expand 1
	pack $w.close -side bottom

	bind $w.entry <Return> "ctkCommandRun $w; break"
        bind $w.entry <Key-Up> "ctkCommandHistory $w.entry -1"
        bind $w.entry <Key-Down> "ctkCommandHistory $w.entry 1"
	bind $w <Cancel> "destroy $w"
    }
    focus $w.entry
    tkwait window $w
}

proc ctkCommandRun w {
    global errorInfo ctkCommandHistory ctkCommandHistoryCurrent
    set command [$w.entry get]
    if {"" ne $command} {
        lappend ctkCommandHistory $command
        set ctkCommandHistoryCurrent [llength $ctkCommandHistory]
    }
    set code [catch {uplevel #0 $command} result]
    $w.output configure -state normal
    $w.output delete 1.0 end
    $w.output insert 1.0 $result
    if $code  { $w.output insert end "\n----\n$errorInfo" }
    $w.output mark set insert 1.0
    $w.output configure -state disabled
    $w.entry delete 0 end
}

set ctkCommandHistory {}
set ctkCommandHistoryCurrent 0

proc ctkCommandHistory {w offset} {
    global ctkCommandHistory ctkCommandHistoryCurrent

    set new $ctkCommandHistoryCurrent
    incr new $offset
    if {$new >= [llength $ctkCommandHistory] || $new < 0} {
        return
    }

    set command [$w get]
    if {"" ne $command} {
        $w delete 0 [string length $command]
    }

    set ctkCommandHistoryCurrent $new
    $w insert 0 [lindex $ctkCommandHistory $new]
}
