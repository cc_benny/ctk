# dialog.tcl --
#
# This file defines the procedure tk_dialog, which creates a dialog
# box containing a bitmap, a message, and one or more buttons.
#
# Copyright (c) 1992-1993 The Regents of the University of California.
# Copyright (c) 1994-1995 Sun Microsystems, Inc.
# Copyright (c) 1995 Cleveland Clinic Foundation
#
# See the file "license.terms" for information on usage and redistribution
# of this file, and for a DISCLAIMER OF ALL WARRANTIES.
#

#
# tk_dialog:
#
# This procedure displays a dialog box, waits for a button in the dialog
# to be invoked, then returns the index of the selected button.
#
# Arguments:
# w -		Window to use for dialog top-level.
# title -	Title to display in dialog's decorative frame.
# text -	Message to display in dialog.
# bitmap -	Bitmap to display in dialog (empty string means none).
# default -	Index of button that is to display the default ring
#		(-1 means none).
# args -	One or more strings to display in buttons across the
#		bottom of the dialog box.

proc tk_dialog {w title text bitmap default args} {
    global tk::Priv

    # 1. Create the top-level window and divide it into top
    # and bottom parts.

    catch {destroy $w}
    toplevel $w -class Dialog
    wm title $w $title
    wm transient $w [winfo toplevel [winfo parent $w]]
    frame $w.top
    pack $w.top -side top -fill both
    frame $w.bot
    pack $w.bot -side bottom -fill both

    # 2. Fill the top part with bitmap and message (use the option
    # database for -wraplength so that it can be overridden by
    # the caller).

    option add *Dialog.msg.wrapLength 3i widgetDefault
    label $w.msg -justify left -text $text
    pack $w.msg -in $w.top -side right -expand 1 -fill both -padx 3m -pady 3m

    # 3. Create a row of buttons at the bottom of the dialog.

    set i 0
    foreach but $args {
	if {$i == $default} {
	    button $w.button$i -text \[$but\] \
		-command "set tk::Priv(button) $i; destroy $w"
	    bind $w <Execute> \
		"$w.default flash; set tk::Priv(button) $i; destroy $w"
	} else {
	    button $w.button$i -text $but \
		-command "set tk::Priv(button) $i; destroy $w"
	}
	pack $w.button$i -in $w.bot -side left -expand 1 -padx 3m -pady 2m
	incr i
    }

    # 4. No need to center window, Ctk defaults to centering.

    # 5. Set a grab and claim the focus too.

    set oldFocus [focus]
    set oldGrab [grab current $w]
    grab $w
    tkwait visibility $w
    if {$default >= 0} {
	focus $w.button$default
    } else {
	focus $w
    }

    # 6. Wait for the user to respond, then restore the focus and
    # return the index of the selected button.  Restore the focus
    # befire deleting the window, since otherwise the window manager
    # may take the focus away so we can't redirect it.  Finally,
    # restore any grab that was in effect.

    set Priv(button) -1

    tkwait window $w

    catch {focus $oldFocus}
    catch {grab $oldGrab}

    return $Priv(button)
}
