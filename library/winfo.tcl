# winfo.tcl --
#
# Parts of the winfo command are implemented here in Tcl.
#
# Copyright (c) 2020 Benjamin Riefenstahl
#
# See the file "license.terms" for information on usage and redistribution
# of this file, and for a DISCLAIMER OF ALL WARRANTIES.

# Subcommands not implemented:
# winfo containing
# winfo interps
# winfo pointerx

namespace eval ctkWinfo {
    # winfo atom/atomname: The registered atoms.  Atoms are just
    # integers assigned to strings, such that for each string that is
    # passed to [winfo atom string], the same integer is returned each
    # time until the end of the process.  Atoms start at 1 and the
    # selection types are pre-registered.
    variable atoms {"" PRIMARY SECONDARY}

    # winfo id/pathname: Registered window ids.  TODO: Remove an id
    # when the window is destroyed.
    variable ids {}

    # Error messages: "wrong # args" error messages are generated from
    # this dict.
    variable usages {
	atom "winfo atom ?-displayof window? name"
	atomname "winfo atomname ?-displayof window? id"
	id "winfo id window"
	pathname "winfo pathname ?-displayof window? id"
	visual "winfo visual window"
	visualid "winfo visualid window"
	visualsavailable "winfo visualsavailable window ?includeids?"
    }
    # Also register the same error messages for namespaced versions of
    # the subcommands.
    foreach {key value} $usages {
	dict set usages ctkWinfo::_$key $value
	dict set usages ::ctkWinfo::_$key $value
    }
    unset key value
}

if {{} != [info commands ctkWinfo::winfo]} {
    puts stderr "[info script]: already loaded, trying to re-initialize"
} else {
    # Use the same name as the original, but inside the namespace, so
    # that the binary command can use argv[0] in error messages and
    # get the same effect as if it was still ::winfo.
    rename winfo ctkWinfo::winfo
}

# A front-end for Tk's winfo to add more commands here in Tcl to the C
# code.
proc winfo {option args} {
    switch -- $option {
	"atom" - "visual" {
	    # These two are substrings of other commands, so the
	    # string match below is ambiguous.
	    set cmd ctkWinfo::_$option
	}
	default {
	    set cmds [info commands ctkWinfo::_$option*]
	    if {1 == [llength $cmds]} {
		set cmd {*}$cmds
	    } else {
		return [namespace eval ctkWinfo [list winfo $option {*}$args]]
	    }
	}
    }
    if {[catch {$cmd {*}$args} result]} {
	# Fix up the "wrong # args" error.
	if {[string match "wrong # args: should be*" $result]} {
	    ctkWinfo::WrongArgs $cmd
	} else {
	    error $result
	}
    } else {
	return $result
    }
}

# Implement "winfo atom".
proc ctkWinfo::_atom {args} {
    ParseDisplay atom $args window name
    variable atoms
    set atom [lsearch -exact $atoms $name]
    if {$atom < 0} {
	set atom [llength $atoms]
	lappend atoms $name
    }
    return $atom
}

# Implement "winfo atomname".
proc ctkWinfo::_atomname {args} {
    ParseDisplay atomname $args window atom
    if {![string is integer -strict $atom]} {
	error "expected integer but got \"$atom\""
    }
    variable atoms
    if {[llength $atoms] <= $atom} {
	error "no atom exists with id \"$atom\""
    }
    return [lindex $atoms $atom]
}

# Implement "winfo id".
proc ctkWinfo::_id {window} {
    if {![winfo exists $window]} {
	error "bad window path name \"$window\""
    }
    variable ids
    set id [lsearch -exact $ids $window]
    if {$id < 0} {
	set id [llength $ids]
	lappend ids $window
    }
    return $id
}

# Implement "winfo pathname".
proc ctkWinfo::_pathname {args} {
    ParseDisplay pathname $args window id
    if {![string is integer -strict $id]} {
	error "expected integer but got \"$id\""
    }
    variable ids
    if {[llength $ids] <= $id} {
	error "window id \"$id\" doesn't exist in this application"
    }
    return [lindex $ids $id]
}

# Implement "winfo visual".
proc ctkWinfo::_visual {window} {
    if {![winfo exists $window]} {
	error "bad window path name \"$window\""
    }
    return staticgray
}

# Implement "winfo visualid".
proc ctkWinfo::_visualid {window} {
    if {![winfo exists $window]} {
	error "bad window path name \"$window\""
    }
    return 42
}

# Implement "winfo visualsavailable".
proc ctkWinfo::_visualsavailable {window args} {
    switch [llength $args] {
	0 {}
	1 {
	    set option {*}$args
	    if {"includeids" ne $option} {
		set cmd "winfo visualsavailable window ?includeids?"
		error "wrong # args: should be \"$cmd\""
	    }
	}
	default {
	    set cmd "winfo visualsavailable window ?includeids?"
	    error "wrong # args: should be \"$cmd\""
	}
    }
    if {![winfo exists $window]} {
	error "bad window path name \"$window\""
    }
    set visual {staticgray 1}
    if {{} != $args} {
	lappend visual 42
    }
    return [list $visual]
}

# Parse the "-displayof" option out of ARGV, put the window id from
# that option into WINDOWVAR and the remaining parameter into
# PARAMVAR.  Use CMD when throwing errors.  This assumes that ARGV has
# the form "?-displayof windowId? param".
proc ctkWinfo::ParseDisplay {cmd argv windowVar paramVar} {
    switch [llength $argv] {
	1 {
	    uplevel 1 set $windowVar "."
	    uplevel 1 set $paramVar {*}$argv
	}
	3 {
	    lassign $argv switch window param
	    if {[string length $switch] < 2
		|| ! [string match $switch* "-displayof"]} {
		WrongArgs $cmd
	    }
	    if {![winfo exists $window]} {
		error "bad window path name \"$window\""
	    }
	    uplevel 1 set $windowVar $window
	    uplevel 1 set $paramVar $param
	}
	default {
	    WrongArgs $cmd
	}
    }
}

# Format a "wrong # args" error message.
proc ctkWinfo::WrongArgs {cmd} {
    variable usages
    set cmd [dict get $usages $cmd]
    error "wrong # args: should be \"$cmd\""
}

#### eof ####
