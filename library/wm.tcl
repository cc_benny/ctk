# wm.tcl --
#
# Partial implementation of the Tk wm and grab commands for Ctk.
#
# Copyright (c) 1995 Cleveland Clinic Foundation
#
# See the file "license.terms" for information on usage and redistribution
# of this file, and for a DISCLAIMER OF ALL WARRANTIES.

namespace eval ctkWm {
    variable attributes {
	-alpha 1.0
	-fullscreen 0
	-topmost 0
    }
    variable prop_client {}
    variable prop_command {}
}

# wm --
#	Cheap simulation of Tk's wm.

proc wm {option window args} {
    global tk::Priv

    if {! [winfo exists $window]} {
	error "bad window path name \"$window\""
    }

    switch -glob -- $option {
	attributes {
	    switch [llength $args] {
		0 {
		    return $ctkWm::attributes
		}
		1 {
		    set key {*}$args
		    if {! [dict exists $ctkWm::attributes $key]} {
			error "bad attribute \"$key\":\
			       must be -alpha, -topmost or -fullscreen"
		    }
		    return [dict get $ctkWm::attributes $key]
		}
		default {
		    while {{} != [set args [lassign $args key value]]} {
			if {! [dict exists $ctkWm::attributes $key]} {
			    error "bad attribute \"$key\":\
				   must be -alpha, -topmost or -fullscreen"
			}
			# Ignore the setting of any values.
		    }
		}
	    }
	}
	client {
	    ctkWm::Property $window client name {*}$args
	}
	command {
	    # Throw an error if value is not a valid list.
	    if {1 == [llength $args]} {
		llength {*}$args
	    }
	    ctkWm::Property $window command value {*}$args
	}
	deiconify {
	    if {$args != ""} {
		error "wrong # args: should be \"wm $option window\""
	    }
	    ctkWm::Place $window
	}
	forget - iconify - withdraw {
	    if {$args != ""} {
		error "wrong # args: should be \"wm $option window\""
	    }
	    place forget $window
	}
	geom* {
	    switch [llength $args] {
		0 {
		    return [winfo geometry $window]
		}
		1 {
		    set geom [string trim {*}$args =]
		    set w {}
		    set h {}
		    set xsign {}
		    set x {}
		    set ysign {}
		    set y {}
		    if {[scan $geom {%d x %d %[+-] %d %[+-] %d} \
			     w h xsign x ysign y] < 2} {
			set w {}
			if {[scan $geom {%[+-] %d %[+-] %d} \
				 xsign x ysign y] == 0} {
			    error "bad geometry specifier \"$args\""
			}
		    }
		    set Priv(wm,$window) [list $w $h $xsign$x $ysign$y]
		    ctkWm::Place $window
		}
		default {
		    error "wrong # args:\
			   should be \"wm $option window ?newGeometry?\""
		}
	    }
	}
	maxsize {
	    set geometry \
		[list [winfo screenwidth $window] [winfo screenheight $window]]
	    if {$args != "" && $args != $geometry} {
		error "unsupported: \"wm $option window $args\""
	    }
	    return $geometry
	}
	minsize {
	    if {$args != "" && $args != "1 1"} {
		error "unsupported: \"wm $option window $args\""
	    }
	    return {1 1}
	}
	overrideredirect {
	    if {$args != "" && $args != "0"} {
		error "unsupported: \"wm $option window $args\""
	    }
	    return 0
	}
	positionfrom {
	    if {$args != "" && $args != "user"} {
		error "unsupported: \"wm $option window $args\""
	    }
	    return user
	}
	title {
	    switch [llength $args] {
		0 { $window cget -title }
		1 { $window configure -title {*}$args }
		default {
		    error "wrong # args:\
			   should be \"wm $option window ?newTitle?\""
		}
	    }
	}
	transient {
	    switch [llength $args] {
		0 {
		    if [info exists Priv(wm-transient,$window)] {
			return $Priv(wm-transient,$window)
		    }
		}
		1 {set Priv(wm-transient,$window) {*}$args}
		default {error {wrong # args}}
	    }
	}
	default { ctk_unsupported "wm $option" }
    }
}

# ctkWm::Property --
#	Get/set a simple property per window.  Used for "wm client"
#	and "wm command".

proc ctkWm::Property {window propName valueName args} {
    upvar #0 ctkWm::prop_$propName prop
    switch [llength $args] {
	0 {
	    if {! [dict exists $prop $window]} {
		return ""
	    } else {
		return [dict get $prop $window]
	    }
	}
	1 {
	    set value {*}$args
	    if {"" == $value} {
		dict unset prop $window
	    } {
		dict set prop $window $value
		trace add command $window delete \
		    [list ctkWm::Property_delete $propName]
	    }
	    return ""
	}
	default {
	    error "wrong # args: should be \"wm $propName window ?$valueName?\""
	}
    }
}

# ctkWm::Property_delete --
#	Trace callback to clear a property when the window goes away.

proc ctkWm::Property_delete {propName window args} {
    Property $window $propName "" ""
}

# ctkWm::Place --
#	Place toplevel window `w' according to window manager settings.

proc ctkWm::Place w {
    global tk::Priv

    if [info exists Priv(wm,$w)] {
	set width [lindex $Priv(wm,$w) 0]
	set height [lindex $Priv(wm,$w) 1]
	set x [lindex $Priv(wm,$w) 2]
	set y [lindex $Priv(wm,$w) 3]
	set placeArgs [list -width $width -height $height]
	switch -glob -- $x {
	    "" { }
	    -* {lappend placeArgs -x [expr [winfo screenwidth $w]+$x] -relx 0}
	    default {lappend placeArgs -x [expr $x] -relx 0}
	}
	switch -glob -- $y {
	    "" { }
	    -* {lappend placeArgs -y [expr [winfo screenheight $w]+$y] -rely 0}
	    default {lappend placeArgs -y [expr $y] -rely 0}
	}
    } else {
	set placeArgs {-relx 0.5 -rely 0.5 -width {} -height {} -anchor center}
    }
    eval place $w $placeArgs
}

# grab --
#	Cheap simulation of Tk's grab.  Currently - a grab has
#	no effect on Ctk - this will change if I add a real
#	window manager.

proc grab {option args} {
    global tk::Priv

    if {! [info exists Priv(grab)]} {
	set Priv(grab) {}
    }
    switch -exact -- $option {
	current { return $Priv(grab) }
	release {
	    if {$args == $Priv(grab)} {
		set Priv(grab) {}
	    }
	    return {}
	}
	status {
	    if {$args == $Priv(grab)} {
		return $Priv(grabType)
	    } else {
		return none
	    }
	}
	set     {
	    set option [lindex $args 0]
	    set args [lrange $args 1 end]
	    # Falls through ...
	}
    }
    if {$option == "-global"} {
	set Priv(grab) $args
	set Priv(grabType) global
    } else {
	set Priv(grab) $option
	set Priv(grabType) local
    }
}

# ctkNextTop
#	Pass focus to the next toplevel window after w's toplevel.

proc ctkNextTop w {
    global tk::Priv

    if {$Priv(grab) != ""}  {bell; return}
    set cur [winfo toplevel $w]
    set tops [lsort ". [winfo children .]"]
    set i [lsearch -exact $tops $cur]
    set tops "[lrange $tops [expr $i+1] end] [lrange $tops 0 [expr $i-1]]"
    foreach top $tops {
	if {[winfo toplevel $top] == $top
		&& ![info exists Priv(wm-transient,$top)]} {
	    wm deiconify $top
	    raise $top
	    focus $top
	    return
	}
    }
}
