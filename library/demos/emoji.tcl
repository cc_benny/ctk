#!/usr/bin/tclsh
# ------------------------------------------------------------------------
#   emoji.tcl
#
#   Show some characters of different types and what Tcl makes of
#   them.  Run this in 8.6 and 8.7 for contrast.
# ------------------------------------------------------------------------

foreach {name ch} {
    "ASCII" a
    "Prec." \U000000E4
    "Comb." a\U0000030B
    "CJK  " \U00003730
    "Emoji" \U0001F638
} {
    set codepoints ""
    foreach e [split $ch ""] {
	append codepoints [format " U+%04X" [scan $e %c]]
    }

    puts "$name $ch: [string length $ch] <[string trimleft $codepoints]>"
}

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
