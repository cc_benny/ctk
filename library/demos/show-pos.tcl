#!/usr/bin/tclsh
# ------------------------------------------------------------------------
#   show-pos.tcl
#
#   A little helper to show the current cursor position all the time.
# ------------------------------------------------------------------------

fconfigure $ctkDisplay -buffering none

# get-pos - Get current cursor position from ctkDisplay using the
# VT100/XTerm control sequence "Report Cursor Position" (see
# /usr/share/doc/xterm/ctlseqs.txt.gz in Debian distributions).  NB:
# This may hang if the escape sequence is not supported by the
# terminal.  It will also interfere with regular Curses commands, if
# stuff happens too fast.
proc get-pos {} {
    global ctkDisplay
    puts -nonewline $ctkDisplay \x1B\[6n
    flush $ctkDisplay
    # Read until the character "R".
    set ch ""
    while {"R" != "$ch"} {
	append result [set ch [read $ctkDisplay 1]]
    }
    if {2 != [scan $result \x1B\[%d\;%dR r c]} {
	error "Bad vt100 response: '$result'"
    }
    list $c $r
}

# show-pos - Permanently show the current position as the title of the
# root window.  Also show what Curses thinks at the same time.
proc show-pos {} {
    after 1000 show-pos
    . configure -title "[get-pos] / [curses cursor]"
}

# Maximize the root window.
. configure -width [winfo screenwidth .] -height [winfo screenheight .]

# Start it once.
show-pos

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
