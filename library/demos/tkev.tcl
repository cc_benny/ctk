#!/usr/bin/wish
# ------------------------------------------------------------------------
#       tkev.tcl
# ------------------------------------------------------------------------

set EVENT_FIELDS {
    %#        "Event serial"
    %t        "Server timestamp"
    %E        "Is synthetic event"

    %W        "Path"
    %R        "Root window"
    %i        "Window"
    0x%S      "Subwindow"

    "%x x %y" "Position"
    "%X x %Y" "Root position"
    "+%w +%h" "Width and Height"

    %f        "Focus was in destination window"
    0x%a      "Above"
    %m        "Mode"
    %o        "Override redirect"
    %p        "Place"
    %B        "Border width"
    %c        "Expose pending event count"

    %b        "Mouse Button"
    %D        "Delta"

    %k        "Keycode"
    "%K %N"   "Keysym"
    "'%A'"    "Character"
    %s        "State"

    %P        "Property key"
    %d        "Detail"
}

set EVENT_TYPES {
    Activate              Destroy         Map
    ButtonPress           Enter           MapRequest
    ButtonRelease         Expose          Motion
    Circulate             FocusIn         MouseWheel
    CirculateRequest      FocusOut        Property
    Colormap              Gravity         Reparent
    Configure             KeyPress        ResizeRequest
    ConfigureRequest      KeyRelease      Unmap
    Create                Leave           Visibility
    Deactivate
}

proc format-output {values} {
    if {[catch {
        foreach {value title} $values {
            if {! [string match {*\?\?*} $value]} {
                if {"Keysym" eq $title} {
                    append value [format " Ox%04X" [lindex $value 1]]
                }
                if {"Keycode" eq $title} {
                    append value [format " 0%03o" [lindex $value 0]]
                }
                if {"State" eq $title} {
                    append value [format-state [lindex $value 0]]
                }
                puts "$title: $value"
            }
        }
        puts stderr "--"
    }]} {
        puts stderr $::errorInfo
    }
}

proc format-state {state} {
    if {! [string is integer -strict $state]} {
	return ""
    }
    set s {}
    foreach {bit name} {0 Shift 1 Lock 2 Control 3 Alt} {
	if {(1 << $bit) & $state} {
	    lappend s $name
	}
    }
    set s [join $s " "]
    return " $s"
}

foreach type $EVENT_TYPES {
    if {[catch { 
        bind . <$type> [list format-output \
                            [concat [list "$type (%T)" "Type"] $EVENT_FIELDS]]
    }]} {
        puts stderr "$type: $::errorInfo"
    }
}

# ------------------------------------------------------------------------
#       eof
# ------------------------------------------------------------------------
