#!/usr/bin/tclsh
# ------------------------------------------------------------------------
#   listbox-test.tcl
#
#   A listbox with rows of characters of different types and different
#   lengths.  <Return> on an entry will show the content with one
#   character underlined, always the same position, as seen from Tcl.
#   When called from tclsh, Ctk is loaded but the display is limited
#   to a smaller region.
#
#   NB: Do not name this "listbox.tcl", that is reserved for the
#   library file for the listbox control.
# ------------------------------------------------------------------------

if {!("winfo" in [info commands winfo])} {
    set env(COLUMNS) 41
    set env(LINES) 16
    set basedir [file dirname [info script]]
    set tk_library [file join $basedir .. library]
    lappend auto_path [file join $basedir ..]
    package require Ctk
}

proc msgbox {title text} {
    set w .msgbox
    toplevel $w
    wm title $w $title

    # Create an entry, so we can run around in it with the cursor.
    global $w.entry
    set $w.entry $text
    entry $w.entry -textvariable $w.entry

    # Create a label, where we can add an underline at a specific
    # position.
    label $w.text -justify left -underline 8 -borderwidth 0 \
	-text "\n$text ([string length $text])\n\n>>> Press 'q'"

    # Construct display.
    pack $w.entry -expand 1 -fill x
    pack $w.text  -anchor w

    # Keyboard handlers.
    bind $w <q> {destroy %W; break}
    bind $w <Escape> {destroy %W; break}
    bind $w.text <Configure> {
	%W configure -wraplength [winfo width %W]
    }
    bind $w.entry <q> {destroy [winfo parent %W]; break}
    bind $w.entry <Control-q> {destroy [winfo parent %W]; break}
    bind $w.entry <Escape> {destroy [winfo parent %W]; break}
    bind $w.entry <c> {ctk_menu; break}
    bind $w.entry <Control-c> {ctk_menu; break}

    focus $w.entry
    tkwait window $w
    unset $w.entry
}

bind . <Control-l> {ctk redraw .}
bind . <Control-q> {exit 0}
bind . <Control-e> {error "Test error"}

bind . <l> {ctk redraw .}
bind . <q> {exit 0}
bind . <e> {error "Test error"}

bind . <Escape> {exit 0}

#bind . <Key> {puts stderr "<%A>\n<%K> <%k>"}

if {[catch {
    set w .thelist
    # Emoji literals: Emojis only work like this in 8.6.10/.11,
    # because 8-digit UCNs are still not supported.
    set thelist {
	"a" "b" "c" "d"
	"12345678790"
	"12345678790 12345678790 12345678790 12345678790"
	"\u00E4\u00F6\u00FC \u00E4\u00F6\u00FC \u00E4\u00F6\u00FC \
	 \u00E4\u00F6\u00FC \u00E4\u00F6\u00FC \u00E4\u00F6\u00FC \
	 \u00E4\u00F6\u00FC \u00E4\u00F6\u00FC"
	"\u00E4\u00F6\u00FC \u00E4\u00F6\u00FC \u00E4\u00F6\u00FC \
	 \u00E4\u00F6\u00FC \u00E4\u00F6\u00FC \u00E4\u00F6\u00FC \
	 \u00E4\u00F6\u00FC \u00E4\u00F6\u00FC \u00E4\u00F6\u00FC \
	 \u00E4\u00F6\u00FC \u00E4\u00F6\u00FC \u00E4\u00F6\u00FC \
	 \u00E4\u00F6\u00FC \u00E4\u00F6\u00FC"
	"a\u030Bo\u030Bu\u030B a\u030Bo\u030Bu\u030B a\u030Bo\u030Bu\u030B"
	"a\u030Bo\u030Bu\u030B a\u030Bo\u030Bu\u030B a\u030Bo\u030Bu\u030B \
	 a\u030Bo\u030Bu\u030B a\u030Bo\u030Bu\u030B a\u030Bo\u030Bu\u030B \
	 a\u030Bo\u030Bu\u030B a\u030Bo\u030Bu\u030B a\u030Bo\u030Bu\u030B \
	 a\u030Bo\u030Bu\u030B a\u030Bo\u030Bu\u030B a\u030Bo\u030Bu\u030B \
	 a\u030Bo\u030Bu\u030B a\u030Bo\u030Bu\u030B"
	"\u3400\u3401\u3402 \u3403\u3404\u3405"
	"😹😺😻 😼😽😾"
	"👏🏻👐🏻👊🏻 👏🏿👐🏿👊🏿"
    }
    listbox $w \
	-width [winfo screenwidth .] -height [winfo screenheight .] \
	-border 0 -foreground red
    $w insert 0 {*}$thelist
    $w selection set 0
    $w activate 0
    wm title . "Listbox ([pid]) test:a\u030Bo\u030Bu\u030B"
    bind $w <Return> {
	msgbox \
	    "Index: Value" \
	    "[%W index active]: [%W get active]"
	break
    }
    proc entry-dup {lb idx fill} {
	set active [$lb index active]
	set selection [$lb curselection]

	set idx [$lb index $idx]
	set v [$lb get $idx]
	$lb delete $idx
	$lb insert $idx "$v$fill$v"

	$lb activate $active
	foreach idx $selection {
	    $lb selection set $idx
	}
    }
    bind $w <d> {entry-dup %W active " "}
    bind $w <D> {entry-dup %W active ""}
    pack $w
    focus $w
} err]} {
    bgerror $err
}

#bind . <Destroy> {set done 1}
#vwait done
tkwait window .
exit 1
#pack .

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
