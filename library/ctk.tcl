# ctk.tcl --
#
# Initialization script normally executed in the interpreter for each
# Ctk-based application.  Arranges class bindings for widgets.
#
# Copyright (c) 1992-1994 The Regents of the University of California.
# Copyright (c) 1994-1995 Sun Microsystems, Inc.
# Copyright (c) 1995 Cleveland Clinic Foundation
#
# See the file "license.terms" for information on usage and redistribution
# of this file, and for a DISCLAIMER OF ALL WARRANTIES.

namespace eval tk { variable Priv }

# Add Tk's directory to the end of the auto-load search path:

lappend auto_path $tk_library

# Turn off strict Motif look and feel as a default.

set tk_strictMotif 0

# ctk_unsupported --
# This procedure is invoked when a Tk command that is not implemented
# by Ctk is invoked.
#
# Arguments:
# cmd -		command that was invoked

proc ctk_unsupported cmd {
    error "Unsupported Tk command: $cmd"
}

# tkScreenChanged --
# This procedure is invoked by the binding mechanism whenever the
# "current" screen is changing.  The procedure does two things.
# First, it uses "upvar" to make global variable "tk::Priv" point at an
# array variable that holds state for the current display.  Second,
# it initializes the array if it didn't already exist.
#
# Arguments:
# screen -		The name of the new screen.

proc tkScreenChanged screen {
    set disp [file rootname $screen]
    uplevel #0 upvar #0 tk::Priv.$disp tk::Priv
    global tk::Priv
    if [info exists Priv] {
	set Priv(screen) $screen
	return
    }
    set Priv(afterId) {}
    set Priv(buttons) 0
    set Priv(buttonWindow) {}
    set Priv(dragging) 0
    set Priv(focus) {}
    set Priv(grab) {}
    set Priv(grabType) {}
    set Priv(inMenubutton) {}
    set Priv(initMouse) {}
    set Priv(listboxPrev) {}
    set Priv(mouseMoved) 0
    set Priv(oldGrab) {}
    set Priv(popup) {}
    set Priv(postedMb) {}
    set Priv(screen) $screen
    set Priv(selectMode) char
    set Priv(window) {}
}

# Do initial setup for tk::Priv, so that it is always bound to
# something (otherwise, if someone references it, it may get set to a
# non-upvar-ed value, which will cause trouble later).

tkScreenChanged [winfo screen .]

# ----------------------------------------------------------------------
# Read in files that define all of the class bindings.
# ----------------------------------------------------------------------

source $tk_library/button.tcl
source $tk_library/entry.tcl
source $tk_library/listbox.tcl
source $tk_library/menu.tcl
source $tk_library/scrollbar.tcl
source $tk_library/text.tcl

# And another one that would not be loaded automatically.
source $tk_library/winfo.tcl

# ----------------------------------------------------------------------
# Default bindings for keyboard traversal.
# ----------------------------------------------------------------------

bind all <Tab> {focus [tk_focusNext %W]}
bind all <Shift-Tab> {focus [tk_focusPrev %W]}

bind all <Control-c> ctk_menu
bind all <Escape> {ctk_event %W KeyPress -key Cancel}
bind all <KP_Enter> {ctk_event %W KeyPress -key Execute}
bind all <space> {ctk_event %W KeyPress -key Select}

bind all <F1> {ctk_event %W KeyPress -key Help}
bind all <F2> {ctk_event %W KeyPress -key Execute}
bind all <F3> {ctk_event %W KeyPress -key Menu}
bind all <F4> {ctk_event %W KeyPress -key Cancel}
