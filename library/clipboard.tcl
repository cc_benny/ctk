# ------------------------------------------------------------------------
#   clipboard.tcl
#
#   Implement the clipboard command.
# ------------------------------------------------------------------------

namespace eval ctkClipboard {
    # Use the code in clipboardX11.tcl or, as a fallback, in
    # clipboardLocal.tcl as the backend for this code.  Check the
    # respective isSupported function and than import the respective
    # getClipboard and setClipboard functions into our own namespace.
    # ctkLocalClipboard::isSupported will always return "true", but
    # only actually calling it will cause the backend code to be
    # loaded.
    if {[ctkX11Clipboard::isSupported]} {
	namespace import ::ctkX11Clipboard::*
    } elseif {[ctkLocalClipboard::isSupported]} {
	namespace import ::ctkLocalClipboard::*
    }

    # Error messages: "wrong # args" error messages are generated from
    # this dict (WrongNoArgs).
    variable usages {
	append "clipboard append ?-option value ...? data"
	clear "clipboard clear ?-displayof window?"
	get "clipboard get ?-displayof window? ?-type type?"
    }
}

# A standard Tk-compatible "clipboard" command.  NB: This does not
# support undocumented calling specs that the original tests check
# for.
proc clipboard {option args} {
    set cmds [info commands ctkClipboard::_$option*]
    if {1 == [llength $cmds]} {
	set cmd {*}$cmds
    } else {
	error "bad option \"$option\": must be append, clear, or get"
    }

    $cmd {*}$args
}

# Implement "clipboard append".
proc ctkClipboard::_append {args} {
    set window .
    set format STRING
    set type STRING
    set data [ParseOpts $args append \
		  {-displayof window -format format -type type}]
    if {1 != [llength $data]} {
	WrongNoArgs append
    }
    set clip [getClipboard $type]
    append clip {*}$data
    setClipboard $type $format $clip
}

# Implement "clipboard clear".
proc ctkClipboard::_clear {args} {
    set window .
    set data [ParseOpts $args clear {-displayof window}]
    if {{} != $data} {
	WrongNoArgs clear
    }
    setClipboard STRING STRING ""
}

# Implement "clipboard get".
proc ctkClipboard::_get {args} {
    set window .
    set type STRING
    set data [ParseOpts $args get {-displayof window -type type}]
    if {{} != $data} {
	WrongNoArgs clear
    }
    getClipboard $type
}

# Parse ARGV for OPTIONS.  OPTIONS is a dict, pairing the option with
# the variable name to set, if the option is present.  CMD contains
# the command that calls this, this is used for finding for error
# messages the correct command usage string in the namespace variable
# "usages".
proc ctkClipboard::ParseOpts {argv cmd options} {
    while {{} != $argv} {
	set argv [lassign $argv key]
	if {"--" == $key} {
	    return $argv
	}
	# An actual option is a prefix of a key in OPTIONS.  To be
	# distinguishable, an option has to start with "-" and at
	# least one other character (string length).  It has to be a
	# unique match (dict filter => one pair) and there has to be a
	# value ($argv not empty).
	if {[string length $key] >= 2} {
	    set candidates [dict filter $options key $key*]
	    if {[llength $candidates] == 2} {
		if {{} == $argv} {
		    WrongNoArgs $cmd
		}
		set argv [lassign $argv value]
		lassign $candidates key varName

		# As a special bit, "-displayof" needs to specify a
		# valid window path.
		if {"-displayof" == "$key" && ![winfo exists $value]} {
		    error "bad window path name \"$value\""
		}

		upvar 1 $varName option
		set option $value
		continue
	    }
	}
	if {[string match "-*" $key]} {
	    # This is not quite what the tests expects, but it is near
	    # enough.
	    error "bad option \"$key\": must be one of: [dict keys $options]"
	}
	# If we get here, we did not interprete the key, so return the
	# rest, restoring the key into the list.
	if {{} == $argv} {
	    return [list $key]
	} else {
	    return [concat [list $key] $argv]
	}
    }
    # Argv should be {} at this time.
    return $argv
}

# Format a "wrong # args" error message.
proc ctkClipboard::WrongNoArgs {cmd} {
    variable usages
    set cmd [dict get $usages $cmd]
    error "wrong # args: should be \"$cmd\""
}

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
