# ------------------------------------------------------------------------
#   clipboardLocal.tcl
#
#   Implement the clipboard command (backend only).  A version that
#   implements a simple internal clipboard based on a global variable.
#
#   This only supports copy-and-paste between controls in the same
#   application, not beyond.  The user needs to use terminal
#   facilities or files to exchange with other applications.
#
#   This backend exports the functions isSupported, getClipboard and
#   setClipboard.
# ------------------------------------------------------------------------

namespace eval ctkLocalClipboard {
    # The current data in our internal clipboard.  This is an array
    # indexed by the clipboard "type" ("STRING", "UTF8_STRING",
    # "FILE_NAME", etc.).
    variable clipboard; array set clipboard {}
}

# If this backend will work in the current application setup.  This
# backend is always supported.  It should checked last.
proc ctkLocalClipboard::isSupported {} {
    return 1
}

# Get the data of a given TYPE (e.g. "STRING", "UTF8_STRING" or
# "FILE_NAME") if it exists on the clipboard.  If data of the TYPE is
# not available, return an empty string.
proc ctkLocalClipboard::getClipboard {type} {
    variable clipboard
    if {[info exists clipboard($type)]} {
	return $clipboard($type)
    } else {
	return ""
    }
}

# Set the clipboard with the DATA of TYPE (e.g. "STRING",
# "UTF8_STRING" or "FILE_NAME") in FORMAT.  If DATA is the empty
# string, clear the clipboard.  The only supported FORMAT is "STRING".
proc ctkLocalClipboard::setClipboard {type format data} {
    if {"STRING" != "$format"} {
	error "Unsupported format '$format'"
    }
    variable clipboard
    if {"" == "$data"} {
	catch {unset clipboard}
    } else {
	set clipboard($type) $data
    }
}

namespace eval ctkLocalClipboard {
    namespace export getClipboard setClipboard
}

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
