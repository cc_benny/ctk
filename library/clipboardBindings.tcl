# ------------------------------------------------------------------------
#   clipboardBindings.tcl
#
#   Clipboard handling.  Keybindings, a simple internal clipboard
#   implementation and a clipboard implementation based on xclip.
# ------------------------------------------------------------------------

# tkSetupClipboardBindings target is_multiline - Setup the usual
# CUA-style key combinations for cut'n'paste.  Expected is that TARGET
# is either "Entry" or "Text" or a specific instance of those widget
# types.
proc tkSetupClipboardBindings {target is_multiline} {
    bind $target <Shift-Right> {tkCall SelectNextChar %W}
    bind $target <Mod1-f>      {tkCall SelectNextChar %W}
    bind $target <Shift-Left>  {tkCall SelectPrevChar %W}
    bind $target <Mod1-b>      {tkCall SelectPrevChar %W}
    # bind $target <Control-Shift-Right> {tkCall SelectNextWord %W}
    # bind $target <Control-Shift-Left>  {tkCall SelectPrevWord %W}
    if {$is_multiline} {
	bind $target <Shift-Down> {tkCall SelectNextLine %W}
	bind $target <Shift-Up>   {tkCall SelectPrevLine %W}
    }
    bind $target <Shift-Home>  {tkCall SelectLineStart %W}
    bind $target <Shift-End>   {tkCall SelectLineEnd %W}
    bind $target <Control-a>   {tkCall SelectAll %W}

    bind $target <Control-c> {
	catch {
	    clipboard clear
	    clipboard append -type UTF8_STRING -- [tkCall GetSelection %W]
	}
	break
    }
    bind $target <Control-x> {
	catch {
	    clipboard clear
	    clipboard append -type UTF8_STRING -- [tkCall GetSelection %W]
	    %W delete sel.first sel.last
	}
	break
    }
    bind $target <Control-v> {
	catch {tkCall Insert %W [clipboard get -type UTF8_STRING]}
	break
    }
}

# tkCall call w args - Call a utility proc CALL for window W with
# additional ARGS.  Assumes the usual proc name conventions.
proc tkCall {call w args} {
    set class [winfo class $w]
    tk$class$call $w {*}$args
}

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
