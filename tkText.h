/*
 * tkText.h (Ctk.h) --
 *
 *	Declarations shared among the files that implement text
 *	widgets.
 *
 * Copyright (c) 1992-1994 The Regents of the University of California.
 * Copyright (c) 1994-1995 Sun Microsystems, Inc.
 * Copyright (c) 1994-1995 Cleveland Clinic Foundation
 *
 * See the file "license.terms" for information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 */

#ifndef _TKTEXT
#define _TKTEXT

#ifndef _TK
#include "tk.h"
#endif

/*
 * Opaque types for structures whose guts are only needed by a single
 * file:
 */

typedef struct TkTextBTree *TkTextBTree;

/*
 * The data structure below defines a single line of text (from newline
 * to newline, not necessarily what appears on one line of the screen).
 */

typedef struct TkTextLine {
    struct Node *parentPtr;		/* Pointer to parent node containing
					 * line. */
    struct TkTextLine *nextPtr;		/* Next in linked list of lines with
					 * same parent node in B-tree.  NULL
					 * means end of list. */
    struct TkTextSegment *segPtr;	/* First in ordered list of segments
					 * that make up the line. */
} TkTextLine;

/*
 * -----------------------------------------------------------------------
 * Segments: each line is divided into one or more segments, where each
 * segment is one of several things, such as a group of characters, a
 * tag toggle, a mark, or an embedded widget.  Each segment starts with
 * a standard header followed by a body that varies from type to type.
 * -----------------------------------------------------------------------
 */

/*
 * The data structure below defines the body of a segment that represents
 * a tag toggle.  There is one of these structures at both the beginning
 * and end of each tagged range.
 */

typedef struct TkTextToggle {
    struct TkTextTag *tagPtr;		/* Tag that starts or ends here. */
    int inNodeCounts;			/* 1 means this toggle has been
					 * accounted for in node toggle
					 * counts; 0 means it hasn't, yet. */
} TkTextToggle;

/*
 * The data structure below defines line segments that represent
 * marks.  There is one of these for each mark in the text.
 */

typedef struct TkTextMark {
    struct TkText *textPtr;		/* Overall information about text
					 * widget. */
    TkTextLine *linePtr;		/* Line structure that contains the
					 * segment. */
    Tcl_HashEntry *hPtr;		/* Pointer to hash table entry for mark
					 * (in textPtr->markTable). */
} TkTextMark;

/*
 * A structure of the following type holds information for each window
 * embedded in a text widget.  This information is only used by the
 * file tkTextWind.c
 */

typedef struct TkTextEmbWindow {
    struct TkText *textPtr;		/* Information about the overall text
					 * widget. */
    TkTextLine *linePtr;		/* Line structure that contains this
					 * window. */
    Tk_Window tkwin;			/* Window for this segment.  NULL
					 * means that the window hasn't
					 * been created yet. */
    const char *create;			/* Script to create window on-demand.
					 * NULL means no such script.
					 * Malloc-ed. */
    int align;				/* How to align window in vertical
					 * space.  See definitions in
					 * tkTextWind.c. */
    int padX, padY;			/* Padding to leave around each side
					 * of window, in pixels. */
    int stretch;			/* Should window stretch to fill
					 * vertical space of line (except for
					 * pady)?  0 or 1. */
    int chunkCount;			/* Number of display chunks that
					 * refer to this window. */
    int displayed;			/* Non-zero means that the window
					 * has been displayed on the screen
					 * recently. */
} TkTextEmbWindow;

/*
 * The data structure below defines line segments.
 */

typedef struct TkTextSegment {
    const struct Tk_SegType *typePtr;	/* Pointer to record describing
					 * segment's type. */
    struct TkTextSegment *nextPtr;	/* Next in list of segments for this
					 * line, or NULL for end of list. */
    int sizeBytes;			/* Size of this segment (# of bytes
					 * of index space it occupies). */
    int sizeChars;			/* Size of this segment (# of
					 * chars). */
    int sizeClusters;			/* Size of this segment (# of character
					 * clusters). */
    union {
	char chars[4];			/* Characters that make up character
					 * info.  Actual length varies to
					 * hold as many characters as needed.*/
	TkTextToggle toggle;		/* Information about tag toggle. */
	TkTextMark mark;		/* Information about mark. */
	TkTextEmbWindow ew;		/* Information about embedded
					 * window. */
    } body;
} TkTextSegment;

/*
 * Data structures of the type defined below are used during the
 * execution of Tcl commands to keep track of various interesting
 * places in a text.  An index is only valid up until the next
 * modification to the character structure of the b-tree so they
 * can't be retained across Tcl commands.  However, mods to marks
 * or tags don't invalidate indices.
 */

typedef struct TkTextIndex {
    TkTextBTree tree;			/* Tree containing desired position. */
    TkTextLine *linePtr;		/* Pointer to line containing position
					 * of interest. */
    int byteOffset;			/* Index within line of the first byte
					 * of the desired character (0 means
					 * first one). */
} TkTextIndex;

/*
 * Types for procedure pointers stored in TkTextDispChunk strutures:
 */

typedef struct TkTextDispChunk TkTextDispChunk;

typedef void 		Tk_ChunkDisplayProc (
			    TkTextDispChunk *chunkPtr, int x, int y,
			    Tk_Window win);
typedef void		Tk_ChunkUndisplayProc (struct TkText *textPtr,
			    TkTextDispChunk *chunkPtr);
typedef int		Tk_ChunkMeasureProc (TkTextDispChunk *chunkPtr, int x);
typedef void		Tk_ChunkBboxProc (
			    TkTextDispChunk *chunkPtr, int index, int y,
			    int *xPtr, int *yPtr,
			    int *widthPtr, int *heightPtr);

/*
 * The structure below represents a chunk of stuff that is displayed
 * together on the screen.  This structure is allocated and freed by
 * generic display code but most of its fields are filled in by
 * segment-type-specific code.
 */

struct TkTextDispChunk {
    /*
     * The fields below are set by the type-independent code before
     * calling the segment-type-specific layoutProc.  They should not
     * be modified by segment-type-specific code.
     */

    int x;				/* X position of chunk, in pixels.
					 * This position is measured from the
					 * left edge of the logical line,
					 * not from the left edge of the
					 * window (i.e. it doesn't change
					 * under horizontal scrolling). */
    struct TkTextDispChunk *nextPtr;	/* Next chunk in the display line
					 * or NULL for the end of the list. */
    struct Style *stylePtr;		/* Display information, known only
					 * to tkTextDisp.c. */

    /*
     * The fields below are set by the layoutProc that creates the
     * chunk.
     */

    Tk_ChunkDisplayProc *displayProc;	/* Procedure to invoke to draw this
					 * chunk on the display or an
					 * off-screen pixmap. */
    Tk_ChunkUndisplayProc *undisplayProc;
					/* Procedure to invoke when segment
					 * ceases to be displayed on screen
					 * anymore. */
    Tk_ChunkMeasureProc *measureProc;	/* Procedure to find byte offset under
					 * a given x-location. */
    Tk_ChunkBboxProc *bboxProc;		/* Procedure to find bounding box
					 * of character in chunk. */
    int numBytes;			/* Number of characters that will be
					 * displayed in the chunk. */
    int minHeight;			/* Minimum total line height needed
					 * by this chunk. */
    int width;				/* Width of this chunk, in pixels.
					 * Initially set by chunk-specific
					 * code, but may be increased to
					 * include tab or extra space at end
					 * of line. */
    int breakIndex;			/* Index within chunk of last
					 * acceptable position for a line
					 * (break just before this character).
					 * <= 0 means don't break during or
					 * immediately after this chunk. */
    ClientData clientData;		/* Additional information for use
					 * of displayProc and undisplayProc. */
};

/*
 * One data structure of the following type is used for each tag in a
 * text widget.  These structures are kept in textPtr->tagTable and
 * referred to in other structures.
 */

typedef struct TkTextTag {
    const char *name;		/* Name of this tag.  This field is actually
				 * a pointer to the key from the entry in
				 * textPtr->tagTable, so it needn't be freed
				 * explicitly. */
    int priority;		/* Priority of this tag within widget.  0
				 * means lowest priority.  Exactly one tag
				 * has each integer value between 0 and
				 * numTags-1. */

    /*
     * Information for displaying text with this tag.  The information
     * belows acts as an override on information specified by lower-priority
     * tags.  If no value is specified, then the next-lower-priority tag
     * on the text determins the value.  The text widget itself provides
     * defaults if no tag specifies an override.
     */

    const char *justifyString;	/* -justify option string (malloc-ed).
				 * NULL means option not specified. */
    Tk_Justify justify;		/* How to justify text: TK_JUSTIFY_LEFT,
				 * TK_JUSTIFY_RIGHT, or TK_JUSTIFY_CENTER.
				 * Only valid if justifyString is non-NULL. */
    const char *lMargin1String;	/* -lmargin1 option string (malloc-ed).
				 * NULL means option not specified. */
    int lMargin1;		/* Left margin for first display line of
				 * each text line, in pixels.  Only valid
				 * if lMargin1String is non-NULL. */
    const char *lMargin2String;	/* -lmargin2 option string (malloc-ed).
				 * NULL means option not specified. */
    int lMargin2;		/* Left margin for second and later display
				 * lines of each text line, in pixels.  Only
				 * valid if lMargin2String is non-NULL. */
    const char *offsetString;	/* -offset option string (malloc-ed).
				 * NULL means option not specified. */
    int offset;			/* Vertical offset of text's baseline from
				 * baseline of line.  Used for superscripts
				 * and subscripts.  Only valid if
				 * offsetString is non-NULL. */
    const char *rMarginString;	/* -rmargin option string (malloc-ed).
				 * NULL means option not specified. */
    int rMargin;		/* Right margin for text, in pixels.  Only
				 * valid if rMarginString is non-NULL. */
    const char *spacing1String;	/* -spacing1 option string (malloc-ed).
				 * NULL means option not specified. */
    int spacing1;		/* Extra spacing above first display
				 * line for text line.  Only valid if
				 * spacing1String is non-NULL. */
    const char *spacing2String;	/* -spacing2 option string (malloc-ed).
				 * NULL means option not specified. */
    int spacing2;		/* Extra spacing between display
				 * lines for the same text line.  Only valid
				 * if spacing2String is non-NULL. */
    const char *spacing3String;	/* -spacing2 option string (malloc-ed).
				 * NULL means option not specified. */
    int spacing3;		/* Extra spacing below last display
				 * line for text line.  Only valid if
				 * spacing3String is non-NULL. */
    const char *tabString;	/* -tabs option string (malloc-ed).
				 * NULL means option not specified. */
    struct TkTextTabArray *tabArrayPtr;
				/* Info about tabs for tag (malloc-ed)
				 * or NULL.  Corresponds to tabString. */
    const char *underlineString;/* -underline option string (malloc-ed).
				 * NULL means option not specified. */
    int underline;		/* Non-zero means draw underline underneath
				 * text.  Only valid if underlineString is
				 * non-NULL. */
    int selected;		/* Non-zero means draw selected (reversed). */
    Tk_Uid wrapMode;		/* How to handle wrap-around for this tag.
				 * Must be tkTextCharUid, tkTextNoneUid,
				 * tkTextWordUid, or NULL to use wrapMode
				 * for whole widget. */
    int affectsDisplay;		/* Non-zero means that this tag affects the
				 * way information is displayed on the screen
				 * (so need to redisplay if tag changes). */
} TkTextTag;

#define TK_TAG_AFFECTS_DISPLAY	0x1
#define TK_TAG_UNDERLINE	0x2
#define TK_TAG_JUSTIFY		0x4
#define TK_TAG_OFFSET		0x10

/*
 * The data structure below is used for searching a B-tree for transitions
 * on a single tag (or for all tag transitions).  No code outside of
 * tkTextBTree.c should ever modify any of the fields in these structures,
 * but it's OK to use them for read-only information.
 */

typedef struct TkTextSearch {
    TkTextIndex curIndex;		/* Position of last tag transition
					 * returned by TkBTreeNextTag, or
					 * index of start of segment
					 * containing starting position for
					 * search if TkBTreeNextTag hasn't
					 * been called yet, or same as
					 * stopIndex if search is over. */
    TkTextSegment *segPtr;		/* Actual tag segment returned by last
					 * call to TkBTreeNextTag, or NULL if
					 * TkBTreeNextTag hasn't returned
					 * anything yet. */
    TkTextSegment *nextPtr;		/* Where to resume search in next
					 * call to TkBTreeNextTag. */
    TkTextSegment *lastPtr;		/* Stop search before just before
					 * considering this segment. */
    TkTextTag *tagPtr;			/* Tag to search for (or tag found, if
					 * allTags is non-zero). */
    int linesLeft;			/* Lines left to search (including
					 * curIndex and stopIndex).  When
					 * this becomes <= 0 the search is
					 * over. */
    int allTags;			/* Non-zero means ignore tag check:
					 * search for transitions on all
					 * tags. */
} TkTextSearch;

/*
 * The following data structure describes a single tab stop.
 */

typedef enum {LEFT, RIGHT, CENTER, NUMERIC} TkTextTabAlign;

typedef struct TkTextTab {
    int location;			/* Offset in pixels of this tab stop
					 * from the left margin (lmargin2) of
					 * the text. */
    TkTextTabAlign alignment;		/* Where the tab stop appears relative
					 * to the text. */
} TkTextTab;

typedef struct TkTextTabArray {
    int numTabs;			/* Number of tab stops. */
    TkTextTab tabs[1];			/* Array of tabs.  The actual size
					 * will be numTabs.  THIS FIELD MUST
					 * BE THE LAST IN THE STRUCTURE. */
} TkTextTabArray;

/*
 * A data structure of the following type is kept for each text widget that
 * currently exists for this process:
 */

typedef struct TkText {
    Tk_Window tkwin;		/* Window that embodies the text.  NULL
				 * means that the window has been destroyed
				 * but the data structures haven't yet been
				 * cleaned up.*/
    Tcl_Interp *interp;		/* Interpreter associated with widget.  Used
				 * to delete widget command.  */
    Tcl_Command widgetCmd;	/* Token for text's widget command. */
    TkTextBTree tree;		/* B-tree representation of text and tags for
				 * widget. */
    Tcl_HashTable tagTable;	/* Hash table that maps from tag names to
				 * pointers to TkTextTag structures. */
    int numTags;		/* Number of tags currently defined for
				 * widget;  needed to keep track of
				 * priorities. */
    Tcl_HashTable markTable;	/* Hash table that maps from mark names to
				 * pointers to mark segments. */
    Tcl_HashTable windowTable;	/* Hash table that maps from window names
				 * to pointers to window segments.  If a
				 * window segment doesn't yet have an
				 * associated window, there is no entry for
				 * it here. */
    Tk_Uid state;		/* Normal or disabled.  Text is read-only
				 * when disabled. */

    /*
     * Default information for displaying (may be overridden by tags
     * applied to ranges of characters).
     */

    int borderWidth;		/* Width of 3-D border to draw around entire
				 * widget. */
    int padX, padY;		/* Padding between text and window border. */
    int spacing1;		/* Default extra spacing above first display
				 * line for each text line. */
    int spacing2;		/* Default extra spacing between display lines
				 * for the same text line. */
    int spacing3;		/* Default extra spacing below last display
				 * line for each text line. */
    const char *tabOptionString;/* Value of -tabs option string (malloc'ed). */
    TkTextTabArray *tabArrayPtr;
				/* Information about tab stops (malloc'ed).
				 * NULL means perform default tabbing
				 * behavior. */

    /*
     * Additional information used for displaying:
     */

    Tk_Uid wrapMode;		/* How to handle wrap-around.  Must be
				 * tkTextCharUid, tkTextNoneUid, or
				 * tkTextWordUid. */
    int width, height;		/* Desired dimensions for window, measured
				 * in characters. */
    int prevWidth, prevHeight;	/* Last known dimensions of window;  used to
				 * detect changes in size. */
    TkTextIndex topIndex;	/* Identifies first character in top display
				 * line of window. */
    struct DInfo *dInfoPtr;	/* Information maintained by tkTextDisp.c. */

    /*
     * Information related to selection.
     */

    TkTextTag *selTagPtr;	/* Pointer to "sel" tag.  Used to tell when
				 * a new selection has been made. */
    TkTextIndex selIndex;	/* Used during multi-pass selection retrievals.
				 * This index identifies the next character
				 * to be returned from the selection. */

    /*
     * Information related to insertion cursor:
     */

    TkTextSegment *insertMarkPtr;
				/* Points to segment for "insert" mark. */

    /*
     * Miscellaneous additional information:
     */

    const char *takeFocus;	/* Value of -takeFocus option;  not used in
				 * the C code, but used by keyboard traversal
				 * scripts.  Malloc'ed, but may be NULL. */
    const char *xScrollCmd;	/* Prefix of command to issue to update
				 * horizontal scrollbar when view changes. */
    const char *yScrollCmd;	/* Prefix of command to issue to update
				 * vertical scrollbar when view changes. */
    int flags;			/* Miscellaneous flags;  see below for
				 * definitions. */
} TkText;

/*
 * Flag values for TkText records:
 *
 * GOT_FOCUS:			Non-zero means this window has the input
 *				focus.
 * UPDATE_SCROLLBARS:		Non-zero means scrollbar(s) should be updated
 *				during next redisplay operation.
 */

#define GOT_FOCUS		4
#define UPDATE_SCROLLBARS	0x10
#define NEED_REPICK		0x20

/*
 * Records of the following type define segment types in terms of
 * a collection of procedures that may be called to manipulate
 * segments of that type.
 */

typedef TkTextSegment *	Tk_SegSplitProc (struct TkTextSegment *segPtr, int index);
typedef int		Tk_SegDeleteProc (struct TkTextSegment *segPtr,
			    TkTextLine *linePtr, int treeGone);
typedef TkTextSegment *	Tk_SegCleanupProc (
			    struct TkTextSegment *segPtr, TkTextLine *linePtr);
typedef void		Tk_SegLineChangeProc (
			    struct TkTextSegment *segPtr, TkTextLine *linePtr);
typedef int		Tk_SegLayoutProc (struct TkText *textPtr,
			    struct TkTextIndex *indexPtr, TkTextSegment *segPtr,
			    int offset, int maxX, int maxBytes,
			    int noBytesYet, Tk_Uid wrapMode,
			    struct TkTextDispChunk *chunkPtr);
typedef void		Tk_SegCheckProc (TkTextSegment *segPtr,
			    TkTextLine *linePtr);

typedef struct Tk_SegType {
    const char *name;			/* Name of this kind of segment. */
    int leftGravity;			/* If a segment has zero size (e.g. a
					 * mark or tag toggle), does it
					 * attach to character to its left
					 * or right?  1 means left, 0 means
					 * right. */
    Tk_SegSplitProc *splitProc;		/* Procedure to split large segment
					 * into two smaller ones. */
    Tk_SegDeleteProc *deleteProc;	/* Procedure to call to delete
					 * segment. */
    Tk_SegCleanupProc *cleanupProc;	/* After any change to a line, this
					 * procedure is invoked for all
					 * segments left in the line to
					 * perform any cleanup they wish
					 * (e.g. joining neighboring
					 * segments). */
    Tk_SegLineChangeProc *lineChangeProc;
					/* Invoked when a segment is about
					 * to be moved from its current line
					 * to an earlier line because of
					 * a deletion.  The linePtr is that
					 * for the segment's old line.
					 * CleanupProc will be invoked after
					 * the deletion is finished. */
    Tk_SegLayoutProc *layoutProc;	/* Returns size information when
					 * figuring out what to display in
					 * window. */
    Tk_SegCheckProc *checkProc;		/* Called during consistency checks
					 * to check internal consistency of
					 * segment. */
} Tk_SegType;

/*
 * The constant below is used to specify a line when what is really
 * wanted is the entire text.  For now, just use a very big number.
 */

#define TK_END_OF_TEXT 1000000

/*
 * The following definition specifies the maximum number of characters
 * needed in a string to hold a position specifier.
 */

#define TK_POS_CHARS 30

/*
 * Declarations for variables shared among the text-related files:
 */

extern int		tkBTreeDebug;
extern int		tkTextDebug;
extern const Tk_SegType	tkTextCharType;
extern Tk_Uid		tkTextCharUid;
extern Tk_Uid		tkTextDisabledUid;
extern const Tk_SegType	tkTextLeftMarkType;
extern Tk_Uid		tkTextNoneUid;
extern Tk_Uid 		tkTextNormalUid;
extern const Tk_SegType	tkTextRightMarkType;
extern const Tk_SegType	tkTextToggleOnType;
extern const Tk_SegType	tkTextToggleOffType;
extern Tk_Uid		tkTextWordUid;

/*
 * Declarations for procedures that are used by the text-related files
 * but shouldn't be used anywhere else in Tk (or by Tk clients):
 */

extern int		TkBTreeCharTagged (TkTextIndex *indexPtr,
			    TkTextTag *tagPtr);
extern void		TkBTreeCheck (TkTextBTree tree);
extern int		TkBTreeCharsInLine (TkTextLine *linePtr);
extern TkTextBTree	TkBTreeCreate (void);
extern void		TkBTreeDestroy (TkTextBTree tree);
extern void		TkBTreeDeleteChars (TkTextIndex *index1Ptr,
			    TkTextIndex *index2Ptr);
extern TkTextLine *	TkBTreeFindLine (TkTextBTree tree, int line);
extern TkTextTag **	TkBTreeGetTags (TkTextIndex *indexPtr, int *numTagsPtr);
extern void		TkBTreeInsertChars (TkTextIndex *indexPtr,
			    const char *string);
extern int		TkBTreeLineIndex (TkTextLine *linePtr);
extern void		TkBTreeLinkSegment (TkTextSegment *segPtr,
			    TkTextIndex *indexPtr);
extern TkTextLine *	TkBTreeNextLine (TkTextLine *linePtr);
extern int		TkBTreeNextTag (TkTextSearch *searchPtr);
extern int		TkBTreeNumLines (TkTextBTree tree);
extern void		TkBTreeStartSearch (TkTextIndex *index1Ptr,
			    TkTextIndex *index2Ptr, TkTextTag *tagPtr,
			    TkTextSearch *searchPtr);
extern void		TkBTreeTag (TkTextIndex *index1Ptr,
			    TkTextIndex *index2Ptr, TkTextTag *tagPtr,
			    int add);
extern void		TkBTreeUnlinkSegment (TkTextBTree tree,
			    TkTextSegment *segPtr, TkTextLine *linePtr);
extern void		TkTextChanged (TkText *textPtr,
			    TkTextIndex *index1Ptr, TkTextIndex *index2Ptr);
extern int		TkTextCharBbox (TkText *textPtr,
			    TkTextIndex *indexPtr, int *xPtr, int *yPtr,
			    int *widthPtr, int *heightPtr);
extern int		TkTextCharLayoutProc (TkText *textPtr,
			    TkTextIndex *indexPtr, TkTextSegment *segPtr,
			    int offset, int maxX, int maxBytes, int noBreakYet,
			    Tk_Uid wrapMode, TkTextDispChunk *chunkPtr);
extern void		TkTextCreateDInfo (TkText *textPtr);
extern int		TkTextDLineInfo (TkText *textPtr,
			    TkTextIndex *indexPtr, int *xPtr, int *yPtr,
			    int *widthPtr, int *heightPtr, int *basePtr);
extern TkTextTag *	TkTextCreateTag (TkText *textPtr, const char *tagName);
extern void		TkTextFreeDInfo (TkText *textPtr);
extern void		TkTextFreeTag (TkTextTag *tagPtr);
extern int		TkTextGetIndex (Tcl_Interp *interp,
			    TkText *textPtr, const char *string,
			    TkTextIndex *indexPtr);
extern TkTextTabArray *	TkTextGetTabs (Tcl_Interp *interp,
			    Tk_Window tkwin, const char *string);
extern void		TkTextIndexBackBytes (const TkTextIndex *srcPtr,
			    int count, TkTextIndex *dstPtr);
extern void		TkTextIndexBackChars (const TkTextIndex *srcPtr,
			    int count, TkTextIndex *dstPtr);
extern int		TkTextIndexCmp (TkTextIndex *index1Ptr,
			    TkTextIndex *index2Ptr);
extern void		TkTextIndexForwBytes (const TkTextIndex *srcPtr,
			    int count, TkTextIndex *dstPtr);
extern void		TkTextIndexForwChars (const TkTextIndex *srcPtr,
			    int count, TkTextIndex *dstPtr);
extern TkTextSegment *	TkTextIndexToSeg (TkTextIndex *indexPtr, int *offsetPtr);
extern void		TkTextLostSelection (
			    ClientData clientData);
extern TkTextIndex *	TkTextMakeIndex (TkTextBTree tree, int lineIndex,
			    int byteOffset, int charIndex, TkTextIndex *indexPtr);
extern int		TkTextMarkCmd (TkText *textPtr,
			    Tcl_Interp *interp, int argc, const char *argv[]);
extern int		TkTextMarkNameToIndex (TkText *textPtr,
			    const char *name, TkTextIndex *indexPtr);
extern void		TkTextMarkSegToIndex (TkText *textPtr,
			    TkTextSegment *markPtr, TkTextIndex *indexPtr);
extern void		TkTextEventuallyRepick (TkText *textPtr);
extern void		TkTextPickCurrent (TkText *textPtr, XEvent *eventPtr);
extern void		TkTextPixelIndex (TkText *textPtr,
			    int x, int y, TkTextIndex *indexPtr);
extern void		TkTextPrintIndex (TkTextIndex *indexPtr, char *string);
extern void		TkTextRedrawRegion (TkText *textPtr,
			    int x, int y, int width, int height);
extern void		TkTextRedrawTag (TkText *textPtr,
			    TkTextIndex *index1Ptr, TkTextIndex *index2Ptr,
			    TkTextTag *tagPtr, int withTag);
extern void		TkTextRelayoutWindow (TkText *textPtr);
extern int		TkTextScanCmd (TkText *textPtr,
			    Tcl_Interp *interp, int argc, const char *argv[]);
extern int		TkTextSeeCmd (TkText *textPtr,
			    Tcl_Interp *interp, int argc, const char *argv[]);
extern int		TkTextSegToOffset (TkTextSegment *segPtr,
			    TkTextLine *linePtr);
extern TkTextSegment *	TkTextSetMark (TkText *textPtr,
                            const char *name, TkTextIndex *indexPtr);
extern void		TkTextSetYView (TkText *textPtr,
			    TkTextIndex *indexPtr, int pickPlace);
extern int		TkTextTagCmd (TkText *textPtr,
			    Tcl_Interp *interp, int argc, const char *argv[]);
extern int		TkTextWindowCmd (TkText *textPtr,
			    Tcl_Interp *interp, int argc, const char *argv[]);
extern int		TkTextWindowIndex (TkText *textPtr,
			    const char *name, TkTextIndex *indexPtr);
extern int		TkTextXviewCmd (TkText *textPtr,
			    Tcl_Interp *interp, int argc, const char *argv[]);
extern int		TkTextYviewCmd (TkText *textPtr,
			    Tcl_Interp *interp, int argc, const char *argv[]);

#endif /* _TKTEXT */
