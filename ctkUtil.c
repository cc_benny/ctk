/* 
 * ctkUtil.c (Ctk) --
 *
 *	This file contains utility procedures that are used by the
 *	rest of Ctk.
 *
 * See the file "license.terms" for information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 */

#include <string.h>
#include "tkInt.h"


/*
 *----------------------------------------------------------------------
 *
 * CtkStrdup --
 *
 *	Create a copy of a string by combining ckalloc and strcpy.
 *
 * Results:
 *	The return value is a writable copy.  It should by freed with
 *	ckfree.
 *
 * Side effects:
 *	None.
 *
 *----------------------------------------------------------------------
 */

char *
CtkStrdup (
    const char * string)
{
    char * copy = ckalloc(strlen(string)+1);
    strcpy(copy, string);
    return copy;
}

