[//]: # (README.md)

Build Jobs for Sourcehut
====

From the [docs](https://man.sr.ht/builds.sr.ht/#gitsrht): `git.sr.ht`
will automatically submit [...] up to 4 builds on each push
[configured by] `.builds/*.yml` (if more are submitted, 4 manifests
will be randomly chosen).

----
[//]: # (eof)
