;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil
  (indent-tabs-mode . t))
 (c++-mode
  (fill-column . 72)
  (c-basic-offset . 4)
  (c-file-offsets
   (arglist-cont-nonempty . 8)
   (arglist-intro . 8)
   (statement-cont . 8)))
 (c-mode
  (fill-column . 72)
  (c-basic-offset . 4)
  (c-file-offsets
   (arglist-cont-nonempty . 8)
   (arglist-intro . 8)
   (statement-cont . 8)))
 (tcl-mode
  (tcl-indent-level . 4)))


