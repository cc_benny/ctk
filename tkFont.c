/* 
 * tkFont.c (Ctk) --
 *
 *	Ctk does not have fonts, but Tk's utility procedures
 *	for measuring and displaying text are provided.
 *
 * Copyright (c) 1990-1994 The Regents of the University of California.
 * Copyright (c) 1994-1995 Sun Microsystems, Inc.
 * Copyright (c) 1994-1995 Cleveland Clinic Foundation
 *
 * See the file "license.terms" for information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 */

#include "tkPort.h"
#include "tkInt.h"
#include "ctkUnicode.h"

/*
 * Characters used when displaying control sequences.
 */

static char hexChars[] = "0123456789abcdefxtnvr\\";

/*
 * The following table maps some control characters to sequences
 * like '\n' rather than '\x10'.  A zero entry in the table means
 * no such mapping exists, and the table only maps characters
 * less than 0x10.
 */

static char mapChars[] = {
    0, 0, 0, 0, 0, 0, 0,
    'a', 'b', 't', 'n', 'v', 'f', 'r',
    0
};

/*
 * Width of tabs, in characters.
 */

#define TAB_WIDTH	8


/*
 *--------------------------------------------------------------
 *
 * TkMeasureChars --
 *
 *	Measure the number of bytes from a string that will fit in a
 *	given horizontal span.  The measurement is done under the
 *	assumption that TkDisplayChars will be used to actually
 *	display the characters.
 *
 * Results:
 *	The return value is the number of bytes from source that fit
 *	in the span given by startX and maxX.  *nextXPtr is filled in
 *	with the x-coordinate at which the first character that didn't
 *	fit would be drawn, if it were to be drawn.
 *
 * Side effects:
 *	None.
 *
 *--------------------------------------------------------------
 */

int
TkMeasureChars(
    const char *source,		/* Characters to be displayed.  Need not
				 * be NULL-terminated. */
    int maxBytes,		/* Maximum # of bytes to consider from
				 * source. */
    int startX,			/* X-position at which first character
				 * will be drawn. */
    int maxX,			/* Don't consider any character that would
				 * cross this x-position. */
    int tabOrigin,		/* X-location that serves as "origin" for
				 * tab stops. */
    int flags,			/* Various flag bits OR-ed together.
				 * TK_WHOLE_WORDS means stop on a word boundary
				 * (just before a space character) if
				 * possible.  TK_AT_LEAST_ONE means always
				 * return a value of at least one, even
				 * if the character doesn't fit. 
				 * TK_PARTIAL_OK means it's OK to display only
				 * a part of the last character in the line.
				 * TK_NEWLINES_NOT_SPECIAL means that newlines
				 * are treated just like other control chars:
				 * they don't terminate the line.
				 * TK_IGNORE_TABS means give all tabs zero
				 * width. */
    int *nextXPtr)		/* Return x-position of terminating
				 * character here. */
{
    register const char *p;	/* Current character. */
    const char *next, *end;
    register int c;
    const char *term;		/* Pointer to most recent character that
				 * may legally be a terminating character. */
    int termX;			/* X-position just after term. */
    int curX;			/* X-position corresponding to p. */
    int newX;			/* X-position corresponding to p+1. */
    int rem;

    /*
     * Scan the input string one character at a time, until a character
     * is found that crosses maxX.
     */

    newX = curX = startX;
    termX = 0;		/* Not needed, but eliminates compiler warning. */
    term = source;
    end = source + maxBytes;
    c = *source & 0xff;
    for (p = source; c != '\0' && p < end; p = next) {
	Tcl_UniChar codepoint;
	if (!CtkIsAscii(c)) {
	    next = Ctk_UtfToCodepoint(p, &codepoint) + p;
	} else {
	    codepoint = c;
	    next = p + 1;
	}
	if (!CtkIsAscii(c)) {
	    newX += CtkCharWidth(codepoint);
	} else if (CtkIsPrint(c)) {
	    newX++;
	} else if (c == '\t') {
	    if (!(flags & TK_IGNORE_TABS)) {
		newX += TAB_WIDTH;
		rem = (newX - tabOrigin) % TAB_WIDTH;
		if (rem < 0) {
		    rem += TAB_WIDTH;
		}
		newX -= rem;
	    }
	} else {
	    if (c == '\n' && !(flags & TK_NEWLINES_NOT_SPECIAL)) {
		break;
	    }
	    if (c < sizeof(mapChars) && mapChars[c]) {
		newX += 2;
	    } else {
		newX += 4;
	    }
	}

	if (newX > maxX) {
	    break;
	}

	if (next < end) {
	    c = *next & 0xff;
	} else {
	    c = 0;
	}

	if (CtkIsSpace(c) || (c == 0)) {
	    term = next;
	    termX = newX;
	}

	curX = newX;
    }

    /*
     * P points to the first character that doesn't fit in the desired
     * span.  Use the flags to figure out what to return.
     */

    if ((flags & TK_PARTIAL_OK) && (curX < maxX)) {
	curX = newX;
	p += CtkClusterNextN(p, end-p, 1);
    }
    if ((flags & TK_AT_LEAST_ONE) && (term == source) && (p < end)
	     && !CtkIsSpace(*term)) {
	term = p;
	termX = curX;
	if (term == source) {
	    term += CtkClusterNextN(term, end-term, 1);
	    termX = newX;
	}
    } else if ((p >= end) || !(flags & TK_WHOLE_WORDS)) {
	term = p;
	termX = curX;
    }
    *nextXPtr = termX;
    return term-source;
}

/*
 *---------------------------------------------------------------------------
 *
 * TkTextWidth --
 *
 *	A wrapper function for the more complicated interface of
 *	Tk_MeasureChars. Computes how much space the given simple string
 *	needs.
 *
 * Results:
 *	The return value is the width (in pixels) of the given string.
 *
 * Side effects:
 *	None.
 *
 *---------------------------------------------------------------------------
 */

int
TkTextWidth(
    const char *string,		/* String whose width will be computed. */
    int numBytes)		/* Number of bytes to consider from string, or
				 * < 0 for strlen(). */
{
    int width;

    if (numBytes < 0) {
	numBytes = strlen(string);
    }
    if (numBytes == 0) {
        return 0;
    }
    TkMeasureChars(string, numBytes, 0, INT_MAX, 0, 0, &width);
    return width;
}

/*
 *--------------------------------------------------------------
 *
 * TkDisplayChars --
 *
 *	Draw a string of characters on the screen, converting
 *	tabs to the right number of spaces and control characters
 *	to sequences of the form "\xhh" where hh are two hex
 *	digits.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	Information gets drawn on the screen.
 *
 *--------------------------------------------------------------
 */

void
TkDisplayChars(
    Tk_Window win,		/* Window in which to draw. */
    Ctk_Style style,		/* Display characters using this style. */
    const char *string,		/* Characters to be displayed. */
    int numBytes,		/* Number of bytes to display from string. */
    int x, int y,		/* Coordinates at which to draw string. */
    int tabOrigin,		/* X-location that serves as "origin" for
				 * tab stops. */
    int flags)			/* Flags to control display.  Only
				 * TK_NEWLINES_NOT_SPECIAL and TK_IGNORE_TABS
				 * are supported right now.  See
				 * TkMeasureChars for information about it. */
{
    register const char *p;	/* Current character being scanned. */
    register int c;
    const char *start;		/* First byte waiting to be displayed. */
    const char *next;		/* Start of next character.  */
    const char *end;		/* Byte after the last to be displayed. */
    int startX;			/* X-coordinate corresponding to start. */
    int curX;			/* X-coordinate corresponding to p. */
    char replace[10];
    int rem;

    /*
     * Scan the string one character at a time.  Display control
     * characters immediately, but delay displaying normal characters
     * in order to pass many characters to the server all together.
     */

    startX = curX = x;
    start = next = string;
    end = string + numBytes;
    for (p = start; p < end; p = next) {
	c = *p & 0xff;
	if (!CtkIsAscii(c)) {
	    Tcl_UniChar codepoint;
	    next = p + Ctk_UtfToCodepoint(p, &codepoint);
	    curX += CtkCharWidth(codepoint);
	    continue;
	} else if (CtkIsPrint(c)) {
            next = p+1;
	    curX++;
	    continue;
	} else {
	    next = p+1;
	}
	if (p != start) {
	    Ctk_DrawString(win, startX, y, style, start, end-start,
		    curX-startX);
	    startX = curX;
	}
	if (c == '\t') {
	    if (!(flags & TK_IGNORE_TABS)) {
		curX += TAB_WIDTH;
		rem = (curX - tabOrigin) % TAB_WIDTH;
		if (rem < 0) {
		    rem += TAB_WIDTH;
		}
		curX -= rem;
		Ctk_FillRect(win, startX, y, startX+1, y+1, style, ' ');
	    }
	} else {
	    if (c == '\n' && !(flags & TK_NEWLINES_NOT_SPECIAL)) {
		y++;
		curX = x;
	    } else {
	    	if (c >= 0 && c < sizeof(mapChars) && mapChars[c]) {
		    replace[0] = '\\';
		    replace[1] = mapChars[c];
		    Ctk_DrawString(win, startX, y, style, replace, 2, 2);
		    curX += 2;
	    	} else {
		    replace[0] = '\\';
		    replace[1] = 'x';
		    replace[2] = hexChars[(c >> 4) & 0xf];
		    replace[3] = hexChars[c & 0xf];
		    Ctk_DrawString(win, startX, y, style, replace, 4, 4);
		    curX += 4;
		}
	    }
	}
	startX = curX;
	start = next;
    }

    /*
     * At the very end, there may be one last batch of normal characters
     * to display.
     */

    if (end != start) {
	Ctk_DrawString(win, startX, y, style, start, end-start, curX-startX);
    }
}

/*
 *----------------------------------------------------------------------
 *
 * TkComputeTextGeometry --
 *
 *	This procedure computes the amount of screen space needed to
 *	display a multi-line string of text.
 *
 * Results:
 *	There is no return value.  The dimensions of the screen area
 *	needed to display the text are returned in *widthPtr, and *heightPtr.
 *
 * Side effects:
 *	None.
 *
 *----------------------------------------------------------------------
 */

void
TkComputeTextGeometry(
    const char *string,		/* String whose dimensions are to be
				 * computed. */
    int numBytes,		/* Number of bytes to consider from
				 * string. */
    int wrapLength,		/* Longest permissible line length, in
				 * pixels.  <= 0 means no automatic wrapping:
				 * just let lines get as long as needed. */
    int *widthPtr,		/* Store width of string here. */
    int *heightPtr)		/* Store height of string here. */
{
    int thisWidth, maxWidth, numLines;
    const char *p;

    if (wrapLength <= 0) {
	wrapLength = INT_MAX;
    }
    maxWidth = 0;
    for (numLines = 1, p = string; (p - string) < numBytes; numLines++) {
	p += TkMeasureChars(p, numBytes - (p - string), 0,
		wrapLength, 0, TK_WHOLE_WORDS|TK_AT_LEAST_ONE, &thisWidth);
	if (thisWidth > maxWidth) {
	    maxWidth = thisWidth;
	}
	if (*p == 0) {
	    break;
	}

	/*
	 * If the character that didn't fit in this line was a white
	 * space character then skip it.
	 */

	if (CtkIsSpace(*p)) {
	    p++;
	}
    }
    *widthPtr = maxWidth;
    *heightPtr = numLines;
}

/*
 *----------------------------------------------------------------------
 *
 * TkDisplayText --
 *
 *	Display a text string on one or more lines.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	The text given by "string" gets displayed at the given location
 *	in the given window with the given style etc.
 *
 *----------------------------------------------------------------------
 */

void
TkDisplayText(
    Tk_Window win,		/* Window in which to draw the text. */
    Ctk_Style style,		/* Style in which to draw characters
    				 * (except for underlined char). */
    const char *string,		/* String to display;  may contain embedded
				 * newlines. */
    int numBytes,		/* Number of bytes to use from string. */
    int x, int y,		/* Pixel coordinates within drawable of
				 * upper left corner of display area. */
    int length,			/* Line length in pixels;  used to compute
				 * word wrap points and also for
				 * justification.   Must be > 0. */
    Tk_Justify justify,		/* How to justify lines. */
    int underline)		/* Index of character to underline, or < 0
				 * for no underlining. */
{
    const char *p;
    int bytesThisLine, lengthThisLine, xThisLine;

    /*
     * Work through the string one line at a time.  Display each line
     * in four steps:
     *     1. Compute the line's length.
     *     2. Figure out where to display the line for justification.
     *     3. Display the line.
     *     4. Underline one character if needed.
     */

    for (p = string; numBytes > 0; ) {
	bytesThisLine = TkMeasureChars(p, numBytes, 0, length, 0,
		TK_WHOLE_WORDS|TK_AT_LEAST_ONE, &lengthThisLine);
	if (justify == TK_JUSTIFY_LEFT) {
	    xThisLine = x;
	} else if (justify == TK_JUSTIFY_CENTER) {
	    xThisLine = x + (length - lengthThisLine)/2;
	} else {
	    xThisLine = x + (length - lengthThisLine);
	}
	TkDisplayChars(win, style, p, bytesThisLine,
		xThisLine, y, xThisLine, 0);
	if ((underline >= 0) && (underline < lengthThisLine)) {
	    int byteOffset, byteLen, underlineOffset;
	    const char *ulChar;

	    byteOffset = CtkCharNextN(p, bytesThisLine, underline);
	    /*
	     * Adjust to the start of the current character cluster.
	     */
	    byteOffset -= CtkClusterPrevN(p, byteOffset, 0);
	    ulChar = p+byteOffset;
	    byteLen = bytesThisLine-byteOffset;
	    byteLen = CtkClusterNextN(ulChar, byteLen, 1);
	    underlineOffset = TkTextWidth(p, byteOffset);

	    TkDisplayChars(win, CTK_UNDERLINE_STYLE, ulChar, byteLen,
		    xThisLine+underlineOffset, y, xThisLine, 0);
	}
	p += bytesThisLine;
	numBytes -= bytesThisLine;
	underline -= lengthThisLine;
	y++;

	/*
	 * If the character that didn't fit was a space character, skip it.
	 */

	if (CtkIsSpace(*p)) {
	    p++;
	    numBytes--;
	    underline--;
	}
    }
}
