/*
 * ctkDisplay.c (Ctk) --
 *
 *	CTK display functions (hides all curses functions).
 *
 * Copyright (c) 1994-1995 Cleveland Clinic Foundation
 *
 * See the file "license.terms" for information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 */

#define _POSIX_SOURCE 1 /* fdopen */
#define _XOPEN_SOURCE_EXTENDED 1 /* ncurses*w*. */

#include "tkPort.h"
#include "tkInt.h"
#include "ctkUnicode.h"
#include <locale.h>
#include <stdint.h>
#include <curses.h>

/*
 * Definitions for weak curses implementations.
 */

#ifndef ACS_ULCORNER
/*
 * This curses does not define the alternate character set constants.
 * Define them locally.
 */
#   define ACS_ULCORNER    '+'
#   define ACS_LLCORNER    '+'
#   define ACS_URCORNER    '+'
#   define ACS_LRCORNER    '+'
#   define ACS_HLINE       '-'
#   define ACS_VLINE       '|'
#   define ACS_PLUS        '+'
#endif /* ACS_ULCORNER */

#ifndef HAVE_CURS_SET
/*
 * Don't have curs_set() function - ignore it.
 *
 * The cursor gets pretty annoying, but haven't found any other
 * way to turn it off.
 */
#   define curs_set(mode)	((void) mode)
#endif

#ifndef A_STANDOUT
    typedef int chtype;
#   define attrset(attr)	((attr) ? standout() : standend())
#   define A_STANDOUT	1
#   define A_INVIS	0
#   define A_NORMAL	0
#   define A_UNDERLINE	0
#   define A_REVERSE	0
#   define A_DIM	0
#   define A_BOLD	0
#   define A_DIM	0
#   define A_BOLD	0
#   define A_REVERSE	0
#endif

/*
 * Macros for the most often used curses operations.  This
 * will hopefully help if someone wants to convert to a different
 * terminal I/O library (like DOS BIOS?).
 */
#define Move(x,y)		move(y,x)
#define PutChar(ch)		addch(ch)
#define PutStr(s,len)		addnstr(s,len)
#define SetStyle(style)		attrset(styleAttributes[style])


/*
 * TextInfo - client data passed to DrawTextSpan() when drawing text.
 */
typedef struct {
    const char *str;	/* String being drawn. */
    int maxBytes;	/* Length of string.  */
    int left;		/* Absolute X coordinate to draw first character
    			 * of string at. */
} TextInfo;

/*
 * Curses attributes that correspond to Ctk styles.
 * This definition must be modified in concert with
 * the Ctk_Style definition in tk.h
 */
static const chtype styleAttributes[] = {
    A_NORMAL, A_NORMAL, A_UNDERLINE, A_REVERSE, A_DIM, A_BOLD,
    A_DIM, A_BOLD, A_STANDOUT, A_REVERSE
};

/*
 * Current display for input/output.  Changed by CtkSetDisplay().
 */

static TkDisplay *curDispPtr = NULL;

/*
 * Whether a refresh event is on its way already.
 */

static int isRefreshNeeded = 0;

#define TriggerRefresh()		\
    do {				\
	if (!isRefreshNeeded) {		\
	    PostRefreshEvent();		\
	    isRefreshNeeded = 1;	\
	}				\
    } while(FALSE)

/*
 * Forward declarations of static functions.
 */

static void	RefreshDisplay (TkDisplay *dispPtr);
static void	DrawTextSpan (int left, int right, int y, ClientData data);
static void	FillSpan (int left, int right, int y, ClientData data);
static int	HandleRefreshEvent (Tcl_Event *evPtr, int flags);
static void	PostRefreshEvent (void);


/*
 *--------------------------------------------------------------
 *
 * CtkDisplayInit --
 *
 *	Opens a connection to terminal with specified name,
 *	and stores terminal information in the display
 *	structure pointed to by `dispPtr'.
 *
 * Results:
 *	Standard TCL result.
 *
 * Side effects:
 *	The screen is cleared, and all sorts of I/O options
 *	are set appropriately for a full-screen application.
 *
 *--------------------------------------------------------------
 */

int
CtkDisplayInit(
    Tcl_Interp *interp,
    TkDisplay *dispPtr,
    const char *termName)
{
    const char *type = strchr(termName, ':');
    int length;

    /*
     * Caller has allocated the memory but not initialized yet.  Do that
     * before we first get a chance to screw it up.
     */
    memset(dispPtr, 0, sizeof(*dispPtr));

    if (type == NULL) {
    	length = strlen(termName);
    	type = getenv("CTK_TERM");
	if (!type || !*type) {
	    type = getenv("TERM");
	    if (!type || !*type) {
		Tcl_AppendResult(interp, "no terminal type given in \"",
			termName, "\" or environment", (char *) NULL);
		goto error;
	    }
	}
    } else {
	length = type - termName;
	type++;
    }
    dispPtr->type = CtkStrdup(type);

    char *new = (char *) ckalloc(length+1);
    strncpy(new, termName, length);
    new[length] = '\0';
    dispPtr->name = new;

    if (strcmp(dispPtr->name, "tty") == 0) {
	dispPtr->chan = Tcl_GetStdChannel(TCL_STDIN);
    } else {
#ifdef HAVE_SET_TERM
	dispPtr->chan = Tcl_OpenFileChannel(interp, dispPtr->name, "r+", 0);
	/*
	 * Make the channel visible in the interpreter for use by
	 * $ctkDisplay.
	 */
	if (dispPtr->chan != NULL) {
	    Tcl_RegisterChannel(interp, dispPtr->chan);
	}
#else
	dispPtr->chan = NULL;
#endif
        if (dispPtr->chan == NULL) {
	    Tcl_ResetResult(interp);
	    Tcl_AppendResult(interp, "couldn't connect to display \"",
		    termName, "\"", (char *) NULL);
	    goto error;
	}
	/*
	 * Create a global variable `ctkDisplay', so that the Tcl
	 * application can access the TTY directly.  E.g. this can be
	 * used by tests to determine the actual position of the cursor
	 * as opposed to what Curses thinks where we should be.
	 */
	Tcl_SetVar(interp, "ctkDisplay", Tcl_GetChannelName(dispPtr->chan),
		TCL_GLOBAL_ONLY);
    }

    ClientData fd = (ClientData) (intptr_t) -1;
    if ( Tcl_GetChannelHandle(dispPtr->chan, TCL_READABLE, &fd ) != TCL_OK )
    {
    	Tcl_AppendResult(interp, "couldn't get device handle for device \"",
    		dispPtr->name, "\", display \"", termName, "\"", (char *) NULL);
        goto error;
    }
    dispPtr->fd = (int) (intptr_t) fd;

    if (!isatty(dispPtr->fd)) {
	Tcl_AppendResult(interp, "display device \"", dispPtr->name,
		"\" is not a tty, display \"", termName, "\"", (char *) NULL);
	goto error;
    }

    FILE *outPtr;
    if (dispPtr->fd == 0) {
	dispPtr->inPtr = stdin;
	outPtr = stdout;
    } else {
	dispPtr->inPtr = fdopen(dispPtr->fd, "r+");
	outPtr = dispPtr->inPtr;
    }

    /*
     * Tcl already does this, but it is still good to repeat it here, because
     * NcursesW relies heavily on the C locale handling.
     */
    setlocale(LC_CTYPE, "");
#ifdef HAVE_SET_TERM
    dispPtr->display = newterm((char *) dispPtr->type, outPtr,
	    dispPtr->inPtr);
    CtkSetDisplay(dispPtr);
#else
    dispPtr->display = initscr();
    (void) outPtr;
#endif
    nonl();
    noecho();

    TriggerRefresh();
    return TCL_OK;

error:
    ckfree((void*)dispPtr->name);
    dispPtr->name = NULL;
    ckfree((void*)dispPtr->type);
    dispPtr->type = NULL;
    return TCL_ERROR;
}

/*
 *--------------------------------------------------------------
 *
 * CtkDisplayEnd --
 *
 *	Ends Ctk's use of terminal.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	The terminal is restored to line mode.
 *
 *--------------------------------------------------------------
 */

void
CtkDisplayEnd(
    TkDisplay *dispPtr)
{
    CtkSetDisplay(dispPtr);
    curs_set(1);
    endwin();

    /*
     * SysVR4 curses seems to need delscreen to free up resources - but
     * HP curses doesn't define it.
     */
#ifdef HAVE_DELSCREEN
    delscreen((SCREEN *) dispPtr->display);
#endif

    if (dispPtr->inPtr != stdin) {
    	fclose(dispPtr->inPtr);
    }
    ckfree((void*)dispPtr->name);
    ckfree((void*)dispPtr->type);
}


/*
 *--------------------------------------------------------------
 *
 * CtkSetDisplay --
 *
 *	Synchronize the current display between Curses and Ctk.
 *
 * Results:
 *	None.
 *
 *--------------------------------------------------------------
 */

void
CtkSetDisplay(
    TkDisplay *dispPtr)
{
#ifdef HAVE_SET_TERM
    if (curDispPtr != dispPtr) {
	curDispPtr = dispPtr;
	set_term((SCREEN *) dispPtr->display);
    }
#endif
}


/*
 *--------------------------------------------------------------
 *
 * Ctk_DisplayFlush --
 *
 *	Flushes all output to the specified display.  If dispPtr
 *	is NULL then output to all connected displays is flushed.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	The terminal display is updated.
 *
 *--------------------------------------------------------------
 */

void
Ctk_DisplayFlush(
    TkDisplay *dispPtr)
{
    if (dispPtr) {
	RefreshDisplay(dispPtr);
    } else {
    	for (dispPtr = tkDisplayList;
    		dispPtr != NULL;
    		dispPtr = dispPtr->nextPtr) {
	    RefreshDisplay(dispPtr);
	}
    }
}

/*
 *--------------------------------------------------------------
 *
 * Ctk_DisplayRedraw --
 *
 *	Force a complete redraw of the specified display.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	The entire terminal display is redrawn.
 *
 *--------------------------------------------------------------
 */

void
Ctk_DisplayRedraw(
    TkDisplay *dispPtr)
{
    CtkSetDisplay(dispPtr);
    clearok(stdscr, 1);
}

/*
 *--------------------------------------------------------------
 *
 * Ctk_SetCursor --
 *
 *	Positions display cursor in window at specified (local)
 *	coordinates.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	Modifies window's display structure.
 *
 *--------------------------------------------------------------
 */

void
Ctk_SetCursor(
    TkWindow *winPtr,
    int x, int y)
{
    TkDisplay *dispPtr = Tk_Display(winPtr);
    dispPtr->cursorPtr = winPtr;
    dispPtr->cursorX = x;
    dispPtr->cursorY = y;
    TriggerRefresh();
}


/*
 *--------------------------------------------------------------
 *
 * RefreshDisplay --
 *
 *	Redraw everything that has changed in the Curses data model
 *	since the last time this was called.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	Updates the screen.
 *
 *--------------------------------------------------------------
 */

static void
RefreshDisplay(
    TkDisplay *dispPtr)
{
    TkWindow *winPtr = dispPtr->cursorPtr;
    int x, y;
    int visible = 0;

    CtkSetDisplay(dispPtr);
    if (CtkIsDisplayed(winPtr)) {
	/*
	 * Convert to absolute screen coordinates
	 */
	x = dispPtr->cursorX + winPtr->absLeft;
	y = dispPtr->cursorY + winPtr->absTop;
	if (y >= winPtr->maskRect.top
		&& y < winPtr->maskRect.bottom
		&& x >= winPtr->maskRect.left
		&& x < winPtr->maskRect.right
		&& CtkPointInRegion(x, y, winPtr->clipRgn) ) {
	    Move(x, y);
	    visible = 1;
	}
    }
    curs_set(visible);
    refresh();
}

/*
 *--------------------------------------------------------------
 *
 * CtkDisplayBell --
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	Sound.
 *
 *--------------------------------------------------------------
 */

void
CtkDisplayBell(
    TkDisplay *dispPtr)
{
#ifdef HAVE_BEEP
    CtkSetDisplay(dispPtr);
    beep();
#endif
}

/*
 *--------------------------------------------------------------
 *
 * Ctk_DisplayWidth --
 * Ctk_DisplayHeight --
 *
 *	Get geometry of terminal.
 *
 * Results:
 *	Size (width/height respectively) of terminal.
 *
 * Side effects:
 *	None.
 *
 *--------------------------------------------------------------
 */

int
Ctk_DisplayWidth(
    TkDisplay *dispPtr)
{
    CtkSetDisplay(dispPtr);
    return COLS;
}

int
Ctk_DisplayHeight(
    TkDisplay *dispPtr)
{
    CtkSetDisplay(dispPtr);
    return LINES;
}

/*
 *--------------------------------------------------------------
 *
 * Ctk_DrawCharacter --
 *
 *	Display a single ASCII character in a view.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	Character is output to the terminal.
 *
 *--------------------------------------------------------------
 */

void
Ctk_DrawCharacter(
    TkWindow *winPtr,		/* Window to draw into. */
    int x, int y,		/* Position, relative to view, to
    				 * start draw at. */
    Ctk_Style style,		/* Style to draw character in. */
    int character)		/* Character to draw. */
{
    if (!CtkIsDisplayed(winPtr)) {
	return;
    }

    /*
     * Convert to absolute screen coordinates
     */
    y += winPtr->absTop;
    x += winPtr->absLeft;

    if (y >= winPtr->clipRect.top
    	    && y < winPtr->clipRect.bottom
    	    && x >= winPtr->clipRect.left
    	    && x < winPtr->clipRect.right
	    && CtkPointInRegion(x, y, winPtr->clipRgn) ) {
	CtkSetDisplay(winPtr->dispPtr);
	SetStyle(style);
	Move(x, y);
	PutChar(character);
    }

    TriggerRefresh();
}

/*
 *--------------------------------------------------------------
 *
 * Ctk_DrawString --
 *
 *	Display `length' characters from `str' into `winPtr'
 *	at position (`x',`y') in specified `style'.  If `length'
 *	is -1 then draw till a null character is reached.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	Characters are output to the terminal.
 *
 *--------------------------------------------------------------
 */

void
Ctk_DrawString(
    TkWindow *winPtr,		/* Window to draw into. */
    int x, int y,		/* Position, relative to view, to
    				 * start drawing. */
    Ctk_Style style,		/* Style to draw characters in. */
    const char *str,		/* Points to characters to be drawn. */
    int maxBytes,		/* Maximum length of string, or -1, if
				 * the string is NUL terminated. */
    int length)			/* Number of characters from str
    				 * to draw, or -1 to draw till NULL
    				 * termination. */
{
    int strLeft, strRight;
    TextInfo text_info;

    if (!CtkIsDisplayed(winPtr)) {
	return;
    }

    /*
     * Convert to absolute screen coordinates
     */
    y += winPtr->absTop;
    if (y < winPtr->clipRect.top || y > winPtr->clipRect.bottom) {
	return;
    }
    x += winPtr->absLeft;

    if (length == -1) {
	length = TkTextWidth(str, -1);
    }
    strLeft = x;
    strRight = x+length;
    CtkIntersectSpans(&strLeft, &strRight,
	    winPtr->clipRect.left, winPtr->clipRect.right);
    if (CtkSpanIsEmpty(strLeft, strRight))  return;

    CtkSetDisplay(winPtr->dispPtr);
    SetStyle(style);
    text_info.str = str;
    text_info.left = x;
    text_info.maxBytes = maxBytes >= 0 ? maxBytes : strlen(str);
    CtkForEachIntersectingSpan(DrawTextSpan, &text_info,
	    strLeft, strRight, y, winPtr->clipRgn);

    TriggerRefresh();
}

/*
 *--------------------------------------------------------------
 *
 * DrawTextSpan --
 *
 *	Called by ForEachSpan() or ForEachIntersectingSpan()
 *	to draw a segment of a string.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	Characters are output to the terminal.
 *
 *--------------------------------------------------------------
 */

static void
DrawTextSpan(
    int left,			/* X coordinate to start drawing. */
    int right,			/* X coordinate to stop drawing (this
    				 * position is not drawn into). */
    int y,			/* Y coordinate to draw at. */
    ClientData data)		/* Points at TextInfo structure. */
{
    const TextInfo *info = (TextInfo*) data;
    int strOffset = left - info->left;
    const char *charPtr = info->str;
    int maxBytes = info->maxBytes;
    int byteCount;
    Tcl_DString encBuf;
    const char *encStr;

    Move(left, y);

    /*
     * Skip characters for the offset into the string.
     */
    byteCount = CtkColumnNextN(charPtr, maxBytes, strOffset);
    charPtr += byteCount;
    maxBytes -= byteCount;

    /*
     * Find out how many UTF-8 bytes are involved in printing into the
     * cells from `left' to `right'.
     */
    byteCount = CtkColumnNextN(charPtr, maxBytes, right-left);

    encStr = Tcl_UtfToExternalDString(NULL, charPtr, byteCount, &encBuf);
    PutStr((char *) encStr, Tcl_DStringLength(&encBuf));
    Tcl_DStringFree(&encBuf);
}

/*
 *--------------------------------------------------------------
 *
 * Ctk_ClearWindow --
 *
 *	Fill view with its background (as defined by
 *	winPtr->fillStyle and winPtr->fillChar).
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	Characters are output to the terminal.
 *
 *--------------------------------------------------------------
 */

void
Ctk_ClearWindow(
    TkWindow * winPtr)	/* Window to clear. */
{
    int left = winPtr->clipRect.left;
    int right = winPtr->clipRect.right;
    int y;

    if (winPtr->fillStyle == CTK_INVISIBLE_STYLE || (!CtkIsDisplayed(winPtr))
    	    || CtkSpanIsEmpty(left, right)) {
	return;
    }

    CtkSetDisplay(winPtr->dispPtr);
    SetStyle(winPtr->fillStyle);
    for (y=winPtr->clipRect.top; y < winPtr->clipRect.bottom; y++) {
	CtkForEachIntersectingSpan(
	    FillSpan, (ClientData) (intptr_t) winPtr->fillChar,
	    left, right, y,
	    winPtr->clipRgn);
    }

    TriggerRefresh();
}

/*
 *--------------------------------------------------------------
 *
 * CtkFillRegion --
 *
 *	Fills in a region with the specified character and style.
 *	Region is in absolute screen coordinates.  No clipping is
 *	performed.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	Characters are output to the terminal.
 *
 *--------------------------------------------------------------
 */

void
CtkFillRegion(
    TkDisplay *dispPtr,
    CtkRegion *rgnPtr,
    Ctk_Style fillStyle,
    int fillChar)
{
    CtkSetDisplay(dispPtr);
    SetStyle(fillStyle);
    CtkForEachSpan(FillSpan, (ClientData) (intptr_t) fillChar, rgnPtr);

    TriggerRefresh();
}

/*
 *--------------------------------------------------------------
 *
 * Ctk_FillRect --
 *
 *	Draw a rectangle filled with the specified character
 *	and style in `winPtr' at relative coordinates (x1,y1)
 *	to (x2-1,y2-1).
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	Characters are output to the terminal.
 *
 *--------------------------------------------------------------
 */

void
Ctk_FillRect(
    TkWindow *winPtr,
    int x1,
    int y1,
    int x2,
    int y2,
    Ctk_Style fillStyle,
    int fillChar)
{
    Ctk_Rect rect;
    int y;

    if (!CtkIsDisplayed(winPtr)) {
	return;
    }
    CtkSetRect(&rect, x1, y1, x2, y2);
    CtkMoveRect(&rect, winPtr->absLeft, winPtr->absTop);
    CtkIntersectRects(&rect, &winPtr->clipRect);
    if ( CtkSpanIsEmpty(rect.left, rect.right) ) {
	return;
    }
    CtkSetDisplay(winPtr->dispPtr);
    SetStyle(fillStyle);
    for (y=rect.top; y < rect.bottom; y++)
    {
	CtkForEachIntersectingSpan( FillSpan,
		(ClientData) (intptr_t) fillChar,
		rect.left, rect.right, y, winPtr->clipRgn);
    }

    TriggerRefresh();
}

/*
 *--------------------------------------------------------------
 *
 * FillSpan --
 *
 *	Called by ForEachSpan() or ForEachIntersectingSpan()
 *	to fill a span with the same character.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	Characters are output to the terminal.
 *
 *--------------------------------------------------------------
 */

static void
FillSpan(
    int left,			/* X coordinate to start filling. */
    int right,			/* X coordinate to stop filling (this
    				 * position is not draw into). */
    int y,			/* Y coordinate to draw at. */
    ClientData data)		/* Character to draw. */
{
    int x;

    Move(left, y);
    for (x=left; x < right; x++) {
	PutChar((int) (intptr_t) data);
    }
}

/*
 *--------------------------------------------------------------
 *
 * Ctk_DrawRect --
 *
 *	Draw outline of rectangle with line drawing characters
 *	and the specified style in `winPtr' at relative
 *	coordinates (x1,y1) to (x2,y2).
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	Characters are output to the terminal.
 *
 *--------------------------------------------------------------
 */

void
Ctk_DrawRect(
    TkWindow *winPtr,
    int x1,
    int y1,
    int x2,
    int y2,
    Ctk_Style lineStyle)
{
    Ctk_Rect *clipRectPtr = &winPtr->clipRect;
    int left;
    int right;
    int top;
    int bottom;
    int y;

    if (!CtkIsDisplayed(winPtr) || x1 > x2 || y1 > y2) {
	return;
    }
    CtkSetDisplay(winPtr->dispPtr);
    SetStyle(lineStyle);

    Ctk_DrawCharacter(winPtr, x1, y1, lineStyle, ACS_ULCORNER);
    Ctk_DrawCharacter(winPtr, x2, y1, lineStyle, ACS_URCORNER);
    Ctk_DrawCharacter(winPtr, x1, y2, lineStyle, ACS_LLCORNER);
    Ctk_DrawCharacter(winPtr, x2, y2, lineStyle, ACS_LRCORNER);

    /* Convert to screen coordinates */
    x1 += winPtr->absLeft;
    x2 += winPtr->absLeft;
    y1 += winPtr->absTop;
    y2 += winPtr->absTop;

    /*
     *	Draw horizontal lines.
     */
    left = x1+1;
    right = x2;
    CtkIntersectSpans(&left, &right, clipRectPtr->left, clipRectPtr->right);
    if (!CtkSpanIsEmpty(left, right)) {
	if ((clipRectPtr->top <= y1) && (clipRectPtr->bottom > y1)) {
	    CtkForEachIntersectingSpan(
                FillSpan, (ClientData) (intptr_t) ACS_HLINE,
		left, right, y1,
		winPtr->clipRgn);
	}
	if ((clipRectPtr->top <= y2) && (clipRectPtr->bottom > y2)) {
	    CtkForEachIntersectingSpan(
                FillSpan, (ClientData) (intptr_t) ACS_HLINE,
		left, right, y2,
		winPtr->clipRgn);
	}
    }

    /*
     *	Draw vertical lines.
     */
    top = y1 + 1;
    bottom = y2;
    CtkIntersectSpans(&top, &bottom, clipRectPtr->top, clipRectPtr->bottom);
    if ((clipRectPtr->left <= x1) && (clipRectPtr->right > x1)) {
	for (y=top; y < bottom; y++) {
	    if (CtkPointInRegion(x1, y, winPtr->clipRgn)) {
		Move(x1, y);
		PutChar(ACS_VLINE);
	    }
	}
    }
    if ((clipRectPtr->left <= x2) && (clipRectPtr->right > x2)) {
	for (y=top; y < bottom; y++) {
	    if (CtkPointInRegion(x2, y, winPtr->clipRgn)) {
		Move(x2, y);
		PutChar(ACS_VLINE);
	    }
	}
    }

    TriggerRefresh();
}


/*
 *--------------------------------------------------------------
 *
 * HandleRefreshEvent --
 *
 *	Tell curses to actually realize its internal state to the
 *	display.
 *
 * Results:
 *	1 if the event was handled, 0 if it should be deferred.
 *
 * Side effects:
 *	The terminal is current with the internal state.
 *
 *--------------------------------------------------------------
 */

static int
HandleRefreshEvent (
    Tcl_Event *evPtr,
    int flags)
{
    (void) evPtr;

    if (! (flags & TCL_WINDOW_EVENTS)) {
	return 0;
    } else {
	Ctk_DisplayFlush(NULL);
        isRefreshNeeded = 0;
	return 1;
    }
}

static void
PostRefreshEvent (void)
{
    Tcl_Event *evPtr = ckalloc(sizeof *evPtr);
    evPtr->proc = &HandleRefreshEvent;
    Tcl_QueueEvent(evPtr, TCL_QUEUE_TAIL);
}
