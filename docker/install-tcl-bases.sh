#!/bin/sh
# ------------------------------------------------------------------------
#   install-tcl-bases.sh
# ------------------------------------------------------------------------

set -e  # Exit on error.

API=https://gitlab.com/api/v4/projects/cc_benny%2Ftcl/packages/generic
PKGDIR=/tmp/pkgs

# Get the currently interesting Tcl packages from the package manager
# and put them into the directory `pkgs'.
get_tcl_bases () {
    local pkgs="
        core-8-6-9-8.6.9-ucs4/8.6.9/tcl-8.6.9-ucs4.tar.gz
        core-8-6-9-8.6.9/8.6.9/tcl-8.6.9.tar.gz
        core-8-6-branch-snapshot-ucs4/0.0.0/tcl-core-8-6-branch-snapshot-ucs4.tar.gz
        core-8-6-branch-snapshot/0.0.0/tcl-core-8-6-branch-snapshot.tar.gz
        core-8-branch-snapshot-ucs4/0.0.0/tcl-core-8-branch-snapshot-ucs4.tar.gz
        core-8-branch-snapshot/0.0.0/tcl-core-8-branch-snapshot.tar.gz
        main-snapshot/0.0.0/tcl-main-snapshot.tar.gz
    "
    local p
    mkdir -p $PKGDIR
    (
        cd $PKGDIR
        for p in $pkgs; do
            echo Getting $(basename $p) ...
            wget -q $API/$p
        done
    )
}

extract () {
    for f in $PKGDIR/*.tar.gz; do
	tar xf $f -C /
    done
}

apt-get install -y wget
get_tcl_bases
extract

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
