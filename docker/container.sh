#!/bin/sh
# ------------------------------------------------------------------------
#    container.sh
#
#    Utilities around our Docker container.
# ------------------------------------------------------------------------

set -e	# Exit on error.

CONTAINER=${CONTAINER:-ctk-build-debian}
DEPENDS=${DEPENDS:-debian}
SCRIPT="$0"
BASEDIR=/src

# Create the container itself.  Parameters will be passed-through to
# "docker build".  This can be used to pass build-args.
create () {
    # user/home: For build-ports.sh, the user and home directory have
    # to be the same as on the host.  But never use "root" inside the
    # container.
    local user=$(id -un)
    local home=$HOME
    if [ root = "$user" ]; then
	user=runner
	home=/home/runner
    fi
    local dir=$(dirname "$SCRIPT")
    local depends
    case "$DEPENDS" in
	debian)
	    depends=$("$dir"/get-deps.awk "$dir"/../debian/control)
	    ;;
	alpine)
	    depends="autoconf ncurses-libs ncurses-dev tcl-dev"
	    ;;
	none)
	    depends=
	    ;;
	*)
	    depends=$DEPENDS
	    ;;
    esac
    docker build \
	   --tag $CONTAINER \
	   --build-arg user=$user \
	   --build-arg home=$home \
           --build-arg depends="$depends" \
	   --build-arg os \
	   --build-arg os_version \
	   "$@" \
	   "$dir"
}

# Run a non-interactive command inside the container.  The parameters
# are setup to work with the `compile' subcommand below.
run () {
    cd $(dirname "$SCRIPT")
    # `:z' is necessary on systems running selinux (Fedora) to install
    # the right labels.
    docker run --rm \
	   --volume $(dirname "$PWD"):"$BASEDIR"/ctk:z \
	   --workdir "$BASEDIR"/ctk \
	   --user $(id -u):$(id -g) \
	   $CONTAINER \
	   "$@"
}

# Re-compile the project from scratch inside the container.  At this
# point, cwd is /src/ctk.
compile_1 () {
    set -x

    # Clean first before we copy to the build directory.
    make -f Makefile.in distclean

    # Create a build directory that has a writable parent where
    # dpkg-buildpackage can put its output.
    rm -rf target
    files=$(echo *)
    mkdir -p target/build
    cp -lr $files target/build/
    cd target/build

    # Prepare and than build the debian package.  dpkg-buildpackage
    # will call `configure' and `make'.
    ./autogen.sh
    # nocheck: Do not run `make test' at this time.
    export DEB_BUILD_OPTIONS=nocheck
    dpkg-buildpackage -us -uc --no-check-builddeps

    # Copy the stuff that make has built to "target", too.
    cp -a cwish8.* libctk.so.8.* pkgIndex.tcl ..
}

# Run the Tcl tests inside the container, starting from the result of
# `compile_1'.  Does no cleanup before running the tests.  Produces a
# report file `test-report.log'.  Assumes that the binaries are
# already there.  At this point, cwd is /src/ctk.
test_1 () {
    set -x

    # Handle error codes in detail and manually in this sub-shell.
    set +e

    # Override the TCLSH variable in Makefile.in, that is usually set
    # by configure.
    local tclsh=$(TCLSH:-tclsh)
    make -f Makefile.in TCLSH="$tclsh" test-only
    local rc=$?

    # tests/all.log: This is what `make test-only' actually produces.
    mv tests/all.log test-report.log

    return $rc
}

# Format the file `test-report.log' as left by `test_1' into a
# JUnit-style XML file `test-report.xml', a formatted HTML file
# `test-report.html' and a file `overview.html' which is a TOC of all
# similar test reports that the current Gitlab instance has about our
# project.  At this point, cwd is /src/ctk.
format_reports_1 () {
    set -x

    if [ ! -s test-report.log ]; then
        echo "test-report.log: No raw test-report available" >&2
        return 1
    fi

    local tclunit=$(
	find_file tclunit/tclunit_xml.tcl .. ../../.. /src)
    $tclunit -log test-report.log > test-report.xml

    local xslt=$(
	find_file junit-format/format-junit.xslt .. ../../.. /src)

    local format=$(
	find_file junit-format/format-junit-for-gitlab.tcl .. ../../.. /src)
    format="$format scrape-backend"

    local job_id=${CI_JOB_ID:-local}
    local job_url=${CI_JOB_URL:-.}
    local project=${CI_PROJECT_TITLE:-Ctk}
    xsltproc \
	--param generate-summary false \
        --stringparam job-id $job_id \
        --stringparam job-url $job_url \
	$xslt \
	test-report.xml > test-report.html

    # Checkpoints: Dash does not support "set -o pipefail", so use
    # these instead.
    local checkpoint1=/tmp/check-$$-1
    local checkpoint2=/tmp/check-$$-2
    touch $checkpoint1 $checkpoint2
    ($format get-jobs-with-artifacts
     echo ""
     $format generate-local-job
     rm -f $checkpoint1) \
        | ($format generate-overview
	   rm -f $checkpoint2) \
        | xsltproc \
	      --stringparam overview-title "Testresults for $project" \
              $xslt - > overview.html
    if [ -r $checkpoint1 ]; then
	echo "Formatting jobs (Tcl) failed" >&2
	return 1
    fi
    if [ -r $checkpoint2 ]; then
	echo "Formatting overview (Tcl) failed" >&2
	return 1
    fi
}

# Find a file in one of a list of directories.  The first directory
# where the file exists wins.
find_file () {
    set +x
    local file="$1"; shift
    local d
    for d in "$@"; do
	if [ -f "$d/$file" ]; then
	    echo "$d/$file"
	    return 0
	fi
    done

    echo "$file: Not found in [$@]" >&2
    return 1
}

# Run `compile_1' inside the container.
run_compile () {
    run ./docker/$(basename "$SCRIPT") compile_1
}

# Run `test_1' inside the container.
run_test () {
    run ./docker/$(basename "$SCRIPT") test_1
}

"$@"

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
