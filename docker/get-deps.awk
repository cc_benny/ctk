#!/usr/bin/awk -f
# ------------------------------------------------------------------------
#       get-deps.awk
#
#	Parse a dpkg control file to get the contents of the
#	Build-Depends header in a format that can be passed to
#	"apt-get install".
# ------------------------------------------------------------------------

# Use the colon at the end of the label as separator.
BEGIN { FS=":"; }

# Continuation lines after the first.
independs && /^ / {
        pkgs = pkgs $0;
}
# A non-continuation line.
independs && ! /^ / {
        independs = 0;
}
# Start of our label.  Only supports "Build-Depends" at this time.
/^Build-Depends:/ {
        pkgs = pkgs " " $2;
        independs = 1;
}

# Process result and output.
END {
	# Drop all version information.  We always use the latest
	# anyway.
        gsub(/\([^)]*\)/, " ", pkgs);
	# Replace the comma separators with spaces.  This creates more
	# spaces than necessary, but we do not care.
        gsub(/,/, " ", pkgs);
        print pkgs;
}

# ------------------------------------------------------------------------
#       eof
# ------------------------------------------------------------------------
