[//]: # (README.md)

Ctk
====

Introduction
----

This is Ctk, a Curses port of John Ousterhout's Tk toolkit for X11.

See more details in the original [README](README).

Dependencies
----

The terminal drawing is done using Curses.  Under Debian the relevant
support package is `libncursesw5-dev`.

Debian
----

The subdirectory `debian` contains the necessary files to make a
Debian package.  `dpkg-buildpackage -us -uc` will build an unsigned
package.  Also see `docker/Dockerfile` for further dependencies on
Debian.

Fedora
----

The necessary packages for building and testing can be installed with

        $ dnf install -y \
            autoconf devel devel fossil-gcc git-libxslt make \
			ncurses tcl tcllib tcltls tdom tmux

Other Systems
----

See [build-ports.md](build-ports.md)

History
----

This version is based on the sources from 1999 as downloaded from
[Michael Schwartz'
website](http://www.schwartzcomputer.com/tcl-tk/tcl-tk.html#Ctk).
This Git repo also contains the tree and history from Roy Keene's
version at [www.rkeene.org](http://www.rkeene.org/devel/ctk/) [(Github
tree)](https://github.com/tcl-mirror/ctk.git) from 2011-2014 which is
originally based on a slightly different version.

References
----

The files `DerivedGeneralCategory.txt` and `DerivedEastAsianWidth.txt`
are downloaded from the [Unicode
site](https://www.unicode.org/Public/UNIDATA/extracted/).  The command
`make update-ucd` will download these files again.

The file `keysymdef.h`is downloaded from the [Xorg
project](https://gitlab.freedesktop.org/xorg/proto/xorgproto/raw/master/include/X11).
The command `make update-keysyms` will download this file again.

Summary of updates since the initial import
----

Drop unused code.

Drop a lot of K&R compatibility stuff.

No direct access to `interp->result` any more.

Fix a couple of memory handling bugs.

Fix focus handling for new windows.

Make it work with the standard Tcl event loop.

Compile as an extension.

Add Debian packaging.

Integrate Tk's test suite.

Built and run tests on Gitlab's CI.

Basic support on more platforms, see `build-ports.sh`.

Work in progress
----

Handle Unicode text.

Todos
----

Build and run tests on Sourcehut's CI.

Mouse handling (mousemask, getmouse).

Colors.

----
[//]: # (eof)
