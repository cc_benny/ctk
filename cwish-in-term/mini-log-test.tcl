#!/usr/bin/tclsh
# ------------------------------------------------------------------------
#   mini-log-test.tcl
#
#   A simple test rig for mini-log.tcl.
# ------------------------------------------------------------------------

lappend auto_path [file dirname [info script]]
package require mini-log
namespace import mini-log::log

set log1 /tmp/test-1.log
set log2 /tmp/test-2.log

file delete $log1 $log2

log no output

mini-log::init $log1
log log1 output

mini-log::init $log2
log log2 output

mini-log::init
log no output 2

proc read-file {fname} {
    set ifs [open $fname r]
    set c [read $ifs]
    close $ifs
    return $c
}

proc assertEq {expected actual} {
    if {$expected ne $actual} {
        error "Exp: $expected\nWas: $actual"
    }
}

assertEq "log1 output" [string trim [read-file $log1]]
assertEq "log2 output" [string trim [read-file $log2]]

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
