#!/usr/bin/env tclsh
# ------------------------------------------------------------------------
#   cwish-in-term.tcl
# ------------------------------------------------------------------------
#   Start a terminal program like e.g. xterm with cwish inside,
#   passing the parameters.  Connect the cwish to the terminal tty by
#   setting CTK_DISPLAY to [exec tty].  Connect the stdin/out/err of
#   the cwish to the current (external) descriptors, synchronizing via
#   file events.  The connection goes through three FIFOs.
#
#   The original cwish-in-term.tcl process is called `controller'.
#   The process inside the terminal program is called `in-term'.
# ------------------------------------------------------------------------

namespace eval cwish-in-term {
    variable script [info script]
    # Given that this is the main program, cater for symbolic links.
    catch {
        set script [file join \
                        [file dirname $script] \
                        [file readlink $script]]
    }
    lappend ::auto_path [file dirname $script]
    package require fifo
    package require lambda
    package require mini-log
    namespace import ::mini-log::log

    variable log [expr {[info exists ::env(CWISH_LOG)] && $::env(CWISH_LOG)}]
    variable cwish [file join [file dirname $script] .. cwish.tcl]
}

# [controller] Find a terminal program.  Use $env(CWISH_TERM) if set.
# Otherwise check for a couple of common programs.  Return the
# absolute executable file name of the program.
proc cwish-in-term::find-term {} {
    global env
    if {[info exists env(CWISH_TERM)]} {
        set exe [auto_execok $env(CWISH_TERM)]
        if {"" ne "$exe"} {
            return $exe
        }
        error "$env(CWISH_TERM): Executable does not exist"
    } else {
        foreach term {i3-sensible-terminal gnome-terminal xterm} {
            set exe [auto_execok $term]
            if {"" ne "$exe"} {
                return $exe
            }
        }
        error "No usable terminal program found"
    }
}

# [controller] Configure the terminal program and make a command line
# for it.  The result is a command line to start TERM with CMD running
# inside it.  The particular syntax depends on the terminal program.
# TERM is the absolute file name for the terminal.  The result is a
# list suitable to pass to `exec' or `open |...'
proc cwish-in-term::prepare-term {term cmd} {
    switch -glob [file tail $term] {
        screen* {
            return [list $term -D -m {*}$cmd]
        }
        tmux* {
	    # If we have a server already, configure it to pass the
	    # caller's value of CWISH_CONTROL to the new session.  If
	    # we are the first caller, no server exists yet and we get
	    # a failure here, but we ignore that.
	    catch {exec $term set-option -g update-environment CWISH_CONTROL}
            return [list $term new-session -d {*}$cmd]
        }
        default {
            return [list $term -e {*}$cmd]
        }
    }
}

# [controller] The `controller' part of this program.  Start a FIFO,
# than the terminal program, monitor the FIFO for a connect attempt,
# vwait.
proc cwish-in-term::controller {} {
    proc bgerror {args} {
	cwish-in-term::log bgerror $::errorInfo
	exit 1
    }

    set controlReqName [fifo::create]
    set controlReq [fifo::for-reading $controlReqName]
    fileevent $controlReq readable \
        [list cwish-in-term::control-handler $controlReq]

    variable script
    global argv
    global env
    set cmd [list [info nameofexecutable] $script {*}$argv]
    set term [find-term]
    log CWISH_CONTROL $controlReqName
    set env(CWISH_CONTROL) $controlReqName
    set cmd [prepare-term $term $cmd]
    log Running $cmd ...
    set remoteTerm [open "|$cmd 2>@1" r]
    fileevent $remoteTerm readable \
        [list cwish-in-term::term-handler $remoteTerm]

    # The command callback will cancel the timeout, but it will not
    # change the variable.
    variable timeout [after 20000 {set cwish-in-term::timeout <timeout>}]
    vwait cwish-in-term::timeout
    error "CWish in terminal failed to start, check logs"
}

# [controller] A file event handler to just do logging and error
# handling for the terminal process.
proc cwish-in-term::term-handler {channel} {
    variable termOutput
    if {[gets $channel line] < 0} {
        log term> <EOF>
        if {[catch {close $channel}]} {
	    set einfo $::errorInfo
	    puts -nonewline stderr $termOutput
	    puts stderr "Terminal exited with code [lindex $::errorCode 2]"
	    log $einfo
	    exit 1
	}
    } else {
	append termOutput $line "\n"
        log term> $line
    }
}

# [controller]+[in-term] Read a full Tcl expression from a channel.
proc cwish-in-term::read-expr {channel expr} {
    upvar 1 $expr result
    set result ""
    while {[gets $channel line] >= 0} {
	append result $line "\n"
        if {[info complete $result]} {
	    return [string length $result]
	}
    }
    return -1
}

# [controller] A file event handler that executes incoming commands.
proc cwish-in-term::control-handler {controlReq} {
    set prefix "controlReq>"
    if {[read-expr $controlReq cmd] < 0} {
        close $controlReq
        log $prefix <EOF>
	exit 0
    } else {
	# Anything coming in should cancel the initial timeout.  NB:
	# Timeout ids are not reused and canceling a non-existent
	# timeout is not an error, so we do not need to set the
	# variable.  And so we can use it for the vwait.
	variable timeout
	after cancel $timeout

        log $prefix $cmd
        variable controlResp ;# Provided by connect-remote.
        if {[catch {uplevel #0 $cmd} result]} {
	    set einfo $::errorInfo
            log $prefix $::errorCode
            log $prefix $einfo
	    # Free stderr from the clutches of fcopy by closing the
	    # input stream there.  Otherwise fcopy causes stderr to be
	    # "busy".
	    catch {variable stderrChannel; close $stderrChannel}
	    catch {puts stderr $einfo}
            catch {puts $controlResp $einfo}
	    log exiting ...
            exit 1
        } else {
            log $prefix Result: $result
            catch {puts $controlResp $result}
        }
    }
}

# [in-term] Send command CMD to the controller and wait for a response
# or EOF.
proc cwish-in-term::control-cmd {controlReq controlResp cmd} {
    log controlReq> $cmd
    variable controlSemaphore "<waiting>"
    puts $controlReq $cmd
    # See control-resp-handler for a discussion why this is done with
    # filevent + vwait.
    set after [after 10000 {set cwish-in-term::controlSemaphore <timeout>}]
    vwait cwish-in-term::controlSemaphore
    after cancel $after
    return [string trimright $controlSemaphore "\n"]
}

# [in-term] The file event handler for the controller response channel
# CONTROLRESP.  We use file events plus vwait instead of synchronous
# calls because without an event loop running, the reader side of a
# FIFO will never complete it's internal configuration and so the
# writer side will never be able to connect.
proc cwish-in-term::control-resp-handler {controlResp} {
    variable controlSemaphore
    set prefix controlResp>
    if {[read-expr $controlResp resp] < 0} {
        close $controlResp
        log $prefix <EOF>
	set controlSemaphore <EOF>
    } else {
        log $prefix $resp
	set controlSemaphore $resp
    }
}

# [in-term] Handle `exit' (or global errors) inside the terminal.
proc cwish-in-term::exit-handler {controlReq controlResp cmd args} {
    control-cmd $controlReq $controlResp $cmd
}

# [in-term] The part of this program that excutes inside the terminal.
# Set up the FIFOs, connect to the controller process and execute the
# actual program.
proc cwish-in-term::in-term {} {
    # First preserve some connection to the tty, otherwise the
    # terminal closes down once all std* channels are redirected away
    # from it.

    # [exec tty]: We can not just use /dev/tty here because that does
    # not work on macOS.
    set ttyName [exec tty]
    set tty [open $ttyName w]
    fconfigure $tty -buffering none
    puts $tty "Waiting for program to start..."

    global env
    set controlReqName $env(CWISH_CONTROL)
    log CWISH_CONTROL $controlReqName
    set controlReq [fifo::for-writing $controlReqName]
    log $controlReqName delete
    file delete $controlReqName

    # NB: here in `in-term' controlResp is not a `variable', because
    # here the responses are ignored, only logged when debugging.
    set controlRespName [fifo::create]
    set controlResp [fifo::for-reading $controlRespName]
    fileevent $controlResp readable \
	[list cwish-in-term::control-resp-handler $controlResp]

    set stdinChannelName  [fifo::create]
    set stdoutChannelName [fifo::create]
    set stderrChannelName [fifo::create]

    close stdin; fifo::for-reading $stdinChannelName
    fconfigure stdin -blocking 1

    # Have the controller side set up what it needs.
    set result [control-cmd $controlReq $controlResp \
		    [list cwish-in-term::connect-remote \
			 $controlRespName \
			 $stdinChannelName $stdoutChannelName \
			 $stderrChannelName]]
    if {"OK" ne "$result"} {
	error "Error in controller: $result"
    }

    close stdout; fifo::for-writing $stdoutChannelName
    log $stdoutChannelName delete
    file delete $stdoutChannelName
    close stderr; fifo::for-writing $stderrChannelName
    log $stderrChannelName delete
    file delete $stderrChannelName

    variable script
    variable cwish
    set env(CTK_DISPLAY) $ttyName
    trace add execution exit enter \
        [concat cwish-in-term::exit-handler $controlReq $controlResp]
    log Starting $cwish on $env(CTK_DISPLAY) ...
    if {[catch {uplevel #0 source $cwish} result]} {
	set errorcmd [list return \
			  -code error \
			  -errorinfo $::errorInfo \
			  -errorcode $::errorCode \
			  $result]
	# We need to wrap this in another `apply', otherwise it will
	# not set the globals `errorInfo' and `errorCode' in the
	# controller.
	set errorcmd [list apply [list {} $errorcmd]]
        exit-handler $controlReq $controlResp $errorcmd
    }
    log $cwish done
}

# [controller] The connect message from the program in terminal to the
# controller.  Set up all the FIFOs.
proc cwish-in-term::connect-remote {controlRespName stdinChannelName
				    stdoutChannelName stderrChannelName} {

    log connect-remote: controlResp $controlRespName stdin $stdinChannelName \
	stdout $stdoutChannelName stderr $stderrChannelName

    # This exec is somewhat wastefull if logging is disabled anyway,
    # but connecting all the FIFOs costs more time for some reason, so
    # this exec is just a minor problem.
    log [exec ls -l $controlRespName \
             $stdinChannelName $stdoutChannelName $stderrChannelName]

    set eofHandler [lambda {streamName args} {
        cwish-in-term::log EOF $streamName
    }]

    set stdoutChannel [fifo::for-reading $stdoutChannelName]
    if {[catch {
	fcopy $stdoutChannel stdout -command [concat $eofHandler stdout]
    } result]} {
	# This happens on Haiku for /dev/null.
	log "fcopy stdout: $::errorInfo"
    }

    variable stderrChannel [fifo::for-reading $stderrChannelName]
    if {[catch {
	fcopy $stderrChannel stderr -command [concat $eofHandler stderr]
    } result]} {
	# This happens on Haiku for /dev/null.
	log "fcopy stderr: $::errorInfo"
    }

    variable controlResp ;# Used by control-handler.
    set controlResp [fifo::for-writing $controlRespName]
    log $controlRespName delete
    file delete $controlRespName

    set stdinChannel [fifo::for-writing $stdinChannelName]
    log $stdinChannelName delete
    file delete $stdinChannelName
    if {[catch {
	fcopy stdin $stdinChannel -command [concat $eofHandler stdin]
    } result]} {
	# This happens on Haiku for /dev/null.
	log "fcopy stdin: $::errorInfo"
    }

    return "OK"
}

# [controller]+[in-term] Execute either `in-term' or `controller'
# depending on which side of the fence we are.
proc cwish-in-term::dispatch {} {
    variable log
    global env

    if {[info exists env(CWISH_CONTROL)]} {
        if {$log} {
            mini-log::init /tmp/cwish-in-term.log
        }
        in-term
	# We should never get here, unless our init goes wrong.
	puts stderr $::errorInfo
	exit 1
    } else {
        if {$log} {
            mini-log::init /tmp/cwish-in-term-controller.log
        }
        controller
    }
}

# [controller]+[in-term] Main.
namespace eval cwish-in-term {
    if {[catch {dispatch}]} {
        log $::errorInfo
	puts stderr $::errorInfo
        exit 1
    } else {
        exit 0
    }
}

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
