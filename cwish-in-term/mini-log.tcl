#!/usr/bin/tclsh
# ------------------------------------------------------------------------
#   mini-log.tcl
#
#   A minimal log interface just for debugging.
# ------------------------------------------------------------------------

namespace eval mini-log {
    # The open log file handle.
    variable log
}

# Create a time stamp with millisecond resolution.
proc mini-log::timestamp {} {
    set stamp [clock format [clock seconds] -format "%H:%M:%S"]
    append stamp [format ".%03d" [expr {[clock milliseconds] % 1000}]]
    return $stamp
}

# A version of the log function that actually does something.
proc mini-log::do-log {args} {
    variable log
    puts $log [concat [timestamp] {*}$args]
}

# A version of the log function does nothing.
proc mini-log::no-log {args} {
}

# Set the log file.  Disable logging if FNAME is "" or left out.  This
# should be called by the application, not by libraries.
proc mini-log::init {{fname ""}} {
    variable log
    if {[info exists log]} {
        close $log
    }
    if {"" ne "$fname"} {
        set log [open $fname a]
        fconfigure $log -buffering none
        interp alias {} ::mini-log::log {} ::mini-log::do-log
    } else {
        interp alias {} ::mini-log::log {} ::mini-log::no-log
    }
}

namespace eval mini-log {
    # Init without a log file.
    init
    # Provide a short version of the main function.
    namespace export log
}

package provide mini-log 1.0

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
