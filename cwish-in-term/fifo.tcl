# ------------------------------------------------------------------------
#	fifo.tcl
#
#	A library for using Unix FIFOs.
# ------------------------------------------------------------------------

lappend auto_path [file dirname [info script]]
package require mini-log

namespace eval fifo {
    # Milliseconds to wait in `for-writing' until we declare that it
    # failed.
    variable openwriter_timeout 5000

    namespace import ::mini-log::log
}

# fifo::is-fifo fname - Check if the file FNAME exists and that it has
# the Unix FIFO bit set.
proc fifo::is-fifo {fname} {
    if {[catch {set perms [file attributes $fname -permissions]}]} {
	return 0
    }
    set S_IFIFO 0o0010000 ;# See inode(7).
    return [expr {0 != ($S_IFIFO | $perms)}]
}

# fifo::create ?fname? - Create a FIFO as FNAME.  If FNAME is the
# empty string or not given, create a FIFO with a temp file name.
# Does not do anything, if the FIFO already exists.
proc fifo::create {{fname ""}} {
    if {"" eq "$fname"} {
	# Make a temp file, but just so that we get a nice new unique
	# name.  Close and delete it immediately, so we can re-create
	# it as a FIFO.  Do not give a directory in the template, so
	# that "file tempfile" will use the standard temp directory.
	close [file tempfile fname "tcl-fifo"]
	file delete $fname
    }
    if {! [file exists $fname]} {
	log $fname create:mkfifo
	exec mkfifo $fname
	if {! [is-fifo $fname]} {
	    error "$fname: FIFO not created by mkfifo"
	}
	return $fname
    } elseif {! [is-fifo $fname]} {
	error "$fname: Is not a FIFO"
    } else {
	log $fname create:exists
	return $fname
    }
}

# fifo::for-reading fname - Create and open FNAME as a FIFO for
# reading.  FNAME may already exist as long as it is a FIFO.  Will not
# block.
proc fifo::for-reading {fname} {
    if {"" eq "$fname"} {
	error "Call fifo::create to make a FIFO with a unique new name"
    }
    set fname [create $fname]
    log $fname read
    set fifo [open $fname {RDONLY NONBLOCK}]
    # Only the `open' should be async, from now on default to regular
    # blocking behaviour.
    fconfigure $fifo -blocking 1
    return $fifo
}

# fifo::for-writing fname - Open FNAME as a FIFO for writing.  Will
# give an error, if the reader is not connected yet.
proc fifo::for-writing {fname} {
    if {"" eq "$fname"} {
	error "Call fifo::create and fifo::for-reading to create a new FIFO"
    }
    if {! [file exists $fname]} {
	error "$fname: Does not exist"
    }
    if {! [is-fifo $fname]} {
	error "$fname: Is not a FIFO"
    }

    # At least on Linux, the FIFO is sometimes not yet ready even
    # though the reader actually thinks it *is* already connected.  It
    # seems there is some minor race in the kernel or something.  Try
    # a minor timeout before we give up.
    variable openwriter_timeout
    set limit [clock milliseconds]
    incr limit $openwriter_timeout
    set i 0
    while {true} {
	try {
	    log $fname write, try [incr i] ...
	    set fifo [open $fname {WRONLY NONBLOCK}]
	    # Only the `open' should be async, from now on default to
	    # regular blocking behaviour.
	    fconfigure $fifo -buffering none -blocking 1
	    log $fname write, try $i ... success
	    return $fifo
	} trap {POSIX ENXIO} {} {
	    # ENXIO: See man page for open(2).

	    if {[clock milliseconds] > $limit} {
		error "Not opened, no reader"
	    }
	    # The condition actually gets fixed rather soon.
	    after 50
	}
    }
}

package provide fifo 1.0

# ------------------------------------------------------------------------
#	eof
# ------------------------------------------------------------------------
