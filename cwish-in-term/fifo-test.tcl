#!/usr/bin/tclsh
# ------------------------------------------------------------------------
#	fifo-test.tcl
#
#	Example clients for fifo.tcl for manual testing and to
#	illustrate how to use the library.
# ------------------------------------------------------------------------

lappend auto_path [file dirname [info script]]
package require fifo

# Create a FIFO file FNAME.  If FNAME is the empty string, create a
# new file name, output a note on stdout and return the new file name.
# Otherwise just return the given file name.
proc make-fifo {{fname ""}} {
    set newname [fifo::create $fname]
    if {"" eq "$fname"} {
	puts "FIFO: $newname"
    }
    return $newname
}

# A simple synchronous reader tool.
proc read-test {{fname ""}} {
    set fifo [fifo::for-reading [make-fifo $fname]]
    puts "Ready to read..."
    # If there was a timeout in for-reading, we get EOF immediately.
    while {[gets $fifo line] >= 0} {
	puts "<< $line"
    }
}

# A reader tool using Tcl's async routines.
proc read-test-async {{fname ""}} {
    set fifo [fifo::for-reading [make-fifo $fname]]
    fconfigure $fifo -blocking 0
    fileevent $fifo readable [list read-async $fifo]
    puts "Ready to read..."
    # If there was a timeout in for-reading, we get EOF immediately.
    vwait readingDone
}

# A callback for Tcl's async routines for use by read-test-async.
proc read-async {fifo} {
    while {[gets $fifo line] >= 0} {
	puts "<< $line"
    }
    if {[eof $fifo]} {
	close $fifo
	set ::readingDone 1
    }
}

# A reader tool using fcopy.
proc read-test-fcopy {{fname ""}} {
    set fifo [fifo::for-reading [make-fifo $fname]]
    puts "Ready to read..."
    fcopy $fifo stdout -command exit
    vwait forever
}

# A simple synchronous writer tool.
proc write-test {fname} {
    set fifo [fifo::for-writing $fname]
    file delete $fname
    puts "Ready to write..."
    while {[gets stdin line] >= 0 && "" ne "$line"} {
	puts ">> $line"
	puts $fifo $line
    }
}

eval $argv

# ------------------------------------------------------------------------
#	eof
# ------------------------------------------------------------------------
