[//]: # (todos.md)

Todos
====

#### Unicode
* [n] ctkUnicode.c: Use Tcl\_NumUtfChars and Tcl\_UniCharAtIndex(0)
      [Tcl\_NumUtfChars is not yet what we need.  Tcl\_UniCharAtIndex
      will never be what we need]
* [x] ctkUnicode.c (*NextN): Tests for maxBytes == -1.
* [x] TkMeasureChars: Check all users of the return value re
      char/bytes.
* [ ] tkFont.c: Check users, if it would be better to pass charCounts
      instead of byteCounts or even both.  Also check what Tk does.
* [x] tkEntry.c: Selection, cursor (icursor, index).  Stuff that we
      checked:
  * strlen
  * displayString
  * numBytes
  * Routines from tkFont.c
  * Make selection work.
* [x] tkEntry.c: Cursor position at 0 -> TkMeasureChars(0).
* [ ] tkText:
  * [ ] tkTextIndex.c
  * [ ] tkText.c [Replace "Char" -> "Bytes" and check semantics]
  * [x] tkTextDisp.c
  * [x] tkTextMark.c
  * [x] tkTextBTree: Positions are passed as TkTextIndex, which come
        from TkTextGetIndex.  Also move indices via other functions
        from tkTextIndex.c.
  * [ ] tkTextBTree: TkBTreeCharsInLine is called in three places for
        line width as measured in columns.  And consequently we want
        to move to a particular column afterwards.  Maybe we want a
        particular function to go to a percentage of a line width.
  * [x] tkTextTag.c (ranges)
* [ ] Entry: Make cursor-forward and -backward use clusters.  Probably
      with Text index syntax "insert + 1c", "insert - 1c".  Maybe want
      to make the index parsing a common function between Entry and
      Text for this.  [The problem is that tkTextIndex.c uses the
      rather involved data structures of tkText.c for representing the
      text.]
* [ ] Entry, Text: Add cursor-word-forward and -word-backward.  Maybe
      want to make the index parsing a common function between Entry
      and Text for this.  Although it seems that this is done on the
      Tcl-level even in Tk.
* [ ] Add "text-path-name count".
* [ ] Add UTF-8 tests to entry.test, compare text*.test.
* [ ] Add UTF-8 cluster tests to entry.test and text*.test.
* [ ] Upcase + downcase -> Tcl_*ToUpper etc.
* [ ] Filter out control chars in the low-level drawing code.
* [x] Double-Width chars.
* [x] Check some fonts for more double-width characters in the
	  extended plane, like e.g. the Domino pieces.  See how this
	  meshes with the Curses understanding (wcwidth).
* [x] Surrogates / extended planes.  Tcl 8.7 will have reasonable
      support, which we can leverage already (Long Live Stubs!).
  * [x] Event input.
  * [x] Event generation.
  * [x] Low-level output.
  * [x] Support in TkCharNextN and whereever we call Tcl_UtfToUniChar.
* [x] Add controls (Bidi etc) to CtkCharWidth.  Check Emacs what else.
* [x] Fix underline to use Tcl units (UTF-16), not clusters.

#### Other

* [x] Fix `Tk_ExitCmd` to use Tcl_CreateThreadExitHandler, like Tk
      does.  Than we can remove the exit stuff from pkgIndex.tcl.
* [x] Support Screen in cwish-in-term.sh.
* [ ] Add ability to set the application title and/or appname (XTerm
      window title, Screen session name).
* [ ] Fix the border drawing for double border and thick border to use
      W/ACS constants.
* [ ] Implement "bracketed paste".
* [ ] Handle SIGWINCH.
* [ ] Make selection work better.
  * [ ] Better key bindings.
  * [x] Integrate with X11.
  * [ ] Handle password entries correctly.
* [x] Add a usefull .gitlab-ci.yml.
* [x] Add "make test" to the .gitlab-ci.yml.
* [ ] Why is a msgbox blank, if the string does not fit?
* [ ] Do something about the non-display of errors that happen before
      the event loop is entered and bgerror is hopefully operational.
* [ ] patchlevel.h
* [ ] Revisit the focus thing.  What does "wm deiconify" do here in
      Ctk and in X11 Tk?  A: In X11, the toplevel gets a FocusIn event
      after all the Configure events and that can presumably be used
      to set up the local focus.  On Ctk we get the FocusIn first,
      which is not really usefull for us.
* [ ] Mouse events infrastructure.
* [ ] Handle mouse clicks.
* [ ] Mouse manipulation of toplevels.  Move, Select.
* [ ] Add and use virtual events.
* [ ] Use fonts for Curses "video attributes".  (Ctk_Style,
      styleAttributes)
* [x] Test DBCS input and output.  Minimal setup is with LUIT as in
      the following command (be sure to paste a supported character,
      like e.g. 辰 U+8FB0)

			$ LANG=ja_JP.eucjp luit ./cwish.tcl library/demos/tkev.tcl

* [n] Reduce the size of the page tables by adding another level. (?)
* [ ] Review all "unsupported" routines.
* [ ] Drop use of the terms "master" and "slave" in code and in Git.
* [ ] Add color support.
* [ ] Compare all calls to Preserve/Release in Tk with ours (esp.
      tkWindow.c, wherewe have seen failing tests in this area).

----
[//]: # (eof)
