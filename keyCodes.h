/*
 * keyCodes.h (Ctk) --
 *
 *	This file defines the mapping from curses key codes to
 *	to X11 keysyms and modifier masks.
 *
 * Copyright (c) 1995 Cleveland Clinic Foundation
 *
 * See the file "license.terms" for information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 */

    { 0001, 0x0061, ControlMask },	/* Control-A */
    { 0002, 0x0062, ControlMask },	/* Control-B */
    { 0003, 0x0063, ControlMask },	/* Control-C */
    { 0004, 0x0064, ControlMask },	/* Control-D */
    { 0005, 0x0065, ControlMask },	/* Control-E */
    { 0006, 0x0066, ControlMask },	/* Control-F */
    { 0007, 0x0067, ControlMask },	/* Control-G */
    { 0010, 0xFF08, 0 },		/* Backspace (Control-H) */
    { 0011, 0xFF09, 0 },		/* Tab (Control-I) */
    { 0012, 0x006A, ControlMask },	/* Control-J */
    { 0013, 0x006B, ControlMask },	/* Control-K */
    { 0014, 0x006C, ControlMask },	/* Control-L */
    { 0015, 0xFF0D, 0 },		/* Carriage Return (Control-M) */
    { 0016, 0x006E, ControlMask },	/* Control-N */
    { 0017, 0x006F, ControlMask },	/* Control-O */
    { 0020, 0x0070, ControlMask },	/* Control-P */
    { 0021, 0x0071, ControlMask },	/* Control-Q */
    { 0022, 0x0072, ControlMask },	/* Control-R */
    { 0023, 0x0073, ControlMask },	/* Control-S */
    { 0024, 0x0074, ControlMask },	/* Control-T */
    { 0025, 0x0075, ControlMask },	/* Control-U */
    { 0026, 0x0076, ControlMask },	/* Control-V */
    { 0027, 0x0077, ControlMask },	/* Control-W */
    { 0030, 0x0078, ControlMask },	/* Control-X */
    { 0031, 0x0079, ControlMask },	/* Control-Y */
    { 0032, 0x007A, ControlMask },	/* Control-Z */
    { 0033, 0xFF1B, 0 },		/* Escape (deprecated) */
    { 0033, 0xFF1B, 0 },		/* Escape (deprecated) */
    /*
     * On a some current terminals the backspace character generates DEL (0x7F)
     * instead of BS (0x08), but the terminfo entry often does not have that
     * mapping (kterm, xterm on BSDs).
     */
    { 0177, 0xFF08, 0 },		/* DEL -> Backspace */
#ifdef KEY_BREAK
    { KEY_BREAK, 0xFF6B, 0 },		/* Break key (unreliable) */
#endif
#ifdef KEY_DOWN
    { KEY_DOWN, 0xFF54, 0 },		/* Down */
#endif
#ifdef KEY_UP
    { KEY_UP, 0xFF52, 0 },		/* Up */
#endif
#ifdef KEY_LEFT
    { KEY_LEFT, 0xFF51, 0 },		/* Left */
#endif
#ifdef KEY_RIGHT
    { KEY_RIGHT, 0xFF53, 0 },		/* Right */
#endif
#ifdef KEY_HOME
    { KEY_HOME, 0xFF50, 0 },		/* Home key (upward+left arrow) */
#endif
#ifdef KEY_BACKSPACE
    { KEY_BACKSPACE, 0xFF08, 0 },	/* backspace (unreliable) */
#endif
#ifdef KEY_F
    { KEY_F(1), 0xFFBE, 0 },		/* F1 */
    { KEY_F(2), 0xFFBF, 0 },		/* F2 */
    { KEY_F(3), 0xFFC0, 0 },		/* F3 */
    { KEY_F(4), 0xFFC1, 0 },		/* F4 */
    { KEY_F(5), 0xFFC2, 0 },		/* F5 */
    { KEY_F(6), 0xFFC3, 0 },		/* F6 */
    { KEY_F(7), 0xFFC4, 0 },		/* F7 */
    { KEY_F(8), 0xFFC5, 0 },		/* F8 */
    { KEY_F(9), 0xFFC6, 0 },		/* F9 */
    { KEY_F(10), 0xFFC7, 0 },		/* F10 */
    { KEY_F(11), 0xFFC8, 0 },		/* F11 */
    { KEY_F(12), 0xFFC9, 0 },		/* F12 */

    /*
     * On a standard PC keyboard, Ncursesw makes these mappings.
     */
    { KEY_F(13), 0xFFBE, ShiftMask },	/* F1 */
    { KEY_F(14), 0xFFBF, ShiftMask },	/* F2 */
    { KEY_F(15), 0xFFC0, ShiftMask },	/* F3 */
    { KEY_F(16), 0xFFC1, ShiftMask },	/* F4 */
    { KEY_F(17), 0xFFC2, ShiftMask },	/* F5 */
    { KEY_F(18), 0xFFC3, ShiftMask },	/* F6 */
    { KEY_F(19), 0xFFC4, ShiftMask },	/* F7 */
    { KEY_F(20), 0xFFC5, ShiftMask },	/* F8 */
    { KEY_F(21), 0xFFC6, ShiftMask },	/* F9 */
    { KEY_F(22), 0xFFC7, ShiftMask },	/* F10 */
    { KEY_F(23), 0xFFC8, ShiftMask },	/* F11 */
    { KEY_F(24), 0xFFC9, ShiftMask },	/* F12 */

    { KEY_F(25), 0xFFBE, ControlMask },	/* F1 */
    { KEY_F(26), 0xFFBF, ControlMask },	/* F2 */
    { KEY_F(27), 0xFFC0, ControlMask },	/* F3 */
    { KEY_F(28), 0xFFC1, ControlMask },	/* F4 */
    { KEY_F(29), 0xFFC2, ControlMask },	/* F5 */
    { KEY_F(30), 0xFFC3, ControlMask },	/* F6 */
    { KEY_F(31), 0xFFC4, ControlMask },	/* F7 */
    { KEY_F(32), 0xFFC5, ControlMask },	/* F8 */
    { KEY_F(33), 0xFFC6, ControlMask },	/* F9 */
    { KEY_F(34), 0xFFC7, ControlMask },	/* F10 */
    { KEY_F(35), 0xFFC8, ControlMask },	/* F11 */
    { KEY_F(36), 0xFFC9, ControlMask },	/* F12 */

    { KEY_F(37), 0xFFBE, ShiftMask|ControlMask },	/* F1 */
    { KEY_F(38), 0xFFBF, ShiftMask|ControlMask },	/* F2 */
    { KEY_F(39), 0xFFC0, ShiftMask|ControlMask },	/* F3 */
    { KEY_F(40), 0xFFC1, ShiftMask|ControlMask },	/* F4 */
    { KEY_F(41), 0xFFC2, ShiftMask|ControlMask },	/* F5 */
    { KEY_F(42), 0xFFC3, ShiftMask|ControlMask },	/* F6 */
    { KEY_F(43), 0xFFC4, ShiftMask|ControlMask },	/* F7 */
    { KEY_F(44), 0xFFC5, ShiftMask|ControlMask },	/* F8 */
    { KEY_F(45), 0xFFC6, ShiftMask|ControlMask },	/* F9 */
    { KEY_F(46), 0xFFC7, ShiftMask|ControlMask },	/* F10 */
    { KEY_F(47), 0xFFC8, ShiftMask|ControlMask },	/* F11 */
    { KEY_F(48), 0xFFC9, ShiftMask|ControlMask },	/* F12 */

    { KEY_F(49), 0xFFBE, Mod1Mask },	/* F1 */
    { KEY_F(50), 0xFFBF, Mod1Mask },	/* F2 */
    { KEY_F(51), 0xFFC0, Mod1Mask },	/* F3 */
    { KEY_F(52), 0xFFC1, Mod1Mask },	/* F4 */
    { KEY_F(53), 0xFFC2, Mod1Mask },	/* F5 */
    { KEY_F(54), 0xFFC3, Mod1Mask },	/* F6 */
    { KEY_F(55), 0xFFC4, Mod1Mask },	/* F7 */
    { KEY_F(56), 0xFFC5, Mod1Mask },	/* F8 */
    { KEY_F(57), 0xFFC6, Mod1Mask },	/* F9 */
    { KEY_F(58), 0xFFC7, Mod1Mask },	/* F10 */
    { KEY_F(59), 0xFFC8, Mod1Mask },	/* F11 */
    { KEY_F(60), 0xFFC9, Mod1Mask },	/* F12 */
#endif
#ifdef KEY_DL
    { KEY_DL, 0xFFFF, 0 },		/* Delete line */
#endif
#ifdef KEY_IL
    { KEY_IL, 0xFF63, 0 },		/* Insert line */
#endif
#ifdef KEY_DC
    { KEY_DC, 0xFFFF, 0 },		/* Delete character */
#endif
#ifdef KEY_IC
    { KEY_IC, 0xFF63, 0 },		/* Insert character/mode */
#endif
#ifdef KEY_EIC
    { KEY_EIC, 0xFF63, 0 },		/* Exit insert mode */
#endif
#ifdef KEY_CLEAR
    { KEY_CLEAR, 0xFF0B, 0 },		/* Clear screen */
#endif
#ifdef KEY_NPAGE
    { KEY_NPAGE, 0xFF56, 0 },		/* Next page */
#endif
#ifdef KEY_PPAGE
    { KEY_PPAGE, 0xFF55, 0 },		/* Previous page */
#endif
#ifdef KEY_ENTER
    { KEY_ENTER, 0xFF8D, 0 },		/* Enter or send (unreliable) */
#endif
#ifdef KEY_PRINT
    { KEY_PRINT, 0xFF61, 0 },		/* Print or copy */
#endif
#ifdef KEY_LL
    { KEY_LL, 0xFF57, ControlMask },	/* home down or bottom (lower left) */
#endif
#ifdef KEY_BTAB
    { KEY_BTAB, 0xFF09, ShiftMask },	/* Back tab */
#endif
#ifdef KEY_BEG
    { KEY_BEG, 0xFF58, 0 },		/* beg(inning) key */
#endif
#ifdef KEY_CANCEL
    { KEY_CANCEL, 0xFF69, 0 },		/* cancel key */
#endif
#ifdef KEY_COMMAND
    { KEY_COMMAND, 0xFF62, 0 },		/* cmd (command) key */
#endif
#ifdef KEY_END
    { KEY_END, 0xFF57, 0 },		/* End key */
#endif
#ifdef KEY_FIND
    { KEY_FIND, 0xFF68, 0 },		/* Find key */
#endif
#ifdef KEY_HELP
    { KEY_HELP, 0xFF6A, 0 },		/* Help key */
#endif
#ifdef KEY_NEXT
    { KEY_NEXT, 0xFF09, 0 },		/* Next object key */
#endif
#ifdef KEY_OPTIONS
    { KEY_OPTIONS, 0xFF67, 0 },		/* Options key */
#endif
#ifdef KEY_PREVIOUS
    { KEY_PREVIOUS, 0xFF09, ShiftMask },/* Previous object key */
#endif
#ifdef KEY_REDO
    { KEY_REDO, 0xFF66, 0 },		/* Redo key */
#endif
#ifdef KEY_SELECT
    { KEY_SELECT, 0xFF60, 0 },		/* Select key */
#endif
#ifdef KEY_SUSPEND
    { KEY_SUSPEND, 0xFF13, 0 },		/* Suspend key */
#endif
#ifdef KEY_UNDO
    { KEY_UNDO, 0xFF65, 0 },		/* Undo key */
#endif


#ifdef KEY_SLEFT
    { KEY_SLEFT, 0xFF51, ShiftMask },	/* Left */
#endif
#ifdef KEY_SRIGHT
    { KEY_SRIGHT, 0xFF53, ShiftMask },	/* Right */
#endif
#ifdef KEY_SHOME
    { KEY_SHOME, 0xFF50, ShiftMask },	/* Home key (upward+left arrow) */
#endif
#ifdef KEY_SDL
    { KEY_SDL, 0xFFFF, ShiftMask },	/* Delete line */
#endif
#ifdef KEY_SIL
    { KEY_SIL, 0xFF63, ShiftMask },	/* Insert line */
#endif
#ifdef KEY_SDC
    { KEY_SDC, 0xFFFF, ShiftMask },	/* Delete character */
#endif
#ifdef KEY_SPRINT
    { KEY_PRINT, 0xFF61, ShiftMask },	/* Print or copy */
#endif
#ifdef KEY_SBEG
    { KEY_SBEG, 0xFF58, ShiftMask },	/* beg(inning) key */
#endif
#ifdef KEY_SCANCEL
    { KEY_SCANCEL, 0xFF69, ShiftMask },	/* cancel key */
#endif
#ifdef KEY_SCOMMAND
    { KEY_SCOMMAND, 0xFF62, ShiftMask },/* cmd (command) key */
#endif
#ifdef KEY_SEND
    { KEY_SEND, 0xFF57, ShiftMask },	/* End key */
#endif
#ifdef KEY_SFIND
    { KEY_SFIND, 0xFF68, ShiftMask },	/* Find key */
#endif
#ifdef KEY_SHELP
    { KEY_SHELP, 0xFF6A, ShiftMask },	/* Help key */
#endif
#ifdef KEY_SNEXT
    { KEY_SNEXT, 0xFF56, ShiftMask },	/* Next object key */
#endif
#ifdef KEY_SOPTIONS
    { KEY_SOPTIONS, 0xFF67, ShiftMask },/* Options key */
#endif
#ifdef KEY_SPREVIOUS
    { KEY_SPREVIOUS, 0xFF55, ShiftMask },/* Previous object key */
#endif
#ifdef KEY_SREDO
    { KEY_SREDO, 0xFF66, ShiftMask },	/* Redo key */
#endif
#ifdef KEY_SSUSPEND
    { KEY_SSUSPEND, 0xFF13, ShiftMask },/* Suspend key */
#endif
#ifdef KEY_SUNDO
    { KEY_SUNDO, 0xFF65, ShiftMask },	/* Undo key */
#endif

#ifdef KEY_SR
    { KEY_SR, 0xFF52, ShiftMask },	/* Up (Scroll back) */
#endif
#ifdef KEY_SF
    { KEY_SF, 0xFF54, ShiftMask },	/* Down (Scroll forward) */
#endif
