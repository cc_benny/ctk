# all.tcl --
#
# This file contains a top-level script to run all of the Tk
# tests.  Execute it by invoking "source all.tcl" when running tktest
# in this directory.
#
# Copyright © 1998-1999 by Scriptics Corporation.
#
# See the file "license.terms" for information on usage and redistribution
# of this file, and for a DISCLAIMER OF ALL WARRANTIES.

encoding system utf-8

# This is the Ctk test suite; fail early if no Ctk!
set tkpkg Ctk
catch {set tkpkg $::env(PKG_UNDER_TEST)}
package require $tkpkg

package require tcltest 2.2
tcltest::configure {*}$argv
tcltest::configure -testdir [file normalize [file dirname [info script]]]
tcltest::configure -load "
	source [file join [tcltest::testsDirectory] constraints.tcl]
	source [file join [tcltest::testsDirectory] prompt.tcl]
"
tcltest::configure -singleproc 1

# Hook into 8.6 tcltest to find out what happened by using its
# master/slave hook.
set failed 0
proc tcltest::ReportToMaster {total passed skipped failed args} {
    set ::failed $failed
}
# If we are compiled with TCL_MEM_DEBUG, use that.
catch {memory init on}
tcltest::runAllTests

# Exit before Ctk starts its default event loop.  Return 1 if any
# tests failed, 0 otherwise.
exit [expr {$failed > 0}]
