# -*- mode: tcl -*-
# ------------------------------------------------------------------------
#   ctkUnicode.test
# ------------------------------------------------------------------------

package require tcltest 2.2
namespace import ::tcltest::*

set caps [ctk capabilities]
set supplCharSize [dict get $caps supplCharSize]
testConstraint supplchars [expr {[dict get $caps supplCharMode] != "none"}]
testConstraint supplchars1 [expr {$supplCharSize == 1}]
testConstraint supplchars2 [expr {$supplCharSize == 2}]
testConstraint tclOver869 [expr {
    [package vcompare [info patchlevel] 8.6.9] > 0
}]
testConstraint tclFixedStringLength [expr {
    [package vcompare [info patchlevel] 8.7a5] >= 0 || $supplCharSize == 1
}]
# Why does "!tclFixedStringLength" not work in combination with other
# constraints?
testConstraint notTclFixedStringLength [expr {
	![testConstraint tclFixedStringLength]
}]
testConstraint tclHasCompatCode [expr {
    [package vcompare [info patchlevel] 9.0a0] < 0
}]

# Do not print debug info, if verbose contains start, which may imply
# that the caller is interested in detailed output and may get
# confused by additional output.  E.g. this applies to the tclunit
# parser.
testConstraint printDebug [expr {-1 == [lsearch -exact [verbose] start]}]

# Evaluate $argv after establishing the default value for
# testContraint `supplchars', so that we can override it.
eval tcltest::configure $argv
tcltest::loadTestedCommands
tk::test::update-constraints

# hex s - Format the string S as a hex dump.
proc hex {s} {
    lmap ch [split $s ""] {
        format "%02X" [scan $ch "%c"]
    }
}

# l2hex l1 l2 - Format L1 and L2 as a list, with L2 converted to a hex
# dump.
proc l2hex {l1 l2} {
    list $l1 [hex $l2]
}

set catFaceCodepoint 0x1F639
set catFaceUtf8 "\xF0\x9F\x98\xB9"
set catFaceUtf8Surrogates "\xED\xA0\xBD\xED\xB8\xB9"
if { "$tcl_platform(byteOrder)" == "littleEndian" } {
    set catFaceUtf16 "\x3D\xD8\x39\xDE"
} else {
    set catFaceUtf16 "\xD8\x3D\xDE\x39"
}

set withSkintones \
    "\xF0\x9F\x91\x8F\xF0\x9F\x8F\xBF\
     \xF0\x9F\x91\x90\xF0\x9F\x8F\xBF\
     \xF0\x9F\x91\x8A\xF0\x9F\x8F\xBF"

# A string with all kinds of specialties, with digits between that can
# be used as anchors for seeing where we are.
#
#  "0"                     /* ASCII delimiters/markers. */
#  "\xC3\xA4"         "1"  /* Non-cluster, half-width (Latin-1).  */
#  "\x61" "\xCC\x8B"  "2"  /* Cluster.  */
#  "\xE3\x90\x80"     "3"  /* Double-width.  */
#  "\xE3\x90\x81"     "4"  /* Double-width.  */
#  "\xF0\x9F\x98\xB9" "5"  /* Suppl. plane, double-Width.  */

set testString \
    [string cat \
     "0"                   \
     "\xC3\xA4"        "1" \
     "\x61" "\xCC\x8B" "2" \
     "\xE3\x90\x80"    "3" \
     "\xE3\x90\x81"    "4" \
     $catFaceUtf8      "5"]

test ctkUnicode-0 {Configuration} printDebug {
    puts ""
    puts "Current Tcl Version: [info patchlevel]"

    puts ""
    parray tcl_platform

    puts ""
    array set ctk_config $caps
    if {"$ctk_config(supplCharMode)" eq "none"} {
        # supplCharSize is useless if there are no supplChars, so
        # delete it to avoid questions.
        unset ctk_config(supplCharSize)
    }
    parray ctk_config
} ""

test ctkUnicode-1.1 {is-ascii: Space is ASCII} {
    ctk_testunicode isascii [scan " " %c]
} 1

test ctkUnicode-1.2 {is-ascii: 'A' is ASCII} {
    ctk_testunicode isascii [scan "A" %c]
} 1

test ctkUnicode-1.3 {is-ascii: Latin-1 is not ASCII} {
    ctk_testunicode isascii 0x00E4
} 0

test ctkUnicode-1.4 {is-ascii: Random Unicode is not ASCII} {
    ctk_testunicode isascii 0x0540
} 0

test ctkUnicode-1.5 {is-ascii: Suppl. plane is not ASCII} {
    ctk_testunicode isascii 0x10064
} 0

test ctkUnicode-2.1 {is-lead-word: 'A' is not a lead word} {
    tclHasCompatCode
} {
    ctk_testunicode isleadword [scan "A" %c]
} 0

test ctkUnicode-2.2 {is-lead-word: Random Unicode is not a lead word} {
    tclHasCompatCode
} {
    ctk_testunicode isleadword 0x0540
} 0

test ctkUnicode-2.3 {is-lead-word: Suppl. plane is not a lead word} {
    tclHasCompatCode
} {
    ctk_testunicode isleadword 0x10064
} 0

test ctkUnicode-2.4 {is-lead-word: Suppl. plane is not a lead word} {
    tclHasCompatCode
} {
    ctk_testunicode isleadword 0x1D800
} 0

test ctkUnicode-2.5 {is-lead-word: U+D800 is lead word} {
    tclHasCompatCode
} {
    ctk_testunicode isleadword 0xD800
} 1

test ctkUnicode-2.6 {is-lead-word: U+DBFF is lead word} {
    tclHasCompatCode
} {
    ctk_testunicode isleadword 0xDBFF
} 1

test ctkUnicode-2.7 {is-lead-word: U+DC00 is not a lead word} {
    tclHasCompatCode
} {
    ctk_testunicode isleadword 0xDC00
} 0

test ctkUnicode-3.1 {utf-to-codepoint: Simple} supplchars {
    l2hex {*}[ctk_testunicode utftocodepoint $catFaceUtf8]
} [list $catFaceCodepoint {}]

test ctkUnicode-3.1-surrogates {utf-to-codepoint: Simple} supplchars2 {
    l2hex {*}[ctk_testunicode utftocodepoint $catFaceUtf8Surrogates]
} [list $catFaceCodepoint {}]

test ctkUnicode-3.2 {utf-to-codepoint: With following 'a'} supplchars {
    l2hex {*}[ctk_testunicode utftocodepoint ${catFaceUtf8}a]
} [list $catFaceCodepoint [hex a]]

test ctkUnicode-3.3 {utf-to-codepoint: ASCII} {
    ctk_testunicode utftocodepoint a
} [list [format 0x%04X [scan a %c]] {}]

test ctkUnicode-4.1-full {codepoint-to-utf: Simple} supplchars1 {
    hex [ctk_testunicode codepointtoutf $catFaceCodepoint]
} [hex $catFaceUtf8]

# Conversion from a codepoint to UTF-8 can result in correct UTF-8 or
# in the internal Tcl UTF-8 format with surrogate pairs.  We can
# handle both.  Output to the outside world should always use an
# encoding and that should fix it if necessary.
test ctkUnicode-4.1-surrogates {codepoint-to-utf: Simple} -constraints {
    supplchars2
} -body {
    hex [ctk_testunicode codepointtoutf $catFaceCodepoint]
} -match regexp -result [hex $catFaceUtf8]|[hex $catFaceUtf8Surrogates]

test ctkUnicode-4.2 {codepoint-to-utf: ASCII} {
    ctk_testunicode codepointtoutf [scan a %c]
} a

test ctkUnicode-5.1 {char-width: ASCII} {
    ctk_testunicode charwidth [scan A %c]
} 1

test ctkUnicode-5.2 {char-width: Space} {
    ctk_testunicode charwidth [scan " " %c]
} 1

test ctkUnicode-5.3 {char-width: Latin-1} {
    ctk_testunicode charwidth 0x00E4
} 1

test ctkUnicode-5.4 {char-width: Modifier} {
    ctk_testunicode charwidth 0x030B
} 0

test ctkUnicode-5.5 {char-width: CJK} {
    ctk_testunicode charwidth 0x3400
} 2

test ctkUnicode-5.6 {char-width: Emoji} {
    ctk_testunicode charwidth 0x1F639
} 2

test ctkUnicode-5.7 {char-width: Domino Tile} knownBug {
    ctk_testunicode charwidth 0x1F035
} 2

test ctkUnicode-5.8 {char-width: Chinese chess} {
    ctk_testunicode charwidth 0x1FA60
} 2

test ctkUnicode-5.9 {char-width: Surrogate pair trail} {
    ctk_testunicode charwidth 0xDC01
} 0

test ctkUnicode-5.10 {char-width: Emoji skintone modifier} {
    ctk_testunicode charwidth 0x1F3FF
} 0

test ctkUnicode-5.11 {char-width: Emoji variation selector #16} {
    ctk_testunicode charwidth 0xFE0F ;# VS-16
} 0

test ctkUnicode-5.12 {char-width: Emoji variation selector #256} {
    ctk_testunicode charwidth 0xE01EF ;# VS-256
} 0

# Run the same character width cases through the runtime library.
# `wcwidth' is what NCurses uses.

test ctkUnicode-5.1rtl {wcwidth: ASCII} {
    ctk_testunicode wcwidth [scan A %c]
} 1

test ctkUnicode-5.2rtl {wcwidth: Space} {
    ctk_testunicode wcwidth [scan " " %c]
} 1

test ctkUnicode-5.3rtl {wcwidth: Latin-1} {
    ctk_testunicode wcwidth 0x00E4
} 1

test ctkUnicode-5.4rtl {wcwidth: Modifier} {
    ctk_testunicode wcwidth 0x030B
} 0

test ctkUnicode-5.5rtl {wcwidth: CJK} {
    ctk_testunicode wcwidth 0x3400
} 2

test ctkUnicode-5.6rtl {wcwidth: Emoji} {!freebsd && !haiku && !netbsd && !sunos} {
    ctk_testunicode wcwidth 0x1F639
} 2

test ctkUnicode-5.7rtl {wcwidth: Domino Tile} knownBug {
    ctk_testunicode wcwidth 0x1F035
} 2

# This does not work with glibc 2.28.
test ctkUnicode-5.8rtl {wcwidth: Chinese chess} {!darwin && !freebsd && !haiku && !linux && !netbsd && !sunos} {
    ctk_testunicode wcwidth 0x1FA60
} 2

# Having this return 0 is a special feature of our own routines, so we
# are not surprised that wcwidth doesn't do this.
test ctkUnicode-5.9rtl {wcwidth: Surrogate pair trail} notSupported {
    ctk_testunicode wcwidth 0xDC01
} 0

# This does not work with glibc 2.28.  The story is more complicated
# than what we implement ourself though, the rules for this are not
# simply black and white, 0 or 1.
test ctkUnicode-5.10rtl {wcwidth: Emoji skintone modifier} {!darwin && !freebsd && !haiku && !linux && !netbsd && !sunos} {
    ctk_testunicode wcwidth 0x1F3FF
} 0

test ctkUnicode-5.11rtl {wcwidth: Emoji variation selector #16} {!darwin && !netbsd} {
    ctk_testunicode wcwidth 0xFE0F ;# VS-16
} 0

# !alpine: Actually Musl is the one that is not up-todate here.
test ctkUnicode-5.12rtl {wcwidth: Emoji variation selector #256} {!darwin && !freebsd && !netbsd && !sunos && !alpine} {
    ctk_testunicode wcwidth 0xE01EF ;# VS-256
} 0

test ctkUnicode-5.13-native {string-width: With skintones} tclFixedStringLength {
    set s [encoding convertfrom utf-8 $withSkintones]
    list [string length $s] [ctk_columncount $s]
} {8 8}

test ctkUnicode-5.13-emulated {string-width: With skintones} {
	supplchars notTclFixedStringLength
} {
    set s [encoding convertfrom utf-8 $withSkintones]
    list [string length $s] [ctk_columncount $s]
} {14 8}

test ctkUnicode-5.14 {string-width: Katakana with mark} {
    set s \u30DA\u30DA\u3099
    list [string length $s] [ctk_columncount $s]
} {3 4}

test ctkUnicode-5b.0 {byte-type: ASCII} tclHasCompatCode {
    list [ctk_testunicode utfbytetype 0x00] \
         [ctk_testunicode utfbytetype 0x7F]
} {1 1}
test ctkUnicode-5b.1 {byte-type: Trail bytes} tclHasCompatCode {
    list [ctk_testunicode utfbytetype 0b10000000] \
         [ctk_testunicode utfbytetype 0b10111111]
} {0 0}
test ctkUnicode-5b.2 {byte-type: Lead bytes} tclHasCompatCode {
    list [ctk_testunicode utfbytetype 0b11000000] \
         [ctk_testunicode utfbytetype 0b11011111] \
         [ctk_testunicode utfbytetype 0b11100000] \
         [ctk_testunicode utfbytetype 0b11101111] \
         [ctk_testunicode utfbytetype 0b11110000] \
         [ctk_testunicode utfbytetype 0b11110111]
} {2 2 3 3 4 4}

test ctkUnicode-6.1-native {char-next: Suppl. plane split} supplchars1 {
    list [string length [ctk_testunicode charnext $catFaceUtf8 -1 1]] \
        [string length [ctk_testunicode charnext $catFaceUtf8 -1 2]] \
        [string length [ctk_testunicode charnext $catFaceUtf8 -1 3]]
} {0 0 0}

test ctkUnicode-6.1-emulated {char-next: Suppl. plane split} {
    supplchars2 tclOver869
} {
    list [string length [ctk_testunicode charnext $catFaceUtf8 -1 1]] \
        [string length [ctk_testunicode charnext $catFaceUtf8 -1 2]] \
        [string length [ctk_testunicode charnext $catFaceUtf8 -1 3]]
} {3 0 0}

test ctkUnicode-6.2-native {char-next: Suppl. plane split with following 'a'} \
    supplchars1 {
    list [string length [ctk_testunicode charnext ${catFaceUtf8}a -1 1]] \
        [string length [ctk_testunicode charnext ${catFaceUtf8}a -1 2]] \
        [string length [ctk_testunicode charnext ${catFaceUtf8}a -1 3]]
} {1 0 0}

test ctkUnicode-6.2-emulated {char-next: Extended Unicode split with following 'a'} {
    supplchars2 tclOver869
} {
    list [string length [ctk_testunicode charnext ${catFaceUtf8}a -1 1]] \
        [string length [ctk_testunicode charnext ${catFaceUtf8}a -1 2]] \
        [string length [ctk_testunicode charnext ${catFaceUtf8}a -1 3]]
} {4 1 0}

# Measurement functions:
#
# * char-next - The Tcl dimension.
# * cluster-next - The human dimension.
# * column-next - The terminal dimension.

set length [string length $testString]
test ctkUnicode-7.1-char {char-next: With limit} {
    set rest [ctk_testunicode charnext $testString $length 9]
    string index $rest 0
} "4"
test ctkUnicode-7.1-cluster {cluster-next: With limit} {
    set rest [ctk_testunicode clusternext $testString $length 8]
    string index $rest 0
} "4"
test ctkUnicode-7.1-column {column-next: With limit} {
    set rest [ctk_testunicode columnnext $testString $length 10]
    string index $rest 0
} "4"

incr length -10
test ctkUnicode-7.2-char {char-next: With short limit} {
    set rest [ctk_testunicode charnext $testString $length 9]
    string index $rest 0
} "3"
test ctkUnicode-7.2-cluster {cluster-next: With short limit} {
    set rest [ctk_testunicode clusternext $testString $length 8]
    string index $rest 0
} "3"
test ctkUnicode-7.2-column {column-next: With short limit} {
    set rest [ctk_testunicode columnnext $testString $length 10]
    string index $rest 0
} "3"

set length -1
test ctkUnicode-7.3-char {char-next: Unlimited} {
    set rest [ctk_testunicode charnext $testString $length 9]
    string index $rest 0
} "4"
test ctkUnicode-7.3-cluster {cluster-next: Unlimited} {
    set rest [ctk_testunicode clusternext $testString $length 8]
    string index $rest 0
} "4"
test ctkUnicode-7.3-column {column-next: Unlimited} {
    set rest [ctk_testunicode columnnext $testString $length 10]
    string index $rest 0
} "4"

test ctkUnicode-7.4-char {char-next: End of string} {
    set rest [ctk_testunicode charnext $testString $length 100]
    string index $rest 0
} ""
test ctkUnicode-7.4-cluster {cluster-next: End of string} {
    set rest [ctk_testunicode clusternext $testString $length 100]
    string index $rest 0
} ""
test ctkUnicode-7.4-column {column-next: End of string} {
    set rest [ctk_testunicode columnnext $testString $length 100]
    string index $rest 0
} ""

set length [string length $testString]
test ctkUnicode-7.5-char {char-next: Suppl. plane with limit} supplchars {
    set rest [ctk_testunicode charnext $testString $length [expr {10 + $supplCharSize}]]
    string index $rest 0
} "5"
test ctkUnicode-7.5-cluster {cluster-next: Suppl. plane with limit} supplchars {
    set rest [ctk_testunicode clusternext $testString $length 10]
    string index $rest 0
} "5"
test ctkUnicode-7.5-column {column-next: Suppl. plane with limit} supplchars {
    set rest [ctk_testunicode columnnext $testString $length 13]
    string index $rest 0
} "5"

set length -1
test ctkUnicode-7.6-char {char-next: Suppl. plane unlimited} supplchars {
    set rest [ctk_testunicode charnext $testString $length \
                  [expr {10 + $supplCharSize}]]
    string index $rest 0
} "5"
test ctkUnicode-7.6-cluster {cluster-next: Suppl. plane unlimited} supplchars {
    set rest [ctk_testunicode clusternext $testString $length 10]
    string index $rest 0
} "5"
test ctkUnicode-7.6-column {column-next: Suppl. plane unlimited} supplchars {
    set rest [ctk_testunicode columnnext $testString $length 13]
    string index $rest 0
} "5"

set clusterStart 4
set clusterEnd 7
set clusterFirst "a"
test ctkUnicode-8.1 {cluster-prev: Simple} {
    set rest [ctk_testunicode clusterprev $testString $clusterEnd 1]
    string index $rest 0
} $clusterFirst
test ctkUnicode-8.2 {cluster-prev: At start} {
    set rest [ctk_testunicode clusterprev $testString 0 0]
    string index $rest 0
} "0"
test ctkUnicode-8.3 {cluster-prev: From the start} {
    set rest [ctk_testunicode clusterprev $testString $clusterStart 0]
    string index $rest 0
} $clusterFirst
test ctkUnicode-8.4 {cluster-prev: From the middle} {
    set rest [ctk_testunicode clusterprev $testString [expr {$clusterStart + 1}] 0]
    string index $rest 0
} $clusterFirst

test ctkUnicode-8.5 {cluster-prev: From the middle of a surrogate pair} {
    # set catFaceUtf8Surrogates "\xED\xA0\xBD\xED\xB8\xB9"
    set rest [ctk_testunicode clusterprev $catFaceUtf8Surrogates 3 0]
    hex $rest
} [hex $catFaceUtf8Surrogates]

test ctkUnicode-8.6 {cluster-next: From inside suppl. plane} {
    supplchars tclOver869
} {
    set rest [ctk_testunicode charnext ${catFaceUtf8}a -1 1]
    set rest [ctk_testunicode clusternext $rest -1 1]
    hex $rest
} ""
test ctkUnicode-8.7 {cluster-prev: From inside suppl. plane} supplchars2 {
    set string a$catFaceUtf8
    # Move into the suppl. plane char.
    set rest [ctk_testunicode charnext $string -1 2]
    # Where are we?
    set restOff [expr [string length $string] - [string length $rest]]
    # Adjust.
    set rest [ctk_testunicode clusterprev $string $restOff 0]
    hex $rest
} [hex $catFaceUtf8]

test ctkUnicode-9.1-good {encoding: Suppl. plane string length} {
	tclFixedStringLength
} {
    set tclstring [encoding convertfrom utf-8 $catFaceUtf8]
    string length $tclstring
} 1

test ctkUnicode-9.1-bad {encoding: Suppl. plane string length} {
	notTclFixedStringLength
} {
    set tclstring [encoding convertfrom utf-8 $catFaceUtf8]
    string length $tclstring
} $supplCharSize

# tclOver869: Encoding suppl. chars is just buggy here, actually until
# 8.6.11.
test ctkUnicode-9.2 {encoding: Suppl. plane UTF-16} {
    supplchars tclOver869
} {
    set tclstring [encoding convertfrom utf-8 $catFaceUtf8]
    set utf16 [encoding convertto unicode $tclstring]
    hex $utf16
} [hex $catFaceUtf16]

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
