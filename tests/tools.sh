#!/bin/bash
# ------------------------------------------------------------------------
#   tools.sh
#
#   Some functions to analyse how far we got.
# ------------------------------------------------------------------------

set -e # Exit on error.

# run-tk-tests test-options... - Run tests in Tk.  TEST-OPTIONS are
# the options documented in `man tcltest'.
function run-tk-tests () {
    local wrap=rlwrap
    if [ "$TERM" = "dumb" ]; then
       wrap=
    fi
    PKG_UNDER_TEST=Tk $wrap wish all.tcl "$@"
}

# run-ctk-tests test-options... - Run tests in Ctk.  TEST-OPTIONS are
# the options documented in `man tcltest'.
function run-ctk-tests () {
    ../cwish-in-term.tcl all.tcl "$@"
}

# get-log tests... - Run test files, run each test file on its own, so
# we get statistics for each file individually.  TESTS defaults to all
# test files.
function get-log () {
    local f
    for f in ${@:-*.test}; do
	CWISH_TERM=tmux LINES=25 COLUMNS=80 \
	    run-ctk-tests -file $f -constraint noPrompt || true
    done
}

# grep-status - Run the result of `get-log' through a filter that
# picks out the statistics, accumulates and sorts them.
function grep-status () {
    gawk '
	/^Tests began at / {
	    getline testfile;
	}
	NF == 9 && $2 == "Total" {
	    total   += $3;
	    passed  += $5;
	    skipped += $7;
	    failed  += $9;

	    sortidx = $9;
	    gsub(/^[^:]*:\t/, "");
	    testfile = sprintf("%-16s", testfile ":");
	    sort[sortidx] = sort[sortidx] testfile "\t" $0 "\n";
	}
	END {
	    # GNU AWK magic for sorting "for (f in array) ...".
	    PROCINFO["sorted_in"] = "@ind_num_asc";

	    for (f in sort) {
		printf("%s", sort[f]);
	    }
	    print "All:\t\t" \
		"\tTotal\t"   total \
		"\tPassed\t"  passed \
		"\tSkipped\t" skipped \
		"\tFailed\t"  failed \
		;
	}
    '
}

# get-status - Combine `get-log' and `grep-status' in one command.
function get-status () {
    get-log "$@" | grep-status
}

# Main.
"${@:-get-status}"

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
