# ------------------------------------------------------------------------
#   prompt.tcl
#
#   A simple routine to interrupt a test at any given place and allow
#   interactive debugging.
# ------------------------------------------------------------------------

# When we run this in regular Tk, we need to add the Ctk library path
# to get command-loop.tcl.
lappend auto_path [file join [file dirname [info script]] .. library]

proc prompt {} {
    if {[testConstraint noPrompt]} {
	return
    }
    set testFile ""
    set testFileLine 0
    set testTest ""
    set testLine -1
    for {set i [info frame]} {$i >= 0} {incr i -1} {
	set frame [info frame $i]
	set line [dict get $frame line]
	if {"source" eq [dict get $frame type]} {
	    set file [dict get $frame file]
	    if {[string match *.test $file]} {
		set testFile [file tail $file]
		set testFileLine $line
	    }
	}
	if {"eval" eq [dict get $frame type]} {
	    set cmd [dict get $frame cmd]
	    set cmd [split $cmd]
	    if {"::tcltest::RunTest" eq [lindex $cmd 0]} {
		set testTest [lindex $cmd 1]
	    }
	    if {"::tcltest::RunTest" eq [dict get $frame proc]} {
		set testLine [expr {$line -1}]
	    }
	}
    }
    # FIXME: The code for line numbers here implies the simplified
    # syntax for tests, it is not accurate for tests that use the
    # label-value syntax with "-body" etc.  To do that we will need to
    # analyse the test command in question in more detail.  But than
    # accuracy is overrated here ;-)
    if {$testLine >= 0} {
	set prompt "$testFile:[expr $testFileLine + $testLine]"
	append prompt " $testTest:$testLine"
    } else {
	set prompt "$testFile:$testFileLine"
    }
    append prompt " % "
    set ::tcl_prompt1 [list puts -nonewline $prompt]

    set ::cmdLoop 1
    proc cont {} {
	incr ::cmdLoop
    }
    ctkCommandLoop::start
    vwait ::cmdLoop
    puts ""
}

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
