/*
 * tkEntry.c (Ctk) --
 *
 *	This module implements entry widgets for the Tk
 *	toolkit.  An entry displays a string and allows
 *	the string to be edited.
 *
 * Copyright (c) 1990-1994 The Regents of the University of California.
 * Copyright (c) 1994-1995 Sun Microsystems, Inc.
 * Copyright (c) 1994-1995 Cleveland Clinic Foundation
 *
 * See the file "license.terms" for information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 */

#include "default.h"
#include "tkPort.h"
#include "tkInt.h"
#include "ctkUnicode.h"

/*
 * A data structure of the following type is kept for each entry
 * widget managed by this file:
 */

typedef struct {
    Tk_Window tkwin;		/* Window that embodies the entry. NULL
				 * means that the window has been destroyed
				 * but the data structures haven't yet been
				 * cleaned up.*/
    Tcl_Interp *interp;		/* Interpreter associated with entry. */
    Tcl_Command widgetCmd;	/* Token for entry's widget command. */
    int numBytes;		/* Number of non-NULL bytes in string
				 * (may be 0). */
    int numChars;		/* Number of characters in string
				 * (may be 0). */
    const char *string;		/* Pointer to storage for string;
				 * NULL-terminated;  malloc-ed. */
    const char *textVarName;	/* Name of variable (malloc'ed) or NULL.
				 * If non-NULL, entry's string tracks the
				 * contents of this variable and vice versa. */
    Tk_Uid state;		/* Normal or disabled.  Entry is read-only
				 * when disabled. */

    /*
     * Information used when displaying widget:
     */

    int borderWidth;		/* Width of 3-D border around window. */
    Tk_Justify justify;		/* Justification to use for text within
				 * window. */
    int prefWidth;		/* Desired width of window, measured in
				 * average characters. */
    int leftIndex;		/* Index of left-most character visible in
				 * window. */
    int leftX;			/* X position at which leftIndex is drawn
				 * (varies depending on justify). */
    int tabOrigin;		/* Origin for tabs (left edge of string[0]). */
    int insertPos;		/* Index of character before which next
				 * typed character will be inserted. */
    const char *showChar;	/* Value of -show option.  If non-NULL, first
				 * character is used for displaying all
				 * characters in entry.  Malloc'ed. */
    const char *displayString;	/* If non-NULL, points to string with same
				 * length as string but whose characters
				 * are all equal to showChar.  Malloc'ed. */
    int displayNumBytes;	/* Number of non-NULL bytes in displayString
				 * (may be 0). */

    /*
     * Information about what's selected, if any.
     */

    int selectFirst;		/* Index of first selected character (-1 means
				 * nothing selected. */
    int selectLast;		/* Index of last selected character (-1 means
				 * nothing selected. */
    int selectAnchor;		/* Fixed end of selection (i.e. "select to"
				 * operation will use this as one end of the
				 * selection). */

    /*
     * Miscellaneous information:
     */

    const char *takeFocus;	/* Value of -takefocus option;  not used in
				 * the C code, but used by keyboard traversal
				 * scripts.  Malloc'ed, but may be NULL. */
    const char *scrollCmd;	/* Command prefix for communicating with
				 * scrollbar(s).  Malloc'ed.  NULL means
				 * no command to issue. */
    int flags;			/* Miscellaneous flags;  see below for
				 * definitions. */
} Entry;

/*
 * Assigned bits of "flags" fields of Entry structures, and what those
 * bits mean:
 *
 * REDRAW_PENDING:		Non-zero means a DoWhenIdle handler has
 *				already been queued to redisplay the entry.
 * BORDER_NEEDED:		Non-zero means 3-D border must be redrawn
 *				around window during redisplay.  Normally
 *				only text portion needs to be redrawn.
 * GOT_FOCUS:			Non-zero means this window has the input
 *				focus.
 * UPDATE_SCROLLBAR:		Non-zero means scrollbar should be updated
 *				during next redisplay operation.
 */

#define REDRAW_PENDING		1
#define BORDER_NEEDED		2
#define GOT_FOCUS		4
#define UPDATE_SCROLLBAR	8

/*
 * The following macro defines how many extra pixels to leave on each
 * side of the text in the entry.
 */

#define XPAD 1
#define YPAD 1

/*
 * Information used for argv parsing.
 */

static Tk_ConfigSpec configSpecs[] = {
    {TK_CONFIG_SYNONYM, "-bd", "borderWidth", NULL, NULL, 0, 0},
    {TK_CONFIG_PIXELS, "-borderwidth", "borderWidth", "BorderWidth",
	DEF_ENTRY_BORDER_WIDTH, Tk_Offset(Entry, borderWidth), 0},
    {TK_CONFIG_JUSTIFY, "-justify", "justify", "Justify",
	DEF_ENTRY_JUSTIFY, Tk_Offset(Entry, justify), 0},
    {TK_CONFIG_STRING, "-show", "show", "Show",
	DEF_ENTRY_SHOW, Tk_Offset(Entry, showChar), TK_CONFIG_NULL_OK},
    {TK_CONFIG_UID, "-state", "state", "State",
	DEF_ENTRY_STATE, Tk_Offset(Entry, state), 0},
    {TK_CONFIG_STRING, "-takefocus", "takeFocus", "TakeFocus",
	DEF_ENTRY_TAKE_FOCUS, Tk_Offset(Entry, takeFocus), TK_CONFIG_NULL_OK},
    {TK_CONFIG_STRING, "-textvariable", "textVariable", "Variable",
	DEF_ENTRY_TEXT_VARIABLE, Tk_Offset(Entry, textVarName),
	TK_CONFIG_NULL_OK},
    {TK_CONFIG_INT, "-width", "width", "Width",
	DEF_ENTRY_WIDTH, Tk_Offset(Entry, prefWidth), 0},
    {TK_CONFIG_STRING, "-xscrollcommand", "xScrollCommand", "ScrollCommand",
	DEF_ENTRY_SCROLL_COMMAND, Tk_Offset(Entry, scrollCmd),
	TK_CONFIG_NULL_OK},
    {TK_CONFIG_END, NULL, NULL, NULL, NULL, 0, 0}
};

/*
 * Flags for GetEntryIndex procedure:
 */

#define ZERO_OK			1
#define LAST_PLUS_ONE_OK	2

/*
 * Forward declarations for procedures defined later in this file:
 */

static int		ConfigureEntry (Tcl_Interp *interp,
			    Entry *entryPtr, int argc, const char *argv[],
			    int flags);
static int		DeleteChars (Entry *entryPtr, int index, int count);
static void		DestroyEntry (Tcl_MemPtr clientData);
static void		DisplayEntry (ClientData clientData);
static void		EntryCmdDeletedProc (ClientData clientData);
static void		EntryComputeGeometry (Entry *entryPtr);
static void		EntryEventProc (ClientData clientData, XEvent *eventPtr);
static void		EventuallyRedraw (Entry *entryPtr);
static void		EntrySetValue (Entry *entryPtr, const char *value);
static void		EntrySelectTo (Entry *entryPtr, int index);
static char *		EntryTextVarProc (ClientData clientData,
			    Tcl_Interp *interp, const char *name1,
			    const char *name2, int flags);
static void		EntryUpdateScrollbar (Entry *entryPtr);
static int		EntryValueChanged (Entry *entryPtr);
static void		EntryVisibleRange (Entry *entryPtr,
			    double *firstPtr, double *lastPtr);
static int		EntryWidgetCmd (ClientData clientData,
			    Tcl_Interp *interp, int argc, const char *argv[]);
static int		GetEntryIndex (Tcl_Interp *interp,
			    Entry *entryPtr, const char *string, int *indexPtr);
static int		InsertChars (Entry *entryPtr, int index,
			    const char *string);

/*
 *--------------------------------------------------------------
 *
 * Tk_EntryCmd --
 *
 *	This procedure is invoked to process the "entry" Tcl
 *	command.  See the user documentation for details on what
 *	it does.
 *
 * Results:
 *	A standard Tcl result.
 *
 * Side effects:
 *	See the user documentation.
 *
 *--------------------------------------------------------------
 */

int
Tk_EntryCmd(
    ClientData clientData,	/* Main window associated with
				 * interpreter. */
    Tcl_Interp *interp,		/* Current interpreter. */
    int argc,			/* Number of arguments. */
    const char *argv[])		/* Argument strings. */
{
    Tk_Window tkwin = (Tk_Window) clientData;
    register Entry *entryPtr;
    Tk_Window new;

    if (argc < 2) {
	Tcl_AppendResult(interp, "wrong # args: should be \"",
		argv[0], " pathName ?-option value ...?\"", (char *) NULL);
	return TCL_ERROR;
    }

    new = Tk_CreateWindowFromPath(interp, tkwin, argv[1], NULL);
    if (new == NULL) {
	return TCL_ERROR;
    }

    /*
     * Initialize the fields of the structure that won't be initialized
     * by ConfigureEntry, or that ConfigureEntry requires to be
     * initialized already (e.g. resource pointers).
     */

    entryPtr = (Entry *) ckalloc(sizeof(Entry));
    entryPtr->tkwin = new;
    entryPtr->interp = interp;
    entryPtr->widgetCmd = Tcl_CreateCommand(interp,
	    Tk_PathName(entryPtr->tkwin), EntryWidgetCmd,
	    entryPtr, EntryCmdDeletedProc);
    entryPtr->numBytes = 0;
    entryPtr->numChars = 0;
    entryPtr->string = CtkStrdup("");
    entryPtr->textVarName = NULL;
    entryPtr->state = tkNormalUid;
    entryPtr->borderWidth = 0;
    entryPtr->justify = TK_JUSTIFY_LEFT;
    entryPtr->prefWidth = 0;
    entryPtr->leftIndex = 0;
    entryPtr->leftX = 0;
    entryPtr->tabOrigin = 0;
    entryPtr->insertPos = 0;
    entryPtr->showChar = NULL;
    entryPtr->displayString = NULL;
    entryPtr->displayNumBytes = 0;
    entryPtr->selectFirst = -1;
    entryPtr->selectLast = -1;
    entryPtr->selectAnchor = 0;
    entryPtr->takeFocus = NULL;
    entryPtr->scrollCmd = NULL;
    entryPtr->flags = 0;

    Tk_SetClass(entryPtr->tkwin, "Entry");
    Tk_CreateEventHandler(entryPtr->tkwin,
    	    CTK_EXPOSE_EVENT_MASK|CTK_MAP_EVENT_MASK|CTK_DESTROY_EVENT_MASK
    	    |CTK_FOCUS_EVENT_MASK,
	    EntryEventProc, entryPtr);
    if (ConfigureEntry(interp, entryPtr, argc-2, argv+2, 0) != TCL_OK) {
	goto error;
    }

    Ctk_SetResult(interp, Tk_PathName(entryPtr->tkwin), TCL_VOLATILE);
    return TCL_OK;

    error:
    Tk_DestroyWindow(entryPtr->tkwin);
    return TCL_ERROR;
}

/*
 *--------------------------------------------------------------
 *
 * EntryWidgetCmd --
 *
 *	This procedure is invoked to process the Tcl command
 *	that corresponds to a widget managed by this module.
 *	See the user documentation for details on what it does.
 *
 * Results:
 *	A standard Tcl result.
 *
 * Side effects:
 *	See the user documentation.
 *
 *--------------------------------------------------------------
 */

static int
EntryWidgetCmd(
    ClientData clientData,		/* Information about entry widget. */
    Tcl_Interp *interp,			/* Current interpreter. */
    int argc,				/* Number of arguments. */
    const char *argv[])			/* Argument strings. */
{
    register Entry *entryPtr = (Entry *) clientData;
    int result = TCL_OK;
    size_t length;
    int c;

    if (argc < 2) {
	Tcl_AppendResult(interp, "wrong # args: should be \"",
		argv[0], " option ?arg ...?\"", (char *) NULL);
	return TCL_ERROR;
    }
    Tcl_Preserve(entryPtr);
    c = argv[1][0];
    length = strlen(argv[1]);
    if ((c == 'c') && (strncmp(argv[1], "cget", length) == 0)
	    && (length >= 2)) {
	if (argc != 3) {
	    Tcl_AppendResult(interp, "wrong # args: should be \"",
		    argv[0], " cget option\"",
		    (char *) NULL);
	    goto error;
	}
	result = Tk_ConfigureValue(interp, entryPtr->tkwin, configSpecs,
		(char *) entryPtr, argv[2], 0);
    } else if ((c == 'c') && (strncmp(argv[1], "configure", length) == 0)
	    && (length >= 2)) {
	if (argc == 2) {
	    result = Tk_ConfigureInfo(interp, entryPtr->tkwin, configSpecs,
		    (char *) entryPtr, NULL, 0);
	} else if (argc == 3) {
	    result = Tk_ConfigureInfo(interp, entryPtr->tkwin, configSpecs,
		    (char *) entryPtr, argv[2], 0);
	} else {
	    result = ConfigureEntry(interp, entryPtr, argc-2, argv+2,
		    TK_CONFIG_ARGV_ONLY);
	}
    } else if ((c == 'd') && (strncmp(argv[1], "delete", length) == 0)) {
	int first, last;

	if ((argc < 3) || (argc > 4)) {
	    Tcl_AppendResult(interp, "wrong # args: should be \"",
		    argv[0], " delete firstIndex ?lastIndex?\"",
		    (char *) NULL);
	    goto error;
	}
	if (GetEntryIndex(interp, entryPtr, argv[2], &first) != TCL_OK) {
	    goto error;
	}
	if (argc == 3) {
	    last = first+1;
	} else {
	    if (GetEntryIndex(interp, entryPtr, argv[3], &last) != TCL_OK) {
		goto error;
	    }
	}
	if ((last >= first) && (entryPtr->state == tkNormalUid)) {
	    if (DeleteChars(entryPtr, first, last-first) != TCL_OK) {
		goto error;
	    }
	}
    } else if ((c == 'g') && (strncmp(argv[1], "get", length) == 0)) {
	if (argc != 2) {
	    Tcl_AppendResult(interp, "wrong # args: should be \"",
		    argv[0], " get\"", (char *) NULL);
	    goto error;
	}
	Ctk_SetResult(interp, entryPtr->string, TCL_VOLATILE);
    } else if ((c == 'i') && (strncmp(argv[1], "icursor", length) == 0)
	    && (length >= 2)) {
	if (argc != 3) {
	    Tcl_AppendResult(interp, "wrong # args: should be \"",
		    argv[0], " icursor pos\"",
		    (char *) NULL);
	    goto error;
	}
	if (GetEntryIndex(interp, entryPtr, argv[2], &entryPtr->insertPos)
		!= TCL_OK) {
	    goto error;
	}
	EventuallyRedraw(entryPtr);
    } else if ((c == 'i') && (strncmp(argv[1], "index", length) == 0)
	    && (length >= 3)) {
	int index;

	if (argc != 3) {
	    Tcl_AppendResult(interp, "wrong # args: should be \"",
		    argv[0], " index string\"", (char *) NULL);
	    goto error;
	}
	if (GetEntryIndex(interp, entryPtr, argv[2], &index) != TCL_OK) {
	    goto error;
	}
	{
          char buffer[20];
	  sprintf(buffer, "%d", index);
	  Tcl_SetResult(interp,buffer,TCL_VOLATILE);
	}
    } else if ((c == 'i') && (strncmp(argv[1], "insert", length) == 0)
	    && (length >= 3)) {
	int index;

	if (argc != 4) {
	    Tcl_AppendResult(interp, "wrong # args: should be \"",
		    argv[0], " insert index text\"",
		    (char *) NULL);
	    goto error;
	}
	if (GetEntryIndex(interp, entryPtr, argv[2], &index) != TCL_OK) {
	    goto error;
	}
	if (entryPtr->state == tkNormalUid) {
	    if (InsertChars(entryPtr, index, argv[3]) != TCL_OK) {
		goto error;
	    }
	}
    } else if ((c == 's') && (length >= 2)
	    && (strncmp(argv[1], "scan", length) == 0)) {
	result = Ctk_Unsupported(interp, "entry scan");
    } else if ((c == 's') && (length >= 2)
	    && (strncmp(argv[1], "selection", length) == 0)) {
	int index, index2;

	if (argc < 3) {
	    Tcl_AppendResult(interp, "wrong # args: should be \"",
		    argv[0], " selection option ?index?\"", (char *) NULL);
	    goto error;
	}

	length = strlen(argv[2]);
	c = argv[2][0];

	/*
	 * Option "present" is allowed even for disabled entries.
	 */

	if ((c == 'p') && (strncmp(argv[2], "present", length) == 0)) {
	    if (argc != 3) {
		Tcl_AppendResult(interp, "wrong # args: should be \"",
			argv[0], " selection present\"", (char *) NULL);
		goto error;
	    }
	    if (entryPtr->selectFirst == -1) {
		Tcl_SetResult(interp,"0",TCL_STATIC);
	    } else {
		Tcl_SetResult(interp,"1",TCL_STATIC);
	    }
	    goto done;
	}

	/*
	 * Otherwise disabled entries don't allow the selection to be modified.
	 */

	if (entryPtr->state == tkDisabledUid) {
	    goto done;
	}

	if ((c == 'c') && (strncmp(argv[2], "clear", length) == 0)) {
	    if (argc != 3) {
		Tcl_AppendResult(interp, "wrong # args: should be \"",
			argv[0], " selection clear\"", (char *) NULL);
		goto error;
	    }
	    if (entryPtr->selectFirst != -1) {
		entryPtr->selectFirst = entryPtr->selectLast = -1;
		EventuallyRedraw(entryPtr);
	    }
	    goto done;
	}
	if (argc >= 4) {
	    if (GetEntryIndex(interp, entryPtr, argv[3], &index) != TCL_OK) {
		goto error;
	    }
	}
	if ((c == 'a') && (strncmp(argv[2], "adjust", length) == 0)) {
	    if (argc != 4) {
		Tcl_AppendResult(interp, "wrong # args: should be \"",
			argv[0], " selection adjust index\"",
			(char *) NULL);
		goto error;
	    }
	    if (entryPtr->selectFirst >= 0) {
		int half1, half2;

		half1 = (entryPtr->selectFirst + entryPtr->selectLast)/2;
		half2 = (entryPtr->selectFirst + entryPtr->selectLast + 1)/2;
		if (index < half1) {
		    entryPtr->selectAnchor = entryPtr->selectLast;
		} else if (index > half2) {
		    entryPtr->selectAnchor = entryPtr->selectFirst;
		} else {
		    /*
		     * We're at about the halfway point in the selection;
		     * just keep the existing anchor.
		     */
		}
	    }
	    EntrySelectTo(entryPtr, index);
	} else if ((c == 'f') && (strncmp(argv[2], "from", length) == 0)) {
	    if (argc != 4) {
		Tcl_AppendResult(interp, "wrong # args: should be \"",
			argv[0], " selection from index\"",
			(char *) NULL);
		goto error;
	    }
	    entryPtr->selectAnchor = index;
	} else if ((c == 'r') && (strncmp(argv[2], "range", length) == 0)) {
	    if (argc != 5) {
		Tcl_AppendResult(interp, "wrong # args: should be \"",
			argv[0], " selection range start end\"",
			(char *) NULL);
		goto error;
	    }
	    if (GetEntryIndex(interp, entryPtr, argv[4], &index2) != TCL_OK) {
		goto error;
	    }
	    if (index >= index2) {
		entryPtr->selectFirst = entryPtr->selectLast = -1;
	    } else {
		entryPtr->selectFirst = index;
		entryPtr->selectLast = index2;
	    }
	    EventuallyRedraw(entryPtr);
	} else if ((c == 't') && (strncmp(argv[2], "to", length) == 0)) {
	    if (argc != 4) {
		Tcl_AppendResult(interp, "wrong # args: should be \"",
			argv[0], " selection to index\"",
			(char *) NULL);
		goto error;
	    }
	    EntrySelectTo(entryPtr, index);
	} else {
	    Tcl_AppendResult(interp, "bad selection option \"", argv[2],
		    "\": must be adjust, clear, from, present, range, or to",
		    (char *) NULL);
	    goto error;
	}
    } else if ((c == 'x') && (strncmp(argv[1], "xview", length) == 0)) {
	int index, type, count, charsPerPage;
	double fraction, first, last;

	if (argc == 2) {
	    char buffer[40];
	    EntryVisibleRange(entryPtr, &first, &last);
	    sprintf(buffer, "%g %g", first, last);
	    Tcl_SetResult(interp,buffer,TCL_VOLATILE);
	    goto done;
	} else if (argc == 3) {
	    if (GetEntryIndex(interp, entryPtr, argv[2], &index) != TCL_OK) {
		goto error;
	    }
	} else {
	    type = Tk_GetScrollInfo(interp, argc, argv, &fraction, &count);
	    index = entryPtr->leftIndex;
	    switch (type) {
		case TK_SCROLL_ERROR:
		    goto error;
		case TK_SCROLL_MOVETO:
		    index = (fraction * entryPtr->numChars);
		    break;
		case TK_SCROLL_PAGES:
		    charsPerPage = Tk_Width(entryPtr->tkwin)
			    - 2*entryPtr->borderWidth - 2;
		    if (charsPerPage < 1) {
			charsPerPage = 1;
		    }
		    index += charsPerPage*count;
		    break;
		case TK_SCROLL_UNITS:
		    index += count;
		    break;
	    }
	}
	if (index >= entryPtr->numChars) {
	    index = entryPtr->numChars-1;
	}
	if (index < 0) {
	    index = 0;
	}
	entryPtr->leftIndex = index;
	entryPtr->flags |= UPDATE_SCROLLBAR;
	EntryComputeGeometry(entryPtr);
	EventuallyRedraw(entryPtr);
    } else {
	Tcl_AppendResult(interp, "bad option \"", argv[1],
		"\": must be cget, configure, delete, get, ",
		"icursor, index, insert, scan, selection, or xview",
		(char *) NULL);
	goto error;
    }
    done:
    Tcl_Release(entryPtr);
    return result;

    error:
    Tcl_Release(entryPtr);
    return TCL_ERROR;
}

/*
 *----------------------------------------------------------------------
 *
 * DestroyEntry --
 *
 *	This procedure is invoked by Tcl_EventuallyFree or Tcl_Release
 *	to clean up the internal structure of an entry at a safe time
 *	(when no-one is using it anymore).
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	Everything associated with the entry is freed up.
 *
 *----------------------------------------------------------------------
 */

static void
DestroyEntry(
    Tcl_MemPtr clientData)		/* Info about entry widget. */
{
    register Entry *entryPtr = (Entry *) clientData;

    /*
     * Free up all the stuff that requires special handling, then
     * let Tk_FreeOptions handle all the standard option-related
     * stuff.
     */

    ckfree((void*)entryPtr->string);
    if (entryPtr->textVarName != NULL) {
	Tcl_UntraceVar(entryPtr->interp, entryPtr->textVarName,
		TCL_GLOBAL_ONLY|TCL_TRACE_WRITES|TCL_TRACE_UNSETS,
		EntryTextVarProc, entryPtr);
    }
    if (entryPtr->displayString != NULL) {
	ckfree((void*)entryPtr->displayString);
    }
    Tk_FreeOptions(configSpecs, (char *) entryPtr, 0);
    ckfree(entryPtr);
}

/*
 *----------------------------------------------------------------------
 *
 * ConfigureEntry --
 *
 *	This procedure is called to process an argv/argc list, plus
 *	the Tk option database, in order to configure (or reconfigure)
 *	an entry widget.
 *
 * Results:
 *	The return value is a standard Tcl result.  If TCL_ERROR is
 *	returned, then interp->result contains an error message.
 *
 * Side effects:
 *	Configuration information, such as colors, border width,
 *	etc. get set for entryPtr;  old resources get freed,
 *	if there were any.
 *
 *----------------------------------------------------------------------
 */

static int
ConfigureEntry(
    Tcl_Interp *interp,		/* Used for error reporting. */
    register Entry *entryPtr,	/* Information about widget;  may or may
				 * not already have values for some fields. */
    int argc,			/* Number of valid entries in argv. */
    const char *argv[],		/* Arguments. */
    int flags)			/* Flags to pass to Tk_ConfigureWidget. */
{
    /*
     * Eliminate any existing trace on a variable monitored by the entry.
     */

    if (entryPtr->textVarName != NULL) {
	Tcl_UntraceVar(interp, entryPtr->textVarName,
		TCL_GLOBAL_ONLY|TCL_TRACE_WRITES|TCL_TRACE_UNSETS,
		EntryTextVarProc, entryPtr);
    }

    if (Tk_ConfigureWidget(interp, entryPtr->tkwin, configSpecs,
	    argc, argv, (char *) entryPtr, flags) != TCL_OK) {
	return TCL_ERROR;
    }

    /*
     * If the entry is tied to the value of a variable, then set up
     * a trace on the variable's value, create the variable if it doesn't
     * exist, and set the entry's value from the variable's value.
     */

    if (entryPtr->textVarName != NULL) {
	const char *value;

	value = Tcl_GetVar(interp, entryPtr->textVarName, TCL_GLOBAL_ONLY);
	if (value == NULL) {

	    /*
	     * Since any trace on the textvariable was eliminated above,
	     * the only possible reason for EntryValueChanged to return
	     * an error is that the textvariable lives in a namespace
	     * that does not (yet) exist. Indeed, namespaces are not
	     * automatically created as needed. Don't trap this error
	     * here, better do it below when attempting to trace the
	     * variable.
	     */

	    EntryValueChanged(entryPtr);
	} else {
	    EntrySetValue(entryPtr, value);
	}
	if (Tcl_TraceVar(interp, entryPtr->textVarName,
		 TCL_GLOBAL_ONLY|TCL_TRACE_WRITES|TCL_TRACE_UNSETS,
		 EntryTextVarProc, entryPtr) != TCL_OK) {
	    return TCL_ERROR;
	}
    }

    /*
     * A few other options also need special processing, such as parsing
     * the geometry and setting the background from a 3-D border.
     */

    if ((entryPtr->state != tkNormalUid)
	    && (entryPtr->state != tkDisabledUid)) {
	Tcl_AppendResult(interp, "bad state \"", entryPtr->state,
		"\": must be disabled or normal", (char *) NULL);
	entryPtr->state = tkNormalUid;
	return TCL_ERROR;
    }

    /*
     * Recompute the window's geometry and arrange for it to be
     * redisplayed.
     */

    Tk_SetInternalBorder(entryPtr->tkwin, entryPtr->borderWidth);
    EntryComputeGeometry(entryPtr);
    entryPtr->flags |= UPDATE_SCROLLBAR;
    EventuallyRedraw(entryPtr);
    return TCL_OK;
}

/*
 *--------------------------------------------------------------
 *
 * DisplayEntry --
 *
 *	This procedure redraws the contents of an entry window.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	Information appears on the screen.
 *
 *--------------------------------------------------------------
 */

static void
DisplayEntry(
    ClientData clientData)	/* Information about window. */
{
    register Entry *entryPtr = (Entry *) clientData;
    register Tk_Window tkwin = entryPtr->tkwin;
    int baseY, selStartX, selEndX, index, cursorX;
    int xBound, count;
    const char *displayString;
    int displayNumBytes;
    int leftOffset, indexOffset;

    if ((entryPtr->tkwin == NULL) || !Tk_IsMapped(tkwin)) {
	goto done;
    }

    /*
     * Update the scrollbar if that's needed.
     */

    if (entryPtr->flags & UPDATE_SCROLLBAR) {

	/*
	 * Preserve/Release because updating the scrollbar can have the
	 * side-effect of destroying or unmapping the entry widget.
	 */

	Tcl_Preserve(entryPtr);
	EntryUpdateScrollbar(entryPtr);

	if ((entryPtr->tkwin == NULL) || !Tk_IsMapped(tkwin)) {
	    Tcl_Release(entryPtr);
	    return;
	}
	Tcl_Release(entryPtr);
    }

    /*
     * Compute x-coordinate of the pixel just after last visible
     * one, plus vertical position of baseline of text.
     */

    xBound = Tk_Width(tkwin) - entryPtr->borderWidth;
    baseY = Tk_Height(tkwin)/2;

    /*
     * Draw the background.
     */

    Ctk_FillRect(tkwin, entryPtr->borderWidth, baseY, xBound, baseY+1,
    	   CTK_UNDERLINE_STYLE, ' ');

    if (entryPtr->displayString == NULL) {
	displayString = entryPtr->string;
	displayNumBytes = entryPtr->numBytes;
    } else {
	displayString = entryPtr->displayString;
	displayNumBytes = entryPtr->displayNumBytes;
    }
    leftOffset = CtkCharNextN(displayString, displayNumBytes,
	    entryPtr->leftIndex);
    if (entryPtr->selectLast > entryPtr->leftIndex) {
	if (entryPtr->selectFirst <= entryPtr->leftIndex) {
	    selStartX = entryPtr->leftX;
	    index = entryPtr->leftIndex;
	    indexOffset = leftOffset;
	} else {
	    int byteCount = CtkCharNextN(displayString + leftOffset,
		    displayNumBytes - leftOffset,
		    entryPtr->selectFirst - entryPtr->leftIndex);
	    TkMeasureChars(displayString + leftOffset, byteCount,
		    entryPtr->leftX, xBound, entryPtr->tabOrigin,
		    TK_PARTIAL_OK|TK_NEWLINES_NOT_SPECIAL, &selStartX);
	    index = entryPtr->selectFirst;
	    indexOffset = CtkCharNextN(displayString, displayNumBytes,
		    index);
	}
	if (selStartX < xBound) {
	    int byteCount = CtkCharNextN(displayString + indexOffset,
		    displayNumBytes - indexOffset,
		    entryPtr->selectLast - index);
	    TkMeasureChars(displayString + indexOffset, byteCount,
		    selStartX, xBound, entryPtr->tabOrigin,
		    TK_PARTIAL_OK|TK_NEWLINES_NOT_SPECIAL, &selEndX);
	} else {
	    selEndX = xBound;
	}
    }

    /*
     * Draw the text in three pieces:  first the piece to the left of
     * the selection, then the selection, then the piece to the right
     * of the selection.
     */

    if (entryPtr->selectLast <= entryPtr->leftIndex) {
	int byteCount = CtkCharNextN(displayString + leftOffset,
		displayNumBytes - leftOffset,
		entryPtr->numChars - entryPtr->leftIndex);
	TkDisplayChars(tkwin, CTK_UNDERLINE_STYLE,
		displayString + leftOffset, byteCount,
		entryPtr->leftX, baseY, entryPtr->tabOrigin,
		TK_NEWLINES_NOT_SPECIAL);
    } else {
	count = entryPtr->selectFirst - entryPtr->leftIndex;
	if (count > 0) {
	    int byteCount = CtkCharNextN(displayString + leftOffset,
		    displayNumBytes - leftOffset, count);
	    TkDisplayChars(tkwin, CTK_UNDERLINE_STYLE,
		    displayString + leftOffset, byteCount,
		    entryPtr->leftX, baseY, entryPtr->tabOrigin,
		    TK_NEWLINES_NOT_SPECIAL);
	    if (index != entryPtr->selectFirst) {
		index = entryPtr->selectFirst;
		indexOffset = CtkCharNextN(displayString, displayNumBytes,
			index);
	    }
	} else {
	    index = entryPtr->leftIndex;
	    indexOffset = leftOffset;
	}
	count = entryPtr->selectLast - index;
	if ((selStartX < xBound) && (count > 0)) {
	    int byteCount = CtkCharNextN(displayString + indexOffset,
		    displayNumBytes - indexOffset, count);
	    TkDisplayChars(tkwin, CTK_SELECTED_STYLE,
		    displayString + indexOffset, byteCount, selStartX, baseY,
		    entryPtr->tabOrigin, TK_NEWLINES_NOT_SPECIAL);
	}
	count = entryPtr->numChars - entryPtr->selectLast;
	if ((selEndX < xBound) && (count > 0)) {
	    int byteOffset = CtkCharNextN(displayString,
		    displayNumBytes, entryPtr->selectLast);
	    int byteCount = CtkCharNextN(displayString + byteOffset,
		    displayNumBytes - byteOffset, count);
	    TkDisplayChars(tkwin, CTK_UNDERLINE_STYLE,
		    displayString + byteOffset, byteCount,
		    selEndX, baseY, entryPtr->tabOrigin,
		    TK_NEWLINES_NOT_SPECIAL);
	}
    }

    /*
     * Position the cursor.
     */

    if ((entryPtr->insertPos >= entryPtr->leftIndex)
	    && (entryPtr->state == tkNormalUid)
	    && (entryPtr->flags & GOT_FOCUS)) {
	int byteCount = CtkCharNextN(displayString + leftOffset,
		displayNumBytes - leftOffset,
		entryPtr->insertPos - entryPtr->leftIndex);
	TkMeasureChars(displayString + leftOffset, byteCount,
		entryPtr->leftX, xBound, entryPtr->tabOrigin,
		TK_PARTIAL_OK|TK_NEWLINES_NOT_SPECIAL, &cursorX);
	if (cursorX < xBound) {
	    Ctk_SetCursor(tkwin, cursorX, baseY);
	}
    }

    if (entryPtr->flags & BORDER_NEEDED) {
	Ctk_DrawBorder(tkwin, CTK_PLAIN_STYLE, NULL);
    }

    done:
    entryPtr->flags &= ~(REDRAW_PENDING|BORDER_NEEDED);
}

/*
 *----------------------------------------------------------------------
 *
 * EntryComputeGeometry --
 *
 *	This procedure is invoked to recompute information about where
 *	in its window an entry's string will be displayed.  It also
 *	computes the requested size for the window.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	The leftX and tabOrigin fields are recomputed for entryPtr,
 *	and leftIndex may be adjusted.  Tk_GeometryRequest is called
 *	to register the desired dimensions for the window.
 *
 *----------------------------------------------------------------------
 */

static void
EntryComputeGeometry(
    Entry *entryPtr)			/* Widget record for entry. */
{
    int totalLength, overflow, maxOffScreen, rightX, byteCount;
    int height, width, i;
    char *p;
    const char *displayString;
    int displayNumBytes;

    /*
     * If we're displaying a special character instead of the value of
     * the entry, recompute the displayString.
     */

    if (entryPtr->displayString != NULL) {
	ckfree((void*)entryPtr->displayString);
	entryPtr->displayString = NULL;
    }
    if (entryPtr->showChar != NULL) {
	/*
	 * FIXME: Ideally this should be ClusterNextN (and
	 * numClusters), but that makes the later calculations more
	 * difficult.  It is probably never actually needed anyway.
	 */
	int showCharLen = CtkCharNextN(entryPtr->showChar, -1, 1);
        char *new;
	new = (char *) ckalloc((entryPtr->numChars + 1) * showCharLen);
	for (p = new, i = entryPtr->numChars; i > 0; i--, p += showCharLen) {
	    memcpy(p, entryPtr->showChar, showCharLen);
	}
	*p = 0;
	displayString = entryPtr->displayString = new;
	displayNumBytes = entryPtr->displayNumBytes
                = entryPtr->numChars * showCharLen;
    } else {
	displayString = entryPtr->string;
        displayNumBytes = entryPtr->numBytes;
    }

    /*
     * Recompute where the leftmost character on the display will
     * be drawn (entryPtr->leftX) and adjust leftIndex if necessary
     * so that we don't let characters hang off the edge of the
     * window unless the entire window is full.
     */

    TkMeasureChars(displayString, displayNumBytes,
	    0, INT_MAX, 0, TK_NEWLINES_NOT_SPECIAL, &totalLength);
    totalLength++;
    overflow = totalLength
    	    - (Tk_Width(entryPtr->tkwin) - 2*entryPtr->borderWidth);
    if (overflow <= 0) {
	entryPtr->leftIndex = 0;
	if (entryPtr->justify == TK_JUSTIFY_LEFT) {
	    entryPtr->leftX = entryPtr->borderWidth;
	} else if (entryPtr->justify == TK_JUSTIFY_RIGHT) {
	    entryPtr->leftX = Tk_Width(entryPtr->tkwin) - entryPtr->borderWidth
		    - totalLength;
	} else {
	    entryPtr->leftX = (Tk_Width(entryPtr->tkwin) - totalLength)/2;
	}
	entryPtr->tabOrigin = entryPtr->leftX;
    } else {
	/*
	 * The whole string can't fit in the window.  Compute the
	 * maximum number of characters that may be off-screen to
	 * the left without leaving more than one empty space on
	 * the right of the window, then don't let leftIndex be any
	 * greater than that.
	 */

	maxOffScreen = TkMeasureChars(displayString,
	    displayNumBytes, 0, overflow, 0,
	    TK_NEWLINES_NOT_SPECIAL|TK_PARTIAL_OK, &rightX);
	maxOffScreen = Tcl_NumUtfChars(displayString, maxOffScreen);
	if (rightX < overflow) {
	    maxOffScreen += 1;
	}
	if (entryPtr->leftIndex > maxOffScreen) {
	    entryPtr->leftIndex = maxOffScreen;
	}
	byteCount = CtkCharNextN(displayString, displayNumBytes,
		entryPtr->leftIndex);
	TkMeasureChars(displayString, byteCount, 0, INT_MAX, 0,
		TK_NEWLINES_NOT_SPECIAL|TK_PARTIAL_OK, &rightX);
	entryPtr->leftX = entryPtr->borderWidth;
	entryPtr->tabOrigin = entryPtr->leftX - rightX;
    }

    height = 1 + 2*entryPtr->borderWidth + 2*(YPAD-XPAD);
    if (entryPtr->prefWidth > 0) {
	width = entryPtr->prefWidth + 2*entryPtr->borderWidth;
    } else {
	if (totalLength == 0) {
	    width = 1 + 2*entryPtr->borderWidth;
	} else {
	    width = totalLength + 2*entryPtr->borderWidth;
	}
    }
    Tk_GeometryRequest(entryPtr->tkwin, width, height);
}

/*
 *----------------------------------------------------------------------
 *
 * InsertChars --
 *
 *	Add new characters to an entry widget.
 *
 * Results:
 *	A standard Tcl result. If an error occurred then an error message is
 *	left in the interp's result.
 *
 * Side effects:
 *	New information gets added to entryPtr;  it will be redisplayed
 *	soon, but not necessarily immediately.
 *
 *----------------------------------------------------------------------
 */

static int
InsertChars(
    register Entry *entryPtr,	/* Entry that is to get the new
				 * elements. */
    int index,			/* Add the new elements before this
				 * element. */
    const char *string)		/* New characters to add (NULL-terminated
				 * string). */
{
    int lengthBytes, length, indexBytes;
    char *new;

    lengthBytes = strlen(string);
    if (lengthBytes == 0) {
	return TCL_OK;
    }

    length = Tcl_NumUtfChars(string, lengthBytes);
    indexBytes = CtkCharNextN(entryPtr->string, entryPtr->numBytes, index);

    new = (char *) ckalloc(entryPtr->numBytes + lengthBytes + 1);
    strncpy(new, entryPtr->string, indexBytes);
    strcpy(new+indexBytes, string);
    strcpy(new+indexBytes+lengthBytes, entryPtr->string+indexBytes);
    ckfree((void*)entryPtr->string);
    entryPtr->string = new;
    entryPtr->numBytes += lengthBytes;
    entryPtr->numChars += length;

    /*
     * Inserting characters invalidates all indexes into the string.
     * Touch up the indexes so that they still refer to the same
     * characters (at new positions).  When updating the selection
     * end-points, don't include the new text in the selection unless
     * it was completely surrounded by the selection.
     */

    if (entryPtr->selectFirst >= index) {
	entryPtr->selectFirst += length;
    }
    if (entryPtr->selectLast > index) {
	entryPtr->selectLast += length;
    }
    if ((entryPtr->selectAnchor > index) || (entryPtr->selectFirst >= index)) {
	entryPtr->selectAnchor += length;
    }
    if (entryPtr->leftIndex > index) {
	entryPtr->leftIndex += length;
    }
    if (entryPtr->insertPos >= index) {
	entryPtr->insertPos += length;
    }
    return EntryValueChanged(entryPtr);
}

/*
 *----------------------------------------------------------------------
 *
 * DeleteChars --
 *
 *	Remove one or more characters from an entry widget.
 *
 * Results:
 *	A standard Tcl result. If an error occurred then an error message is
 *	left in the interp's result.
 *
 * Side effects:
 *	Memory gets freed, the entry gets modified and (eventually)
 *	redisplayed.
 *
 *----------------------------------------------------------------------
 */

static int
DeleteChars(
    register Entry *entryPtr,	/* Entry widget to modify. */
    int index,			/* Index of first character to delete. */
    int count)			/* How many characters to delete. */
{
    char *new;
    int indexBytes, countBytes;

    if ((index + count) > entryPtr->numChars) {
	count = entryPtr->numChars - index;
    }
    if (count <= 0) {
	return TCL_OK;
    }

    indexBytes = CtkCharNextN(entryPtr->string,
	    entryPtr->numBytes, index);
    countBytes  = CtkCharNextN(entryPtr->string + indexBytes,
	    entryPtr->numBytes - indexBytes, count);

    new = (char *) ckalloc(entryPtr->numBytes + 1 - countBytes);
    strncpy(new, entryPtr->string, indexBytes);
    strcpy(new+indexBytes, entryPtr->string+indexBytes+countBytes);
    ckfree((void*)entryPtr->string);
    entryPtr->string = new;
    entryPtr->numBytes -= countBytes;
    entryPtr->numChars -= count;

    /*
     * Deleting characters results in the remaining characters being
     * renumbered.  Update the various indexes into the string to reflect
     * this change.
     */

    if (entryPtr->selectFirst >= index) {
	if (entryPtr->selectFirst >= (index+count)) {
	    entryPtr->selectFirst -= count;
	} else {
	    entryPtr->selectFirst = index;
	}
    }
    if (entryPtr->selectLast >= index) {
	if (entryPtr->selectLast >= (index+count)) {
	    entryPtr->selectLast -= count;
	} else {
	    entryPtr->selectLast = index;
	}
    }
    if (entryPtr->selectLast <= entryPtr->selectFirst) {
	entryPtr->selectFirst = entryPtr->selectLast = -1;
    }
    if (entryPtr->selectAnchor >= index) {
	if (entryPtr->selectAnchor >= (index+count)) {
	    entryPtr->selectAnchor -= count;
	} else {
	    entryPtr->selectAnchor = index;
	}
    }
    if (entryPtr->leftIndex > index) {
	if (entryPtr->leftIndex >= (index+count)) {
	    entryPtr->leftIndex -= count;
	} else {
	    entryPtr->leftIndex = index;
	}
    }
    if (entryPtr->insertPos >= index) {
	if (entryPtr->insertPos >= (index+count)) {
	    entryPtr->insertPos -= count;
	} else {
	    entryPtr->insertPos = index;
	}
    }
    return EntryValueChanged(entryPtr);
}

/*
 *----------------------------------------------------------------------
 *
 * EntryValueChanged --
 *
 *	This procedure is invoked when characters are inserted into
 *	an entry or deleted from it.  It updates the entry's associated
 *	variable, if there is one, and does other bookkeeping such
 *	as arranging for redisplay.
 *
 * Results:
 *	A standard Tcl result. If an error occurred then an error message is
 *	left in the interp's result.
 *
 * Side effects:
 *	None.
 *
 *----------------------------------------------------------------------
 */

static int
EntryValueChanged(
    Entry *entryPtr)		/* Entry whose value just changed. */
{
    const char *newValue;

    if (entryPtr->textVarName == NULL) {
	newValue = NULL;
    } else {
	newValue = Tcl_SetVar(entryPtr->interp, entryPtr->textVarName,
		entryPtr->string, TCL_GLOBAL_ONLY|TCL_LEAVE_ERR_MSG);
	if (newValue == NULL) {
	    return TCL_ERROR;
	}
    }

    if ((newValue != NULL) && (strcmp(newValue, entryPtr->string) != 0)) {
	/*
	 * The value of the variable is different than what we asked for.
	 * This means that a trace on the variable modified it.  In this
	 * case our trace procedure wasn't invoked since the modification
	 * came while a trace was already active on the variable.  So,
	 * update our value to reflect the variable's latest value.
	 */

	EntrySetValue(entryPtr, newValue);
    } else {
	/*
	 * Arrange for redisplay.
	 */

	entryPtr->flags |= UPDATE_SCROLLBAR;
	EntryComputeGeometry(entryPtr);
	EventuallyRedraw(entryPtr);
    }

    return TCL_OK;
}

/*
 *----------------------------------------------------------------------
 *
 * EntrySetValue --
 *
 *	Replace the contents of a text entry with a given value.  This
 *	procedure is invoked when updating the entry from the entry's
 *	associated variable.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	The string displayed in the entry will change.  The selection,
 *	and insertion point may have to be adjusted to keep them
 *	within the bounds of the new string.  Note: this procedure
 *	does *not* update the entry's associated variable, since that
 *	could result in an infinite loop.
 *
 *----------------------------------------------------------------------
 */

static void
EntrySetValue(
    register Entry *entryPtr,		/* Entry whose value is to be
					 * changed. */
    const char *value)			/* New text to display in entry. */
{
    ckfree((void*)entryPtr->string);
    entryPtr->numBytes = strlen(value);
    entryPtr->numChars = Tcl_NumUtfChars(value, entryPtr->numBytes);
    entryPtr->string = CtkStrdup(value);

    if (entryPtr->selectFirst >= 0) {
	if (entryPtr->selectFirst >= entryPtr->numChars) {
	    entryPtr->selectFirst = -1;
	    entryPtr->selectLast = -1;
	} else if (entryPtr->selectLast > entryPtr->numChars) {
	    entryPtr->selectLast = entryPtr->numChars;
	}
    }

    if (entryPtr->leftIndex >= entryPtr->numChars) {
	if (entryPtr->numChars > 0) {
	    entryPtr->leftIndex = entryPtr->numChars - 1;
	} else {
	    entryPtr->leftIndex = 0;
	}
    }

    if (entryPtr->insertPos > entryPtr->numChars) {
	entryPtr->insertPos = entryPtr->numChars;
    }

    entryPtr->flags |= UPDATE_SCROLLBAR;
    EntryComputeGeometry(entryPtr);
    EventuallyRedraw(entryPtr);
}

/*
 *--------------------------------------------------------------
 *
 * EntryEventProc --
 *
 *	This procedure is invoked by the Tk dispatcher for various
 *	events on entryes.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	When the window gets deleted, internal structures get
 *	cleaned up.  When it gets exposed, it is redisplayed.
 *
 *--------------------------------------------------------------
 */

static void
EntryEventProc(
    ClientData clientData,	/* Information about window. */
    XEvent *eventPtr)		/* Information about event. */
{
    Entry *entryPtr = (Entry *) clientData;
    if (eventPtr->type == CTK_EXPOSE_EVENT) {
	EventuallyRedraw(entryPtr);
	entryPtr->flags |= BORDER_NEEDED;
    } else if (eventPtr->type == CTK_DESTROY_EVENT) {
	if (entryPtr->tkwin != NULL) {
	    entryPtr->tkwin = NULL;
	    Tcl_DeleteCommandFromToken(entryPtr->interp, entryPtr->widgetCmd);
	}
	if (entryPtr->flags & REDRAW_PENDING) {
	    Tcl_CancelIdleCall(DisplayEntry, entryPtr);
	}
	Tcl_EventuallyFree(entryPtr, DestroyEntry);
    } else if (eventPtr->type == CTK_MAP_EVENT) {
	Tcl_Preserve(entryPtr);
	entryPtr->flags |= UPDATE_SCROLLBAR;
	EntryComputeGeometry(entryPtr);
	EventuallyRedraw(entryPtr);
	Tcl_Release(entryPtr);
    } else if (eventPtr->type == CTK_FOCUS_EVENT) {
	entryPtr->flags |= GOT_FOCUS;
	EventuallyRedraw(entryPtr);
    } else if (eventPtr->type == CTK_UNFOCUS_EVENT) {
	entryPtr->flags &= ~GOT_FOCUS;
    }
}

/*
 *----------------------------------------------------------------------
 *
 * EntryCmdDeletedProc --
 *
 *	This procedure is invoked when a widget command is deleted.  If
 *	the widget isn't already in the process of being destroyed,
 *	this command destroys it.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	The widget is destroyed.
 *
 *----------------------------------------------------------------------
 */

static void
EntryCmdDeletedProc(
    ClientData clientData)	/* Pointer to widget record for widget. */
{
    Entry *entryPtr = (Entry *) clientData;
    Tk_Window tkwin = entryPtr->tkwin;

    /*
     * This procedure could be invoked either because the window was
     * destroyed and the command was then deleted (in which case tkwin
     * is NULL) or because the command was deleted, and then this procedure
     * destroys the widget.
     */

    if (tkwin != NULL) {
	entryPtr->tkwin = NULL;
	Tk_DestroyWindow(tkwin);
    }
}

/*
 *--------------------------------------------------------------
 *
 * GetEntryIndex --
 *
 *	Parse an index into an entry and return either its value
 *	or an error.
 *
 * Results:
 *	A standard Tcl result.  If all went well, then *indexPtr is
 *	filled in with the index (into entryPtr) corresponding to
 *	string.  The index value is guaranteed to lie between 0 and
 *	the number of characters in the string, inclusive.  If an
 *	error occurs then an error message is left in interp->result.
 *
 * Side effects:
 *	None.
 *
 *--------------------------------------------------------------
 */

static int
GetEntryIndex(
    Tcl_Interp *interp,		/* For error messages. */
    Entry *entryPtr,		/* Entry for which the index is being
				 * specified. */
    const char *string,		/* Specifies character in entryPtr. */
    int *indexPtr)		/* Where to store converted index. */
{
    size_t length;

    length = strlen(string);

    if (string[0] == 'a') {
	if (strncmp(string, "anchor", length) == 0) {
	    *indexPtr = entryPtr->selectAnchor;
	} else {
	    badIndex:

	    /*
	     * Some of the paths here leave messages in interp->result,
	     * so we have to clear it out before storing our own message.
	     */

	    Tcl_ResetResult(interp);
	    Tcl_AppendResult(interp, "bad entry index \"", string,
		    "\"", (char *) NULL);
	    return TCL_ERROR;
	}
    } else if (string[0] == 'e') {
	if (strncmp(string, "end", length) == 0) {
	    *indexPtr = entryPtr->numChars;
	} else {
	    goto badIndex;
	}
    } else if (string[0] == 'i') {
	if (strncmp(string, "insert", length) == 0) {
	    *indexPtr = entryPtr->insertPos;
	} else {
	    goto badIndex;
	}
    } else if (string[0] == 's') {
	if (entryPtr->selectFirst == -1) {
	    Tcl_ResetResult(interp);
	    Tcl_AppendResult(interp, "selection isn't in widget ",
		    Tk_PathName(entryPtr->tkwin), (char *) NULL);
	    return TCL_ERROR;
	}
	if (length < 5) {
	    goto badIndex;
	}
	if (strncmp(string, "sel.first", length) == 0) {
	    *indexPtr = entryPtr->selectFirst;
	} else if (strncmp(string, "sel.last", length) == 0) {
	    *indexPtr = entryPtr->selectLast;
	} else {
	    goto badIndex;
	}
    } else if (string[0] == '@') {
	int x, dummy, roundUp;

	if (Tcl_GetInt(interp, string+1, &x) != TCL_OK) {
	    goto badIndex;
	}
	if (x < entryPtr->borderWidth) {
	    x = entryPtr->borderWidth;
	}
	roundUp = 0;
	if (x >= (Tk_Width(entryPtr->tkwin) - entryPtr->borderWidth)) {
	    x = Tk_Width(entryPtr->tkwin) - entryPtr->borderWidth - 1;
	    roundUp = 1;
	}
	if (entryPtr->numChars == 0) {
	    *indexPtr = 0;
	} else {
	    const char *displayString;
	    int displayNumBytes;
	    if (entryPtr->displayString == NULL) {
		displayString = entryPtr->string;
		displayNumBytes = entryPtr->numBytes;
	    } else {
		displayString = entryPtr->displayString;
		displayNumBytes = entryPtr->displayNumBytes;
	    }
	    *indexPtr = TkMeasureChars(
		    displayString, displayNumBytes, entryPtr->tabOrigin, x,
		    entryPtr->tabOrigin, TK_NEWLINES_NOT_SPECIAL, &dummy);
	    *indexPtr = Tcl_NumUtfChars(displayString, *indexPtr);
	}

	/*
	 * Special trick:  if the x-position was off-screen to the right,
	 * round the index up to refer to the character just after the
	 * last visible one on the screen.  This is needed to enable the
	 * last character to be selected, for example.
	 */

	if (roundUp && (*indexPtr < entryPtr->numChars)) {
	    *indexPtr += 1;
	}
    } else {
	if (Tcl_GetInt(interp, string, indexPtr) != TCL_OK) {
	    goto badIndex;
	}
	if (*indexPtr < 0){
	    *indexPtr = 0;
	} else if (*indexPtr > entryPtr->numChars) {
	    *indexPtr = entryPtr->numChars;
	}
    }
    return TCL_OK;
}

/*
 *----------------------------------------------------------------------
 *
 * EntrySelectTo --
 *
 *	Modify the selection by moving its un-anchored end.  This could
 *	make the selection either larger or smaller.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	The selection changes.
 *
 *----------------------------------------------------------------------
 */

static void
EntrySelectTo(
    register Entry *entryPtr,		/* Information about widget. */
    int index)				/* Index of element that is to
					 * become the "other" end of the
					 * selection. */
{
    int newFirst, newLast;

    /*
     * Pick new starting and ending points for the selection.
     */

    if (entryPtr->selectAnchor > entryPtr->numChars) {
	entryPtr->selectAnchor = entryPtr->numChars;
    }
    if (entryPtr->selectAnchor <= index) {
	newFirst = entryPtr->selectAnchor;
	newLast = index;
    } else {
	newFirst = index;
	newLast = entryPtr->selectAnchor;
	if (newLast < 0) {
	    newFirst = newLast = -1;
	}
    }
    if ((entryPtr->selectFirst == newFirst)
	    && (entryPtr->selectLast == newLast)) {
	return;
    }
    entryPtr->selectFirst = newFirst;
    entryPtr->selectLast = newLast;
    EventuallyRedraw(entryPtr);
}

/*
 *----------------------------------------------------------------------
 *
 * EventuallyRedraw --
 *
 *	Ensure that an entry is eventually redrawn on the display.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	Information gets redisplayed.  Right now we don't do selective
 *	redisplays:  the whole window will be redrawn.  This doesn't
 *	seem to hurt performance noticeably, but if it does then this
 *	could be changed.
 *
 *----------------------------------------------------------------------
 */

static void
EventuallyRedraw(
    register Entry *entryPtr)		/* Information about widget. */
{
    if ((entryPtr->tkwin == NULL) || !Tk_IsMapped(entryPtr->tkwin)) {
	return;
    }

    /*
     * Right now we don't do selective redisplays:  the whole window
     * will be redrawn.  This doesn't seem to hurt performance noticeably,
     * but if it does then this could be changed.
     */

    if (!(entryPtr->flags & REDRAW_PENDING)) {
	entryPtr->flags |= REDRAW_PENDING;
	Tcl_DoWhenIdle(DisplayEntry, entryPtr);
    }
}

/*
 *----------------------------------------------------------------------
 *
 * EntryVisibleRange --
 *
 *	Return information about the range of the entry that is
 *	currently visible.
 *
 * Results:
 *	*firstPtr and *lastPtr are modified to hold fractions between
 *	0 and 1 identifying the range of characters visible in the
 *	entry.
 *
 * Side effects:
 *	None.
 *
 *----------------------------------------------------------------------
 */

static void
EntryVisibleRange(
    Entry *entryPtr,			/* Information about widget. */
    double *firstPtr,			/* Return position of first visible
					 * character in widget. */
    double *lastPtr)			/* Return position of char just after
					 * last visible one. */
{
    const char *displayString;
    int displayNumBytes, byteCount, charsInWindow, endX;

    if (entryPtr->numChars == 0) {
	*firstPtr = 0.0;
	*lastPtr = 1.0;
	return;
    }

    if (entryPtr->displayString == NULL) {
	displayString = entryPtr->string;
	displayNumBytes = entryPtr->numBytes;
    } else {
	displayString = entryPtr->displayString;
	displayNumBytes = entryPtr->displayNumBytes;
    }
    byteCount = CtkCharNextN(displayString, displayNumBytes,
	    entryPtr->leftIndex);
    charsInWindow = TkMeasureChars(displayString + byteCount,
	    displayNumBytes - byteCount, entryPtr->borderWidth,
	    Tk_Width(entryPtr->tkwin) - entryPtr->borderWidth,
	    entryPtr->borderWidth, TK_AT_LEAST_ONE|TK_NEWLINES_NOT_SPECIAL,
	    &endX);
    charsInWindow = Tcl_NumUtfChars(displayString + byteCount,
	    charsInWindow);
    *firstPtr = ((double) entryPtr->leftIndex)/entryPtr->numChars;
    *lastPtr = ((double) (entryPtr->leftIndex + charsInWindow))
	    /entryPtr->numChars;
}

/*
 *----------------------------------------------------------------------
 *
 * EntryUpdateScrollbar --
 *
 *	This procedure is invoked whenever information has changed in
 *	an entry in a way that would invalidate a scrollbar display.
 *	If there is an associated scrollbar, then this procedure updates
 *	it by invoking a Tcl command.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	A Tcl command is invoked, and an additional command may be
 *	invoked to process errors in the command.
 *
 *----------------------------------------------------------------------
 */

static void
EntryUpdateScrollbar(
    Entry *entryPtr)			/* Information about widget. */
{
    int code;
    double first, last;
    Tcl_Interp *interp;
    Tcl_Obj *cmd;

    if (entryPtr->scrollCmd == NULL) {
	return;
    }

    interp = entryPtr->interp;
    Tcl_Preserve(interp);

    EntryVisibleRange(entryPtr, &first, &last);
    cmd = Tcl_ObjPrintf("%s %g %g", entryPtr->scrollCmd, first, last);
    code = Tcl_EvalObjEx(interp, cmd, TCL_EVAL_GLOBAL | TCL_EVAL_DIRECT);
    if (code != TCL_OK) {
	Tcl_AddErrorInfo(interp,
		"\n    (horizontal scrolling command executed by entry)");
	Tcl_BackgroundError(interp);
    }

    Tcl_ResetResult(interp);
    Tcl_Release(interp);
}

/*
 *--------------------------------------------------------------
 *
 * EntryTextVarProc --
 *
 *	This procedure is invoked when someone changes the variable
 *	whose contents are to be displayed in an entry.
 *
 * Results:
 *	NULL is always returned.
 *
 * Side effects:
 *	The text displayed in the entry will change to match the
 *	variable.
 *
 *--------------------------------------------------------------
 */

	/* ARGSUSED */
static char *
EntryTextVarProc(
    ClientData clientData,	/* Information about button. */
    Tcl_Interp *interp,		/* Interpreter containing variable. */
    const char *name1,		/* Not used. */
    const char *name2,		/* Not used. */
    int flags)			/* Information about what happened. */
{
    register Entry *entryPtr = (Entry *) clientData;
    const char *value;

    /*
     * If the variable is unset, then immediately recreate it unless
     * the whole interpreter is going away.
     */

    if (flags & TCL_TRACE_UNSETS) {
	if ((flags & TCL_TRACE_DESTROYED)
		&& !Tcl_InterpDeleted(interp)
		&& entryPtr->textVarName) {
	    /*
	     * An unset trace on some variable brought us here, but is
	     * it the variable we have stored in textVarName ?
	     */

            ClientData probe = NULL;
            do {
                probe = Tcl_VarTraceInfo(interp,
                        entryPtr->textVarName,
                        TCL_GLOBAL_ONLY|TCL_TRACE_WRITES|TCL_TRACE_UNSETS,
                        EntryTextVarProc, probe);
                if (probe == entryPtr) {
                    break;
                }
            } while (probe);
            if (probe) {
                /*
                 * We were able to fetch the unset trace for our
                 * textVarName, which means it is not unset and not
                 * the cause of this unset trace. Instead some outdated
                 * former variable must be, and we should ignore it.
                 */
                return NULL;
            }
	    Tcl_SetVar(interp, entryPtr->textVarName, entryPtr->string,
		    TCL_GLOBAL_ONLY);
	    Tcl_TraceVar(interp, entryPtr->textVarName,
		    TCL_GLOBAL_ONLY|TCL_TRACE_WRITES|TCL_TRACE_UNSETS,
		    EntryTextVarProc, clientData);
	}
	return NULL;
    }

    /*
     * Update the entry's text with the value of the variable, unless
     * the entry already has that value (this happens when the variable
     * changes value because we changed it because someone typed in
     * the entry).
     */

    value = Tcl_GetVar(interp, entryPtr->textVarName, TCL_GLOBAL_ONLY);
    if (value == NULL) {
	value = "";
    }
    if (strcmp(value, entryPtr->string) != 0) {
	EntrySetValue(entryPtr, value);
    }
    return NULL;
}
