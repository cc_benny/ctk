#!/bin/bash

# A simple script to run cwish in the compilation directory, without
# installing it.

dir=$(dirname "$0")
export LD_LIBRARY_PATH="$dir:$LD_LIBRARY_PATH"
export CTK_LIBRARY="$dir/library"
exec $dir/cwish "$@"

# eof
