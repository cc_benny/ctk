/*
 * ctkKeys.c (Ctk) --
 *
 *	CTK display functions (hides all curses functions).
 *
 * See the file "license.terms" for information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 */

#define _XOPEN_SOURCE_EXTENDED 1 /* ncurses*w*. */

#include "tkPort.h"
#include "tkInt.h"
#include "ctkUnicode.h"
#include <stdint.h>
#include <sys/times.h>
#include <curses.h>
#include <term.h>

#ifdef CLK_TCK
#   define MS_PER_CLOCK	(1000.0/CLK_TCK)
#elif defined HZ
#   define MS_PER_CLOCK	(1000.0/HZ)
#else
    /*
     * If all else fails, assume 60 clock ticks per second -
     * hope that is okay!
     */
#   define MS_PER_CLOCK	(1000.0/60)
#endif

/*
 * The data structure and hash table below are used to map from
 * raw keycodes (curses) to keysyms and modifier masks.
 */

typedef struct {
    int code;			/* Curses key code. */
    KeySym sym;			/* Key sym. */
    int modMask;		/* Modifiers. */
} KeyCodeInfo;

static const KeyCodeInfo keyCodeArray[] = {
#include "keyCodes.h"
    {0, 0, 0}
};
static Tcl_HashTable keyCodeTable;	/* Hashed form of above structure. */


#ifdef HAVE_KEY_DEFINED
/*
 * Following Ncurses' man page user_caps.5, there are a couple of keys that we
 * can detect dynamically.  The keys actually produced can be enumerated by
 * using the script in subdirectory `termkeys'.
 */
typedef struct {
    const char *name;
    KeySym sym;
} SpecialKeyCodes;

static const SpecialKeyCodes specialKeyCodes[] = {
    /*
     * These are only the keys that Ncurses actually handles and produces as
     * special keys as of 6.0+20161126.
     */
    {"kDC",   0xFFFF}, /* Delete */
    {"kDN",   0xFF54}, /* Down */
    {"kEND",  0xFF57}, /* End */
    {"kFND",  0xFF68}, /* Find */
    {"kHOM",  0xFF50}, /* Home */
    {"kIC",   0xFF63}, /* Insert */
    {"kLFT",  0xFF51}, /* Left */
    {"kNXT",  0xFF56}, /* Next page */
    {"kPRV",  0xFF55}, /* Previous page */
    {"kRIT",  0xFF53}, /* Right */
    {"kUP",   0xFF52}, /* Up */

    /*
     * Function keys on a keypad:
     *
     * Home	Up	PrPag
     * ka1	ka2	ka3
     *
     * Left	[5]	Right
     * kb1	kb2	kb1
     *
     * End	Down	NxPag
     * kc1	kc2	kc3
     *
     * These keypad keys have no modifier variants, but we match the plain
     * terminfo key without a modifier index at `specialMasks' index 0.  Note
     * that ka1, ka3, kc1 and kc3 are only mentioned for completeness, Ncurses
     * does not actually produce these as special keys at this time.
     */
    {"ka1",   0xFF50}, /* Home */
    {"ka2",   0xFF52}, /* Up */
    {"ka3",   0xFF55}, /* Previous page */
    {"kb1",   0xFF51}, /* Left */
    {"kb3",   0xFF53}, /* Right */
    {"kc1",   0xFF57}, /* End */
    {"kc2",   0xFF54}, /* Down */
    {"kc3",   0xFF56}, /* Next page */

    { NULL, 0 }
};

static const int specialMasks[] = {
    /*0*/ 0,
    /*1*/ 0,
    /*2*/ ShiftMask,
    /*3*/ Mod1Mask,
    /*4*/ ShiftMask | Mod1Mask,
    /*5*/ ControlMask,
    /*6*/ ShiftMask | ControlMask,
    /*7*/ Mod1Mask | ControlMask,
    /*8*/ ShiftMask | Mod1Mask | ControlMask,
    -1
};
#endif

/*
 * Forward declarations of static functions.
 */

static void	TermFileProc (ClientData clientData, int mask);
static void	HandleKey (TkDisplay *dispPtr, int key, int modMask);
static int	KeydefinedCmd (Tcl_Interp *interp, int argc, const char *argv[]);
static int	KeynameCmd (Tcl_Interp *interp, int argc, const char *argv[]);
static int	TerminfoCmd (Tcl_Interp *interp, int argc, const char *argv[]);
static int	CursorCmd (Tcl_Interp *interp, int argc, const char *argv[]);


/*
 *--------------------------------------------------------------
 *
 * CtkKeysInit --
 *
 *	Set up an event handler for keyboard input.  Set up
 *	data structures to handle keys.  This function assumes
 *	that CtkDisplayInit was called before to initialize
 *	Curses.
 *
 * Side effects:
 *	Keyboard input will now cause key events.
 *
 *--------------------------------------------------------------
 */

void
CtkKeysInit(
    Tcl_Interp *interp,
    TkDisplay *dispPtr)
{
    CtkSetDisplay(dispPtr);
    raw();
#ifdef HAVE_KEYPAD
    keypad(stdscr, TRUE);
#endif

    Tcl_CreateChannelHandler(dispPtr->chan, TCL_READABLE,
	    TermFileProc, dispPtr);

    static int initialized = 0;

    if (!initialized) {
	const KeyCodeInfo *codePtr;
	Tcl_HashEntry *hPtr;
	int dummy;

	initialized = 1;
	Tcl_InitHashTable(&keyCodeTable, TCL_ONE_WORD_KEYS);

	for (codePtr = keyCodeArray; codePtr->code != 0; codePtr++) {
	    hPtr = Tcl_CreateHashEntry(&keyCodeTable,
		   (char *) (intptr_t) codePtr->code, &dummy);
	    Tcl_SetHashValue(hPtr, (ClientData) codePtr);
	}

#ifdef HAVE_KEY_DEFINED
	/*
	 * Dynamic key assigments according to Ncurses'
	 * user_caps.5.  These can only be checked after
	 * newterm() and keypad() have been called.
	 */
	const SpecialKeyCodes *scPtr;
	int modeIdx;
	char key[20];

	for (scPtr = specialKeyCodes; scPtr->name != NULL; scPtr++) {
	    for (modeIdx = 0; specialMasks[modeIdx] != -1; modeIdx++) {

		if (modeIdx > 0) {
		    sprintf(key, "%s%d", scPtr->name, modeIdx);
		} else {
		    strcpy(key, scPtr->name);
		}

		const char *escSeq = tigetstr(key);
		if (escSeq == NULL || escSeq == (const char*) -1) {
		    continue;
		}
		int code = key_defined(escSeq);
		if (code == -1 || code < KEY_MAX) {
		    continue;
		}

		KeyCodeInfo *codePtr = ckalloc(sizeof *codePtr);
		codePtr->code = code;
		codePtr->sym = scPtr->sym;
		codePtr->modMask = specialMasks[modeIdx];
		hPtr = Tcl_CreateHashEntry(&keyCodeTable,
			(char *) (intptr_t) codePtr->code, &dummy);
		Tcl_SetHashValue(hPtr, (ClientData) codePtr);
	    }
	}
#endif
    }
}

/*
 *--------------------------------------------------------------
 *
 * CtkKeysEnd --
 *
 *	Stops the event handler for keyboard input.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	No key events are generated any more.
 *
 *--------------------------------------------------------------
 */

void
CtkKeysEnd(
    TkDisplay *dispPtr)
{
    Tcl_DeleteChannelHandler(dispPtr->chan, TermFileProc, dispPtr);
}

/*
 *--------------------------------------------------------------
 *
 * TermFileProc --
 *
 *	File handler for keyboard input.
 *
 * Side effects:
 *	Dispatches events (invoking event handlers).
 *
 *--------------------------------------------------------------
 */

#define KEY_ESCAPE 27

static void
TermFileProc(
    ClientData clientData,
    int mask)
{
    TkDisplay *dispPtr = (TkDisplay *) clientData;
    int key;
    int isUtf8;

    static char encBuf[10] = "";
    static int encLen = 0;
    static int modMask = 0;

    if (! (mask & TCL_READABLE)) {
	return;
    }

    CtkSetDisplay(dispPtr);
    key = getch();

    /*
     * Alt key combinations get coded with a leading ESC.  We set
     * nodelay for that, because in the case that this was real ESC,
     * we do not want to wait.
     */

    if (key == KEY_ESCAPE) {
	nodelay(stdscr, TRUE);
	key = getch();
	nodelay(stdscr, FALSE);
	if (key == -1) {
	    /*
	     * No other key was waiting for us, so this is a real ESC.
	     */
	    key = KEY_ESCAPE;
	} else {
	    modMask |= Mod1Mask;
	}
    }

    /*
     * If the key has a bit set above the 8 bits of a regular byte,
     * than it is a curses keycode.  If it is non-ASCII it needs to be
     * decoded to a Tcl_UniChar.  Because of multi-byte encodings, we
     * need to collect bytes until a character can be decoded.  If the
     * system encoding is not UTF-8, trail-bytes may look like ASCII
     * characters, so in that case, even if we get an ASCII character,
     * keep collecting when there already is something in the buffer.
     */

    isUtf8 = strcmp("utf-8", Tcl_GetEncodingName(NULL)) == 0;
    if (0 == (key & ~0xFF)
	&& (!CtkIsAscii(key)
	    || (!isUtf8 && encLen > 0))) {

	encBuf[encLen++] = key;

	/*
	 * Tcl_ExternalToUtf does not detect incomplete byte sequences
	 * with UTF-8, so we need to handle UTF-8 input with extra
	 * code.  OTOH it's quite a bit simpler.
	 */

	if (isUtf8) {
	    Tcl_UniChar codepoint;

	    if (!CtkUtfCharComplete(encBuf, encLen)) {
		return;
	    }
	    encLen = 0;

	    Ctk_UtfToCodepoint(encBuf, &codepoint);
	    HandleKey(dispPtr, CTK_UNICODE_TO_KEYSYM(codepoint), modMask);
	    modMask = 0;

	} else {

	    char utf8Buf[30];
	    const char *utf8;
	    int utf8Len = 0;

	    if (Tcl_ExternalToUtf(NULL, NULL, encBuf, encLen,
				  TCL_ENCODING_STOPONERROR, NULL,
				  utf8Buf, sizeof(utf8Buf)-1,
				  NULL, &utf8Len, NULL)
		!= TCL_OK) {
		return;
	    }

	    /*
	     * One input character can translate into multiple output
	     * characters.
	     */
	    utf8Buf[utf8Len] = '\0';
	    utf8 = utf8Buf;
	    while (*utf8 != '\0') {
		Tcl_UniChar codepoint;
		utf8 += Ctk_UtfToCodepoint(utf8, &codepoint);

		/*
		 * Once we have consumed at least one UTF-8 character,
		 * we can not got back to the original bytes, because
		 * we would not know where exactly we are.  So just
		 * declare it handled now.
		 */
		encLen = 0;

		if (CtkIsAscii(codepoint)) {
		    HandleKey(dispPtr, codepoint, modMask);
		} else {
		    HandleKey(dispPtr, CTK_UNICODE_TO_KEYSYM(codepoint),
			    modMask);
		}
	    }
	    modMask = 0;
	}
    } else {
	/*
	 * If something intervenes, drop the accumulated bytes.  This is
	 * also the standard path for ASCII and Curses key codes.
	 */
	encLen = 0;
	HandleKey(dispPtr, key, modMask);
	modMask = 0;
    }
}

/*
 *--------------------------------------------------------------
 *
 * HandleKey --
 *
 *	Generate and handle one key event.
 *
 * Side effects:
 *	Dispatches the even (invoking event handlers).
 *
 *--------------------------------------------------------------
 */

static void
HandleKey(
    TkDisplay *dispPtr,
    int key,
    int modMask)
{
    Ctk_Event event;
    struct tms timesBuf;
    Tcl_HashEntry *hPtr;
    KeyCodeInfo *codePtr;

    hPtr = Tcl_FindHashEntry(&keyCodeTable, (char *) (intptr_t) key);
    if (hPtr) {
	codePtr = (KeyCodeInfo *) Tcl_GetHashValue(hPtr);
	event.u.key.sym = codePtr->sym;
	event.u.key.state = modMask | codePtr->modMask;
    } else {
	event.u.key.sym = key;
	event.u.key.state = modMask;
    }
    event.type = CTK_KEY_EVENT;
    event.window = dispPtr->focusPtr;
    event.u.key.code = key;
    event.u.key.time = (unsigned long) (times(&timesBuf)*MS_PER_CLOCK);
    Tk_HandleEvent(&event);
}

/*
 *--------------------------------------------------------------
 *
 * Ctk_CursesCmd --
 *
 *	Some curses specific Tcl commands.
 *
 * Side effects:
 *	None.
 *
 *--------------------------------------------------------------
 */

int
Ctk_CursesCmd(
    ClientData clientData,	/* Ignored. */
    Tcl_Interp *interp,		/* Current interpreter. */
    int argc,			/* Number of arguments. */
    const char *argv[])		/* Argument strings. */
{
    if (argc < 2) {
	Tcl_AppendResult(interp, "wrong # args: should be \"",
		argv[0], " subcommand ...\"", (char *) NULL);
	return TCL_ERROR;
    }

    const char *subcommand = argv[1];

    if (strcmp("key_defined", subcommand) == 0) {
	return KeydefinedCmd(interp, argc, argv);
    } else if (strcmp("keyname", subcommand) == 0) {
	return KeynameCmd(interp, argc, argv);
    } else if (strcmp("terminfo", subcommand) == 0) {
	return TerminfoCmd(interp, argc, argv);
    } else if (strcmp("cursor", subcommand) == 0) {
	return CursorCmd(interp, argc, argv);
    } else {
	Tcl_AppendResult(interp, "invalid subcommand \"",
		subcommand, "\": should be \"key_defined\", "
                "\"keyname\" or \"terminfo\"",
		(char *) NULL);
	return TCL_ERROR;
    }
}

static int
KeydefinedCmd (
    Tcl_Interp *interp,		/* Current interpreter. */
    int argc,			/* Number of arguments. */
    const char *argv[])		/* Argument strings. */
{
    if (3 != argc) {
	Tcl_AppendResult(interp, "wrong # args: should be \"",
		argv[0], " ", argv[1], " controlsequence\"", (char *) NULL);
	return TCL_ERROR;
    }

#ifdef HAVE_KEY_DEFINED
    const char *name = argv[2];
    int code = key_defined(name);
    if (code == 0) {
	Tcl_AppendResult(interp, "key not defined: \"",
		name, "\"", (char *) NULL);
	return TCL_ERROR;
    }

    if (code < 0) {
	Tcl_AppendResult(interp, "keyname not a unique prefix: \"",
		name, "\"", (char *) NULL);
	return TCL_ERROR;
    }

    char numbuf[20];
    sprintf(numbuf, "0o%03o", code);
    Tcl_SetResult(interp, numbuf, TCL_VOLATILE);

    return TCL_OK;
#else
    Tcl_AppendResult(interp, "not implemented: \"",
	    argv[0], " ", argv[1], " keyname\"", (char *) NULL);
    return TCL_ERROR;
#endif
}

static int
KeynameCmd (
    Tcl_Interp *interp,		/* Current interpreter. */
    int argc,			/* Number of arguments. */
    const char *argv[])		/* Argument strings. */
{
    if (3 != argc) {
	Tcl_AppendResult(interp, "wrong # args: should be \"",
		argv[0], " ", argv[1], " keycode\"", (char *) NULL);
	return TCL_ERROR;
    }

    const char *string = argv[2];
    char *end = NULL;
    int code = strtol(string, &end, 0);
    if (end == NULL || *end != 0) {
	Tcl_AppendResult(interp, "expected integer argument but got \"",
		string, "\"", (char *) NULL);
	return TCL_ERROR;
    }

    const char *name = keyname(code);
    if (name == NULL) {
	Tcl_AppendResult(interp, "key not defined: \"", string, "\"",
		(char *) NULL);
	return TCL_ERROR;
    }

    Ctk_SetResult(interp, name, TCL_STATIC);

    return TCL_OK;
}

static int
TerminfoCmd (
    Tcl_Interp *interp,		/* Current interpreter. */
    int argc,			/* Number of arguments. */
    const char *argv[])		/* Argument strings. */
{
    if (3 != argc) {
	Tcl_AppendResult(interp, "wrong # args: should be \"",
		argv[0], " ", argv[1], " terminfo-capability\"", (char *) NULL);
	return TCL_ERROR;
    }

    const char *name = argv[2];

    int nvalue = tigetnum((char *) name);
    if (nvalue == -1) {
	Tcl_AppendResult(interp, "terminfo capability not defined: \"",
		name, "\"", (char *) NULL);
	return TCL_ERROR;
    }
    if (nvalue != -2) {
	char numbuf[10];
	sprintf(numbuf, "%d", nvalue);
	Tcl_SetResult(interp, numbuf, TCL_VOLATILE);
	return TCL_OK;
    }

    int bvalue = tigetflag((char *) name);
    if (bvalue >= 0) {
	Tcl_SetResult(interp, bvalue ? "1" : "0", TCL_STATIC);
	return TCL_OK;
    }

    const char * svalue = tigetstr((char *) name);
    if (svalue != (const char*)-1 && svalue != NULL) {
	Ctk_SetResult(interp, svalue, TCL_STATIC);
	return TCL_OK;
    }

    Tcl_AppendResult(interp, "terminfo capability not defined: \"",
	    name, "\"", (char *) NULL);
    return TCL_ERROR;
}

static int
CursorCmd (
    Tcl_Interp *interp,		/* Current interpreter. */
    int argc,			/* Number of arguments. */
    const char *argv[])		/* Argument strings. */
{
    if (2 != argc) {
	Tcl_AppendResult(interp, "wrong # args: should be \"",
		argv[0], " ", argv[1], (char *) NULL);
	return TCL_ERROR;
    }

    int x = -1, y = -1;
    getyx(stdscr, y, x);

    char buffer[50];
    sprintf(buffer, "%d %d", x, y);
    Tcl_AppendResult(interp, buffer, (char *) NULL);
    return TCL_OK;
}
