#!/usr/bin/tclsh
# ------------------------------------------------------------------------
#   generate-pagetable.tcl
#
#   Use Unicode's DerivedCombiningClass.txt or EastAsianWidth.txt to
#   generate a page tables with corresponding bits for all characters.
#   Bits are collected in buckets of 64 bits each and `words_per_page'
#   buckets per page.  The buckets can than be represented as "long
#   long" in C.  See ctkUnicode.c on how the result is used.
# ------------------------------------------------------------------------

variable BITS_PER_WORD 64
variable verbose 0

# Parse the range RAW in the form "1234..5678" into a list {0x1234
# 0x5678} and plain "1234" into {0x1234 0x1234}.
proc parse-range {raw} {
    lassign [split $raw "."] r1 r2 r3
    if {"" eq $r3} {
	return [list 0x$r1 0x$r1]
    } else {
	return [list 0x$r1 0x$r3]
    }
}

# Parse DerivedCombiningClass.txt or DerivedEastAsianWidth.txt into a
# list of ranges.
proc parse-ucd {types args} {
    set ranges {}
    foreach fname $args {
	set ifs [open $fname r]
	while {[gets $ifs line] >= 0} {
	    if {"" eq $line
		|| [string match "#*" $line]
		|| [string match "*<reserved*" $line]} {
		continue
	    }
	    set fields [split $line ";#"]
	    set range [string trim [lindex $fields 0]]
	    set range [parse-range $range]

	    set derivedtype [string trim [lindex $fields 1]]
	    if {$derivedtype in $types} {
		lappend ranges $range
	    }
	}
	close $ifs
    }

    return $ranges
}

# Structure a list of ranges into a dictionary of pages.  A page is a
# dictionary of character offsets and buckets.  The result is again
# indexed by character offsets, this time for the pages.
proc collect-pages {bits_per_word words_per_page ranges} {
    set bits_per_page [expr {$words_per_page * $bits_per_word}]
    foreach range $ranges {
	lassign $range s e
	for {set i $s} {$i <= $e} {incr i} {
	    # Offset in the page in bits.
	    set pageoff	 [expr {$i % $bits_per_page}]
	    # Page address (first represented character).
	    set pagebase [expr {$i - $pageoff}]
	    # Offset in the word in bits.
	    set wordoff	 [expr {$i % $bits_per_word}]
	    # Word address (first represented character).
	    set wordbase [expr {$i - $wordoff}]
	    #set i [expr {$i}]
	    #puts "$i: wo $wordoff wb: $wordbase po $pageoff pb $pagebase"
	    if {[catch {
		set b [dict get $pages($pagebase) $wordbase]
	    }]} {
		set b 0
	    }
	    set b [expr {$b | (1 << $wordoff)}]
	    dict set pages($pagebase) $wordbase $b
	}
    }

    return [array get pages]
}

# Taking a page list as produced by COLLECT-PAGES, count the number of
# distinct pages.  Add 1 for the empty page.
proc count-pages {pages words_per_page} {

    variable BITS_PER_WORD
    set pagesizebits [expr {$words_per_page * $BITS_PER_WORD}]
    foreach {offset page} $pages {
	set end [expr {$offset + $pagesizebits}]
	set page [format-page $page $offset $end]
	set pagecache($page) $page
    }

    set result [array size pagecache]
    incr result
    return $result
}

# Format and output pages as C code, usable by `ctkUnicode.c'.
proc print-pages {varname pages words_per_page} {
    variable BITS_PER_WORD
    set pagesizebits [expr {$words_per_page * $BITS_PER_WORD}]
    puts "static const BitBucket ${varname}_pages\[\] = {"
    # First create the empty page.
    print-page [format-page {} 0 $pagesizebits] 0 $pagesizebits
    set pagelist {}
    # Where we are in the page array.
    set wordoffset $words_per_page
    array set pagearr $pages
    for {set i 0} {[array size pagearr] > 0} {set i $end} {
	set end [expr {$i + $pagesizebits}]
	if {[catch {set page $pagearr($i)}]} {
	    # Point the index to the empty page.
	    lappend pagelist 0
	} else {
	    unset pagearr($i)
	    set page [format-page $page $i $end]
	    if {[catch {
		set index $pagecache($page)
		lappend pagelist $index
	    }]} {
		set pagecache($page) $wordoffset
		lappend pagelist $wordoffset
		print-page $page $i $end
		incr wordoffset $words_per_page
	    }
	}
    }
    puts "};"

    puts "static const unsigned short ${varname}_offsets\[\] = {"
    puts [join $pagelist ", "]
    puts "};"

    puts "#define ${varname}_wordsPerPage $words_per_page"
    puts "#define ${varname}_bitsPerWord $BITS_PER_WORD"
    puts "#define ${varname}_maxOffset $end"
}

# Format a single page in a canonical form, suitable for use by
# PRINT-PAGE to output a C code array initializer.
proc format-page {page start end} {
    variable BITS_PER_WORD
    set result ""
    for {set word $start} {$word < $end} {incr word $BITS_PER_WORD} {
	if {[catch {set w [dict get $page $word]}]} {
	    set w 0
	}
	append result [format " 0x%016lX," $w]
    }
    return $result
}

# Print a page given in the canonical with a small intro comment.
proc print-page {page start end} {
    puts [format "/* Page U+%04X - U+%04X */" $start $end]
    puts $page
}

# Top-level command: Output statistics for possible page tables trying
# different values for WORDS_PER_PAGE, running from START to END.  At
# the end output the best and the worst solution.
proc stats {types start end args} {
    variable BITS_PER_WORD
    variable verbose

    set maxsize 0; set max_wpp -1
    set minsize 1000000; set min_wpp -1

    for {set words_per_page $start} \
	{$words_per_page <= $end} \
	{incr words_per_page} {

	    set pages [collect-pages $BITS_PER_WORD $words_per_page \
			   [parse-ucd $types {*}$args]]

	    set pagecount    [count-pages $pages $words_per_page]
	    set pagesize     [expr {$words_per_page * $BITS_PER_WORD / 8}]
	    set pages        [lsort -integer -stride 2 -index 0 -decreasing \
				  $pages]
	    set maxoffset    [lindex $pages 0]
	    set pagelistsize [expr {8 * $maxoffset / $pagesize + 1}]
	    set size	     [expr {$pagecount * $pagesize + $pagelistsize * 2}]

	    if {$size == $maxsize} {
		lappend max_wpp $words_per_page
	    } elseif {$size > $maxsize} {
		set maxsize $size
		set max_wpp $words_per_page
	    }

	    if {$size == $minsize} {
		lappend min_wpp $words_per_page
	    } elseif {$size < $minsize} {
		set minsize $size
		set min_wpp $words_per_page
	    }

	    if {$verbose} {
		puts "---"
		puts "Words per page: $words_per_page"
		puts "Pages: $pagecount"
		puts "Page size: $pagesize"
		puts "Max offset: $maxoffset"
		puts "Index size: $pagelistsize"
		puts "Bytes: $size"
	    } else {
		puts -nonewline .
		flush stdout
	    }
	}

    if {$verbose} {
	puts "---"
	puts "Maximum size $maxsize, words per page $max_wpp."
    } else {
	puts ""
    }
    puts "Minimum size $minsize, words per page $min_wpp."
}

# Top-level command: Generate C code for a pagetable.
proc pagetable {varname types words_per_page args} {
    variable BITS_PER_WORD
    set pages [collect-pages $BITS_PER_WORD $words_per_page \
		   [parse-ucd $types {*}$args]]
    print-pages $varname $pages $words_per_page
}

# Top-level command: Make a simple database-type list, one codepoint
# at the time.  This can than be compared with other similar lists.
proc table {fname types} {
    set bits {}
    foreach range [parse-ucd $types $fname] {
        lassign $range s e
        for {set ch $s} {$ch <= $e} {incr ch} {
            lappend bits [expr $ch]
        }
    }
    set bits [lsort -integer $bits]
    while {[lindex $bits 0] < 32} {
        set bits [lrange $bits 1 end]
    }
    set next [lindex $bits 0]
    for {set ch 32} {[llength $bits] > 0} {incr ch} {
        if {$ch == $next} {
            set value 1
            set bits [lrange $bits 1 end]
            set next [lindex $bits 0]
        } else {
            set value 0
        }
        puts [format "U+%04X %d" $ch $value]
    }
}

if {"-v" eq [lindex $argv 0]} {
    set verbose 1
    set argv [lrange $argv 1 end]
}
eval $argv

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
