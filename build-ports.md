[//]: # (build-ports.md)

Build-Ports: Installation notes
====

Some notes on how individual systems work for `build-ports.sh`.
Mostly notes on how to install the various OSs in libvirt/KVM.

Bits covered:

* VM configuration in `virt-manager.`
* Configuration of `/etc/hosts`
* SSH configuration.
* Locale confguration.
* Linking Bash to `/bin/bash`.  This is optional, the build does not
  need that any more.
* TLS configuration and `.netrc` for `odoacer`.  This is optional,
  it's not needed, when the Git checkout is configured to pull from
  the host via SSH.
* Package manager and list of packages to install to run the build and
  tests for Ctk.
* Arrange for the correct path of the Ctk project.  `build-ports.sh`
  requires that this is the same on the host and the agents.

libvirt/KVM in Debian
----

What's needed to install libvirt/KVM in Debian:

* `libvirt-daemon-system:` Also pulls the basic daemon and the CLI
  client utils.
* `virt-manager:` The GUI.
* `dnsmasq:` Software-defined networking.
* `qemu-utils:` For `qemu-image,` so `virt-manager` can provide
  storage.
* `gir1.2-spiceclientgtk-3.0:` For Spice, which provides access to the
  screen of a VM from `virt-manager.`

        # apt install libvirt-daemon-system dnsmasq \
                      virt-manager qemu-utils gir1.2-spiceclientgtk-3.0
        # adduser benny libvirt

Linux (Debian)
----

Note on locales: Some tests require a UTF-8 locale.  Our `configure`
script will detect `C.UTF-8` or `en_US.UTF-8` and set it in the
makefile.  In a minimal Debian, `C.UTF-8` is sometimes not
automatically activated, in that case install the package `locales`
(for the command `locale-gen`) and call

        # echo C.UTF-8 > /etc/locale.gen
		# locale-gen

Note: The generated locale may actually be called `C.utf8`, that's ok,
`configure` will find that, too.

OpenIndiana (Solaris)
----

Basic provision of the VM:

* Get the minimal installation ISO from https://www.openindiana.org/ .
* Create a VM via `virt-manager.`
* OS is `Generic default`
* 2GB memory.
* 10GB IDE-Disk.

Outside the VM:

* Put host IP and name into `/etc/hosts`
* Install SSH keys:

        $ ssh-copy-id openindiana

Inside the VM:

* PAM blocks direct root login via SSH.  I have not found yet how to
  make a PAM config that allows it.
* Install packages:

        # pkg refresh --full
        # pkg search gcc # To find the actual package name for current GCC.
        # pkg install \
            pkg:/developer/build/autoconf \
            pkg:/developer/build/gnu-make \
            pkg:/developer/gcc-10 \
            pkg:/developer/versioning/git \
            pkg:/file/gnu-coreutils \
            pkg:/terminal/tmux

* Fix `/home` (OpenIndiana 2020.10 puts home directories into
  `/export/home` and blocks `/home` as a mount point):

        # umount /home
        # rmdir /home
        # ln -s /export/home /home

* Install `odoacers's` certificate as `/etc/certs/CA/odoacer-both.pem`
  (extension is important) and execute `svcadm restart
  ca-certificates`.
* Create a file `~/.netrc` and store the password for `odoacer` there.

NetBSD
----

Basic provision of the VM:

* Get the minimal installation ISO from https://www.netbsd.org/ .
* Create a VM via `virt-manager.`
* OS is `Generic default`
* 1GB memory.
* 5GB IDE-Disk.
* Install `Minimal install`
* Enable SSH and NTP

Inside the VM:

        # passwd
        # groupadd benny
        # useradd -m -g benny benny
        # passwd benny

Outside the VM:

* Put IP and name into `/etc/hosts`
* Install SSH keys:

        $ ssh-copy-id netbsd

Inside the VM:

* Setup:
  * Edit `/etc/rc.conf` to add `hostname=netbsd.turtle-trading.net`
  * Copy `~benny/.ssh/authorized_keys` to `~root/.ssh/authorized_keys`
  * Call `sysinst`:
    * Under `Config menu` install `pkgin` and `pkgsrc`.
    * Under `Re-install sets ...` -> `Custom installation` install the
      sets `Manual pages` and `Compiler tools`.  NB: Do not select the
      other already existing sets again!

* Install packages:

        # pkgin install \
            autoconf \
            bash \
            git \
            gmake \
            mozilla-rootcerts \
            ncursesw \
            tcl \
            tmux
        # ln -s /usr/pkg/bin/bash /bin/bash
        # mozilla-rootcerts install

* Install (2):

        # pkg_add ...

  (Later updates with option `-u`)

* Install `odoacers's` certificate as `/etc/openssl/certs/odoacer-both.pem`
  (extension is important) and execute `mozilla-rootcerts rehash`.
* Create a file `~/.netrc` and store the password for `odoacer` there.


FreeBSD
----

Basic provision of the VM:

* Get the DVD ISO from https://www.freebsd.org/where/ .
* Unzip the ISO.
* Create a VM via `virt-manager.`
* OS is `Generic default`
* 1GB memory.
* 5GB IDE-Disk.
* Configure network with `virtio`.
* De-select debug packages.
* Select SSHd and NTP
* Install documentation (this also adds the `pkg` tool along the way).

Inside the VM:

* Edit `/etc/ssh/sshd_config` to have `PermitRootLogin
  prohibit-password`.
* `pkill -HUP sshd`

Outside the VM:

* Put host IP and name into `/etc/hosts`
* Install SSH keys:

        $ ssh-copy-id freebsd

Inside the VM:

* Setup:
  * Edit `/etc/rc.conf` to add domain to `hostname=freebsd.turtle-trading.net`
  * Copy `~benny/.ssh/authorized_keys` to `~root/.ssh/authorized_keys`

* Install packages:

        # pkg install \
            autoconf \
            bash \
            binutils \
            gcc \
            git \
            gmake \
            ncurses \
            openssl \
            tcl86 \
            tmux
        # ln -s /usr/local/bin/bash /bin/bash
        # chsh -s /usr/local/bin/bash benny

* Install `odoacers's` certificate as `/etc/ssl/certs/odoacer-both.pem`
  (extension is important) and execute `openssl rehash`.
* Create a file `~/.netrc` and store the password for `odoacer` there.
* Git uses GnuTLS, point it at OpenSSL's truststore `git config
  --global http.sslcapath /etc/ssl/certs/`.


Haiku (née BeOS)
----

Basic VM setup:

* Virt/KVM has the template `haikunightly`.
* Video is QXL.  The resolution can be configured from the inside
  later.

Outside of the VM:

* SSH: `~/config/settings/ssh/authorized_keys`

        $ scp benny@192.168.122.1:.ssh/id_rsa.pub \
              ~/config/settings/ssh/authorized_keys

* SSH login: No password-based access, the only user in this
  single-user system has uid 0 (in other systems known as `root`), so
  `PermitRootLogin without-password` catches us out.  The username for
  login is `user`, which can of course be configured in
  `~/.ssh/config` on the host.

Inside of the VM:

* Certificates: `odoacer's` goes into
  `/boot/system/non-packaged/data/ssl/certs`.  Then `openssl rehash`.
* Create a file `~/.netrc` and store the password for `odoacer` there.
* Git uses GnuTLS, point it at OpenSSL's truststore:

        $ git config --global http.sslcapath \
              /boot/system/non-packaged/data/ssl/certs:/boot/system/data/ssl/certs`

* For home directory, put the following into
  `~/config/settings/boot/UserBootscript` (this does not automatically
  persist, it needs to execute on boot each time):

        mkdir -p /home
        ln -sf /boot/home /home/benny

* Executable scripts:

        $ echo 'ls -la --color "$@"' > config/non-packaged/bin/dir
        $ chmod +x config/non-packaged/bin/dir
        $ hash -r

* Install packages:

        $ pkgman refresh
        $ pkgman update
        $ pkgman install tcl_devel ncurces6_devel tmux

* Source-level ports (only for screen, but that does not work at this
  time anyway):

        $ git clone https://github.com/haikuports/haikuports.git
        $ pkgman install haikuporter

  Edits in `config/settings/haikuports.conf`:

        PACKAGER=...
        ALLOW_UNTESTED=yes
        ALLOW_UNSAFE_SOURCES=yes

  Install command:

        $ haikuporter -S -j2 --get-dependencies --no-source-packages screen

Ctk Results:

* Compiles with minimal changes.
* `make test` works only with `tmux`, `screen` is available in testing
  from Haikuporter but does not work.
* `cwish-in-term.sh` does not work because of hardcoded permissions to
  the result of `$(tty)`.  Implement `cwish-in-term.tcl` instead,
  which has the structure inverted, but which is also more code and
  has slightly slower setup.
* The native Terminal app does not work well with non-spacing
  modifiers.
* The native Terminal app does not work well with emojis.
* The `shutdown` command results in a kernel level debug prompt.

macOS (aka MacOSX)
----

Install: Get a KVM/Qemu config from one of the several sites or Git
repos.  This should also tell where to get the matching macOS software
version.

Put host name into `/etc/hosts`:

        # echo 192.168.122.1 arrian >> /etc/hosts

The directory `/home` is managed/blocked by the automounter.  To get
the space for symlinks to simulate the usual directories, stop that:

        # launchctl disable system/com.apple.automountd
        # launchctl disable system/com.apple.autofsd
        # ln -sf ~benny /home/benny

To install packages, get Macports (via installer) and:

        # port selfupdate
        # port install \
            autoconf \
            coreutils \
            curl-ca-bundle \
            git \
            libxslt \
            ncurses \
            rlwrap \
            tcl \
            tcl-tls \
            tcllib \
            tdom \
            timeout \
            tmux

Install `odoacer's` certificate for Curl:

        # cd /opt/local/share/curl/
        # cp curl-ca-bundle.crt curl-ca-bundle.crt.orig
        # echo Odoacer >> curl-ca-bundle.crt
        # echo ======= >> curl-ca-bundle.crt
        # cat odoacer-both.crt >> curl-ca-bundle.crt

Create a file `~/.netrc` and store the password for `odoacer` there.

Useful contents of `.profile`:

        source /etc/profile
        PATH=/opt/local/bin:$PATH
        export BASH_SILENCE_DEPRECATION_WARNING=1

Docker (non-default architectures)
----

The basic build environment is created by

        $ cd docker
        $ ./container.sh create

A non-default architecture is selected by the `--platform` parameter
of `docker pull`.  This is an experimental feature at this moment, it
must be enabled by a property in `/etc/docker/daemon.json`.  If this
is the only property, the content of `daemon.json` is just:

        {"experimental":true}

The possible values for `--parameter` are found on the page
https://hub.docker.com/_/debian?tab=tags&page=1&ordering=last_updated
.

Because these foreign architecture images are named the same on the
hub as the default architecture, the best way to get them and work
with them seems to be along the lines

        $ docker tag debian:stable-slim debian-amd64:stable-slim
        $ docker pull --platform linux/s390x debian:stable-slim
        $ docker tag debian:stable-slim debian-s390x:stable-slim
        $ docker tag debian-amd64:stable-slim debian:stable-slim

This works initially as well as for updates.

Creating the Ctk build container becomes:

        $ cd docker
        $ export os=debian-s390x
        $ export CONTAINER=ctk-build-debian:s390x
        $ ./container.sh create

And building and running the tests is then:

        $ ./build-ports.sh run-in-docker ctk-build-debian:s390x

(This does not work anymore, games with pull by digest,
debian@sha256:1234..., seem required now.  Also it seems, `docker run`
now also requires the parameter `--platform`)

----
[//]: # (eof)
