#!/bin/bash
# ------------------------------------------------------------------------
#   cwish-in-term.sh
#
#   A little utility to run a cwish in a separate terminal window.
#   This can be used to output stuff to stdout/stderr, or to run a
#   command loop concurrently with the UI.
# ------------------------------------------------------------------------

# Exit on error.
set -e

# Do not randomly clobber variables COLUMNS and LINES in here.
shopt -u checkwinsize

# If CWISH_AGENT is set, we are the agent inside the terminal
# emulation.  The variable contains a name of a FIFO for communication
# with the main program.  First we output environment variables to set
# the behaviour of the main program and than we wait for the main
# program to signal that we are not needed any more.
if [ -n "$CWISH_AGENT" ]; then
    # Say something.
    echo "Main program is not running ..."

    # Close stdin so we do not confuse the main program.
    <&-

    # Open a permanent file handle for the FIFO.  The FIFO should
    # never be closed, because the other side would get an EOF and
    # shut down, too.
    mkfifo $CWISH_AGENT
    exec 3> $CWISH_AGENT

    # Notify the caller about the info it needs.
    (echo export CTK_DISPLAY=$(tty):$TERM; echo .) >&3

    # Wait for the closing of the window or an exit command or that
    # the FIFO disappears.
    trap "rm -f $CWISH_AGENT" EXIT
    while true; do
	read -t 1 cmd || true
	if [ "exit" = "$cmd" ]; then
	    break
	fi
	if [ ! -r $CWISH_AGENT ]; then
	    break
	fi
    done < $CWISH_AGENT

    exit 0
fi

# The rest of the code is the main program.

# Find a terminal emulator.
if [ -n "$CWISH_TERM" ]; then
    term="$CWISH_TERM"
else
    for term in i3-sensible-terminal gnome-terminal xterm; do
	if type $term &> /dev/null; then
	    break
	fi
    done
fi

# Run our agent code inside the terminal emulator.
export CWISH_AGENT=/tmp/cwish-agent-$$
trap "echo exit >> $CWISH_AGENT; rm -f $CWISH_AGENT" EXIT

case "$term" in
    screen*)
	$term -D -m "$0" &
	;;
    tmux*)
	# If we have a server already, configure it to pass the
	# caller's value of CWISH_AGENT to the new session.  If we are
	# the first caller, no server exists yet and we get a failure
	# here, but we ignore that.
	$term set-option -g update-environment CWISH_AGENT \
	      2> /dev/null || true
	$term new-session -d "$0" &
	;;
    tmux*)
	CWISH_AGENT=$tmpfile $term new-session -d "$0" &
	;;
    *)
	$term -e "$0" &
	;;
esac

# Wait for the FIFO to appear.
for ((i=0; i<100; ++i)); do
    if [ -r $CWISH_AGENT ]; then
	break
    fi
    sleep .1
done
if [ ! -r $CWISH_AGENT ]; then
    echo "The FIFO for communication with the agent did not appear" >&2
    exit 1
fi

# Run content of the FIFO and than run our main program.
line=
while true; do
    read line
    if [ "." = "$line" ]; then
	break
    fi
    eval $line
done < $CWISH_AGENT

# cwish.tcl has /usr/bin/tclsh as its shebang, but the BSDs do not
# have the binary there.
if [ -z "$TCLSH" ]; then
    TCLSH=$(type -path tclsh)
fi

if [ "x-gdb" = "x$1" ]; then
    wrap="gdb --args $TCLSH"
    shift
elif [ "dumb" = "$TERM" ]; then
    wrap=$TCLSH
elif type rlwrap &> /dev/null; then
    wrap="rlwrap $TCLSH"
else
    wrap=$TCLSH
fi

# No exec: We need to handle the trap above otherwise the agent keeps
# running.
$wrap $(dirname "$0")/cwish.tcl "$@"

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
