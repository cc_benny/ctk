#!/usr/bin/tclsh
# ------------------------------------------------------------------------
#   add-cmd-tags.tcl
#
#   Add the commands implemented in C (the table from tkWindow.c) to
#   the tags table.
# ------------------------------------------------------------------------

# Get all commands from the table in tkWindow.c.  Uses a simple
# brute-force regexp parser.
proc get-commands {} {
    set ifs [open tkWindow.c r]
    while {[gets $ifs line] >= 0} {
	if {[regexp {^[[:space:]]*{"([^\"]+)",[[:space:]]*([a-zA-Z_]*Cmd)},} \
		 $line -> name proc]} {
	    lappend commands $proc $name
	}
    }
    close $ifs
    return $commands
}

# Find the key from COMMANDS in the TAGS file and for each one, add a
# tag with the value of the entry in COMMANDS.  Assumes a specific
# format of the tags in the TAGS file.
proc patch-tags {commands} {
    # In general, tags in TAGS look like
    #
    #   text tag text[DEL]tag[SOH]coordinates
    #
    # Where "tag[SOH]" is optional and "[DEL]" and "[SOH]" denote the
    # byte values 0x7F and 0x01.  In our particular case, the existing
    # C-level entries look like
    #
    #   Tk_*Cmd([DEL]coordinates
    #
    # Note the trailing "(" just before the "[DEL]".
    set RE [format {^(.* )?(%s)[\(]?\x7F([^\x01]*)$} \
		[join [dict keys $commands] "|"]]

    set ifs [open TAGS r]
    set outfile TAGS.[pid]
    set ofs [open $outfile w]

    while {[gets $ifs line] >= 0} {
	# Drop existing entries.
	if {[string match "*Cmd*\x7F*\1*" $line]} {
	    continue
	}

	# Print the input as-is.
	puts $ofs $line

	# If this line matches, add the corresponding entry from
	# COMMANDS.
	if {[string match "*Cmd*\x7F*" $line]
	    && [regexp $RE $line -> pre proc post]} {
	    puts $ofs "$pre$proc\x7F[dict get $commands $proc]\1$post"
	}
    }

    close $ifs
    close $ofs
    file rename -force $outfile TAGS
}

patch-tags [get-commands]

# ------------------------------------------------------------------------
#    eof
# ------------------------------------------------------------------------
