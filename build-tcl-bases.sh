#!/bin/bash
# ------------------------------------------------------------------------
#   build-tcl-bases.sh
#
#   Build a couple of relevant Tcl versions and install them into
#   subdirectories of /usr/local.
# ------------------------------------------------------------------------

set -e	# Exit on error.

use_git=false

if $use_git; then
    giturl=https://github.com/tcltk/tcl
    mainbranch=main
else
    fossilurl=https://core.tcl-lang.org/tcl
    # "fossil clone" takes a lot of time.  Use a local fossil database
    # instead.
    fossilurl=~/Projects/tcl.fossil
    mainbranch=trunk
fi

basedir=$(cd $(dirname "$0") && pwd)
checkout="$basedir"/target/tcl
tclbases="$basedir"/target/tcl-bases

# build ref destdir ?options? - Build a particular Tcl version given
# by REF into DESTDIR.  Pass OPTIONS to configure.  Assume that we are
# in the "unix" subdirectory of the Tcl checkout, and that we can do
# whatever we want in this checkout, like delete any file we do not
# care for.
function build () {
    if [ "unix" != $(basename $PWD) ]; then
	echo "Not a Tcl checkout, or not in 'unix' subdirectory" >&2
	exit 1
    fi

    local ref=$1; shift
    local destdir=$1; shift

    echo "Building Tcl $ref and installing into $destdir ..."

    if $use_git; then
	git clean -fxd
	git checkout $ref
	# Call git pull, but only if we are not detached.
	fgrep -q ref: ../.git/HEAD && git pull
    else
	fossil clean -x
	fossil checkout $ref
    fi

    ./configure --enable-symbols --prefix=$destdir "$@"
    make -j

    rm -rf $destdir
    # Some versions of 8.7 are missing install-libraries when calling
    # install.
    make install install-libraries
}

function make-checkout () {
    local checkout=$1
    if $use_git; then
	if [ ! -d "$checkout" ]; then
	    git clone --depth 1 --no-single-branch $giturl "$checkout"
	else
	    (cd "$checkout" && git fetch)
	fi
    else
	if [ ! -r "$checkout".fossil ]; then
	    local base=$(dirname "$checkout")
	    mkdir -p "$base"
	    if [ -r "$fossilurl" ]; then
		cp "$fossilurl" "$checkout".fossil
	    else
		fossil clone $fossilurl "$checkout".fossil
	    fi
	fi
	if [ ! -d "$checkout" ]; then
	    mkdir -p "$checkout"
	    (cd "$checkout" && fossil open "$checkout".fossil)
	else
	    (cd "$checkout" && fossil pull)
	fi
    fi
}

make-checkout "$checkout"

cd "$checkout"/unix

build core-8-6-9 "$tclbases"/tcl-8.6.9
build core-8-6-9 "$tclbases"/tcl-8.6.9-ucs4 CFLAGS=-DTCL_UTF_MAX=5

build core-8-6-branch "$tclbases"/tcl-8.6-snapshot
build core-8-6-branch "$tclbases"/tcl-8.6-snapshot-ucs4 CFLAGS=-DTCL_UTF_MAX=5

build core-8-branch "$tclbases"/tcl-8.7-snapshot
build core-8-branch "$tclbases"/tcl-8.7-snapshot-ucs4 CFLAGS=-DTCL_UTF_MAX=5

build $mainbranch "$tclbases"/tcl-9.0-snapshot

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
