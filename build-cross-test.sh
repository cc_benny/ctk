#!/bin/bash
# ------------------------------------------------------------------------
#   build-cross-test.sh
#
#   Test the cross of compilation modes against Tcl runtime modes (Ctk
#   for 8.6 in Tcl 8.7, Ctk UCS-4 in Tcl UTF-16).
# ------------------------------------------------------------------------

set -e	# exit on error
set -o pipefail

export CWISH_TERM=tmux
export LANG=C.UTF-8 # The test requires this.

target=$(dirname "$0")/target
mkdir -p "$target"
target=$(cd "$target" && pwd)
tclbases=${TCL_BASEDIR:-"$target"/tcl-bases}
destdir="$target"/cross-test

# The Tcl installations to consider.  Optional, so that we can call
# parse-test-log on files that we pull from Gitlab.
if [ -d "$tclbases" ]; then
    versions="system-tcl $(cd "$tclbases" && echo *)"
fi

mkdir -p "$destdir"


# Duplicate the descriptor 1 into the descriptor 3.  This gives us an
# additional way to output that is not subject to the usual
# redirections.
exec 3>&1

# log string ... - Use the additional descriptor 3 to do logging.
function log () {
    echo "$*" >&3
}

# check-out destdir - Checkout Ctk and reset to our current commit.
function check-out () {
    local vtcl=$1; shift
    local builddir=$destdir/$vtcl
    local ref=$(git rev-parse HEAD)

    if [ ! -d "$builddir" ]; then
	git clone -q $PWD "$builddir"
    fi

    (
	cd "$builddir"
	git fetch -q
	git reset -q --hard $ref
    )
}

# build-one destdir - Checkout and update the extension build in
# compilation mode directory.
function build-one () {
    local vtcl=$1
    local builddir=$destdir/$vtcl
    local options

    log "Building Ctk for Tcl '$vtcl' (in $builddir) ..."

    (
	if [ "system-tcl" != "$vtcl" ]; then
	    export LD_LIBRARY_PATH="$tclbases"/$vtcl/lib
	    options=--with-tcl="$tclbases"/$vtcl
	fi

	cd "$builddir"
	git clean -fxd
	./autogen.sh
	./configure $options
	make DEBUG_FLAGS='-O0 -g' -j
    )
}

# test-one destdir - Run the test of the extension build in
# compilation mode directory.
function test-one () {
    local vctk=$1
    local vtcl=$2
    local builddir=$destdir/$vctk

    log "Testing Ctk for '$vctk' in Tcl '$vtcl' (in $builddir) ..."

    (
	TCLSH=tclsh
	if [ "system-tcl" != "$vtcl" ]; then
	    export TCLSH="$tclbases"/$vtcl/bin/tclsh*
	fi

	cd "$builddir"
	$TCLSH ./cwish-in-term.tcl tests/all.tcl -file ctkUnicode.test
    )
}

# parse-test-log logfile - Grep the most important bits from a test
# log.  Returns 1 if an error was detected in the log, 0 for
# successfull logs.
function parse-test-log () {
    local log=$1
    local errors

    if ! fgrep -q $(printf "Failed\t") "$log"; then
	errors="Test did not finish: "
	errors+=$(tail -1 "$log")
    elif ! errors=$(grep '^==== .* .* FAILED' "$log"); then
	return 0
    fi

    basename "$log"
    grep '^  \(Suppl\|Unicode\)' "$log" || true
    echo "$errors" | sed 's/^/  /'
    return 1
}

# check-out-all - Create/update all source directories.
function check-out-all () {
    local vtcl
    for vtcl in $versions; do
	case $vtcl in
	    *-ucs4)
		continue
		;;
	esac

	check-out $vtcl
    done
}

# build-all - Create all compilation versions.
function build-all () {
    local vtcl
    for vtcl in $versions; do
	# For every version *-ucs4 there is also a version without
	# that, containing the same headers, giving the same output,
	# so we do not need to compile those versions separately.  We
	# always set TCL_UTF_MAX=5 in Ctk, so we do not need to
	# compile two versions here.
	case $vtcl in
	    *-ucs4)
		continue
		;;
	esac

	check-out $vtcl
	build-one $vtcl 2>&1 | tee "$destdir"/build.$vtcl.log
    done
}

# test-all - Run all the tests in a cross of all relevant
# configurations.  Result is that all combinations work, except
# tcl86-in-tcl87u4.  That one fails, because we need the
# Tcl_UtfToChar16 from the other 8.7 branch for that.
function test-all () {
    local vtcl
    local vctk
    local counter="$destdir"/error-count
    rm -rf "$counter"
    touch "$counter"
    for vtcl in $versions; do
	for vctk in $versions; do
	    # "x-": Our new Tcl packages in the Gitlab package manager
	    # do not have a prefix, they start with the version
	    # number.  Adding this prefix lets the patterns work in
	    # both cases without getting confused with the digits 8
	    # and 9 somewhere else in the version number.
	    case x-$vctk/x-$vtcl in
		# Extension compiled for Tcl 8 do not work in Tcl 9
		# and vice-versa.
		*-9.*/*-8.*)
		    continue
		    ;;
		*-8.*/*-9.*)
		    continue
		    ;;
		*-9.*/*system*)
		    continue
		    ;;
		*system*/*-9.*)
		    continue
		    ;;
		# Extensions compiled for 8.7 do not work in 8.6.
		*-8.7*/*-8.6*)
		    continue
		    ;;
		*-8.7*/*system*)
		    continue
		    ;;
		# Ctk versions with *-ucs4 are not compiled extra,
		# s.a.
		*-ucs4/*)
		    continue
		    ;;
	    esac

	    (
		set +e

		log="$destdir"/test.$vctk-in-$vtcl.log
		check-out $vctk

		test-one $vctk $vtcl > $log 2>&1
		local rcexec=$?

		parse-test-log $log
		local rclog=$?

		if [ 0 != $rcexec -o 0 != $rclog ]; then
		    echo "Some error occurred."
		    echo "" >> "$counter"
		fi

		true
	    )
	done
    done

    return $(stat -c %s "$counter")
}

"$@"

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
