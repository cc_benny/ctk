#!/usr/bin/env tclsh
# ------------------------------------------------------------------------
#   cwish.tcl
#
#   A simple script to simulate cwish with tclsh + libctk.so.  Does
#   not support any cwish options like -geometry or -display.
# ------------------------------------------------------------------------

# A simple list filtering meta-proc.
proc ctk_lfilter {list lambda} {
    set r {}
    foreach i $list {
	if {[apply $lambda $i]} {
	    lappend r $i
	}
    }
    return $r
}
# Drop all entries from auto_path that contain the Tk library.
set auto_path [ctk_lfilter $auto_path {p {
    expr {! [file exists [file join $p tk.tcl]]}
}}]

set basedir [file dirname [info script]]
set tk_library [file join [pwd] $basedir library]
# For slave interpreters.
set env(CTK_LIBRARY) $tk_library
lappend auto_path $basedir

package require Ctk

if {{} != $argv} {
    set argv0 [lindex $argv 0]
    set argv  [lrange $argv 1 end]
    source $argv0
} elseif {[info exists env(CTK_DISPLAY)]
	  && ""    ne $env(CTK_DISPLAY)
	  && "tty" ne $env(CTK_DISPLAY)
	  && ! [string match tty:* $env(CTK_DISPLAY)]
	  && ! [string match :*    $env(CTK_DISPLAY)]} {
    # If the UI is on a file channel other than stdin/stdout, start a
    # command-line.
    # FIXME: Have a way to query this state from Ctk instead of the
    # above complicated condition.
    ctkCommandLoop::start stdin stdout stopHandler {destroy .}
}

tkwait window .
exit

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
