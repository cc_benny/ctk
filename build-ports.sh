#!/bin/bash
# ------------------------------------------------------------------------
#   build-ports.sh
#
#   Build the project in different environments in VMs (managed with
#   `virsh') or chroots (called through `schroot') or Docker
#   containers.
#
#   Requirements for all environments:
#
#   * All build prerequisites and dependencies have to be already
#     installed: Autoconf, Bash (as `/bin/bash'), Curses, Gcc, Git,
#     GNU-Make (as `gmake' or `make'), Tcl.
#
#   Requirements for the VMs and chroots:
#
#   * A checkout of the project in the same directory as the one where
#     this script is started.
#
#   * The checkout already includes this script.
#
#   * In the checkout, the remote `origin' has to be accessible and
#     contain this script.  The simplest is to make `origin' just
#     `arrian:Project/ctk'.  This assumes that SSH from the VM back to
#     the host works.
#
#   Requirements for VMs:
#
#   * The VM has to be accessible via SSH without a password.
#
#   Requirements for Docker containers:
#
#   * During the build, the project directory will be re-created
#     inside the container.  The user inside the container has to have
#     permissions to create that directory.
# ------------------------------------------------------------------------

# Exit on error.
set -e
# Any error in a pipe makes the pipe itself report failure.
set -o pipefail

function timestamp () {
    date +%H:%M:%S
}

# start-dom dom hostname - Start the VM (domain) via virsh.  Assumes
# that the network "default" is used by the VM.  If the VM is already
# started, nothing happens.  If the VM is not started already, this
# waits for the SSH service in the VM to come up.
function start-dom () {
    local dom="$1"
    local hostname="$2"

    if ! virsh net-info default | grep -q Active:.*yes; then
	virsh net-start default
    fi

    if ! virsh dominfo $dom | grep -q State:.*running; then
	virsh start $dom
    fi

    if ! ssh-add -l >& /dev/null; then
	ssh-add
    fi

    local i
    for ((i=0; i<50; ++i)); do
	# < /dev/null: SSH eats up stdin.  Prevent this from
	# interfering with the stream programming in this script.
	if ssh -o BatchMode=yes -o ConnectTimeout=1 $hostname true \
	       2> /dev/null < /dev/null; then
	    break
	fi
	if (( 0 == i )); then
	    echo -n "$(timestamp)  $dom: Waiting to start ."
	fi
	sleep 3
	echo -n .
    done
    echo ""
    if (( i >= 50 )); then
	echo "$dom: SSH not started" >&2
	return 1
    fi
}

# run-in-dom dom hostname script - Run a script in a VM.
function run-in-dom () {
    local dom="$1"
    local hostname="$2"
    local script="${3:-$BUILD_SCRIPT}"

    start-dom $dom $hostname
    echo "$(timestamp)  $hostname: ====== Running build (VM) ======"
    # < /dev/null: SSH eats up stdin.  Prevent this from interfering
    # with the stream programming in this script.
    local options=${-//[hB]/} # -hB not supported in Dash.
    ssh -o BatchMode=yes $hostname "
	# Inherit all options we have in the current shell.
	set -$options
	$script
    " < /dev/null
}

# run-in-chroot chroot script - Run a script in a chroot.
function run-in-chroot () {
    local chroot="$1"
    local script="${2:-$BUILD_SCRIPT}"

    echo "$(timestamp)  $chroot: ====== Running build (chroot) ======"
    local options=${-//[hB]/} # -hB not supported in Dash.
    schroot --chroot $chroot --directory "$PROJECTDIR" -- bash -c "
	# Inherit all options we have in the current shell.
	set -$options
	$script
    "
}

# Our DSL below uses `docker' as a function.  Make sure we get the
# real thing here.
docker=$(which docker || true)

# run-in-docker image script - Run a script in Docker container.
# Inside the container `PROJECTDIR' needs to be createable by the user
# in which the container runs, but not exist yet.
function run-in-docker () {
    local image="$1"
    local script="${2:-$BUILD_SCRIPT}"

    if [ -z "$docker" ]; then
	echo "Docker executable not found" >&2
	return 1
    fi

    echo "$(timestamp)  $image: ====== Running build (Docker) ======"
    local options=${-//[hB]/} # -hB not supported in Dash.
    $docker run --rm --volume $PROJECTDIR:/ctk-git:ro $image sh -c "
	# Inherit all options we have in the current shell.
	set -$options
	git clone /ctk-git -b $BRANCH $PROJECTDIR
	$script
    "
}

# checkout branch - In the VM: Checkout the latest source.  This is
# separate from `build', so that we can leave this script directly
# after the checkout, when the script may have changed.
function checkout () {
    local branch="$1"

    # Drop all extra files and clean out the staging area.
    git clean -fdx
    git reset --hard HEAD

    # Get an exact checkout from `origin' with no compromises.
    git fetch
    # `exec' ensures that we do not get back into this script. That
    # could lead to failures, if the script changes because of the
    # checkout.
    local options=${-//[hB]/} # -hB not supported in Dash.
    exec bash -c "
	# Inherit all options we have in the current shell.
	set -$options
	git checkout '$branch'
	git reset --hard origin/'$branch'
    "
}

# build branch - In the VM: Actually do the build.
function build () {
    # We want GNU make.  On BSDs that is called `gmake', so prefer
    # that name if it exists.
    local make=gmake
    if ! type $make > /dev/null 2>&1; then
	make=make
    fi

    # Build.
    ./autogen.sh
    ./configure
    $make -j
    $make test
}

PROJECTDIR=$(cd $(dirname "$0"); pwd)
SCRIPT=$(basename "$0")
BRANCH=$(git symbolic-ref --short HEAD)

BUILD_SCRIPT="
    # Get specific environment.
    test -r ~/.profile && source ~/.profile

    # Give some indication what this is. Do not rely on uname alone
    # (unreliable in chroots and containers) or the type of /bin/sh (is
    # 32-bit in 64-bit OpenIndiana).

    set +e
    echo
    echo Shell options: \$-
    (test -r /etc/os-release \
	 && . /etc/os-release \
	 && echo \$PRETTY_NAME)
    uname -a
    tclsh=\$(which tclsh)
    if [ -z \"\$tclsh\" ]; then
        tclsh=\$(which tclsh8.6)
    fi
    if [ -z \"\$tclsh\" ]; then
        tclsh=\$(which tclsh8.7)
    fi
    if [ -n \"\$tclsh\" ]; then
	# echo $tclsh: Fallback for macOS where \`readlink' is limited,
	# but not needed anyway, because \`file' does the needed thing
	# with symlinks.
	file \$(readlink -f \$tclsh 2>/dev/null || echo \$tclsh)
    fi
    echo

    set -e
    cd '$PROJECTDIR'

    # Activate these two lines, when the script is syntactically
    # broken in the current checkout in the agents.
    #git fetch
    #git reset --hard 'origin/$BRANCH'

    bash -$- './$SCRIPT' checkout '$BRANCH'
    bash -$- './$SCRIPT' build
"

# run-all agent-file ?script? - Run the build script with agents
# listed in the agent list file.  `script' overrides the standard
# build script (FIXME: This does not really work yet, especially with
# end-of-line comments in the agents-file.)
#
# The agent list file contains lines of the form:
#
# * "chroot chroot-name"
# * "docker container-name"
# * "vm vm-name vm-hostname"
function run-all () {
    local agent_file="$1"
    if ! shift; then
	echo "$0 run-all agent-file ?script?" >&2
	return 1
    fi

    # This expression filters empty lines and whole-line comments.
    grep '^[^#]' "$agent_file" | {
	logs=
	# Run all agents in parallel.
	while read cmd; do
	    # Strip trailing comments.
	    cmd=${cmd%%#*}
	    # Strip trailing spaces.
	    cmd=$(echo $cmd)
	    # Get the last word.
	    name=${cmd##* }

	    log=$(log-file $name)
	    logs="$logs $log"

	    run-one "$cmd $@" $log &
	done
	wait # ... for all the agents.

	# Return error, if any non-0 can be found, else ok.  IOW
	# reverse the result from grep.
	! grep -q "^<rc=[^0]" $logs 2>/dev/null
    }
}

# log-file basename - Get a distinct log file name from each call.
# Use `basename' in the name.  The names are garanteed not add any
# spaces in them apart from `basename'.
LOGBASE=/tmp/build-ports-$$-
rm -f $LOGBASE*
function log-file () {
    local basename=$1
    # The idx can not be a global, because we want to run this
    # function in a subshell.  We do not worry about performance, so
    # we can just start the count fresh each time.
    local idx
    # The first try is without a counter.
    local name=$LOGBASE$basename.log
    for ((idx=1; idx<=1000; ++idx)); do
	if [ -r $name ]; then
	    # Name for the next round.
	    name=$LOGBASE$basename-$(printf %02d $idx).log
	    continue
	else
	    touch $name
	    echo $name
	    return
	fi
    done
    echo "Can not find a free logfile name" >&2
    return 1
}

# run-one cmd log - Run a command and collect the result in a log
# file.  The log file will have as its last line the form "<rc=42>"
# giving the exit code of the command.  Commands can in theory be any
# shell command, but are actually expected to be one of:
#
# * "chroot chroot-name"
# * "docker container-name"
# * "vm vm-name vm-hostname"
export SHOW_RESULT_MUTEX=/tmp/build-ports-$$-result-mutex
trap 'rm -f $SHOW_RESULT_MUTEX' EXIT
function run-one () {
    local cmd=$1
    local log=$2
    (
	# Do not exit if the command ends in an error.
	set +e

	# Local aliases for the stuff that we expect.
	function chroot () { run-in-chroot "$@"; }
	function docker () { run-in-docker "$@"; }
	function vm     () { run-in-dom    "$@"; }

	echo "Running $cmd ..."
	(eval $cmd ; echo "<rc=$?>") &> $log < /dev/null

	# If all we have in the log file is a success code
	# i.e. "<rc=0>\n", than it's not a test, so ignore.
	if ((7 == $(stat --format %s $log))); then
	    # Wait until `log-file' has generated all log files.
	    # Otherwise this would interfere.
	    sleep 10
	    rm $log
	    return 0
	fi

	# From here on hold the mutex (see below) so that the results
	# of the parallel runs of various agents do not mix.
	flock -n 9

	# This result format can probably be improved ;-).
	fgrep ===== $log | head -1
	grep '^all[.]tcl:' $log | tail -1 | sed 's/^/  /'
	if tail -1 $log | fgrep -q '<rc=0>'; then
	    echo "$(timestamp)  Success -> $log"
	else
	    echo "$(timestamp)  Failure -> $log"
	fi
    ) 9> "$SHOW_RESULT_MUTEX"
}

# run-default ?script? - Run `run-all` with a host-specific agent list
# file. `script' overrides the standard build script.
function run-default () {
    run-all build-ports.$(hostname -s).rc "$@"
}

"${@:-run-default}"

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
